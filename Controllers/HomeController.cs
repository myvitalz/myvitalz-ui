﻿using MyVitalz.Web.Helpers;
using System.Web.Mvc;

namespace MyVitalz.Web.Controllers
{
    public class HomeController : BaseController
    {
        #region Action Methods

        public ActionResult Index()
        {
            return RedirectToPartyHome(CurrentUser);
        }

        public ActionResult ManageSubscription(string email, bool subscribe)
        {
            string message;
            Service.ManageSubscription(email, subscribe);
            if (subscribe == true)
            {
                message = "You have been subscribed successfully.";
            }
            else
            {
                message = "You have been unsubscribed successfully.";
            }
            return View("Success", (object)message);
        }

        public ActionResult SubscribeOrUnsubscribe(string email, bool subscribe)
        {
            Service.ManageSubscription(email, subscribe);
            return EmptyResult;
        }

        public ActionResult UptimeTestPage()
        {
            //string message = "";
            Helper.Service.GetGroups();
            return EmptyResult;
        } 
        
        #endregion

    }
}
