﻿using MyVitalz.Web.Helpers;
using System.Web.Mvc;

namespace MyVitalz.Web.Controllers
{
    [Authorize(Roles = Role.Doctor)]
    public class DoctorController : BaseDoctorNurseController
    {
    }
}
