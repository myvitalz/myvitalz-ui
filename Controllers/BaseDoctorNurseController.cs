﻿using MyVitalz.Web.Helpers;
using MyVitalz.Web.Models;
using System;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Net;
using System.IO;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using System.Linq;

namespace MyVitalz.Web.Controllers
{
    public class BaseDoctorNurseController : BasePartyController
    {

        #region Action Methods

        public ActionResult Index()
        {
            if (GetCookie(CookieName.IsMobileNurse) == Constant.Yes)
            {
                return RedirectToAction("Patients");
            }
            else
            {
                return RedirectToAction("DailyReport");
            }

        }

        #region Common Methods

        public ActionResult Patients()
        {
            return BaseShowPatients(CurrentUser.UserId);
        }

        public ActionResult PatientReading(string patientId)
        {
            return BaseShowPatientReading(patientId);
        }

        public ActionResult PatientDevices(string patientId)
        {
            return BaseShowPatientDevices(patientId);
        }

        public ActionResult GetDeviceQuadrant(string patientId)
        {
            return BaseGetDeviceQuadrant(patientId);
        }

        //public ActionResult GetDeviceQuadrantSliding(string patientId)
        //{
        //    return BaseGetDeviceQuadrantSliding(patientId);
        //}

        //public void SaveNewQuadrantSliding(string[] SlidingOrder)
        //{
        //    BaseSaveNewQuadrantSliding(SlidingOrder);
        //}

        public ActionResult Device(string deviceType, string patientId, DateTime? startDate, DateTime? endDate)
        {
            return BaseShowDevice(deviceType, patientId, startDate, endDate);
        }

        public ActionResult ShowEhr(string patientId)
        {
            return BaseShowEHR(patientId);
        }

        public ActionResult EditEhr(string patientId)
        {
            return BaseEditEhr(patientId);
        }

        public ActionResult EditPatient(string partyId)
        {
            return BaseShowEHR(partyId);
        }

        public string SaveEhr(EHRDetailsModel model)
        {
            return BaseSaveEhr(model, CurrentUser.UserId);
        }

        [HttpPost]
        public ActionResult AddDoctor(EHRDetailsModel model)
        {
            return BaseAddDoctor(model);
        }

        [HttpPost]
        public ActionResult RemoveDoctor(EHRDetailsModel model)
        {
            return BaseRemoveDoctor(model);
        }

        [HttpPost]
        public ActionResult AddCaregiver(EHRDetailsModel model)
        {
            return BaseAddCaregiver(model);
        }

        [HttpPost]
        public ActionResult RemoveCaregiver(EHRDetailsModel model)
        {
            return BaseRemoveCaregiver(model);
        }

        [HttpPost]
        public ActionResult AddClinician(EHRDetailsModel model)
        {
            return BaseAddClinician(model);
        }

        [HttpPost]
        public ActionResult RemoveClinician(EHRDetailsModel model)
        {
            return BaseRemoveClinician(model);
        }

        [HttpPost]
        public ActionResult AddAssociation(EHRDetailsModel model)
        {
            return BaseAddAssociation(model);
        }

        [HttpPost]
        public ActionResult RemoveAssociation(EHRDetailsModel model)
        {
            return BaseRemoveAssociation(model);
        }

        public ActionResult ShowParameters(string patientId)
        {
            return BaseShowParameters(patientId);
        }

        public ActionResult SaveParameters(ParameterModel model)
        {
            return BaseSaveParameters(model);
        }

        public ActionResult DailyReport(bool isListView = false)
        {
            List<string> pList = new List<string>();
            pList = (List<string>)Session["PatientToHide"];
            var model = new DailyReportModel()
            {
                IsFullReport = isListView?false:true,
                IsListView = isListView,
                PatientToHide = pList
            };

            //add the review completed patient list to the model
            string rcPatients = GetCookie("ReviewCompletedPatients");
            if (rcPatients != null)
            {
                string[] rcPids = rcPatients.Split(',');
                if (rcPids.Length > 0)
                {
                    model.ReviewCompletedPatients = rcPids.ToList();
                }
            }

            return BaseDailyReport(model);
        }

        public ActionResult FullDailyReport(bool isListView = false)
        {
            ViewBag.ControllerName = "BaseDoctorNurse";
            var model = new DailyReportModel()
            {
                IsFullReport = true,
                IsListView = isListView
            };
            return BaseDailyReport(model);
        }

        public void PatientsToHide(string patientToHide)
        {

            if (Session["PatientToHide"] == null) 
            {
                Session["PatientToHide"] = new List<string>();
            }
            List<string> pList = Session["PatientToHide"] as List<string>;
            pList.Add(patientToHide);
            Session["PatientToHide"] = pList;
        }

        public ActionResult DailyReportWithHiddenValues(bool isListView, string ConName)
        {
            List<string> pList = new List<string>();
            pList = (List<string>)Session["PatientToHide"];

            var model = new DailyReportModel()
            {
                IsFullReport = true,
                IsListView = isListView,
                PatientToHide = pList
            };

            model = BaseDailyReportDetails(model);

            //add the review completed patient list to the model
            string rcPatients = GetCookie("ReviewCompletedPatients");
            if (rcPatients != null)
            {
                string[] rcPids = rcPatients.Split(',');
                if( rcPids.Length > 0 )
                {
                    model.ReviewCompletedPatients = rcPids.ToList();
                }
            }

            if (model.IsListView == true)
            {
                return PartialView("_DailyReportList", model.Report);
            }
            else
            {
                return PartialView("_DailyReportGrid", model);
            }
        }

        public ActionResult FullDailyReportPartial(bool isListView, string ConName) 
        {
            //List<string> pList = new List<string>();
            //pList = (List<string>)Session["PatientToHide"];
            Session.Remove("PatientToHide");
            var model = new DailyReportModel()
            {
                IsFullReport = true,
                IsListView = isListView,
                //PatientToHide = pList
            };            

            model = BaseDailyReportDetails(model);
           
            if (model.IsListView == true) 
            {
                return PartialView("_DailyReportList", model.Report);
            }
            else
            {
                return PartialView("_DailyReportGrid", model);
            }
        }

        public ActionResult EmailDailyReport(string emails)
        {
            return BaseEmailDailyReport(emails);
        }

        public ActionResult GetEmailDailyReportDialog()
        {
            return BaseEmailDailyReportDialog();
        }

        public void AddToReviewCompletedList(string patientId)
        {
            DateTime current = DateTime.Now; // current time
            DateTime tomorrow = current.AddDays(1).Date; // this is the "next" midnight
            int minsUntilMidnight = (int)(tomorrow - current).TotalMinutes; // get the difference between the DateTimes and get seconds

            List<string> rcPatients = new List<string>();
            string reviewCompletePatients = GetCookie("ReviewCompletedPatients");
            if (reviewCompletePatients != null )
            {
                // split the string into an array
                string[] pIds = reviewCompletePatients.Split(',');
                if( pIds.Length > 0 )
                {
                    rcPatients = pIds.ToList<string>();
                }
            }
            // add the patient to the list ...
            if( (rcPatients == null) || (rcPatients != null && rcPatients.Count == 0) || ( rcPatients != null && rcPatients.Count > 0  && !rcPatients.Contains(patientId)) )
            {
                rcPatients.Add(patientId);
            }
            reviewCompletePatients = string.Join(",", rcPatients);
            CreateCookie("ReviewCompletedPatients",  reviewCompletePatients, minsUntilMidnight);
        }

        #endregion

        [HttpGet]
        public string GetPauseMonitoringForDate(string patientId, string devicetype, string date)
        {
            string result = "";
            try
            {
                string rt;
                WebRequest request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetReadingDetails"] + devicetype + "/" + patientId + "/" + date);
                WebResponse response = request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                rt = reader.ReadToEnd();
                Console.WriteLine(rt);
                reader.Close();
                response.Close();
                return rt;

            }
            catch (WebException e)
            {
                Console.Out.WriteLine("Error : " + e.Message);
            }
            return result;
        }

        #endregion

    }


}
