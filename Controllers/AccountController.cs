﻿using MyVitalz.Web.Helpers;
using MyVitalz.Web.Models;
using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Security;

namespace MyVitalz.Web.Controllers
{
    public class AccountController : BaseController
    {
        #region Support Methods

        public static string GetErrorMessage(ModelStateDictionary state)
        {
            var errorMessage = new StringBuilder();

            var errorList = (from e in state.Values
                             where e.Errors.Count > 0
                             select e).ToList();

            foreach (var error in errorList)
            {
                foreach (var message in error.Errors)
                {
                    errorMessage.AppendLine(message.ErrorMessage);
                }
            }

            return errorMessage.ToString();
        }

        #endregion

        #region Login / LogOff

        public ActionResult LogOn()
        {
            //Check if the user logged in before. If so, redirect them
            if (CurrentUser != null
                    && CurrentUser.IsAuthenticated == true)
            {
                return RedirectToPartyHome(CurrentUser);
            }

            var model = new LogOnModel();
            return View(model);
        }

        public ActionResult MobileNurseLogOn()
        {
            //Write a cookie to track the Mobile Nurse
            CreateCookie(CookieName.IsMobileNurse, Constant.Yes, 400);

            //Check if the user logged in before. If so, redirect them
            if (CurrentUser != null
                    && CurrentUser.IsAuthenticated == true)
            {
                return RedirectToPartyHome(CurrentUser);
            }

            var model = new LogOnModel();
            return View("LogOn", model);
        }

        public ActionResult ReLogOn(LogOnModel model)
        {
            ModelState.Clear();
            return View("LogOn", model);
        }

        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid == true)
            {
                //Validate the User
                if (Membership.ValidateUser(model.UserName, model.Password))
                {
                    var user = (User)Membership.GetUser();
                    var subGroup = MyVitalzRestApi.GetSubGroupTypeForParty(user.UserId);
                    if (subGroup != null && subGroup.Length > 0)
                    {
                        user.Group = subGroup;
                    }
                    //Set the Auth Cookie
                    Authentication.SetAuthCookie(model.UserName, model.RememberMe, user);
                    //Set the JWT Token
                    model.userCredentialResponse = Service.ValidateUserCredential(model.UserName, model.Password);
                    CreateCookie("JWT", model.userCredentialResponse.token, 400);

                    //Redirect to return URL or default URL as needed
                    if (Url.IsLocalUrl(returnUrl)
                          && returnUrl.Length > 1
                          && (returnUrl.StartsWith("/")
                          && returnUrl.StartsWith("//") == false)
                          && returnUrl.StartsWith("/\\") == false)
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToPartyHome(user);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                }
            }
            return View(model);
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            //Delete the Admin Cookie
            if (Request.Cookies[CookieName.PartySearchCriteria] != null)
            {
                Response.Cookies[CookieName.PartySearchCriteria].Expires = DateTime.Now.AddDays(-1);
            }
            if (Request.Cookies[CookieName.IsMobileNurse] != null)
            {
                Response.Cookies[CookieName.IsMobileNurse].Expires = DateTime.Now.AddDays(-1);
            }
            Session.Remove("PatientToHide");
            return RedirectToAction("Index", "Home");
        }

        #endregion

        #region Register

        public ActionResult Register()
        {
            var model = new RegisterModel();
            return View("Register", model);
        }

        public void ValidateRegister(RegisterModel model)
        {
            if (ModelState.IsValid == false)
            {
                throw new ArgumentException(GetErrorMessage(ModelState));
            }
            Service.Register(model.Party, model.UserName, model.Password);
            if (Membership.ValidateUser(model.UserName, model.Password))
            {
               //14/08/2020 This code hide for When registering a group admin, the auto login allows the newly
               //created group admin to view patients from other sub group in the same group
               //start
               // var user = (User)Membership.GetUser();
               // Authentication.SetAuthCookie(model.UserName, false, user);
               //end
            }
        }

        #endregion

        #region Profile

        [Authorize]
        public new ActionResult Profile()
        {            
            var response = Helper.Service.GetProfile(CurrentUser.UserId);
            var model = new BasePartyModel()
            {
                AssociationTypes = GetAssociationTypes(),
                GroupTypes = GetGroupTypes(),
                Party = Helper.GetPatient(response.party),
                Preferences = response.preferences,
                FieldPrefix = "Party.",
                IsProfile = true,
               // MainGroupTypes = GetMainGroupTypes(CurrentUser.Group)
            };
            model.SubGroupTypes = Service.GetAllSubGroupTypes();
            model.Insurance = MyVitalzRestApi.GetPatientInsurance(CurrentUser.UserId);
            if( model.Insurance == null )
            {
                model.Insurance = new InsuranceModel();
            }
            return View("EditProfile", model);
        }

        [Authorize]
        public ActionResult SaveProfile(BasePartyModel model)
        {
            var stringBuilder = new StringBuilder();
            ValidateOtherUsers(model.Party, stringBuilder);
            if (stringBuilder.Length != 0)
            {
                throw new ArgumentException(((object)stringBuilder).ToString());
            }
            Service.SaveParty(model.Party, model.Preferences, CurrentUser.UserId);
            return EmptyResult;
        }

        [Authorize]
        public ActionResult Reports(BasePartyModel model)
        {
            var stringBuilder = new StringBuilder();
            if (model.Party.partyType == "P")
            {
                ValidatePatient(model.Party, stringBuilder);
            }
            else
            {
                ValidateOtherUsers(model.Party, stringBuilder);
            }
            //ValidatePatient(model.Party, stringBuilder);
            if (stringBuilder.Length != 0)
            {
                throw new ArgumentException(((object)stringBuilder).ToString());
            }
            Service.SaveParty(model.Party, model.Preferences, CurrentUser.UserId);
            return EmptyResult;
        }

        #endregion

        #region Password Handling

        public ActionResult ForgotPassword(string userName)
        {
            return View("ForgotPassword", (object)userName);
        }

        public ActionResult SendPassword(string userName)
        {
            if (string.IsNullOrWhiteSpace(userName))
            {
                throw new Exception("UserName can't be empty.");
            }
            Service.ForgotPassword(userName);
            return View("SendPassword", (object)userName);
        }

        [Authorize]
        public ActionResult ChangePassword()
        {
            var model = new ChangePasswordModel();
            return View("ChangePassword", model);
        }

        [HttpPost]
        [Authorize]
        public ActionResult UpdatePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid == false)
            {
                return View("ChangePassword", model);
            }
            Service.UpdatePassword(CurrentUser.Identity.Name, model.OldPassword, model.NewPassword);
            return View("Success", (object)"Password was changed successfully.");
        }

        #endregion

        #region Quadrant Sliding

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult GetDeviceQuadrantSliding(string patientId)
        {
            return PartialView("_DQSliding", new PatientDevicesModel()
            {
                //QuadrantDetail = Service.GetQuadrantDetailsByPatient(patientId, CurrentUser.UserId),
                //Patient = Service.GetPartyById(patientId),
                QuadrantOrder = Service.GetQuadrantOrder(CurrentUser.UserId, patientId)
            });
        }

        public void SaveNewQuadrantSliding(string[] SlidingOrder)
        {
            string order = null;
            string deviceType = null;
            string partyId = null;
            string patientId = null;
            string updateUser = null;
            Service.QuadrantOrder[] qo = new Service.QuadrantOrder[9];
            List<Service.QuadrantOrder> qoList = new List<Service.QuadrantOrder>();

            if (SlidingOrder != null)
            {
                int x = 0;
                for (int i = 0; i < 9; i++)
                {
                    if (i == 0)
                    {
                        order = SlidingOrder[x].ToString();
                        deviceType = SlidingOrder[x + 1].ToString();
                        partyId = SlidingOrder[x + 2].ToString();
                        patientId = SlidingOrder[x + 3].ToString();
                        updateUser = "admin";

                        Service.QuadrantOrder qoObj = new Service.QuadrantOrder();
                        qoObj.order = order;
                        qoObj.deviceType = deviceType;
                        qoObj.partyId = partyId;
                        qoObj.patientId = patientId;
                        qoObj.updateUser = updateUser;

                        qo.SetValue(qoObj, i);
                        x = x + 3;
                    }
                    else
                    {
                        order = SlidingOrder[x + 1].ToString();
                        deviceType = SlidingOrder[x + 2].ToString();
                        partyId = SlidingOrder[x + 3].ToString();
                        patientId = SlidingOrder[x + 4].ToString();
                        updateUser = "admin";

                        Service.QuadrantOrder qoObj = new Service.QuadrantOrder();
                        qoObj.order = order;
                        qoObj.deviceType = deviceType;
                        qoObj.partyId = partyId;
                        qoObj.patientId = patientId;
                        qoObj.updateUser = updateUser;

                        qo.SetValue(qoObj, i);
                        x = x + 4;
                    }
                }
            }
            var model = new PatientDevicesModel()
            {
                QuadrantOrderArr = qo
            };
            Service.SaveQuadrantOrderRequest sqor = new Service.SaveQuadrantOrderRequest();
            sqor.patientId = null;
            sqor.quadrantOrder = model.QuadrantOrderArr;
            Service.SaveQuadrantSliding(sqor);
        }
        #endregion
    }
}
