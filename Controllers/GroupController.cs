﻿using MyVitalz.Web.Helpers;
using MyVitalz.Web.Service;
using System;
using System.Text;
using System.Web.Mvc;

namespace MyVitalz.Web.Controllers
{
    [Authorize(Roles = Role.Admin)]
    public class GroupController : BaseController
    {
        #region Action Methods

        public ActionResult Index()
        {
            var model = Service.GetGroups();
            return View("Index", model);
        }

        public ActionResult GetGroupDialog(GroupType model)
        {
            return PartialView("_GroupDialog", model);
        }

        public ActionResult SaveGroup(GroupType model)
        {
            if (model == null)
            {
                throw new ArgumentException("Name & Description cannot be empty.");
            }
            var errorMessage = new StringBuilder();
            if (string.IsNullOrWhiteSpace(model.groupType) == true)
            {
                errorMessage.AppendLine("Name cannot be empty.");
            }
            if (string.IsNullOrWhiteSpace(model.groupTypeDesc) == true)
            {
                errorMessage.AppendLine("Description cannot be empty.");
            }
            if (errorMessage.Length > 0)
            {
                throw new ArgumentException(errorMessage.ToString());
            }
            model.updateUser = CurrentUser.UserId;

            Service.SaveGroup(model, CurrentUser.UserId);
            var associationTypes = Service.GetGroups();
            return PartialView("_ListGroups", associationTypes);
        }

        #endregion
    }
}
