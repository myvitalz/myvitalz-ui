﻿using System.Web.Mvc;

namespace MyVitalz.Web.Controllers
{
    public class ErrorController : BaseController
    {
        #region Action Methods

        public ActionResult Index()
        {
            return View("Error");
        }

        public ActionResult Http404()
        {
            return View("Lost");
        }

        public ActionResult ShowError(HandleErrorInfo errorInfo)
        {
            return View("Error", errorInfo);
        }
 
        #endregion
    }
}
