﻿using MyVitalz.Web.Helpers;
using MyVitalz.Web.Models;
using MyVitalz.Web.Service;
using System;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;
using System.IO;
using System.Net;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;

namespace MyVitalz.Web.Controllers
{
    [Authorize(Roles = Role.Admin + Role.Separator + Role.GroupAdmin)]
    public partial class AdminController : BasePartyController
    {
        #region Support Methods

        private EHRDetailsModel GetEmptyEhrDetailsModel(string partyType)
        {
            var model = new EHRDetailsModel()
            {
                Details = new EHRDetails()
                {
                    ehr = new Patient()
                    {
                        address = new Address(),
                        partyType = partyType
                    },
                    diseaseType = GetDiseases(),
                    preferences = new Preferences[1],
                    patientFeatures = new PatientFeatures[1]
                },
                AssociationTypes = GetAssociationTypes(),
                GroupTypes = GetGroupTypes(),
                AllSubGroups = GetAllSubGroupTypes()           
            };

            // initialize manual entry to true
            // initialize the features to true
            if (partyType.Equals(Role.Patient) )
            {
                model.Details.patientFeatures[0] = new PatientFeatures();
                model.Details.patientFeatures[0].dailyQuestionsValue = "0";
                model.Details.patientFeatures[0].feelingsValue = "1";
                model.Details.patientFeatures[0].notificationsValue = "1";
                //setting the manual entry
                model.Details.preferences[0] = new Preferences();
                model.Details.preferences[0].preferenceKeyType = "MRE";
                model.Details.preferences[0].preferenceValue = "1";
            }

            // initialize the diseasepicked to false
            model.DiseasesPicked = new string[model.Details.diseaseType.Length];
            for (var idx = 0; idx < model.DiseasesPicked.Length; idx++ )
            {
                model.DiseasesPicked[idx] = "0";
            }

            //SubGroupWrapper sgw = new SubGroupWrapper();
            //List<SubGroupWrapper> sgwLst = new List<SubGroupWrapper>();
            //if (model.Details.allSubGroups != null && model.Details.allSubGroups.Count() > 0)
            //{
            //    for (var x = 0; x < model.Details.allSubGroups.Length; x++)
            //    {
            //        if (model.Details.ehr.group != null && model.Details.allSubGroups[x].groupType != null)
            //        {
            //            if (model.Details.allSubGroups[x].groupType.Equals(model.Details.ehr.group.ToString()))
            //            {
            //                sgw = model.Details.allSubGroups[x];
            //                sgwLst.Add(sgw);
            //            }
            //        }
            //    }
            //    model.AllSubGroups = sgwLst.ToArray();
            //}

            if (model.AllSubGroups != null)
            {
                model.PrimaryPicked = new string[model.AllSubGroups.Length];
                model.SecondaryPicked = new string[model.AllSubGroups.Length];
                for (var x = 0; x < model.AllSubGroups.Length; x++)
                {
                    model.PrimaryPicked[x] = "0";
                    model.SecondaryPicked[x] = "0";
                }
            }

            model.ShowSubGroups = "false";
            //model.ShowSubGroups = Role.GroupAdmin == "E" ? "true" : "false";
            //model.ShowSubGroups = CurrentUser.IsInRole(Role.GroupAdmin) == true ? "true" : "false";

            if (CurrentUser.IsInRole(Role.GroupAdmin) == true)
            {
                var al = new ArrayList();
                //BasePartyModel bpModel = new BasePartyModel();
                //EHRDetailsModel ehrModel = new EHRDetailsModel();
                SubGroupWrapper[] selectedSG = null;
                model.AllSubGroups = null;              


                string[] selectedGroup = new string[1];
                selectedGroup.SetValue(CurrentUser.Group, 0);

                var allSGT = Service.GetAllSubGroupTypes();

                if (allSGT != null && selectedGroup != null)
                {
                    selectedSG = allSGT.Where(c => c.groupType == selectedGroup[0]).ToArray();
                }

                if (selectedSG != null)
                {
                    if (selectedSG.Length != 0)
                    {
                        model.AllSubGroups = selectedSG;
                    }

                    al.Add(selectedSG);

                    model.PrimaryPicked = new string[selectedSG.Length];
                    model.SecondaryPicked = new string[selectedSG.Length];
                    for (var x = 0; x < selectedSG.Length; x++)
                    {
                        model.PrimaryPicked[x] = "false";
                        model.SecondaryPicked[x] = "false";
                    }
                    al.Add(model.PrimaryPicked);
                    al.Add(model.SecondaryPicked);
                }
                else
                {
                    model.AllSubGroups = null;
                }

                model.ShowSubGroups = "true";
                if (selectedGroup != null)
                {
                    model.SelectedGT = selectedGroup[0];
                    model.Details.ehr.homeGroup = selectedGroup[0];
                }
            }

            if (partyType.Equals(Role.Patient) && (CurrentUser.Group.Equals("MHI") || CurrentUser.Group.Equals("CW") || CurrentUser.Group.Equals("CFP") || CurrentUser.Group.Equals("ECDHD")))
            {
                model.DailyQuestionsValue = "0";
                model.FeelingsValues = "1";
                model.NotificationsValue = "1";
                model.PreferenceKeyType = "MRE";   /// manual entry
                model.PreferenceValue = "1";
            } else
            {
                model.DailyQuestionsValue = "0";
                model.FeelingsValues = "0";
                model.NotificationsValue = "0";
            }

            model.ParameterModel = new ParameterModel();
            model.Insurance = new InsuranceModel();
            return model;
        }

        #endregion

        #region Action Methods

        #region Search

        public ActionResult Index()
        {
            return RedirectToAction("Search");
        }

        public ActionResult Search()
        {
            var searchModel = new SearchModel()
            {
                Criteria = SearchCriteria.LastName,
                Status = "Active"
            };

            // setting cookies for maintaining Search session
            var ehrPage = Request.Cookies["EHRPage"];
            
            if (ehrPage != null)
            {
                searchModel.EHRPage = Request.Cookies["EHRPage"].Value;                
                var cookie = Request.Cookies[CookieName.PartySearchCriteria];
                //If cookie was set, then get the criteria from the cookies
                if (cookie != null)
                {
                    searchModel.Criteria = (SearchCriteria)int.Parse(cookie.Values["Criteria"]);
                    searchModel.SearchText = cookie.Values["SearchText"];
                    searchModel.Status = cookie.Values["Status"];
                    searchModel.Parties = Service.GetParties(CurrentUser.Group, searchModel);
                }

                Response.Cookies["EHRPage"].Value = null;
                Response.Cookies["EHRPage"].Expires = DateTime.Now.AddMonths(-1); 
            }


            if (CurrentUser.IsInRole(Role.Admin) == false)
            {
                searchModel.ExcludedCriteria.Add(SearchCriteria.Group);
            }

            return View("Index", searchModel);
        }

        public ActionResult SearchParties(SearchModel searchModel)
        {
            //Validate the User Selection before you store it in the cookie
            if (searchModel.SearchFor == SearchFor.AllParties 
                    && string.IsNullOrWhiteSpace(searchModel.SearchText))
            {
                throw new ArgumentException("Search Text cannot be empty.");
            }
            else if (searchModel.SearchFor != SearchFor.AllParties && searchModel.PartyId == "0")
            {
                throw new ArgumentException("Invalid Party / Search Text.");
            }

            if (searchModel.SearchFor == SearchFor.AllParties
                    && searchModel.SearchText.Length < 2)
            {
                throw new ArgumentException("Please enter more than one character for search.");
            }

            var cookie = Request.Cookies[CookieName.PartySearchCriteria];
            //Create the cookie
            if (cookie == null)
            {
                cookie = new HttpCookie(CookieName.PartySearchCriteria);
            }
            //Store it in the cookie 
            cookie.Values["Criteria"] = ((int)searchModel.Criteria).ToString();
            cookie.Values["SearchText"] = searchModel.SearchText;
            cookie.Values["Status"] = searchModel.Status;
            cookie.Expires = DateTime.Now.AddMinutes(30);
            Response.Cookies.Remove(CookieName.PartySearchCriteria);
            Response.Cookies.Add(cookie);

            Response.Cookies.Remove("EHRPage");

            //var ehrPage = Request.Cookies[CookieName.EHRPage];
            //if (ehrPage == null)
            //{
            //    ehrPage = new HttpCookie(CookieName.EHRPage);
            //}
            //ehrPage.Values["EHRPage"] = searchModel.EHRPage;
            //ehrPage.Expires = DateTime.Now.AddMinutes(30);
            //Response.Cookies.Remove(CookieName.EHRPage);
            //Response.Cookies.Add(ehrPage);

            searchModel.Parties = Service.GetParties("ALL", searchModel);
            return PartialView("_ListParties", searchModel);
        }

        public ActionResult SearchKits(SearchModel searchModel)
        {
            //Validate the User Selection before you store it in the cookie
            if (searchModel.SearchFor == SearchFor.AllKits
                    && string.IsNullOrWhiteSpace(searchModel.KitSearchText))
            {
                throw new ArgumentException("Search Text cannot be empty.");
            }
            else if (searchModel.SearchFor != SearchFor.AllKits && searchModel.KitId == "0")
            {
                throw new ArgumentException("Invalid Kit / Search Text.");
            }

            if (searchModel.SearchFor == SearchFor.AllKits
                    && searchModel.KitSearchText.Length < 2)
            {
                throw new ArgumentException("Please enter more than one character for search.");
            }

            var cookie = Request.Cookies[CookieName.KitSearchCriteria];
            //Create the cookie
            if (cookie == null)
            {
                cookie = new HttpCookie(CookieName.KitSearchCriteria);
            }
            //Store it in the cookie 
            cookie.Values["Criteria"] = ((int)searchModel.KitsCriteria).ToString();
            cookie.Values["SearchText"] = searchModel.SearchText;
            //cookie.Values["Status"] = searchModel.Status;
            cookie.Expires = DateTime.Now.AddMinutes(30);
            Response.Cookies.Remove(CookieName.KitSearchCriteria);
            Response.Cookies.Add(cookie);

            //if (User.IsInRole(Role.GroupAdmin))
            //    if (Helper.CurrentUser.IsInRole(Role.GroupAdmin) == false)
            //{
                searchModel.Group = CurrentUser.Group;
            //}
            
//            searchModel.DeviceKit = Service.SearchKitByCriteria(searchModel);
//            searchModel.Parties = Service.SearchPartyInKitByCriteria(searchModel);
                searchModel.Patients = Service.SearchKitAndPartyDetails(searchModel);
            return PartialView("_ListKits", searchModel);
        }

        public ActionResult ExportPartiesAndKits(SearchModel searchModel)
        {
            //Validate the User Selection before you store it in the cookie
            if ((searchModel.SearchFor == SearchFor.AllParties && string.IsNullOrWhiteSpace(searchModel.SearchText)) ||
                  (searchModel.SearchFor == SearchFor.AllKits && string.IsNullOrWhiteSpace(searchModel.KitSearchText)))
            {
                throw new ArgumentException("Search Text cannot be empty.");
            }
            if ( (searchModel.SearchFor != SearchFor.AllParties ||
                  searchModel.SearchFor != SearchFor.AllKits) &&
                  searchModel.PartyId == "0")
            {
                throw new ArgumentException("Invalid Party / Search Text.");
            }

            if (User.IsInRole(Role.GroupAdmin))
            {
                searchModel.Group = CurrentUser.Group;
            }

            if (searchModel.SearchFor == SearchFor.AllKits)
            {
                var fileDownloadName = "Kits.xlsx";
                var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                var fileStream = Service.GetKitsExcel(searchModel);
                fileStream.Position = 0;
                var fsr = new FileStreamResult(fileStream, contentType);
                fsr.FileDownloadName = fileDownloadName;
                return fsr;
            }
            else
            {
                var fileDownloadName = "Parties.xlsx";
                var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                var fileStream = Service.GetPartiesExcel("ALL", searchModel);
                fileStream.Position = 0;
                var fsr = new FileStreamResult(fileStream, contentType);
                fsr.FileDownloadName = fileDownloadName;
                return fsr;
            }
        }

        //public ActionResult ExportKits(SearchModel searchModel)
        //{
        //    //Validate the User Selection before you store it in the cookie
        //    if (searchModel.SearchFor == SearchFor.AllParties
        //            && string.IsNullOrWhiteSpace(searchModel.SearchText))
        //    {
        //        throw new ArgumentException("Search Text cannot be empty.");
        //    }
        //    else if (searchModel.SearchFor != SearchFor.AllParties
        //             && searchModel.PartyId == "0")
        //    {
        //        throw new ArgumentException("Invalid Party / Search Text.");
        //    }

        //    var fileDownloadName = "Kits.xlsx";
        //    var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //    var fileStream = Service.GetKitsExcel(searchModel);
        //    fileStream.Position = 0;
        //    var fsr = new FileStreamResult(fileStream, contentType);
        //    fsr.FileDownloadName = fileDownloadName;
        //    return fsr;
        //}

        public ActionResult GAPatients()
        {
            return RedirectToAction("DailyReport");
        }

        #endregion

        #region QuickBook

        public ActionResult ExportToQuickBooks()
        {
            var fileDownloadName = "Myvitalz.xlsx";
            var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            var fileStream = Service.GetQuickBookExportFile(CurrentUser.Group);
            fileStream.Position = 0;
            var fsr = new FileStreamResult(fileStream, contentType);
            fsr.FileDownloadName = fileDownloadName;
            return fsr;
        }

        #endregion


        #region Party Methods

        public ActionResult ShowPatients(string ShowAll)
        {
            var response = Service.GetPatientsByGroup(CurrentUser.Group, Constant.FirstPageIndex);

            var model = new PatientListModel(int.Parse(response.totalCount), 1000, 1)
            {
                Patients = response.patients
            };

            if (ShowAll != null)
            {
                model.Patients = model.Patients.OrderBy(o => o.isPartyActive ? 0 : 1).ToArray();
                model.showAll = true;
            }
            else
            {
                model.Patients = model.Patients != null ? model.Patients.ToList().Where(o => o.isPartyActive).ToArray() : null;
                model.showAll = false;
            }

            return View("ShowPatients", model);
        }

        // public ActionResult ShowParties()
        // {
        //     var response = Service.GetPartiesDetails(CurrentUser.Group, Constant.FirstPageIndex);
        //     var model = new PatientListModel(int.Parse(response.totalCount), Properties.Settings.Default.DisplayRowsCount, 1)
        //     {
        //         Parties = response.party
        //     };
        //     return View("ShowParties", model);
        // }

        public ActionResult ShowParties(string ShowAll)
        {
            var response = Service.GetPartiesDetails(CurrentUser.Group, Constant.FirstPageIndex);
            var model = new PatientListModel(0, Properties.Settings.Default.DisplayRowsCount, 1);
            if (response.totalCount != null)
            {
                model.Parties = response.party;
                model.TotalRowsCount = int.Parse(response.totalCount);
                //var model = new PatientListModel(int.Parse(response.totalCount), Properties.Settings.Default.DisplayRowsCount, 1)
                //{
                //    Parties = response.party
                //};

                if (ShowAll != null)
                {
                    model.Parties = model.Parties.OrderBy(o => o.isPartyActive ? 0 : 1).ToArray();
                    model.showAll = true;
                }
                else
                {
                    model.Parties = model.Parties.ToList().Where(o => o.isPartyActive).ToArray();
                    model.showAll = false;
                }
            }
            return View("ShowParties", model);
        }

        public ActionResult ExportPatients()
        {
            var response = Service.GetPatientsByGroup(CurrentUser.Group, Constant.FirstPageIndex);
            var model = new PatientListModel(int.Parse(response.totalCount), Properties.Settings.Default.DisplayRowsCount, 1)
            {
                Patients = response.patients
            };
            var fileDownloadName = "Patients.xlsx";
            var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            var fileStream = Service.GetPatientsExcel(CurrentUser.Group);
            fileStream.Position = 0;
            var fsr = new FileStreamResult(fileStream, contentType);
            fsr.FileDownloadName = fileDownloadName;
            return fsr; ;
        }

        public ActionResult ExportDNCParties()
        {
            var response = Service.GetPartiesDetails(CurrentUser.Group, Constant.FirstPageIndex);
            var model = new PatientListModel(int.Parse(response.totalCount), Properties.Settings.Default.DisplayRowsCount, 1)
            {
                Patients = response.party
            };
            var fileDownloadName = "Parties.xlsx";
            var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            var fileStream = Service.GetPartiesDNCExcel(CurrentUser.Group);
            fileStream.Position = 0;
            var fsr = new FileStreamResult(fileStream, contentType);
            fsr.FileDownloadName = fileDownloadName;
            return fsr; ;
        }

        public ActionResult ShowPatientsPage(PatientListModel model)
        {
            var response = Service.GetPatientsByGroup(CurrentUser.Group, model.GetStarRowtIndex());
            model.Patients = response.patients;

            return PartialView("_ShowPatientsList", model);
        }

        public ActionResult ShowPartiesPage(PatientListModel model)
        {
            var response = Service.GetPartiesDetails(CurrentUser.Group, model.GetStarRowtIndex());
            model.Parties = response.party;

            return PartialView("_ShowPartiesList", model);
        }

        public ActionResult AddBaseParty(string partyType)
        {
            var model = new BasePartyModel("Party.");
            model.AssociationTypes = GetAssociationTypes();
            model.GroupTypes = GetGroupTypes();
            model.Party = new Patient()
            {
                partyType = partyType,
                address = new Address()
            };

            return PartialView("_EditBaseParty", model);
        }

        public ActionResult EditBaseParty(string partyId)
        {
            var details = Service.GetPartyById(partyId);
            var patient = Helper.GetPatient(details);
            var model = new BasePartyModel("Party.", patient);
            model.AssociationTypes = GetAssociationTypes();
            model.GroupTypes = GetGroupTypes();
            return PartialView("_EditBaseParty", model);
        }

        public string SaveBaseParty(BasePartyModel model)
        {
            var stringBuilder = new StringBuilder();
            //ValidatePatient(model.Party, stringBuilder);
            if (model.Party.partyType == "P")
            {
                ValidatePatient(model.Party, stringBuilder);
            }
            else
            {
                ValidateOtherUsers(model.Party, stringBuilder);
            }
            if (stringBuilder.Length != 0)
                throw new ArgumentException(((object)stringBuilder).ToString());

            var details = new EHRDetails();
            details.ehr = model.Party;
            var response = Service.SaveParty((Party)model.Party, null, CurrentUser.UserId);
            return response.partyId;
        }

        public ActionResult AddPatientParty()
        {
            var details = GetEmptyEhrDetailsModel(Role.Patient);
            
            return View("EditEhr", details);
        }

        public ActionResult AddClinicianParty()
        {
            var details = GetEmptyEhrDetailsModel(Role.Clinician);
            return View("EditEhr", details);
        }

        public ActionResult AddCaregiverParty()
        {
            var details = GetEmptyEhrDetailsModel(Role.Caregiver);
            return View("EditEhr", details);
        }

        public ActionResult AddDoctorParty()
        {
            var details = GetEmptyEhrDetailsModel(Role.Doctor);
            return View("EditEhr", details);
        }

        public ActionResult AddAdminParty()
        {
            var details = GetEmptyEhrDetailsModel(Role.Admin);
            return View("EditEhr", details);
        }

        public ActionResult AddGroupAdminParty()
        {
            var details = GetEmptyEhrDetailsModel(Role.GroupAdmin);
            return View("EditEhr", details);
        }

        public ActionResult EditPatient(string partyId)
        {
            return BaseEditEhr(partyId);
        }

        public ActionResult DeleteParty(string partyId, string partyType)
        {
            Service.DeleteParty(partyId, partyType);
            return EmptyResult;
        }

        //public ActionResult DeleteParty(string partyId, string partyType)
        //{
        //    Service.DeleteParty(partyId, partyType);
        //    return EmptyResult;
        //}

        public ActionResult EditParty(string partyId)
        {
            return BaseEditParty(partyId);
        }

        public ActionResult DailyReport(bool isListView = false)
        {
            var model = new DailyReportModel()
            {
                IsFullReport = isListView ? false : true,
                IsListView = isListView
            };
            return BaseDailyReport(model);
        }

        public async Task<ActionResult> DeleteDeviceReading(string deviceType, string reading1, string reading2, string mtime, string patientId)
        {
            HttpResponseMessage response;
            string rr;

            //call the restapi to delete the reading
            using (var httpClient = new System.Net.Http.HttpClient())
            {
                ////var url1 = "http://localhost:8080/MyvitalzDataWeb/rest/data";
                var url = System.Configuration.ConfigurationManager.AppSettings["ManageDataUrl"];
                var rsrc = url + "/" + deviceType + "/" + patientId + "/" + reading1 + "/" + reading2 + "/" + mtime;
                Console.Out.WriteLine("Url: " + rsrc);
                var content = new System.Net.Http.StringContent(string.Empty, Encoding.UTF8, "application/json");
                response = await httpClient.DeleteAsync(rsrc);
                rr = await response.Content.ReadAsStringAsync();
                Console.Out.WriteLine("Response : " + response.StatusCode);
                Console.Out.WriteLine("RR : " + rr);
                return Content(rr);
            }
        }

        public async Task<ActionResult> DeleteFeelingReading(string deviceType, string pain, string mood, string energy, string breath, string mtime, string patientId)
        {
            HttpResponseMessage response;
            string rr;

            //call the restapi to delete the reading
            using (var httpClient = new System.Net.Http.HttpClient())
            {
                ////var url1 = "http://localhost:8080/MyvitalzDataWeb/rest/data";
                var url = System.Configuration.ConfigurationManager.AppSettings["ManageDataUrl"];
                var rsrc = url + "/" + deviceType + "/" + patientId + "/" + pain + "/" + mood + "/" + energy + "/" + breath + "/" + mtime;
                Console.Out.WriteLine("Url: " + rsrc);
                var content = new System.Net.Http.StringContent(string.Empty, Encoding.UTF8, "application/json");
                response = await httpClient.DeleteAsync(rsrc);
                rr = await response.Content.ReadAsStringAsync();
                Console.Out.WriteLine("Response : " + response.StatusCode);
                Console.Out.WriteLine("RR : " + rr);
                return Content(rr);
            }
        }

        public async Task<ActionResult> DeleteDeviceReadingById(string deviceType, string id)
        {
            HttpResponseMessage response;
            string rr;

            //call the restapi to delete the reading
            using (var httpClient = new System.Net.Http.HttpClient())
            {
                ////var url1 = "http://localhost:8080/MyvitalzDataWeb/rest/data";
                var url = System.Configuration.ConfigurationManager.AppSettings["ManageDataUrl"];
                var rsrc = url + "/" + deviceType + "/" + id;
                Console.Out.WriteLine("Url: " + rsrc);
                var content = new System.Net.Http.StringContent(string.Empty, Encoding.UTF8, "application/json");
                response = await httpClient.DeleteAsync(rsrc);
                rr = await response.Content.ReadAsStringAsync();
                Console.Out.WriteLine("Response : " + response.StatusCode);
                Console.Out.WriteLine("RR : " + rr);
                return Content(rr);
            }
        }

        [HttpPost]
        public object UpdatePauseMonitor(string id, string partyid, string startDate, string endDate, string reason, string description)
        {
            object result = "";
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["PauseMonitorSavedUrl"]);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "PUT";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"date\":\"" + DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss zz") + "\"," +
                                   "\"patient\":\"" + partyid.ToString() + "\"," +
                                   "\"type\":\"pauseMonitoring\"," +
                                   "\"pm\": {" +
                                   "\"id\":\"" + id + "\"," +
                                   "\"startDate\":\"" + startDate + "\"," +
                                   "\"endDate\":\"" + endDate + "\"," +
                                   "\"reason\":\"" + reason + "\"," +
                                   "\"description\":\"" + description + "\" }}";
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
            }
            catch (WebException e)
            {
                return "{\"status\":\"" + e.Message + "\"}";
            }
            return result;
        }


        #endregion

        #region Common Methods

        public ActionResult PatientDevices(string patientId)
        {
            return BaseShowPatientDevices(patientId);
        }
        public ActionResult PatientReading(string patientId)
        {
            return BaseShowPatientReading(patientId);
        }

        public ActionResult GetDeviceQuadrant(string patientId)
        {
            return BaseGetDeviceQuadrant(patientId);
        }

        //public ActionResult GetDeviceQuadrantSliding(string patientId)
        //{
        //    return BaseGetDeviceQuadrantSliding(patientId);
        //}
        
        [HttpPost]
        public ActionResult getNewSubGroup(string[] selectedGroup)
        {
            return BaseGetNewSubGroup(selectedGroup);
        }

        //[HttpPost]
        //public ActionResult getHomeGroup(string[] selectedGroup)
        //{
        //    return BaseGetHomeGroup(selectedGroup);
        //}

        //[HttpPost]
        //public JsonResult SaveSubGroups(BasePartyModel model)
        //{
        //    return BaseSaveSubGroups(model);
        //}

        //[HttpPost]
        //public void SaveNewQuadrantSliding(string[] SlidingOrder)
        //{
        //    BaseSaveNewQuadrantSliding(SlidingOrder);
        //}

        public ActionResult Device(string deviceType, string patientId, DateTime? startDate, DateTime? endDate)
        {
            return BaseShowDevice(deviceType, patientId, startDate, endDate);
        }

        public ActionResult ShowEhr(string patientId)
        {
            return BaseShowEHR(patientId);
        }

        public ActionResult EditEhr(string patientId)
        {
            return BaseEditEhr(patientId);
        }

        public ActionResult RefreshEhrDetails(EHRDetailsModel model)
        {
            return PartialView("_EditEHRForm", model);
        }

        public string SaveEhr(EHRDetailsModel model)
        {
            return BaseSaveEhr(model, CurrentUser.UserId);
        }

        [HttpPost]
        public ActionResult AddDoctor(EHRDetailsModel model)
        {
            return BaseAddDoctor(model);
        }

        [HttpPost]
        public ActionResult RemoveDoctor(EHRDetailsModel model)
        {
            return BaseRemoveDoctor(model);
        }

        [HttpPost]
        public ActionResult AddCaregiver(EHRDetailsModel model)
        {
            return BaseAddCaregiver(model);
        }

        [HttpPost]
        public ActionResult RemoveCaregiver(EHRDetailsModel model)
        {
            return BaseRemoveCaregiver(model);
        }

        [HttpPost]
        public ActionResult AddClinician(EHRDetailsModel model)
        {
            return BaseAddClinician(model);
        }

        [HttpPost]
        public ActionResult RemoveClinician(EHRDetailsModel model)
        {
            return BaseRemoveClinician(model);
        }

        [HttpPost]
        public ActionResult AddAssociation(EHRDetailsModel model)
        {
            return BaseAddAssociation(model);
        }

        [HttpPost]
        public ActionResult RemoveAssociation(EHRDetailsModel model)
        {
            return BaseRemoveAssociation(model);
        }
        #endregion

        #endregion

    }
}
