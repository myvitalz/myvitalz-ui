﻿using MyVitalz.Web.Helpers;
using MyVitalz.Web.Service;
using System;
using System.Text;
using System.Web.Mvc;

namespace MyVitalz.Web.Controllers
{
    [Authorize(Roles = Role.Admin)]
    public class AssociationController : BaseController
    {
        #region Action Methods

        public ActionResult Index()
        {
            var model = Service.GetAssociationTypes();
            return View("Index", model);
        }

        public ActionResult GetAssociationDialog(XrefType model)
        {
            return PartialView("_AssociationDialog", model);
        }

        public ActionResult SaveAssociation(XrefType model)
        {
            if (model == null)
            {
                throw new ArgumentException("Name & Description cannot be empty.");
            }
            var errorMessage = new StringBuilder();
            if (string.IsNullOrWhiteSpace(model.xrefType) == true)
            {
                errorMessage.AppendLine("Name cannot be empty.");
            }
            if (string.IsNullOrWhiteSpace(model.xrefTypeDesc) == true)
            {
                errorMessage.AppendLine("Description cannot be empty.");
            }
            if (errorMessage.Length > 0)
            {
                throw new ArgumentException(errorMessage.ToString());
            }
            model.updateUser = CurrentUser.UserId;
            Service.SaveAssociation(model, CurrentUser.UserId);
            var associationTypes = Service.GetAssociationTypes();
            return PartialView("_ListAssociations", associationTypes);
        }
 
        #endregion
    }
}
