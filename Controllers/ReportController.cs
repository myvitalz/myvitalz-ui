﻿using MyVitalz.Web.Helpers;
using MyVitalz.Web.Models;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MyVitalz.Web.Controllers 
{
    public class ReportController : BaseController
    {
        //
        // GET: /Report/
        [Authorize]
        public ActionResult Index()
        {
            string associatedPartyId = CurrentUser.UserId;
            string partyType = CurrentUser.Roles[0];
            string jwt = GetCookie("JWT");
            return Redirect("/Reports/dailyreadingreport.aspx?prmAssociatedPartyId=" + associatedPartyId + "&PartyType=" + partyType);

        }


        [Authorize]
        public ActionResult Create()
        {
            string associatedPartyId = CurrentUser.UserId;
            string partyType = CurrentUser.Roles[0];
            //if (partyType == "D")
            //{
            string jwt = GetCookie("JWT");
            return Redirect("/Reports/NewReadingReport.aspx?prmAssociatedPartyId=" + associatedPartyId + "&PartyType=" + partyType);
            //}
            //else
            //{
            //    return Redirect("/Reports/displayreport.aspx?PartyType=" + partyType);
            //}
        }

        [Authorize]
        public ActionResult CustomReports()
        {
            string associatedPartyId = CurrentUser.UserId;
            string partyType = CurrentUser.Roles[0];
            string jwt = GetCookie("JWT");
            //return Redirect("/Reports/customReports.aspx?prmAssociatedPartyId=" + associatedPartyId + "&PartyType=" + partyType + "&Jwt=" + jwt);
            return Redirect(string.Concat(Properties.Settings.Default.CrumReportUrl,jwt));
        }

        [Authorize]
        public ActionResult DetailedReports()
        {
            string associatedPartyId = CurrentUser.UserId;
            string partyType = CurrentUser.Roles[0];
            string jwt = GetCookie("JWT");
            return Redirect("https://rpts-ui.myvitalz.com/detailedreport?partyid=" + associatedPartyId + "&client=" + System.Configuration.ConfigurationManager.AppSettings["ClientName"].ToString() + "&Jwt=" + jwt);

      
            //return Redirect(string.Concat(Properties.Settings.Default.CrumReportUrl, jwt));
        }

        [Authorize]
        public ActionResult caretimeDetailedReports()
        {
            string associatedPartyId = CurrentUser.UserId;
            string partyType = CurrentUser.Roles[0];
            string jwt = GetCookie("JWT");
            return Redirect("https://rpts-ui.myvitalz.com/caretimedetailed?partyid=" + associatedPartyId + "&client=" + System.Configuration.ConfigurationManager.AppSettings["ClientName"].ToString() + "&Jwt=" + jwt);


            //return Redirect(string.Concat(Properties.Settings.Default.CrumReportUrl, jwt));
        }

        #region A1cReports
        [Authorize]
        public ActionResult A1cReports()
        {
            return RedirectToAction("BaseA1cReport");
        }

        #endregion

        public ActionResult ExportA1cReport()
        {
            BloodA1cReportModel rptModel = BaseA1cReportDetails(CurrentUser.UserId, CurrentUser.Group, CurrentUser.Roles.ElementAtOrDefault(0), null, null);
            // sort the results, last name first, then by measurementtime
            BloodA1cReportModel sortedList = new BloodA1cReportModel();
            sortedList.a1cReports = rptModel.a1cReports.OrderByDescending(a1c => a1c.MeasurementTime).ThenByDescending(a1c => a1c.LastName);
            var model = new BloodA1cReportModel()
            {
                a1cReports = sortedList.a1cReports
            };
            var fileDownloadName = "HbA1cReport.xlsx";
            var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            var fileStream = getA1cReportExcel(model);
            fileStream.Position = 0;
            var fsr = new FileStreamResult(fileStream, contentType);
            fsr.FileDownloadName = fileDownloadName;
            return fsr; ;
        }


        public ActionResult BaseA1cReport()
        {
            ViewBag.ControllerName = "BaseParty";
            BloodA1cReportModel rptModel = BaseA1cReportDetails(CurrentUser.UserId, CurrentUser.Group, CurrentUser.Roles.ElementAtOrDefault(0), null, null);
            // sort the results, last name first, then by measurementtime
            BloodA1cReportModel sortedList = new BloodA1cReportModel();
            sortedList.a1cReports = rptModel.a1cReports.OrderBy(a1c => a1c.LastName).ThenByDescending(a1c => a1c.MeasurementTime);
            return View("A1cReport", sortedList);
        }

        public ActionResult BaseA1cReportRefresh(DateTime? fromDate, DateTime? toDate)
        {
            ViewBag.ControllerName = "BaseParty";
            BloodA1cReportModel rptModel = BaseA1cReportDetails(CurrentUser.UserId, CurrentUser.Group, CurrentUser.Roles.ElementAtOrDefault(0), fromDate, toDate);
            // sort the results, last name first, then by measurementtime
            BloodA1cReportModel sortedList = new BloodA1cReportModel();
            sortedList.a1cReports = rptModel.a1cReports.OrderBy(a1c => a1c.LastName).ThenByDescending(a1c => a1c.MeasurementTime);
            return PartialView("_ShowA1cReport", sortedList);
        }

        public BloodA1cReportModel BaseA1cReportDetails(string userId, string group, string role, DateTime? fromDate, DateTime? toDate)
        {
            BloodA1cReportModel mdl = new BloodA1cReportModel();
            List<BloodHbA1cReportModel> lst = new List<BloodHbA1cReportModel>();
            //
            // call the service to get the data from the backend ...getHbA1cReport
            // pass the partyId, group and role to the backend
            // get these values from CurrentUser data.
            //
            string a1cReport = "";
            if( role.Equals(Role.Admin))
            {
                a1cReport = getHbA1cReport(userId, group, role, fromDate, toDate);
            }
            if (role.Equals(Role.GroupAdmin))
            {
                a1cReport = getHbA1cReport(userId, group, role, fromDate, toDate);
            }
            if (role.Equals(Role.Doctor) || role.Equals(Role.Clinician) || role.Equals(Role.Caregiver))
            {
                a1cReport = getHbA1cReport(userId, group, role, fromDate, toDate);
            }
            // parse the response

            if (a1cReport == null || a1cReport.Length == 0)
            {
                // return an empty response ...
                mdl.a1cReports = new List<BloodHbA1cReportModel>();
            }
            else
            {
                JObject joResponse = JObject.Parse(a1cReport);
                JArray rspList = (JArray)joResponse["RPT"];
                for (int i = 0; i < rspList.Count; i++)
                {
                    JToken ele = rspList.ElementAt(i);
                    BloodHbA1cReportModel rm = new BloodHbA1cReportModel();
                    rm.FirstName = (string)ele.ElementAt(0);
                    rm.LastName = (string)ele.ElementAt(1);
                    rm.Dob = (string)ele.ElementAt(2);
                    rm.PatientId = (string)ele.ElementAt(3);
                    rm.Group = (string)ele.ElementAt(4);
                    rm.SubGroup = (string)ele.ElementAt(5);
                    rm.County = (string)ele.ElementAt(6);
                    rm.AvgGlucose = (string)ele.ElementAt(7);
                    rm.Id = (string)ele.ElementAt(8);
                    rm.DeviceId = (string)ele.ElementAt(9);
                    rm.HbA1c_READING = (string)ele.ElementAt(10);
                    rm.MeasurementTime = (string)ele.ElementAt(11);
                    rm.TransmissionTime = (string)ele.ElementAt(12);
                    rm.UpdateTs = (string)ele.ElementAt(13);
                    rm.UpdateUser = (string)ele.ElementAt(14);
                    rm.PartyId = (string)ele.ElementAt(15);
                    lst.Add(rm);
                }
                mdl.a1cReports = lst;
            }
            return mdl;
        }

        public Stream getA1cReportExcel(BloodA1cReportModel reports)
        {
            var package = new ExcelPackage();

            #region Worksheet1
            var worksheet = package.Workbook.Worksheets.Add("HbA1cReport");

            //Format the header
            using (var header = worksheet.Cells["A1:F1"])
            {
                header.Style.Font.Bold = true;
                header.Style.WrapText = true;
                header.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                header.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                header.Style.Fill.PatternType = ExcelFillStyle.Solid;
                header.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(237, 237, 237));
                header.Style.Font.Size = 12;
                header.AutoFilter = true;
            }
            worksheet.View.FreezePanes(2, 3);
            worksheet.Cells["A1"].Value = "Last Name";
            worksheet.Cells["B1"].Value = "First Name";
            worksheet.Cells["C1"].Value = "ID";
            worksheet.Cells["D1"].Value = "County";
            worksheet.Cells["E1"].Value = "Group";
            worksheet.Cells["F1"].Value = "SubGroup";
            worksheet.Cells["G1"].Value = "Date";
            worksheet.Cells["H1"].Value = "HbA1c";
            worksheet.Cells["I1"].Value = "Avg Glucose";

            int rowIndexCurrentRecord = 2;
            foreach (var a1cRpt in reports.a1cReports)
            {
                worksheet.Cells["A" + rowIndexCurrentRecord].Value = a1cRpt.LastName;
                worksheet.Cells["B" + rowIndexCurrentRecord].Value = a1cRpt.FirstName;
                worksheet.Cells["C" + rowIndexCurrentRecord].Value = a1cRpt.PatientId;
                worksheet.Cells["D" + rowIndexCurrentRecord].Value = a1cRpt.County;
                worksheet.Cells["E" + rowIndexCurrentRecord].Value = a1cRpt.Group;
                worksheet.Cells["F" + rowIndexCurrentRecord].Value = a1cRpt.SubGroup;
                worksheet.Cells["G" + rowIndexCurrentRecord].Value = a1cRpt.MeasurementTime;
                worksheet.Cells["H" + rowIndexCurrentRecord].Value = a1cRpt.HbA1c_READING;
                worksheet.Cells["I" + rowIndexCurrentRecord].Value = a1cRpt.AvgGlucose;
                rowIndexCurrentRecord++;
            }
            worksheet.Cells["A1:F" + rowIndexCurrentRecord].AutoFitColumns();
            #endregion
            package.Workbook.Properties.Title = "Myvitalz HbA1c Report";
            package.Workbook.Properties.Author = "Myvitalz";
            package.Workbook.Properties.Company = "Myvitalz LLC";
            var stream = new MemoryStream();
            package.SaveAs(stream);
            return stream;
        }

        //[HttpGet]
        //public string getHbA1cReportByGroup(string partyId, string group)
        //{
        //    string result = "";
        //    try
        //    {
        //        ///A1C/Report/Group/{groupName}
        //        string rt;
        //        WebRequest request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetReadingDetails"] + "A1C/Report/Group/" + group + "");
        //        WebResponse response = request.GetResponse();
        //        Stream dataStream = response.GetResponseStream();
        //        StreamReader reader = new StreamReader(dataStream);
        //        rt = reader.ReadToEnd();
        //        Console.WriteLine(rt);
        //        reader.Close();
        //        response.Close();
        //        return rt;

        //    }
        //    catch (WebException e)
        //    {
        //        Console.WriteLine(e.Message);
        //    }
        //    return result;
        //}

        [HttpGet]
        public string getHbA1cReport(string partyId, string group, string role, DateTime? fromDate, DateTime? toDate)
        {
            string result = "";
            try
            {
                ///A1C/Report/Group/{groupName}
                string rt;
                string begin = fromDate.HasValue ? fromDate.Value.ToString("yyyy-MM-dd") : "begin";
                string end = toDate.HasValue ? toDate.Value.ToString("yyyy-MM-dd") : "end";

                WebRequest request = null;
                if (role.Equals(Role.Admin))
                {
                    request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetReadingDetails"] + "A1C/Report/All/" + begin + "/" + end + "");
                }
                if (role.Equals(Role.GroupAdmin))
                {
                    request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetReadingDetails"] + "A1C/Report/Group/" + group + "/" + begin + "/" + end + "");
                }
                if (role.Equals(Role.Doctor))
                {
                    request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetReadingDetails"] + "A1C/Report/Id/" + partyId + "/" + begin + "/" + end + "");
                }
                if (role.Equals(Role.Clinician))
                {
                    request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetReadingDetails"] + "A1C/Report/Id/" + partyId + "/" + begin + "/" + end + "");
                }
                if (role.Equals(Role.Caregiver))
                {
                    request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetReadingDetails"] + "A1C/Report/Id/" + partyId + "/" + begin + "/" + end + "");
                }
                WebResponse response = request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                rt = reader.ReadToEnd();
                Console.WriteLine(rt);
                reader.Close();
                response.Close();
                return rt;

            }
            catch (WebException e)
            {
                Console.WriteLine(e.Message);
            }
            return result;
        }


    }














}
