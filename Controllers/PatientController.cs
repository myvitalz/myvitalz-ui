﻿using MyVitalz.Web.Helpers;
using MyVitalz.Web.Service;
using System;
using System.Text;
using System.Web.Mvc;
using System.Web;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using MyVitalz.Web.Models;
using System.Net.Http;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Http.Headers;

namespace MyVitalz.Web.Controllers
{
    [Authorize(Roles = Role.Patient + Role.Separator + Role.Admin)]
    public class PatientController : BasePartyController
    {
        #region Common Actions

        public ActionResult Index()
        {
            return RedirectToAction("PatientDevices", (object)new
            {
                patientId = CurrentUser.UserId
            });
        }

        public ActionResult Calendar()
        {
            return BaseShowPatientReading(CurrentUser.UserId);
        }

        public ActionResult PatientDevices(string patientId)
        {
            return BaseShowPatientDevices(patientId);
        }

        public ActionResult GetDeviceQuadrant(string patientId)
        {
            return BaseGetDeviceQuadrant(patientId);
        }

        public ActionResult Device(string deviceType, string patientId, DateTime? startDate, DateTime? endDate)
        {
            return BaseShowDevice(deviceType, patientId, startDate, endDate);
        }

        public ActionResult UploadPatients()
        {
            return View("UploadPatients");
        }

        public ActionResult UploadCareTeam()
        {
            return View("UploadCareTeam");
        }

        [HttpPost]
        public ActionResult PatientUpload(string file)
        {
            //try
            //{
            var userId = CurrentUser.UserId;
            //byte userIdByte = Byte.Parse(userId);
            //byte[] userIdByteArray = new byte[userIdByte];
            MemoryStream ms;
            if (Request.Files.Count != 0)
            {
                try
                {
                    foreach (string file1 in Request.Files)
                    {
                        var fileContent = Request.Files[file1];
                        if (fileContent != null && fileContent.ContentLength > 0)
                        {
                            // get a stream
                            var stream = fileContent.InputStream;
                            byte[] fileContents = new byte[fileContent.ContentLength];
                            using (ms = new MemoryStream())
                            {
                                int read = 0;
                                while ((read = fileContent.InputStream.Read(fileContents, 0, fileContent.ContentLength)) > 0)
                                {
                                    ms.Write(fileContents, 0, read);
                                    //ms.Write(userIdByteArray, 0, read);
                                }
                            }

                            Uri webService = new Uri(System.Configuration.ConfigurationManager.AppSettings["PatientUploadUrl"]);
                            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, webService);

                            MultipartContent multiPartContent = new MultipartContent("mixed");
                            ByteArrayContent byteArrayContent = new ByteArrayContent(ms.ToArray());
                            byteArrayContent.Headers.Add("Content-Type", "multipart/mixed");
                            byteArrayContent.Headers.Add("Content-Disposition", "mixed");
                            byteArrayContent.Headers.ContentDisposition.FileName = fileContent.FileName;
                            byteArrayContent.Headers.Add("UserId", CurrentUser.UserId);
                            //byteArrayContent.Headers.ContentDisposition.FileName = file.FileName;
                            multiPartContent.Add(byteArrayContent);

                            requestMessage.Content = multiPartContent;
                            requestMessage.Content.Headers.ContentType.MediaType = "multipart/mixed";

                            HttpClient httpClient = new HttpClient();
                            try
                            {
                                Task<HttpResponseMessage> httpRequest = httpClient.SendAsync(requestMessage, HttpCompletionOption.ResponseContentRead, CancellationToken.None);

                                HttpResponseMessage httpResponse = httpRequest.Result;
                                HttpStatusCode statusCode = httpResponse.StatusCode;
                                HttpContent responseContent = httpResponse.Content;

                                if (responseContent != null)
                                {
                                    Task<String> stringContentsTask = responseContent.ReadAsStringAsync();
                                    String stringContents = stringContentsTask.Result;
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                        else
                        {
                            throw new ArgumentException("Please choose correct file.");
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new ArgumentException(e.ToString());
                }

                return View("Success", (object)"File uploaded successfully.");
            }
            else
            {
                throw new ArgumentException("Please choose atleast one file.");
            }
            //}
            //catch (Exception)
            //{
            //    Response.StatusCode = (int)HttpStatusCode.BadRequest;
            //    //return View("Upload failed");
            //    return View("_FileUploadDialog.cshtml");
            //}
            //return View("Success", (object)"File uploaded successfully.");
        }



        [HttpPost]
        public ActionResult CareTeamUpload(string file)
        {
            var userId = CurrentUser.UserId;
            MemoryStream ms;
            if (Request.Files.Count != 0)
            {
                try
                {
                    foreach (string file1 in Request.Files)
                    {
                        var fileContent = Request.Files[file1];
                        if (fileContent != null && fileContent.ContentLength > 0)
                        {
                            // get a stream
                            var stream = fileContent.InputStream;
                            byte[] fileContents = new byte[fileContent.ContentLength];
                            using (ms = new MemoryStream())
                            {
                                int read = 0;
                                while ((read = fileContent.InputStream.Read(fileContents, 0, fileContent.ContentLength)) > 0)
                                {
                                    ms.Write(fileContents, 0, read);
                                }
                            }

                            Uri webService = new Uri(System.Configuration.ConfigurationManager.AppSettings["CareTeamUploadUrl"]);
                            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, webService);

                            MultipartContent multiPartContent = new MultipartContent("mixed");
                            ByteArrayContent byteArrayContent = new ByteArrayContent(ms.ToArray());
                            byteArrayContent.Headers.Add("Content-Type", "multipart/mixed");
                            byteArrayContent.Headers.Add("Content-Disposition", "mixed");
                            byteArrayContent.Headers.ContentDisposition.FileName = fileContent.FileName;
                            byteArrayContent.Headers.Add("UserId", CurrentUser.UserId);
                            //byteArrayContent.Headers.ContentDisposition.FileName = file.FileName;
                            multiPartContent.Add(byteArrayContent);

                            requestMessage.Content = multiPartContent;
                            requestMessage.Content.Headers.ContentType.MediaType = "multipart/mixed";

                            HttpClient httpClient = new HttpClient();
                            try
                            {
                                Task<HttpResponseMessage> httpRequest = httpClient.SendAsync(requestMessage, HttpCompletionOption.ResponseContentRead, CancellationToken.None);

                                HttpResponseMessage httpResponse = httpRequest.Result;
                                HttpStatusCode statusCode = httpResponse.StatusCode;
                                HttpContent responseContent = httpResponse.Content;

                                if (responseContent != null)
                                {
                                    Task<String> stringContentsTask = responseContent.ReadAsStringAsync();
                                    String stringContents = stringContentsTask.Result;
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                        else
                        {
                            throw new ArgumentException("Please choose correct file.");
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new ArgumentException(e.ToString());
                }

                return View("Success", (object)"File uploaded successfully.");
            }
            else
            {
                throw new ArgumentException("Please choose atleast one file.");
            }
        }




        #endregion
    }
}
