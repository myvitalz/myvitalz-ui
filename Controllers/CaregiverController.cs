﻿// Type: MyVitalz.Web.Controllers.CaregiverController
// Assembly: MyVitalz.Web, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5DFB40F2-FD4B-4A92-9CB7-01EA3C877ECE
// Assembly location: D:\Data\Haraku\MyVitalz\bin\MyVitalz.Web.dll

using MyVitalz.Web.Helpers;
using MyVitalz.Web.Models;
using System;
using System.Web.Mvc;

namespace MyVitalz.Web.Controllers
{
    [Authorize(Roles = Role.Caregiver)]
    public class CaregiverController : BasePartyController
    {
        #region Action Methods

        public ActionResult Index()
        {
            return RedirectToAction("Patients");
        } 

        #region Common Methods

        public ActionResult Patients()
        {
            return BaseShowPatients(CurrentUser.UserId);
        }

        public ActionResult PatientDevices(string patientId)
        {
            return BaseShowPatientDevices(patientId);
        }

        public ActionResult GetDeviceQuadrant(string patientId)
        {
            return BaseGetDeviceQuadrant(patientId);
        }

        public ActionResult Device(string deviceType, string patientId, DateTime? startDate, DateTime? endDate)
        {
            return BaseShowDevice(deviceType, patientId, startDate, endDate);
        }

        public ActionResult ShowEhr(string patientId)
        {
            return BaseShowEHR(patientId);
        }

        #endregion

        #endregion
    }
}
