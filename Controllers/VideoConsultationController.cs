﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyVitalz.Web.Controllers
{
    [Authorize]
    public class VideoConsultationController : BaseController
    {
        public ActionResult Customer()
        {
            return View();
        }

        public ActionResult Provider()
        {
            return View();
        }
    }
}
