﻿using System.IO;
using System.Net;
using System.Text;
using System.Web.Mvc;

namespace MyVitalz.Web.Controllers
{
    public class QualCommController : Controller
    {
        #region Action Methods

        public ActionResult UploadKit(string file)
        {
            var webRequest = WebRequest.Create("http://localhost:8080/MyvitalzFileUploadWeb/rest/file/upload/");
            webRequest.Method = "POST";
            file = "file=" + System.IO.File.ReadAllText("C:\\Users\\KPragasam\\Downloads\\MyvitalzQuickBook.csv");
            webRequest.ContentLength = (long)file.Length;
            webRequest.ContentType = "multipart/mixed";
            var requestStream = webRequest.GetRequestStream();
            var parameter = Encoding.ASCII.GetBytes(file);
            requestStream.Write(parameter, 0, parameter.Length);
            requestStream.Close();
            var httpWebResponse = webRequest.GetResponse() as HttpWebResponse;
            if (httpWebResponse.StatusCode != HttpStatusCode.OK)
                return new HttpStatusCodeResult((int)httpWebResponse.StatusCode, httpWebResponse.StatusDescription);
            using (Stream responseStream = httpWebResponse.GetResponseStream())
                Response.Write(new StreamReader(responseStream, Encoding.UTF8).ReadToEnd());
            return new EmptyResult();
        }
 
        #endregion
    }
}
