﻿using MyVitalz.Web.Helpers;
using MyVitalz.Web.Service;
using System;
using System.Text;
using System.Web.Mvc;
using System.Web;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using MyVitalz.Web.Models;
using System.Net.Http;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Http.Headers;

namespace MyVitalz.Web.Controllers
{
    public class FileUploadController : BaseController
    {
        #region Action Methods

        public ActionResult Index()
        {
            return View("_FileUploadDialog");     
        }

        public ActionResult BrowseFile()
        {
            return PartialView("_FileUploadDialog");
        }
        
        [HttpPost]
        public ActionResult FileUpload(string file)       
        {
            //try
            //{
                MemoryStream ms;
                if (Request.Files.Count != 0)
                {
                    try
                    {
                        foreach (string file1 in Request.Files)
                        {
                            var fileContent = Request.Files[file1];
                            if (fileContent != null && fileContent.ContentLength > 0)
                            {
                                // get a stream
                                var stream = fileContent.InputStream;
                                byte[] fileContents = new byte[fileContent.ContentLength];
                                using (ms = new MemoryStream())
                                {
                                    int read = 0;
                                    while ((read = fileContent.InputStream.Read(fileContents, 0, fileContent.ContentLength)) > 0)
                                    {
                                        ms.Write(fileContents, 0, read);
                                    }
                                }


                                //var webRequest = WebRequest.Create(Properties.Settings.Default.FileUploadUrl);
                                //Uri webService = new Uri(@"http://localhost:8080/MyvitalzFileUploadWeb/rest/file/upload");
                                //HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, webService);

                                Uri webService = new Uri(System.Configuration.ConfigurationManager.AppSettings["FileUploadUrl"]);
                                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, webService);


                                MultipartContent multiPartContent = new MultipartContent("mixed");
                                ByteArrayContent byteArrayContent = new ByteArrayContent(ms.ToArray());
                                byteArrayContent.Headers.Add("Content-Type", "multipart/mixed");
                                byteArrayContent.Headers.Add("Content-Disposition", "mixed");
                                byteArrayContent.Headers.ContentDisposition.FileName = fileContent.FileName;
                                //byteArrayContent.Headers.ContentDisposition.FileName = file.FileName;
                                multiPartContent.Add(byteArrayContent);

                                requestMessage.Content = multiPartContent;
                                requestMessage.Content.Headers.ContentType.MediaType = "multipart/mixed";

                                HttpClient httpClient = new HttpClient();
                                try
                                {
                                    Task<HttpResponseMessage> httpRequest = httpClient.SendAsync(requestMessage, HttpCompletionOption.ResponseContentRead, CancellationToken.None);

                                    HttpResponseMessage httpResponse = httpRequest.Result;
                                    HttpStatusCode statusCode = httpResponse.StatusCode;
                                    HttpContent responseContent = httpResponse.Content;

                                    if (responseContent != null)
                                    {
                                        Task<String> stringContentsTask = responseContent.ReadAsStringAsync();
                                        String stringContents = stringContentsTask.Result;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message);
                                }
                            }
                            else
                            {
                                throw new ArgumentException("Please choose correct file.");
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        throw new ArgumentException(e.ToString());                     
                    }

                    return View("Success", (object)"File uploaded successfully.");
                }
                else
                {
                    throw new ArgumentException("Please choose atleast one file.");
                }
            //}
            //catch (Exception)
            //{
            //    Response.StatusCode = (int)HttpStatusCode.BadRequest;
            //    //return View("Upload failed");
            //    return View("_FileUploadDialog.cshtml");
            //}
            //return View("Success", (object)"File uploaded successfully.");
        }

        #endregion
    }
}
