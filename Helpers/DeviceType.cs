﻿namespace MyVitalz.Web.Helpers
{
    public struct DeviceType
    {
        public const string BloodPressure = "BPM";
        public const string Systolic = "SYS";
        public const string Diastolic = "DIA";
        public const string BloodGlucose = "BCM";
        public const string PulseOximeter = "POM";
        public const string PulseRate = "PRM";
        public const string Temperature = "TMM";
        public const string WeighingScale = "WSM";
        public const string Spirometer = "SPR";
        public const string Feelings = "FEL";
        //public const string DailyQaScore = "DQA";
        //public const string ImpakHealthJournal = "IMP";

        public const string MentalIllness = "MIL";
        public const string Diabetes = "DBT";
        //public const string DailyQuestions = "MIL";
        //public const string DailyQuestions = "DBT";

        public const string PauseMonitor = "PMN";
        public const string CareTime = "CRT";
        public const string Readmission = "REA";

        public const string Medication = "MED";

        public const string Covid = "CVD";
    }
}
