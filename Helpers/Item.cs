﻿using System;

namespace MyVitalz.Web.Helpers
{
    [Serializable]
    public class Item : IEquatable<Item>
    {
        #region Properties
        
        public string Id { get; set; }

        public string Value { get; set; }

        public string Data { get; set; } 

        #endregion

        #region Constructor
        
        public Item(string id, string value, string data)
        {
            Value = value;
            Id = id;
            Data = data;
        }

        public Item(string id, string value)
            : this(id, value, null)
        {
        } 

        #endregion

        #region Methods

        public bool Equals(Item item)
        {
            return Id.Equals(item.Id);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
 
        #endregion
    }
}
