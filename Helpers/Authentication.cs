﻿using MyVitalz.Web.Models;
using System;
using System.Web;
using System.Web.Security;

namespace MyVitalz.Web.Helpers
{
    public class Authentication
    {
        #region Local Variables

        private const char DataSeparator = '|';
        private const char RoleSeparator = '~';
        
        #endregion

        #region Methods

        public static string Serailize(User user)
        {
            return string.Format("{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}", '|', user.UserId, user.NickName, user.FullName, user.Email, user.Group, user.Roles == null ? "" : string.Join('~'.ToString(), user.Roles));
        }

        public static User DeSerailize(FormsAuthenticationTicket ticket)
        {
            User user = null;
            string[] strArray = ticket.UserData.Split(DataSeparator);
            if (strArray.Length != 1)
            {
                user = new User(ticket.Name)
                {
                    UserId = strArray[0],
                    NickName = strArray[1],
                    FullName = strArray[2],
                    Email = strArray[3],
                    Group = strArray[4],
                    Roles = strArray[5].Split(RoleSeparator),
                    IsAuthenticated = true
                };
            }
            return user;
        }

        public static void SetAuthCookie(string name, bool rememberMe, User user)
        {
            var authCookie = FormsAuthentication.GetAuthCookie(name, rememberMe);
            var authenticationTicket = FormsAuthentication.Decrypt(authCookie.Value);
            if (authenticationTicket == null)
            {
                throw new Exception("Unable to decrypt the authentication cookie");
            }
            var str = FormsAuthentication.Encrypt(new FormsAuthenticationTicket(authenticationTicket.Version, authenticationTicket.Name, authenticationTicket.IssueDate, authenticationTicket.Expiration, authenticationTicket.IsPersistent, Authentication.Serailize(user), authenticationTicket.CookiePath));
            authCookie.Value = str;
            HttpContext.Current.Response.Cookies.Add(authCookie);
        }

        public static User GetUserFromAuthCookie()
        {
            User user = null;
            if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated &&
                HttpContext.Current.User.Identity is FormsIdentity)
            {
                user = Authentication.DeSerailize(((FormsIdentity)HttpContext.Current.User.Identity).Ticket);
            }
            return user;
        } 
        #endregion
    }
}
