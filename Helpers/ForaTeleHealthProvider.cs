﻿using MyVitalz.Web.Helpers.Repository;
using MyVitalz.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyVitalz.Web.Helpers
{
    public class ForaTeleHealthProvider
    {
        public ForaTeleHealthRepository foraRepo;


        public ForaTeleHealthProvider()
        {
            foraRepo = new ForaTeleHealthRepository();
        }

        public string AddPatientToForaTeleHealth(EHRDetailsModel model)
        {
            string rsp = foraRepo.AddPatientToForaTeleHealth(model);
            return rsp;
        }

        public string RemoveKitFromTeleHealthPatient(string kitId, string partyId)
        {
            string rsp = foraRepo.DeletePatientFromForaTeleHealth(kitId, partyId);
            return rsp;
        }
        public string DeleteForaKite(string kiteId)
        {
            string rsp = foraRepo.DeleteForaKite(kiteId);
            return rsp;
        }
    }
}