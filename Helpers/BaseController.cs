﻿using MyVitalz.Web.Helpers;
using MyVitalz.Web.Helpers.ActionFilters;
using MyVitalz.Web.Models;
using MyVitalz.Web.Service;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using OfficeOpenXml;
using MyVitalz.Web.Helpers.Repository;
using System.Collections.Generic;
namespace MyVitalz.Web.Controllers
{
    [ExceptionHandler]
    public class BaseController : Controller
    {
        #region Local Variables

        private User _user;
        private IServiceRepository _service;
        private IMyvitalzRestApiRepository _myVitalzRestApi;

        #endregion

        #region Properties

        public User CurrentUser
        {
            get
            {
                if (_user == null && User != null && User is User)
                    _user = (User)User;
                return _user;
            }
        }

        public IServiceRepository Service
        {
            get
            {
                if (_service == null)
                    _service = Helper.Service;
                return _service;
            }
        }

        public IMyvitalzRestApiRepository MyVitalzRestApi
        {
            get
            {
                if( _myVitalzRestApi == null )
                {
                    _myVitalzRestApi = new MyvitalzRestApiRepository();
                }
                return _myVitalzRestApi;
            }
        }

        public ActionResult EmptyResult
        {
            get { return new EmptyResult(); }
        }

        #endregion

        #region Support Methods

        protected static string CleanInput(string fileName)
        {
            return Regex.Replace(fileName, "[^\\w]", "");
        }

        protected static void ValidatePatient(Patient patient, StringBuilder stringBuilder)
        {
            if (string.IsNullOrWhiteSpace(patient.firstName))
            {
                stringBuilder.AppendLine("FirstName is required.");
            }
            if (string.IsNullOrWhiteSpace(patient.lastName))
            {
                stringBuilder.AppendLine("LastName is required.");
            }
            if (string.IsNullOrWhiteSpace(patient.dob))
            {
                stringBuilder.AppendLine("Date Of Birth is required.");
            }
            if (string.IsNullOrWhiteSpace(patient.gender)
                    || patient.gender == "0")
            {
                stringBuilder.AppendLine("Gender is required.");
            }
            if (string.IsNullOrWhiteSpace(patient.RACE)
                    || patient.RACE == "0")
            {
                stringBuilder.AppendLine("Race is required.");
            }
            if (string.IsNullOrWhiteSpace(patient.address.addressLine1))
            {
                stringBuilder.AppendLine("AddressLine1 is required.");
            }
            if (string.IsNullOrWhiteSpace(patient.address.city))
            {
                stringBuilder.AppendLine("City is required.");
            }
            if (string.IsNullOrWhiteSpace(patient.address.state))
            {
                stringBuilder.AppendLine("State is required.");
            }
            //if (string.IsNullOrWhiteSpace(patient.group)
            //        || patient.group == "0")
            //{
            //    stringBuilder.AppendLine("Group is required.");
            //}
            if ((patient.partyType == Role.GroupAdmin
                    || patient.partyType == Role.Patient)
                    && patient.group == Constant.AllGroup)
            {
                stringBuilder.AppendLine( patient.partyType.ToRoleName() + " cannot be in the ALL group.");
            }
            else if (patient.partyType != Role.GroupAdmin
                        && patient.partyType != Role.Patient
                        && patient.group != Constant.AllGroup)
            {
                stringBuilder.AppendLine(patient.partyType.ToRoleName() + " should be in the ALL group.");
            }
        }

        protected static void ValidateOtherUsers(Patient patient, StringBuilder stringBuilder)
        {
            if (string.IsNullOrWhiteSpace(patient.firstName))
            {
                stringBuilder.AppendLine("FirstName is required.");
            }
            if (string.IsNullOrWhiteSpace(patient.lastName))
            {
                stringBuilder.AppendLine("LastName is required.");
            }
            if (string.IsNullOrWhiteSpace(patient.dob))
            {
                stringBuilder.AppendLine("Date Of Birth is required.");
            }
            //if (string.IsNullOrWhiteSpace(patient.gender)
            //        || patient.gender == "0")
            //{
            //    stringBuilder.AppendLine("Gender is required.");
            //}
            if (string.IsNullOrWhiteSpace(patient.address.addressLine1))
            {
                stringBuilder.AppendLine("AddressLine1 is required.");
            }
            if (string.IsNullOrWhiteSpace(patient.address.city))
            {
                stringBuilder.AppendLine("City is required.");
            }
            if (string.IsNullOrWhiteSpace(patient.address.state))
            {
                stringBuilder.AppendLine("State is required.");
            }
            if (string.IsNullOrWhiteSpace(patient.group)
                    || patient.group == "0")
            {
                stringBuilder.AppendLine("Group is required.");
            }
            if ((patient.partyType == Role.GroupAdmin
                    || patient.partyType == Role.Patient)
                    && patient.group == Constant.AllGroup)
            {
                stringBuilder.AppendLine(patient.partyType.ToRoleName() + " cannot be in the ALL group.");
            }
            // 15-08-2020 When create a doctor, nurse, caregiver by Super Admin, their
            // home group is set to ALL, we need ability to set the home group

            //else if (patient.partyType != Role.GroupAdmin
            //            && patient.partyType != Role.Patient
            //            && patient.group != Constant.AllGroup)
            //{
            //    stringBuilder.AppendLine(patient.partyType.ToRoleName() + " should be in the ALL group.");
            //}
        }

        #endregion

        #region Cookie Methods

        public void CreateCookie(string name, string value, int exiryInMinutes)
        {
            HttpContext.Response.Cookies.Add(new HttpCookie(name)
            {
                Value = value,
                Expires = DateTime.Now.AddMinutes(exiryInMinutes)
            });
        }

        public string GetCookie(string name)
        {
            string str = null;
            HttpCookie httpCookie = HttpContext.Request.Cookies[name];
            if (httpCookie != null)
                str = httpCookie.Value;
            return str;
        }

        public void DeleteCookie(string name)
        {
            HttpContext.Response.Cookies.Add(new HttpCookie(name, string.Empty)
            {
                Expires = DateTime.Now.AddYears(-1)
            });
        }
 
        #endregion

        #region Role Methods

        public bool IsInRole(string role)
        {
            return User.IsInRole(role);
        }

        public ActionResult RedirectToPartyHome(User user)
        {
            if (user != null && user.IsAuthenticated)
            {
                if (user.IsInRole(Role.Admin))
                    return RedirectToAction("Index", "Admin");
                if (user.IsInRole(Role.GroupAdmin))
                    return RedirectToAction("GAPatients", "Admin");                
                if (user.IsInRole(Role.Doctor))
                    return RedirectToAction("Index", "Doctor");
                if (user.IsInRole(Role.Caregiver))
                    return RedirectToAction("Index", "Caregiver");
                if (user.IsInRole(Role.Patient))
                    return RedirectToAction("Index", "Patient");
                if (user.IsInRole(Role.Clinician))
                    return RedirectToAction("Index", "Nurse");
            }
            return RedirectToAction("Logon", "Account");
        }
 
        #endregion

        #region AutoComplete Methods

        public JsonResult GetPartiesBySearchType(string partyType, string wildCardType, string wildCard)
        {
            var parties = Service.GetParties("ALL", partyType, wildCardType, wildCard);
            return Json(new { Parties = parties }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPartiesBySearchFor(SearchModel model)
        {
            var partyType = string.Empty;
            var wildCardType = string.Empty;
            if (string.IsNullOrWhiteSpace(model.PartyName) == true)
            {
                throw new ArgumentException("Search Text can't be empty.");
            }

            if (model.SearchFor.ToString() == "AllKits")
            {
                //GetKitsBySearchFor(SearchModel model);
            }

            switch (model.PatientCriteria)
            {
                case SearchPatientCriteria.FirstName:
                    wildCardType = WildCardType.FirstName;
                    break;
                case SearchPatientCriteria.LastName:
                    wildCardType = WildCardType.LastName;
                    break;
                case SearchPatientCriteria.Email:
                    wildCardType = WildCardType.Email;
                    break;
                default:
                    throw  new ArgumentException("Invalid SearchBy");
            }
            switch (model.SearchFor)
            {
                case SearchFor.PatientsByDoctor:
                    partyType = Role.Doctor;
                    break;
                case SearchFor.PatientsByCaregiver:
                    partyType = Role.Caregiver;
                    break;
                case SearchFor.PatientsByClinician:
                    partyType = Role.Clinician;
                    break;
                default:
                    throw new ArgumentException("Invalid SearchFor");
            }
            //var parties = Service.GetParties(CurrentUser.Group, partyType, wildCardType, model.PartyName);
            var parties = Service.GetParties("ALL", partyType, wildCardType, model.PartyName);
            return Json(new { Parties = parties }, JsonRequestBehavior.AllowGet);
        }

        //public JsonResult GetKitsBySearchFor(SearchModel model)
        //{
        //    var partyType = string.Empty;
        //    var wildCardType = string.Empty;
        //    if (string.IsNullOrWhiteSpace(model.PartyName) == true)
        //    {
        //        throw new ArgumentException("Search Text can't be empty.");
        //    }
        //    switch (model.Criteria)
        //    {
        //        case SearchCriteria.KitId:
        //            wildCardType = WildCardType.KitId;
        //            break;
        //        default:
        //            throw new ArgumentException("Invalid SearchBy");
        //    }
        //    var deviceKits = Service.SearchKits(model.PartyName);
        //    return Json(new { DeviceKit = deviceKits }, JsonRequestBehavior.AllowGet);
        //}

        [ValidateInput(false)]
        public JsonResult GetAssociatedParties(string patientId, string wildCard)
        {
            var parties = Service.GetAssociatedParties(patientId);
            //Filter the result if there is a wild card
            if (string.IsNullOrWhiteSpace(wildCard) == false)
            {
                parties = parties.Where(p => p.GetDisplayEmail().IndexOf(wildCard, StringComparison.CurrentCultureIgnoreCase) >= 0).ToArray();
            }

            return Json(new { Parties = parties }, JsonRequestBehavior.AllowGet);
        }
 
        #endregion

        #region CSV / Excel Methods

        public ActionResult ExportDatatableToExcel(string fileName, DataTable table)
        {
            if (string.IsNullOrWhiteSpace(fileName))
            {
                throw new ArgumentException("Filename can't be empty");
            }

            fileName = CleanInput(fileName);
            var gridView = new GridView();
            gridView.DataSource = table;
            gridView.DataBind();

            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add("Readings");
            var totalCols = gridView.Rows[0].Cells.Count;
            var totalRows = gridView.Rows.Count;
            var headerRow = gridView.HeaderRow;

            for (var col = 0; col < totalCols; col++)
            {
                workSheet.Cells[1, col + 1].Value = headerRow.Cells[col].Text;
                if (totalCols > 4)
                {
                    workSheet.Column(totalCols).Hidden = true;
                    workSheet.Column(totalCols - 1).Hidden = true;
                }
                else
                {
                   // workSheet.Column(totalCols).Hidden = true;
                }
            }

            for (var row = 1; row <= totalRows; row++)
            {
                for (var col = 0; col < totalCols; col++)
                {
                    workSheet.Cells[row + 1, col + 1].Value = gridView.Rows[row - 1].Cells[col].Text;
                }
            }

            #region HIGHLIGHTING ABNORMAL READING

            for (int i = 0; i < table.Rows.Count; i++)
            {
                if(table.Columns[0].ToString() == "Systolic")
                {
                    if (table.Rows[i].ItemArray[table.Rows[i].ItemArray.Count() - 2].Equals(true))
                    {
                        workSheet.Cells[i + 2, 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        workSheet.Cells[i+2, 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
                        workSheet.Cells[i+2, 1].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                    }
                    if (table.Rows[i].ItemArray[table.Rows[i].ItemArray.Count() - 1].Equals(true))
                    {
                        workSheet.Cells[i + 2, 2].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        workSheet.Cells[i+2, 2].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
                        workSheet.Cells[i + 2, 2].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                    }
                }
                if (table.Columns[0].ToString() == "Pulse Rate")
                {
                    if (table.Rows[i].ItemArray[table.Rows[i].ItemArray.Count() - 1].Equals(true))
                    {
                        workSheet.Cells[i + 2, 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        workSheet.Cells[i + 2, 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
                        workSheet.Cells[i + 2, 1].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                    }
                }
                if (table.Columns[0].ToString() == "Pulse")
                {
                    if (table.Rows[i].ItemArray[table.Rows[i].ItemArray.Count() - 1].Equals(true))
                    {
                        workSheet.Cells[i + 2, 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        workSheet.Cells[i + 2, 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
                        workSheet.Cells[i + 2, 1].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                    }
                }
                if (table.Columns[0].ToString() == "Temperature")
                {
                    if (table.Rows[i].ItemArray[table.Rows[i].ItemArray.Count() - 1].Equals(true))
                    {
                        workSheet.Cells[i + 2, 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        workSheet.Cells[i + 2, 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
                        workSheet.Cells[i + 2, 1].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                    }
                }
                if (table.Columns[0].ToString() == "Weight")
                {
                    if (table.Rows[i].ItemArray[table.Rows[i].ItemArray.Count() - 2].Equals(true))
                    {
                        workSheet.Cells[i + 2, 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        workSheet.Cells[i + 2, 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
                        workSheet.Cells[i + 2, 1].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                    }

                    if (table.Rows[i].ItemArray[table.Rows[i].ItemArray.Count() - 1].Equals(true))
                    {
                        if (Convert.ToDouble(table.Rows[i].ItemArray[1]) != 0.0)
                        {
                            workSheet.Cells[i + 2, 2].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            workSheet.Cells[i + 2, 2].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
                            workSheet.Cells[i + 2, 2].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                        }
                        if (Convert.ToDouble(table.Rows[i].ItemArray[2]) != 0.0)
                        {
                            workSheet.Cells[i + 2, 3].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            workSheet.Cells[i + 2, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
                            workSheet.Cells[i + 2, 3].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                        }
                    }
                }
                if (table.Columns[0].ToString() == "Glucose")
                {
                    if (table.Rows[i].ItemArray[table.Rows[i].ItemArray.Count() - 1].Equals(true))
                    {
                        workSheet.Cells[i + 2, 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        workSheet.Cells[i + 2, 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
                        workSheet.Cells[i + 2, 1].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                    }
                }

                if (table.Columns[0].ToString() == "FevBest")
                {
                    if (table.Rows[i].ItemArray[table.Rows[i].ItemArray.Count() - 2].Equals(true))
                    {
                        gridView.Rows[i].Cells[0].BackColor = System.Drawing.Color.Yellow;
                        gridView.Rows[i].Cells[0].ForeColor = System.Drawing.Color.Red;
                        workSheet.Cells[i + 2, 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        workSheet.Cells[i + 2, 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
                        workSheet.Cells[i + 2, 1].Style.Font.Color.SetColor(System.Drawing.Color.Red);

                        gridView.Rows[i].Cells[2].BackColor = System.Drawing.Color.Yellow;
                        gridView.Rows[i].Cells[2].ForeColor = System.Drawing.Color.Red;
                        workSheet.Cells[i + 2, 3].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        workSheet.Cells[i + 2, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
                        workSheet.Cells[i + 2, 3].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                    }
                    if (table.Rows[i].ItemArray[table.Rows[i].ItemArray.Count() - 1].Equals(true))
                    {
                        gridView.Rows[i].Cells[1].BackColor = System.Drawing.Color.Yellow;
                        gridView.Rows[i].Cells[1].ForeColor = System.Drawing.Color.Red;
                        workSheet.Cells[i + 2, 2].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        workSheet.Cells[i + 2, 2].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
                        workSheet.Cells[i + 2, 2].Style.Font.Color.SetColor(System.Drawing.Color.Red);

                        gridView.Rows[i].Cells[3].BackColor = System.Drawing.Color.Yellow;
                        gridView.Rows[i].Cells[3].ForeColor = System.Drawing.Color.Red;
                        workSheet.Cells[i + 2, 4].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        workSheet.Cells[i + 2, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
                        workSheet.Cells[i + 2, 4].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                    }
                }
                if (table.Columns[0].ToString() == "Score")
                {
                    if (table.Rows[i].ItemArray[table.Rows[i].ItemArray.Count() - 1].Equals(true))
                    {
                        workSheet.Cells[i + 2, 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        workSheet.Cells[i + 2, 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
                        workSheet.Cells[i + 2, 1].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                    }
                }
            }
            #endregion

            using (var memoryStream = new MemoryStream())
            {
                Response.ClearContent();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment; filename="+fileName +".xlsx");
                excel.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
            return new EmptyResult();
        }

        public ActionResult ExportDatatableToCsv(string fileName, DataTable table)
        {
            if (string.IsNullOrWhiteSpace(fileName))
            {
                throw new ArgumentException("Filename can't be empty");
            }
            fileName = CleanInput(fileName);
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=\"" + fileName + ".csv\"");
            Response.ContentType = "application/text";
            Response.Charset = string.Empty;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Output.Write(BaseController.ToCsv(table));
            Response.Flush();
            Response.End();
            return new EmptyResult();
        }

        public static string ToCsv(DataTable table)
        {
            var stringBuilder = new StringBuilder();
            for (int index = 0; index < table.Columns.Count; ++index)
            {
                stringBuilder.Append(table.Columns[index].ColumnName);
                stringBuilder.Append(index == table.Columns.Count - 1 ? "\n" : ",");
            }
            foreach (DataRow dataRow in table.Rows)
            {
                for (var index = 0; index < table.Columns.Count; ++index)
                {
                    stringBuilder.Append(dataRow[index].ToString());
                    stringBuilder.Append(index == table.Columns.Count - 1 ? "\n" : ",");
                }
            }
            return stringBuilder.ToString();
        } 
        #endregion

        #region Groups & Associations

        protected GroupType[] GetGroupTypes()
        {
            return GetFilteredGroupTypes(false);
        }

        
        protected List<MainGroupTypes> GetMainGroupTypes(string subgroup)
        {
            return new DataServices().GetMainGroupTypes(subgroup);
        }

        protected SubGroupWrapper[] GetAllSubGroupTypes()
        {
            SubGroupWrapper[] allsubgroups = null;
            allsubgroups = Service.GetAllSubGroupTypes();
            //if (allsubgroups == null
            //    || allsubgroups.Length == 0)
            //{
            //    throw new ArgumentException("There is no group associated with you. Please contact Administrator");
            //}
            return allsubgroups;
        }

        protected GroupType[] GetFilteredGroupTypes(bool canFilter)
        {
            GroupType[] groups = null;
            groups = Service.GetGroups();
            if (canFilter == true
                    && CurrentUser.IsInRole(Role.GroupAdmin) == true)
            {
                groups = groups.Where(g => g.groupType == CurrentUser.Group
                                            || g.groupType == Constant.AllGroup).ToArray();
            }
            if (groups == null
                || groups.Length == 0)
            {
                throw new ArgumentException("There is no group associated with you. Please contact Administrator");
            }
            return groups;
        }

        protected XrefType[] GetAssociationTypes()
        {
            XrefType[] groups = null;
            groups = Service.GetAssociationTypes();
            if (groups == null
                  || groups.Length == 0)
            {
                throw new ArgumentException("There is no associations with you. Please contact Administrator");
            }
            return groups;
        }

        protected DiseaseType[] GetDiseases()
        {
            DiseaseType[] diseases = null;
            diseases = Service.GetDiseases();
            if (diseases == null)
            {
                throw new ArgumentException("DiseaseType is not listed. Please contact Administrator");
            }
            return diseases;
        }
 
        #endregion

        #region Success Page

        public ActionResult Success(string message)
        {
            return View("Success", (object)message);
        }
 
        #endregion
    }
}
