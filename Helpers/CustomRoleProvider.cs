﻿// Type: MyVitalz.Web.Helpers.CustomRoleProvider
// Assembly: MyVitalz.Web, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5DFB40F2-FD4B-4A92-9CB7-01EA3C877ECE
// Assembly location: D:\Data\Haraku\MyVitalz\bin\MyVitalz.Web.dll

using System;
using System.Collections.Specialized;
using System.Web.Hosting;
using System.Web.Security;

namespace MyVitalz.Web.Helpers
{
    public class CustomRoleProvider : RoleProvider
    {
        #region Local Variables

        private string _applicationName;

        #endregion

        #region Overridden Properties

        public override string ApplicationName
        {
            get
            {
                return _applicationName;
            }
            set
            {
                _applicationName = value;
            }
        }

        #endregion

        #region Support Methods

        private string GetConfigValue(string configValue, string defaultValue)
        {
            if (string.IsNullOrEmpty(configValue))
            {
                return defaultValue;
            }
            else
            {
                return configValue;
            }
        }

        #endregion

        #region Overridden Methods

        public override void Initialize(string name, NameValueCollection config)
        {
            base.Initialize(name, config);
            _applicationName = GetConfigValue(config["applicationName"], HostingEnvironment.ApplicationVirtualPath);
        }

        #region Non Implemented Methods

        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string username)
        {
            return new string[1] { Role.Doctor };
        }
        #endregion

        #endregion
    }
}
