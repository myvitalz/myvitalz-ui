﻿using MyVitalz.Web.BgService;
using MyVitalz.Web.Helpers;
using MyVitalz.Web.Models;
using MyVitalz.Web.Properties;
using MyVitalz.Web.Service;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.WebPages;
using System.Xml;
using System.Xml.Xsl;

using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Net.Http;
using Newtonsoft.Json;
using System.Runtime.Serialization.Json;
using System.Web.Security;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;

namespace MyVitalz.Web.Controllers
{
    public class BasePartyController : BaseController
    {
        #region Local Variables

        private Regex _emailRegEx;

        #endregion

        #region Properties

        public const string Hyphen = "-";

        public Regex EmailRegEx
        {
            get
            {
                if (_emailRegEx == null)
                    _emailRegEx = new Regex("^(([-\\w \\.]+)|(\\\"[-\\w \\.]+\\\") )?\\<?([\\w\\-\\.]+)@((\\[([0-9]{1,3}\\.){3}[0-9]{1,3}\\])|(([\\w\\-]+\\.)+)([a-zA-Z]{2,4}))\\>?$");
                return _emailRegEx;
            }
        }

        #endregion

        #region Support Methods

        private DataTable GetDatatable(string deviceType, string patientId, DateTime startDate, DateTime endDate)
        {
            var dataTable = new DataTable();
            switch (deviceType)
            {
                case DeviceType.WeighingScale:
                    var weighingScaleReadings = Service.GetWeighingScaleReadings(patientId, startDate, endDate);
                    if (weighingScaleReadings == null || weighingScaleReadings.readings == null)
                    {
                        throw new ArgumentException("Sorry! There are no readings for " + deviceType.ToDeviceType());
                    }
                    else
                    {
                        dataTable.Columns.Add("Weight", typeof(double));
                        dataTable.Columns.Add("Weight Difference", typeof(double));
                        dataTable.Columns.Add("Weekly Difference", typeof(double));
                        dataTable.Columns.Add("Time", typeof(DateTime));
                        dataTable.Columns.Add("AboveThreshold", typeof(bool));
                        dataTable.Columns.Add("ShowWeightDifference", typeof(bool));
                        weighingScaleReadings.readings = weighingScaleReadings.readings.OrderByDescending(r => DateTime.Parse(r.measurementTime)).ToArray();
                        foreach (var weighingScale in weighingScaleReadings.readings)
                        {
                            dataTable.Rows.Add(
                                weighingScale.weightReadingPound,
                                weighingScale.weightDifference,
                                weighingScale.weeklyWeightDifference,
                                weighingScale.measurementTime,
                                weighingScale.aboveThreshold,
                                weighingScale.showWeightDifference
                                );
                        }
                    }
                    break;
                case DeviceType.BloodPressure:
                    var bloodPressureReadings = Service.GetBloodPressureReadings(patientId, startDate, endDate);
                    if (bloodPressureReadings == null || bloodPressureReadings.readings == null)
                    {
                        throw new ArgumentException("Sorry! There are no readings for " + deviceType.ToDeviceType());
                    }
                    else
                    {
                        dataTable.Columns.Add("Systolic", typeof(int));
                        dataTable.Columns.Add("Diastolic", typeof(int));
                        dataTable.Columns.Add("Time", typeof(DateTime));
                        dataTable.Columns.Add("AboveSysThreshold", typeof(bool));
                        dataTable.Columns.Add("AboveDiaThreshold", typeof(bool));
                        foreach (var bloodPressure in bloodPressureReadings.readings.OrderByDescending(r => DateTime.Parse(r.measurementTime)).ToArray())
                        {
                            dataTable.Rows.Add(
                                bloodPressure.systolic,
                                bloodPressure.diastolic,
                                bloodPressure.measurementTime,
                                bloodPressure.systolicAboveThreshold,
                                bloodPressure.diastolicAboveThreshold
                                );
                        }
                    }
                    break;
                case DeviceType.PulseRate:
                    var pressureReadings = Service.GetBloodPressureReadings(patientId, startDate, endDate);
                    if (pressureReadings == null || pressureReadings.readings == null)
                    {
                        throw new ArgumentException("Sorry! There are no readings for " + deviceType.ToDeviceType());
                    }
                    else
                    {
                        dataTable.Columns.Add("Pulse Rate", typeof(int));
                        dataTable.Columns.Add("Time", typeof(DateTime));
                        dataTable.Columns.Add("AboveThreshold", typeof(bool));
                        foreach (var bloodPressure in pressureReadings.readings.OrderByDescending(r => DateTime.Parse(r.measurementTime)).ToArray())
                        {
                            dataTable.Rows.Add(
                                bloodPressure.pulseRate,
                                bloodPressure.measurementTime,
                                bloodPressure.pulseRateAboveThreshold);
                        }
                    }
                    break;
                case DeviceType.Temperature:
                    var temperatureReadings = Service.GetTemperatureReadings(patientId, startDate, endDate);
                    if (temperatureReadings == null || temperatureReadings.readings == null)
                    {
                        throw new ArgumentException("Sorry! There are no readings for " + deviceType.ToDeviceType());
                    }
                    else
                    {
                        dataTable.Columns.Add("Temperature", typeof(double));
                        dataTable.Columns.Add("Time", typeof(DateTime));
                        dataTable.Columns.Add("AboveThreshold", typeof(bool));
                        foreach (var temperature in temperatureReadings.readings.OrderByDescending(r => DateTime.Parse(r.measurementTime)).ToArray())
                        {
                            dataTable.Rows.Add(
                                temperature.temperatureFahrenheit,
                                temperature.measurementTime,
                                temperature.aboveThreshold);
                        }
                    }
                    break;
                case DeviceType.Spirometer:
                    var spirometerReadings = Service.GetSpirometerReadings(patientId, startDate, endDate);
                    if (spirometerReadings == null || spirometerReadings.readings == null)
                    {
                        throw new ArgumentException("Sorry! There are no readings for " + deviceType.ToDeviceType());
                    }
                    else
                    {
                        dataTable.Columns.Add("FevBest", typeof(double));
                        dataTable.Columns.Add("PefBest", typeof(double));
                        dataTable.Columns.Add("Fev Reading Difference", typeof(double));
                        dataTable.Columns.Add("Pef Reading Difference", typeof(double));
                        dataTable.Columns.Add("Time", typeof(DateTime));
                        dataTable.Columns.Add("FevAboveThreshold", typeof(bool));
                        dataTable.Columns.Add("PefAboveThreshold", typeof(bool));
                        foreach (var spirometer in spirometerReadings.readings.OrderByDescending(r => DateTime.Parse(r.measurementTime)).ToArray())
                        {
                            dataTable.Rows.Add(
                                spirometer.fev1Best,
                                spirometer.pefBest,
                                spirometer.fevReadingDiff,
                                spirometer.pefReadingDiff,
                                spirometer.measurementTime,
                                spirometer.fevAboveThreshold,
                                spirometer.pefAboveThreshold);
                        }
                    }
                    break;
                case DeviceType.PulseOximeter:
                    var oximeterReadings = Service.GetPulseOximeterReadings(patientId, startDate, endDate);
                    if (oximeterReadings == null || oximeterReadings.readings == null)
                    {
                        throw new ArgumentException("Sorry! There are no readings for " + deviceType.ToDeviceType());
                    }
                    else
                    {
                        dataTable.Columns.Add("Oxygen", typeof(int));
                        dataTable.Columns.Add("Time", typeof(DateTime));
                        dataTable.Columns.Add("AboveThreshold", typeof(bool));
                        foreach (var pulseOxiMeter in oximeterReadings.readings.OrderByDescending(r => DateTime.Parse(r.measurementTime)).ToArray())
                        {
                            dataTable.Rows.Add(
                                pulseOxiMeter.oxygenReading,
                                pulseOxiMeter.measurementTime,
                                pulseOxiMeter.aboveThreshold);
                        }
                    }
                    break;
                case DeviceType.BloodGlucose:
                    var bloodGlucoseReadings = Service.GetBloodGlucoseReadings(patientId, startDate, endDate);
                    if (bloodGlucoseReadings == null || bloodGlucoseReadings.readings == null)
                    {
                        throw new ArgumentException("Sorry! There are no readings for " + deviceType.ToDeviceType());
                    }
                    else
                    {
                        dataTable.Columns.Add("Glucose", typeof(double));
                        dataTable.Columns.Add("Period", typeof(string));
                        dataTable.Columns.Add("Time", typeof(DateTime));
                        dataTable.Columns.Add("AboveThreshold", typeof(bool));
                        foreach (var bloodGlucose in bloodGlucoseReadings.readings.OrderByDescending(r => DateTime.Parse(r.measurementTime)).ToArray())
                        {
                            dataTable.Rows.Add(
                                bloodGlucose.glucoseReading,
                                bloodGlucose.period,
                                bloodGlucose.measurementTime,
                                bloodGlucose.aboveThreshold);
                        }
                    }
                    break;
                case DeviceType.Feelings:
                    var feelingReadings = Service.GetFeelingReadings(patientId, startDate, endDate);
                    if (feelingReadings == null || feelingReadings.reading == null)
                    {
                        throw new ArgumentException("Sorry! There are no readings for " + deviceType.ToDeviceType());
                    }
                    else
                    {
                        dataTable.Columns.Add("Pain", typeof(string));
                        dataTable.Columns.Add("Mood", typeof(string));
                        dataTable.Columns.Add("Energy", typeof(string));
                        dataTable.Columns.Add("Breathing", typeof(string));
                        dataTable.Columns.Add("Recorded By", typeof(string));
                        dataTable.Columns.Add("Time", typeof(DateTime));
                        dataTable.Columns.Add("AboveThreshold", typeof(bool));
                        foreach (var feeling in feelingReadings.reading.OrderByDescending(r => DateTime.Parse(r.measurementTime)).ToArray())
                        {
                            dataTable.Rows.Add(feeling.painReading + " " + feeling.painDesc,
                                                    feeling.moodReading + " " + feeling.moodDesc,
                                                    feeling.energyReading + " " + feeling.energyDesc,
                                                    feeling.breathingReading + " " + feeling.breathingDesc,
                                                    feeling.userType + ":  " + feeling.userName,
                                                    feeling.measurementTime,
                                                    false);
                        }
                    }
                    break;
                case DeviceType.MentalIllness:
                    var MIDailyQaScoresReadings = Service.GetMIDailyQaScoreReadings(patientId, startDate, endDate);
                    if (MIDailyQaScoresReadings == null)
                    {
                        throw new ArgumentException("Sorry! There are no readings for " + deviceType.ToDeviceType());
                    }
                    else
                    {
                        dataTable.Columns.Add("Score", typeof(string));
                        dataTable.Columns.Add("Time", typeof(DateTime));
                        dataTable.Columns.Add("AboveThreshold", typeof(bool));
                        foreach (var MIDailyQaScores in MIDailyQaScoresReadings.OrderByDescending(r => DateTime.Parse(r.measurementTime)).ToArray())
                        {
                            dataTable.Rows.Add(
                                MIDailyQaScores.dailyQaScore,
                                MIDailyQaScores.measurementTime,
                                MIDailyQaScores.aboveThreshold);
                        }
                    }
                    break;
                case DeviceType.Diabetes:
                    var DiabetesDailyQaScoresReadings = Service.GetDiabetesDailyQaScoreReadings(patientId, startDate, endDate);
                    if (DiabetesDailyQaScoresReadings == null)
                    {
                        throw new ArgumentException("Sorry! There are no readings for " + deviceType.ToDeviceType());
                    }
                    else
                    {
                        dataTable.Columns.Add("Score", typeof(string));
                        dataTable.Columns.Add("Time", typeof(DateTime));
                        dataTable.Columns.Add("AboveThreshold", typeof(bool));
                        foreach (var DiabetesDailyQaScores in DiabetesDailyQaScoresReadings.OrderByDescending(r => DateTime.Parse(r.measurementTime)).ToArray())
                        {
                            dataTable.Rows.Add(
                                DiabetesDailyQaScores.dailyQaScore,
                                DiabetesDailyQaScores.measurementTime,
                                DiabetesDailyQaScores.aboveThreshold);
                        }
                    }
                    break;


                case DeviceType.CareTime:
                    string response = ReadingGridDetails(patientId, deviceType, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
                    if (response == null || response.IsEmpty() == true || response.Length == 0)
                    {
                        throw new ArgumentException("Sorry! There are no readings for " + deviceType.ToDeviceType());
                    }
                    else
                    {                       

                        CareTimeCollection collection;
                        using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(response)))
                        {
                            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(CareTimeCollection));
                            collection = (CareTimeCollection)serializer.ReadObject(ms);
                        }
                        dataTable.Columns.Add("Care Date", typeof(string));
                        dataTable.Columns.Add("Begin Time", typeof(string));
                        dataTable.Columns.Add("Duration", typeof(string));
                        dataTable.Columns.Add("Interventions", typeof(string));
                        foreach (var LogCare in collection.CareTime)
                        {
                            dataTable.Rows.Add(
                                LogCare.CareTimeDate,
                                LogCare.BeginTime,
                                LogCare.Duration,
                                LogCare.InterventionType);
                        }
                    }
                    break;

                case DeviceType.PauseMonitor:
                    string responsePauseMonitor = ReadingGridDetails(patientId, deviceType, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
                    if (responsePauseMonitor == null || responsePauseMonitor.IsEmpty() == true || responsePauseMonitor.Length == 0)
                    {
                        throw new ArgumentException("Sorry! There are no readings for " + deviceType.ToDeviceType());
                    }
                    else
                    {

                        PauseMonitorCollection collection;
                        using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(responsePauseMonitor)))
                        {
                            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(PauseMonitorCollection));
                            collection = (PauseMonitorCollection)serializer.ReadObject(ms);
                        }
                        dataTable.Columns.Add("Start Date", typeof(string));
                        dataTable.Columns.Add("End Date", typeof(string));
                        dataTable.Columns.Add("Reason", typeof(string));                       
                        foreach (var PauseMonitor in collection.PauseMonitor)
                        {
                            dataTable.Rows.Add(
                                PauseMonitor.StartDate,
                                PauseMonitor.EndDate,
                                PauseMonitor.Reason
                               );
                        }
                    }
                    break;
                case DeviceType.Readmission:
                    string responseReadmission = ReadingGridDetails(patientId, deviceType, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
                    if (responseReadmission == null || responseReadmission.IsEmpty() == true || responseReadmission.Length == 0)
                    {
                        throw new ArgumentException("Sorry! There are no readings for " + deviceType.ToDeviceType());
                    }
                    else
                    {

                        ReadmissionModelCollection collection;
                        using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(responseReadmission)))
                        {
                            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(ReadmissionModelCollection));
                            collection = (ReadmissionModelCollection)serializer.ReadObject(ms);
                        }
                        dataTable.Columns.Add("Readmission Date", typeof(string));
                        
                        foreach (var Readmission in collection.Readmission)
                        {
                            dataTable.Rows.Add(
                                Readmission.ReadmissionDate

                               );
                        }
                    }
                    break;

                    
                case DeviceType.Medication:
                    string responseMedication = ReadingGridDetails(patientId, deviceType, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
                    if (responseMedication == null || responseMedication.IsEmpty() == true || responseMedication.Length == 0)
                    {
                        throw new ArgumentException("Sorry! There are no readings for " + deviceType.ToDeviceType());
                    }
                    else
                    {

                        MedicationModelCollection collection;
                        using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(responseMedication)))
                        {
                            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(MedicationModelCollection));
                            collection = (MedicationModelCollection)serializer.ReadObject(ms);
                        }
                        dataTable.Columns.Add("Medication", typeof(string));
                        dataTable.Columns.Add("Date/Time", typeof(string));
                        dataTable.Columns.Add("DeviceId", typeof(string));
                        foreach (var medication in collection.Medication)
                        {
                            dataTable.Rows.Add(
                                medication.MedicationName,
                                medication.MeasurementTime,
                                medication.DeviceId
                               );
                        }
                    }
                    break;
                    
                 case DeviceType.Covid:
                    string responseCovid = ReadingGridDetails(patientId, deviceType, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
                    if (responseCovid == null || responseCovid.IsEmpty() == true || responseCovid.Length == 0)
                    {
                        throw new ArgumentException("Sorry! There are no readings for " + deviceType.ToDeviceType());
                    }
                    else
                    {

                        CovidModelCollection collection;
                        using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(responseCovid)))
                        {
                            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(CovidModelCollection));
                            collection = (CovidModelCollection)serializer.ReadObject(ms);
                        }
                        dataTable.Columns.Add("Score", typeof(string));
                        dataTable.Columns.Add("Date/Time", typeof(string));                      
                        foreach (var covid in collection.CovidReading)
                        {
                            dataTable.Rows.Add(
                                covid.DiseaseScore,
                                covid.DailyQaTakenTime                             
                               );
                        }
                    }
                    break;

            }
            return dataTable;
        }

        private SoapNotesModel GetSoapNoteModel(string patientId, string patientFullName, string soapNoteId, DateTime? fromDate, DateTime? toDate)
        {
            DateTime startDate;
            DateTime endDate;
            ValidateDates(fromDate, toDate, out startDate, out endDate);

            var soapNotesModel = new SoapNotesModel()
            {
                soapNoteId = soapNoteId,
                patientId = patientId,
                patientFullName = patientFullName,
                userFullName = CurrentUser.FullName,
                FromDate = startDate,
                ToDate = endDate,
                FromDateForDR = startDate,
                ToDateForDR = endDate,
                SoapNotes = Service.GetSoapNotes(patientId, soapNoteId, startDate, endDate)
            };
            return soapNotesModel;
        }

        private DeviceModel GetDeviceModel(string patientId, DateTime? fromDate, DateTime? toDate)
        {
            DateTime startDate;
            DateTime endDate;
            ValidateDates(fromDate, toDate, out startDate, out endDate);

            var deviceModel = new DeviceModel()
            {
                Patient = (Party)new Patient() { id = patientId },
                Device = new Device(),
                FromDate = startDate,
                ToDate = endDate
            };

            var aggregateHistory = Service.GetAggregateHistory(patientId, startDate, endDate);
            deviceModel.WSReadings = new GetWeighingScaleHistoryResponse()
            {
                readings = aggregateHistory.weighingScaleReading,
                threshold = aggregateHistory.weighingScaleThreshold
            };
            if (deviceModel.WSReadings.readings.Count() != 0)
            {
                deviceModel.WSReadingsAvg = Math.Round((deviceModel.WSReadings.readings.Average(r => r.weightReadingPound.AsFloat())), 2);
            }
            else
            {
                deviceModel.WSReadingsAvg = 0.0;
            }

            deviceModel.BPReadings = new GetBloodPressureHistoryResponse()
            {
                readings = aggregateHistory.bloodPressureReading,
                threshold = aggregateHistory.bloodPressureThreshold
            };

            if (deviceModel.BPReadings.readings.Count() != 0)
            {
                deviceModel.BPSystolicReadingsAvg = Math.Round((deviceModel.BPReadings.readings.Average(r => r.systolic.AsFloat())), 2);
                deviceModel.BPDiastolicReadingsAvg = Math.Round((deviceModel.BPReadings.readings.Average(r => r.diastolic.AsFloat())), 2);
                deviceModel.BPPRReadingsAvg = Math.Round((deviceModel.BPReadings.readings.Average(r => r.pulseRate.AsFloat())), 2);
            }
            else
            { deviceModel.BPSystolicReadingsAvg = 0.0; deviceModel.BPDiastolicReadingsAvg = 0.0; deviceModel.BPPRReadingsAvg = 0.0; }

            deviceModel.TMReadings = new GetTemperatureHistoryResponse()
            {
                readings = aggregateHistory.temperatureReading,
                threshold = aggregateHistory.temperatureThreshold
            };
            if (deviceModel.TMReadings.readings.Count() != 0)
            {
                deviceModel.TMReadingsAvg = Math.Round((deviceModel.TMReadings.readings.Average(r => r.temperatureFahrenheit.AsFloat())), 2);
            }
            else
            {
                deviceModel.TMReadingsAvg = 0.0;
            }

            deviceModel.POReadings = new GetPulseOxiMeterHistoryResponse()
            {
                readings = aggregateHistory.pulseOxiMeterReading,
                threshold = aggregateHistory.pulseOxiMeterThreshold
            };
            if (deviceModel.POReadings.readings.Count() != 0)
            {
                deviceModel.POReadingsAvg = Math.Round(deviceModel.POReadings.readings.Average(r => r.oxygenReading.AsFloat()), 2);
            }
            else
            {
                deviceModel.POReadingsAvg = 0.0;
            }

            deviceModel.BGReadings = new GetBloodGlucoseHistoryResponse()
            {
                readings = aggregateHistory.bloodGlucoseReading,
                threshold = aggregateHistory.bloodGlucoseThreshold
            };
            if (deviceModel.BGReadings.readings.Count() != 0)
            {
                deviceModel.BGReadingsAvg = Math.Round((deviceModel.BGReadings.readings.Average(r => r.glucoseReading.AsFloat())), 2);
            }
            else
            {
                deviceModel.BGReadingsAvg = 0.0;
            }

            deviceModel.FeelingReadings = new GetFeelingHistoryResponse()
            {
                reading = aggregateHistory.feeling,
            };

            deviceModel.SPReadings = aggregateHistory.spirometerReading;
            if (deviceModel.SPReadings.Count() != 0)
            {
                deviceModel.SPFevReadingsAvg = Math.Round((deviceModel.SPReadings.Average(r => r.fev1Best.AsFloat())), 2);
                deviceModel.SPPefReadingsAvg = Math.Round((deviceModel.SPReadings.Average(r => r.pefBest.AsFloat())), 2);
            }
            else
            {
                deviceModel.SPFevReadingsAvg = 0.0; deviceModel.SPPefReadingsAvg = 0.0;
            }

            // get the daily qa score for Mental Illness
            deviceModel.MIDailyQaScores = aggregateHistory.mentalIllnessDailyQaScore;
            if (deviceModel.MIDailyQaScores.Count() != 0)
            {
                deviceModel.MIDailyQaScoresAvg = Math.Round((deviceModel.MIDailyQaScores.Average(r => r.dailyQaScore.AsFloat())), 2);
            }
            else
            {
                deviceModel.MIDailyQaScoresAvg = 0.0;
            }

            // get the daily qa score for Diabetes
            deviceModel.DiabetesDailyQaScores = aggregateHistory.diabetesDailyQaScore;
            if (deviceModel.DiabetesDailyQaScores.Count() != 0)
            {
                deviceModel.DiabetesDailyQaScoresAvg = Math.Round((deviceModel.DiabetesDailyQaScores.Average(r => r.dailyQaScore.AsFloat())), 2);
            }
            else
            {
                deviceModel.DiabetesDailyQaScoresAvg = 0.0;
            }

            return deviceModel;
        }

        private BasicParty[] AddPatientParty(string partyType, BasicParty[] parties, string partyId)
        {
            var list = new List<BasicParty>();
            if (parties != null)
            {
                list.AddRange(parties.ToList());
            }

            var activePartyCount = 0;
            for (var index = 0; index < list.Count; ++index)
            {
                if (list[index].status != Operation.Delete)
                {
                    activePartyCount++;
                }
                if (string.IsNullOrWhiteSpace(list[index].partyID))
                {
                    throw new ArgumentException(string.Format("{0} {1} cannot be empty", partyType, (activePartyCount)));
                }
            }
            BasicParty party;
            if (string.IsNullOrWhiteSpace(partyId) == true)
            {
                party = new BasicParty()
                {
                    status = Operation.Add
                };
            }
            else
            {
                var newParty = Service.GetPartyById(partyId);
                party = new BasicParty()
                {
                    partyID = newParty.id,
                    firstName = newParty.firstName,
                    lastName = newParty.lastName,
                    email = newParty.email,
                    status = Operation.Add
                };
            }
            list.Add(party);
            parties = list.ToArray();
            return parties;
        }

        private static BasicParty[] RemovePatientParty(string partyType, BasicParty[] parties, int removedIndex)
        {
            var list = parties.ToList();
            if (list.Any() == false)
            {
                throw new ArgumentException(string.Format("There is no {0} to remove", partyType));
            }
            if (string.IsNullOrWhiteSpace(list[removedIndex].partyID)
                || list[removedIndex].partyID == "0")
            {
                list.RemoveAt(removedIndex);
            }
            else
            {
                list[removedIndex].status = Operation.Delete;
            }
            parties = list.ToArray();
            return parties;
        }

        private DataTable GetDatatable(string deviceType, string patientId, DateTime? fromDate, DateTime? toDate)
        {
            var startDate = DateTime.Now.AddDays(-30.0);
            var endDate = DateTime.Now;
            if (fromDate.HasValue == true)
            {
                startDate = fromDate.Value;
            }
            if (toDate.HasValue == true)
            {
                endDate = toDate.Value;
            }
            var datatable = GetDatatable(deviceType, patientId, startDate, endDate);
            return datatable;
        }

        private DeviceModel GetPillsyDeviceModel(string patientId, string deviceId, string deviceType, DateTime? fromDate, DateTime? toDate)
        {
            DateTime startDate;
            DateTime endDate;
            ValidateDates(fromDate, toDate, out startDate, out endDate);
            var deviceModel = new DeviceModel()
            {
                Patient = Service.GetPartyById(patientId),
                Device = Service.GetDevice(deviceType),
                FromDate = startDate,
                ToDate = endDate
            };
            switch (deviceModel.Device.deviceType)
            {
                case DeviceType.Medication:
                    string responsePillsy = getPillsyHistoricalReadings(patientId, deviceId, deviceType, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
                    if (responsePillsy == null || responsePillsy.IsEmpty() == true || responsePillsy.Length == 0)
                    {
                        deviceModel.MEDReadings = new PillsyReadingModel[0];
                    }
                    else
                    {
                        PillsyReadingModelCollection collection;
                        using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(responsePillsy)))
                        {
                            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(PillsyReadingModelCollection));
                            collection = (PillsyReadingModelCollection)serializer.ReadObject(ms);
                        }
                        deviceModel.MEDReadings = collection.PillsyReading;
                        deviceModel.TakenDays = collection.PillsyReading.Select(r => r.MeasurementTime.AsDateTime().ToShortDateString()).Distinct().Count();
                        deviceModel.MissedDays = deviceModel.TotalDays - deviceModel.TakenDays;
                        deviceModel.TotalReadings = collection.PillsyReading.Count();

                    }
                    // add an logCareTime entry for reviewing patients medication
                    AddCareTimeForViewingReadings(patientId, "Review patient medication data");
                    break;
            }
            return deviceModel;
        }

        private DeviceModel GetDeviceModel(string patientId, string deviceType, DateTime? fromDate, DateTime? toDate)
        {
            DateTime startDate;
            DateTime endDate;
            ValidateDates(fromDate, toDate, out startDate, out endDate);
            var deviceModel = new DeviceModel()
            {
                Patient = Service.GetPartyById(patientId),
                Device = Service.GetDevice(deviceType),
                FromDate = startDate,
                ToDate = endDate
            };
            switch (deviceModel.Device.deviceType)
            {



                case DeviceType.Covid:
                    string covid = ReadingGridDetails(patientId, deviceType, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
                    if (covid == null || covid.IsEmpty() == true || covid.Length == 0)
                    {
                        deviceModel.CVDReadings = new CovidModel[0];
                    }
                    else
                    {
                        CovidModelCollection collection;
                        using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(covid)))
                        {
                            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(CovidModelCollection));
                            collection = (CovidModelCollection)serializer.ReadObject(ms);
                        }

                        deviceModel.CVDReadings = collection.CovidReading;
                        if (deviceModel.CVDReadings.Count() > 0)
                        {
                            deviceModel.TakenDays = collection.CovidReading.Select(r => r.DailyQaTakenTime.AsDateTime().ToShortDateString()).Distinct().Count();
                            deviceModel.MissedDays = deviceModel.TotalDays - deviceModel.TakenDays;
                            deviceModel.TotalReadings = collection.CovidReading.Count();
                        }
                        else
                        {
                            deviceModel.CVDReadings = new CovidModel[0];
                        }

                    }
                    // add an logCareTime entry for reviewing patients medication
                    AddCareTimeForViewingReadings(patientId, "Review patient medication data");
                    break;




                case DeviceType.Medication:
                    string medication = ReadingGridDetails(patientId, deviceType, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
                    if (medication == null || medication.IsEmpty() == true || medication.Length == 0)
                    {
                        deviceModel.MEDReadings = new PillsyReadingModel[0];
                    }
                    else
                    {
                        PillsyReadingModelCollection collection;
                        using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(medication)))
                        {
                            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(PillsyReadingModelCollection));
                            collection = (PillsyReadingModelCollection)serializer.ReadObject(ms);
                        }

                        deviceModel.MEDReadings = collection.PillsyReading;
                        if (deviceModel.MEDReadings.Count() > 0)
                        {
                            deviceModel.TakenDays = collection.PillsyReading.Select(r => r.MeasurementTime.AsDateTime().ToShortDateString()).Distinct().Count();
                            deviceModel.MissedDays = deviceModel.TotalDays - deviceModel.TakenDays;
                            deviceModel.TotalReadings = collection.PillsyReading.Count();
                        }
                        else
                        {
                            deviceModel.MEDReadings = new PillsyReadingModel[0];
                        }

                    }
                    // add an logCareTime entry for reviewing patients medication
                    AddCareTimeForViewingReadings(patientId, "Review patient medication data");
                    break;

                case DeviceType.Readmission:
                    string responseReadmission = ReadingGridDetails(patientId, deviceType, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
                    if (responseReadmission == null || responseReadmission.IsEmpty() == true || responseReadmission.Length == 0)
                    {
                        deviceModel.REAReadings = new ReadmissionModel[0];
                    }
                    else
                    {                        
                        ReadmissionModelCollection collection;
                        using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(responseReadmission)))
                        {
                            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(ReadmissionModelCollection));
                            collection = (ReadmissionModelCollection)serializer.ReadObject(ms);
                        }

                        deviceModel.REAReadings = collection.Readmission;
                        if (deviceModel.REAReadings.Count() > 0)
                        {
                            deviceModel.TakenDays = collection.Readmission.Select(r => r.ReadmissionDate.AsDateTime().ToShortDateString()).Distinct().Count();
                            deviceModel.MissedDays = deviceModel.TotalDays - deviceModel.TakenDays;
                            deviceModel.TotalReadings = collection.Readmission.Count();
                        }
                        else
                        {
                            deviceModel.REAReadings = new ReadmissionModel[0];
                        }

                    }
                    // add an logCareTime entry for reviewing patients readmission
                    AddCareTimeForViewingReadings(patientId, "Review patient readmission data");
                    break;

                case DeviceType.PauseMonitor:

                    string responsePausemonitor = ReadingGridDetails(patientId, deviceType, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));      
                    if (responsePausemonitor == null || responsePausemonitor.IsEmpty() == true || responsePausemonitor.Length == 0)
                    {
                        deviceModel.PMNReadings = new PauseMonitorModel[0];
                    }
                    else
                    {                       
                        PauseMonitorCollection collection;
                        using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(responsePausemonitor)))
                        {
                            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(PauseMonitorCollection));
                            collection = (PauseMonitorCollection)serializer.ReadObject(ms);
                        }

                        deviceModel.PMNReadings = collection.PauseMonitor;
                        if (deviceModel.PMNReadings.Count() > 0)
                        {
                            deviceModel.TakenDays = collection.PauseMonitor.Select(r => r.StartDate.AsDateTime().ToShortDateString()).Distinct().Count();
                            deviceModel.MissedDays = deviceModel.TotalDays - deviceModel.TakenDays;
                            deviceModel.TotalReadings = collection.PauseMonitor.Count();
                        }
                        else
                        {
                            deviceModel.PMNReadings = new PauseMonitorModel[0];
                        }
                    }
                    // add an logCareTime entry for reviewing patients pause monitor
                    AddCareTimeForViewingReadings(patientId, "Review patient pause monitor data");
                    break;


                case DeviceType.CareTime:
                    string response = ReadingGridDetails(patientId, deviceType, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
                   
                    if (response == null || response.IsEmpty() == true || response.Length == 0)
                    {
                        deviceModel.CRTReadings = new CareTimeModel[0];
                    }
                    else
                    {

                        // string data;

                        // fill the json in data variable

                        CareTimeCollection collection;
                        using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(response)))
                        {
                            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(CareTimeCollection));
                            collection = (CareTimeCollection)serializer.ReadObject(ms);
                        }
                        /// collection.CareTime.se
                        // Newtonsoft.Json.Linq.JObject json = Newtonsoft.Json.Linq.JObject.Parse(response);
                        // Newtonsoft.Json.Linq.JObject ojObject = (Newtonsoft.Json.Linq.JObject)json["CRT"];
                        // Newtonsoft.Json.Linq.JArray array = (Newtonsoft.Json.Linq.JArray)ojObject["chats"];
                        //dynamic crt = JsonConvert.DeserializeObject<object>(response);
                        //CareTimeModel[] readings = JsonConvert.DeserializeObject<CareTimeModel[]>(json);
                        //var readings = jQuery.parseJSON(deviceModel.CRTReadings);
                        ///  CareTimeModel[] readings = null;
                        ///  
                        deviceModel.CRTReadings = collection.CareTime;
                        if (deviceModel.CRTReadings.Count() > 0)
                        {
                            deviceModel.TakenDays = collection.CareTime.Select(r => r.CareTimeDate.AsDateTime().ToShortDateString()).Distinct().Count();
                            deviceModel.CRTReadingsCallTimeAvg = Math.Round(collection.CareTime.Average(r => r.Duration.AsFloat()), 2);
                            deviceModel.MissedDays = deviceModel.TotalDays - deviceModel.TakenDays;
                            deviceModel.TotalReadings = collection.CareTime.Count();
                        }
                        else
                        {
                            deviceModel.CRTReadings = new CareTimeModel[0];
                        }

                    }
                    break;
                case DeviceType.WeighingScale:
                    deviceModel.WSReadings = Service.GetWeighingScaleReadings(patientId, startDate, endDate);
                    //if (deviceModel.WSReadings == null)
                    //{
                    //    throw new ArgumentException("Sorry! There are no readings for " + deviceModel.Device.deviceType.ToDeviceType());
                    //}
                    if (deviceModel.WSReadings.readings == null || deviceModel.WSReadings.readings.Length == 0)
                    {
                        deviceModel.WSReadings.readings = new WeighingScale[0];
                    }
                    else
                    {
                        deviceModel.WSReadings.readings = deviceModel.WSReadings.readings.OrderBy(r => r.measurementTime.AsDateTime()).ToArray();

                        deviceModel.TakenDays = deviceModel.WSReadings.readings.Select(r => r.measurementTime.AsDateTime().ToShortDateString()).Distinct().Count();
                        //if (deviceModel.TakenDays == 0)
                        //{
                        //    throw new ArgumentException("There are NO readings taken for past 3 months!!!");
                        //}
                        deviceModel.WSReadingsAvg = Math.Round(deviceModel.WSReadings.readings.Average(r => r.weightReadingPound.AsFloat()), 2);
                        deviceModel.MissedDays = deviceModel.TotalDays - deviceModel.TakenDays;
                        deviceModel.TotalReadings = deviceModel.WSReadings.readings.Count();
                        deviceModel.TotalAbNormalReadings = deviceModel.WSReadings.readings.Count(r => r.aboveThreshold == true);
                        deviceModel.TotalNormalReadings = deviceModel.WSReadings.readings.Count(r => r.aboveThreshold == false);
                    }
                    // add an logCareTime entry for reviewing patients weight
                    AddCareTimeForViewingReadings(patientId, "Review patient weight data");
                    break;
                case DeviceType.BloodPressure:
                case DeviceType.PulseRate:
                    deviceModel.BPReadings = Service.GetBloodPressureReadings(patientId, startDate, endDate);
                    //if (deviceModel.BPReadings == null)
                    //{
                    //    throw new ArgumentException("Sorry! There are no readings for " + deviceModel.Device.deviceType.ToDeviceType());
                    //}
                    if (deviceModel.BPReadings.readings == null || deviceModel.BPReadings.readings.Length == 0)
                    {
                        deviceModel.BPReadings.readings = new BloodPressure[0];
                    }
                    else
                    {
                        deviceModel.BPReadings.readings = deviceModel.BPReadings.readings.OrderBy(r => r.measurementTime.AsDateTime()).ToArray();
                        deviceModel.TakenDays = deviceModel.BPReadings.readings.Select(r => r.measurementTime.AsDateTime().ToShortDateString()).Distinct().Count();
                        //if (deviceModel.TakenDays == 0)
                        //{
                        //    throw new ArgumentException("There are NO readings taken for past 3 months!!!");
                        //}
                        deviceModel.BPSystolicReadingsAvg = Math.Round((deviceModel.BPReadings.readings.Average(r => r.systolic.AsFloat())), 2);
                        deviceModel.BPDiastolicReadingsAvg = Math.Round((deviceModel.BPReadings.readings.Average(r => r.diastolic.AsFloat())), 2);
                        deviceModel.BPPRReadingsAvg = Math.Round((deviceModel.BPReadings.readings.Average(r => r.pulseRate.AsFloat())), 2);

                        deviceModel.MissedDays = deviceModel.TotalDays - deviceModel.TakenDays;
                        deviceModel.TotalReadings = deviceModel.BPReadings.readings.Count();
                        if (deviceModel.Device.deviceType == DeviceType.BloodPressure)
                        {
                            int SysAbNormalReadings = deviceModel.BPReadings.readings.Count(r => r.systolicAboveThreshold == true);
                            int DiaAbNormalReadings = deviceModel.BPReadings.readings.Count(r => r.diastolicAboveThreshold == true);
                            if (SysAbNormalReadings > DiaAbNormalReadings)
                            {
                                deviceModel.TotalAbNormalReadings = SysAbNormalReadings;
                            }
                            else
                            {
                                deviceModel.TotalAbNormalReadings = DiaAbNormalReadings;
                            }

                            deviceModel.TotalNormalReadings = deviceModel.BPReadings.readings.Count() - deviceModel.TotalAbNormalReadings;

                            //int SysNormalReadings = deviceModel.BPReadings.readings.Count(r => r.systolicAboveThreshold == false);
                            //int DiaNormalReadings = deviceModel.BPReadings.readings.Count(r => r.diastolicAboveThreshold == false);
                            //if (SysNormalReadings > DiaNormalReadings)
                            //{
                            //    deviceModel.TotalNormalReadings = SysNormalReadings;
                            //}
                            //else
                            //{
                            //    deviceModel.TotalNormalReadings = DiaNormalReadings;
                            //}
                        }
                        else
                        {
                            deviceModel.TotalAbNormalReadings = deviceModel.BPReadings.readings.Count(r => r.pulseRateAboveThreshold == true);
                            deviceModel.TotalNormalReadings = deviceModel.BPReadings.readings.Count(r => r.pulseRateAboveThreshold == false);
                        }
                    }
                    // add an logCareTime entry for reviewing patients Blood pressure
                    AddCareTimeForViewingReadings(patientId, "Review patient blood pressure and pulse rate data");
                    break;
                case DeviceType.Spirometer:
                    var spirometerReadings = Service.GetSpirometerReadings(patientId, startDate, endDate);
                    //if (spirometerReadings == null)
                    //{
                    //    throw new ArgumentException("Sorry! There are no readings for " + deviceModel.Device.deviceType.ToDeviceType());
                    //}
                    deviceModel.SPReadings = spirometerReadings.readings;
                    if (deviceModel.SPReadings == null || deviceModel.SPReadings.Length == 0)
                    {
                        deviceModel.SPReadings = new AsthmaMonitor[0];
                    }
                    else
                    {
                        deviceModel.SPReadings = spirometerReadings.readings;
                        deviceModel.SPReadings = deviceModel.SPReadings.OrderBy(r => r.measurementTime.AsDateTime()).ToArray();

                        deviceModel.TakenDays = deviceModel.SPReadings.Select(r => r.measurementTime.AsDateTime().ToShortDateString()).Distinct().Count();
                        //if (deviceModel.TakenDays == 0)
                        //{
                        //    throw new ArgumentException("There are NO readings taken for past 3 months!!!");
                        //}
                        deviceModel.SPFevReadingsAvg = Math.Round((deviceModel.SPReadings.Average(r => r.fev1Best.AsFloat())), 2);
                        deviceModel.SPPefReadingsAvg = Math.Round((deviceModel.SPReadings.Average(r => r.pefBest.AsFloat())), 2);
                        deviceModel.MissedDays = deviceModel.TotalDays - deviceModel.TakenDays;
                        deviceModel.TotalReadings = deviceModel.SPReadings.Count();
                        deviceModel.TotalAbNormalReadings = deviceModel.SPReadings.Count(r => r.aboveThreshold == true);
                        deviceModel.TotalNormalReadings = deviceModel.SPReadings.Count(r => r.aboveThreshold == false);
                    }
                    // add an logCareTime entry for reviewing patients spirometer
                    AddCareTimeForViewingReadings(patientId, "Review patient spirometer data");
                    break;
                case DeviceType.Temperature:
                    deviceModel.TMReadings = Service.GetTemperatureReadings(patientId, startDate, endDate);
                    //if (deviceModel.TMReadings == null)
                    //{
                    //    throw new ArgumentException("Sorry! There are no readings for " + deviceModel.Device.deviceType.ToDeviceType());
                    //}
                    if (deviceModel.TMReadings.readings == null || deviceModel.TMReadings.readings.Length == 0)
                    {
                        deviceModel.TMReadings.readings = new Temperature[0];
                    }
                    else
                    {
                        deviceModel.TMReadings.readings = deviceModel.TMReadings.readings.OrderBy(r => r.measurementTime.AsDateTime()).ToArray();

                        deviceModel.TakenDays = deviceModel.TMReadings.readings.Select(r => r.measurementTime.AsDateTime().ToShortDateString()).Distinct().Count();
                        //if (deviceModel.TakenDays == 0)
                        //{
                        //    throw new ArgumentException("There are NO readings taken for past 3 months!!!");
                        //}
                        deviceModel.TMReadingsAvg = Math.Round(deviceModel.TMReadings.readings.Average(r => r.temperatureFahrenheit.AsFloat()), 2);
                        deviceModel.MissedDays = deviceModel.TotalDays - deviceModel.TakenDays;
                        deviceModel.TotalReadings = deviceModel.TMReadings.readings.Count();
                        deviceModel.TotalAbNormalReadings = deviceModel.TMReadings.readings.Count(r => r.aboveThreshold == true);
                        deviceModel.TotalNormalReadings = deviceModel.TMReadings.readings.Count(r => r.aboveThreshold == false);
                    }
                    // add an logCareTime entry for reviewing patients temperature
                    AddCareTimeForViewingReadings(patientId, "Review patient temperature data");
                    break;
                case DeviceType.PulseOximeter:
                    deviceModel.POReadings = Service.GetPulseOximeterReadings(patientId, startDate, endDate);
                    //if (deviceModel.POReadings == null)
                    //{
                    //    throw new ArgumentException("Sorry! There are no readings for " + deviceModel.Device.deviceType.ToDeviceType());
                    //}
                    if (deviceModel.POReadings.readings == null || deviceModel.POReadings.readings.Length == 0)
                    {
                        deviceModel.POReadings.readings = new PulseOxiMeter[0];
                    }
                    else
                    {
                        deviceModel.POReadings.readings = deviceModel.POReadings.readings.OrderBy(r => r.measurementTime.AsDateTime()).ToArray();

                        deviceModel.TakenDays = deviceModel.POReadings.readings.Select(r => r.measurementTime.AsDateTime().ToShortDateString()).Distinct().Count();
                        //if (deviceModel.TakenDays == 0)
                        //{
                        //    throw new ArgumentException("There are NO readings taken for past 3 months!!!");
                        //}
                        deviceModel.POReadingsAvg = Math.Round(deviceModel.POReadings.readings.Average(r => r.oxygenReading.AsFloat()), 2);
                        deviceModel.MissedDays = deviceModel.TotalDays - deviceModel.TakenDays;
                        deviceModel.TotalReadings = deviceModel.POReadings.readings.Count();
                        deviceModel.TotalAbNormalReadings = deviceModel.POReadings.readings.Count(r => r.aboveThreshold == true);
                        deviceModel.TotalNormalReadings = deviceModel.POReadings.readings.Count(r => r.aboveThreshold == false);
                    }
                    // add an logCareTime entry for reviewing patients pulse ox
                    AddCareTimeForViewingReadings(patientId, "Review patient pulse oximeter data");
                    break;
                case DeviceType.BloodGlucose:
                    deviceModel.BGReadings = Service.GetBloodGlucoseReadings(patientId, startDate, endDate);
                    //if (deviceModel.BGReadings == null)
                    //{
                    //    throw new ArgumentException("Sorry! There are no readings for " + deviceModel.Device.deviceType.ToDeviceType());
                    //}
                    if (deviceModel.BGReadings.readings == null || deviceModel.BGReadings.readings.Length == 0)
                    {
                        deviceModel.BGReadings.readings = new BloodGlucose[0];
                    }
                    else
                    {
                        deviceModel.BGReadings.readings = deviceModel.BGReadings.readings.OrderBy(r => r.measurementTime.AsDateTime()).ToArray();
                        //}
                        deviceModel.TakenDays = deviceModel.BGReadings.readings.Select(r => r.measurementTime.AsDateTime().ToShortDateString()).Distinct().Count();
                        //if (deviceModel.TakenDays == 0)
                        //{
                        //     throw new ArgumentException("There are NO readings taken for past 3 months!!!");
                        //}
                        //else
                        //{
                        deviceModel.BGReadingsAvg = Math.Round(deviceModel.BGReadings.readings.Average(r => r.glucoseReading.AsFloat()), 2);
                        deviceModel.MissedDays = deviceModel.TotalDays - deviceModel.TakenDays;
                        deviceModel.TotalReadings = deviceModel.BGReadings.readings.Count();
                        deviceModel.TotalAbNormalReadings = deviceModel.BGReadings.readings.Count(r => r.aboveThreshold == true);
                        deviceModel.TotalNormalReadings = deviceModel.BGReadings.readings.Count(r => r.aboveThreshold == false);
                    }
                    // get the A1C readings for this patient ...
                    string responseHbA1c = getHbA1cHistoricalReadings(patientId);
                    if (responseHbA1c == null || responseHbA1c.IsEmpty() == true || responseHbA1c.Length == 0)
                    {
                        deviceModel.A1CReadings = new BloodHbA1cModel[0];
                    }
                    else
                    {
                        BloodHbA1cCollection collection;
                        using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(responseHbA1c)))
                        {
                            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(BloodHbA1cCollection));
                            collection = (BloodHbA1cCollection)serializer.ReadObject(ms);
                        }

                        deviceModel.A1CReadings = collection.BloodHbA1cCol;
                    }
                    // add an logCareTime entry for reviewing patients blood glucose
                    AddCareTimeForViewingReadings(patientId, "Review patient blood glucose data");
                    break;
                case DeviceType.Feelings:
                    deviceModel.FeelingReadings = Service.GetFeelingReadings(patientId, startDate, endDate);
                    //if (deviceModel.FeelingReadings == null)
                    //{
                    //    throw new ArgumentException("Sorry! There are no readings for " + deviceModel.Device.deviceType.ToDeviceType());
                    //}
                    if (deviceModel.FeelingReadings.reading == null || deviceModel.FeelingReadings.reading.Length == 0)
                    {
                        deviceModel.FeelingReadings.reading = new Feeling[0];
                    }
                    else
                    {
                        deviceModel.FeelingReadings.reading = deviceModel.FeelingReadings.reading.OrderBy(r => r.measurementTime.AsDateTime()).ToArray();

                        deviceModel.TakenDays = deviceModel.FeelingReadings.reading.Select(r => r.measurementTime.AsDateTime().ToShortDateString()).Distinct().Count();
                        //if (deviceModel.TakenDays == 0)
                        //{
                        //    throw new ArgumentException("There are NO readings taken for past 3 months!!!");
                        //}
                        deviceModel.MissedDays = deviceModel.TotalDays - deviceModel.TakenDays;
                        deviceModel.TotalReadings = deviceModel.FeelingReadings.reading.Count();
                        deviceModel.TotalAbNormalReadings = deviceModel.FeelingReadings.reading.Count(r => (r.painReading == "4"
                                                                                                                || r.painReading == "5"
                                                                                                                || r.moodReading == "4"
                                                                                                                || r.moodReading == "5"
                                                                                                                || r.energyReading == "4"
                                                                                                                || r.energyReading == "5"
                                                                                                                || r.breathingReading == "4"
                                                                                                                || r.breathingReading == "5"));
                        deviceModel.TotalNormalReadings = deviceModel.FeelingReadings.reading.Count(r => (r.painReading != "4"
                                                                                                                && r.painReading != "5"
                                                                                                                && r.moodReading != "4"
                                                                                                                && r.moodReading != "5"
                                                                                                                && r.energyReading != "4"
                                                                                                                && r.energyReading != "5"
                                                                                                                && r.breathingReading != "4"
                                                                                                                && r.breathingReading != "5"));
                    }
                    // add an logCareTime entry for reviewing patients feelings
                    AddCareTimeForViewingReadings(patientId, "Review patient feeling data");
                    break;
                case DeviceType.MentalIllness:
                    deviceModel.MIDailyQaScores = Service.GetMIDailyQaScoreReadings(patientId, startDate, endDate);
                    //To get filled Answers on DeviceModel
                    //deviceModel.FilledDailyQaStats = Service.GetFilledDailyQa(patientId, "1704aed63a74e617ff605902f5f64f13");
                    if (deviceModel.MIDailyQaScores == null || deviceModel.MIDailyQaScores.Length == 0)
                    {
                        deviceModel.MIDailyQaScores = new PatientDailyQaScore[0];
                        //throw new ArgumentException("Sorry! There are no readings for " + deviceModel.Device.deviceType.ToDeviceType());
                    }
                    else
                    {
                        deviceModel.MIDailyQaScores = deviceModel.MIDailyQaScores.OrderBy(r => r.measurementTime.AsDateTime()).ToArray();

                        deviceModel.TakenDays = deviceModel.MIDailyQaScores.Select(r => r.measurementTime.AsDateTime().ToShortDateString()).Distinct().Count();
                        //if (deviceModel.TakenDays == 0)
                        //{
                        //    throw new ArgumentException("There are NO readings taken for past 3 months!!!");
                        //}
                        deviceModel.MIDailyQaScoresAvg = Math.Round(deviceModel.MIDailyQaScores.Average(r => r.dailyQaScore.AsFloat()), 2);
                        deviceModel.MissedDays = deviceModel.TotalDays - deviceModel.TakenDays;
                        deviceModel.TotalReadings = deviceModel.MIDailyQaScores.Count();
                        deviceModel.TotalAbNormalReadings = deviceModel.MIDailyQaScores.Count(r => r.aboveThreshold == true);
                        deviceModel.TotalNormalReadings = deviceModel.MIDailyQaScores.Count(r => r.aboveThreshold == false);
                    }
                    // add an logCareTime entry for reviewing patients mental illness
                    AddCareTimeForViewingReadings(patientId, "Review patient mental illness data");
                    break;
                case DeviceType.Diabetes:
                    deviceModel.DiabetesDailyQaScores = Service.GetDiabetesDailyQaScoreReadings(patientId, startDate, endDate);
                    //To get filled Answers on DeviceModel
                    //deviceModel.FilledDailyQaStats = Service.GetFilledDailyQa(patientId, "1704aed63a74e617ff605902f5f64f13");
                    if (deviceModel.DiabetesDailyQaScores == null || deviceModel.DiabetesDailyQaScores.Length == 0)
                    {
                        deviceModel.DiabetesDailyQaScores = new PatientDailyQaScore[0];
                        //throw new ArgumentException("Sorry! There are no readings for " + deviceModel.Device.deviceType.ToDeviceType());
                    }
                    else
                    {
                        deviceModel.DiabetesDailyQaScores = deviceModel.DiabetesDailyQaScores.OrderBy(r => r.measurementTime.AsDateTime()).ToArray();

                        deviceModel.TakenDays = deviceModel.DiabetesDailyQaScores.Select(r => r.measurementTime.AsDateTime().ToShortDateString()).Distinct().Count();
                        //if (deviceModel.TakenDays == 0)
                        //{
                        //    throw new ArgumentException("There are NO readings taken for past 3 months!!!");
                        //}
                        deviceModel.DiabetesDailyQaScoresAvg = Math.Round(deviceModel.DiabetesDailyQaScores.Average(r => r.dailyQaScore.AsFloat()), 2);
                        deviceModel.MissedDays = deviceModel.TotalDays - deviceModel.TakenDays;
                        deviceModel.TotalReadings = deviceModel.DiabetesDailyQaScores.Count();
                        deviceModel.TotalAbNormalReadings = deviceModel.DiabetesDailyQaScores.Count(r => r.aboveThreshold == true);
                        deviceModel.TotalNormalReadings = deviceModel.DiabetesDailyQaScores.Count(r => r.aboveThreshold == false);
                    }
                    // add an logCareTime entry for reviewing patients diabetes
                    AddCareTimeForViewingReadings(patientId, "Review patient diabetes data");
                    break;
            }
            return deviceModel;
        }

        private static void ValidateDates(DateTime? fromDate, DateTime? toDate, out DateTime startDate, out DateTime endDate)
        {
            startDate = DateTime.Today.AddDays(-30.0);
            endDate = DateTime.Today.AddDays(1.0);
            if (fromDate.HasValue == true)
            {
                startDate = fromDate.Value;
            }
            if (toDate.HasValue == true)
            {
                endDate = toDate.Value;
            }
            if (startDate > endDate)
            {
                throw new ArgumentException("Start Date cannot be greater than End Date.");
            }
        }

        #endregion

        #region Protected Methods

        protected ActionResult BaseShowPatients(string partyId)
        {
            var model = Service.GetPatients(CurrentUser.Group, partyId);
            return View("ShowPatients", model);
        }

        protected ActionResult BaseShowPatientReading(string patientId)
        {
            var response = Helper.Service.GetProfile(patientId);

            //To get Patient Features   
            var Pf = Service.GetPatientFeatures(patientId);
            var ehr = Service.GetEhrDetail(patientId);
            var patient = Service.GetPartyById(patientId);
            List<String> dName = null;
            if (ehr != null && ehr.diseaseAssociations != null)
            {
                dName = (List<String>)(from item in ehr.diseaseAssociations where item.partyId == patientId select item.diseaseTypeId).ToList();
            }

            var model = new PatientDevicesModel()
            {
                QuadrantDetail = Service.GetQuadrantDetailsByPatient(patientId.ToString(), CurrentUser.UserId),
                Patient = patient,
                Preferences = response.preferences,
                diseaseAssociations = ehr.diseaseAssociations,
                DiseaseNameByPatientLst = dName,
                CurrentUserId = CurrentUser.UserId,
                PF = Pf.patientFeatures,
                patientFirstName = patient.firstName,
                patientMiddleName = patient.middleName,
                patientLastName = patient.lastName,
                patientId = patientId,
                QuadrantOrder = Service.GetQuadrantOrder(CurrentUser.UserId, patientId)
            };


            return View("PatientReading", model);
        }


        protected ActionResult BaseShowPatientDevices(string patientId)
        {
            var response = Helper.Service.GetProfile(patientId);

            //To get Patient Features   
            var Pf = Service.GetPatientFeatures(patientId);
            var ehr = Service.GetEhrDetail(patientId);
            var patient = Service.GetPartyById(patientId);
            List<String> dName = null;
            if (ehr != null && ehr.diseaseAssociations != null)
            {
                dName = (List<String>)(from item in ehr.diseaseAssociations where item.partyId == patientId select item.diseaseTypeId).ToList();
            }

            var model = new PatientDevicesModel()
            {
                QuadrantDetail = Service.GetQuadrantDetailsByPatient(patientId.ToString(), CurrentUser.UserId),
                Patient = patient,
                Preferences = response.preferences,
                diseaseAssociations = ehr.diseaseAssociations,
                DiseaseNameByPatientLst = dName,
                CurrentUserId = CurrentUser.UserId,
                PF = Pf.patientFeatures,
                patientFirstName = patient.firstName,
                patientMiddleName = patient.middleName,
                patientLastName = patient.lastName,
                patientId = patientId,
                QuadrantOrder = Service.GetQuadrantOrder(CurrentUser.UserId, patientId)
            };

            return View("ShowPatientDevices", model);
        }

        protected ActionResult BaseGetDeviceQuadrant(string patientId)
        {
            return PartialView("_DeviceQuadrant", new PatientDevicesModel()
            {
                QuadrantDetail = Service.GetQuadrantDetailsByPatient(patientId, CurrentUser.UserId),
                Patient = Service.GetPartyById(patientId),
                PF = Service.GetPatientFeatures(patientId) != null ? Service.GetPatientFeatures(patientId).patientFeatures : null,
                patientId = patientId,
                QuadrantOrder = Service.GetQuadrantOrder(CurrentUser.UserId, patientId)
            });
        }

        protected void SaveQuadrantSliding(SaveQuadrantOrderRequest QuadrantOrder)
        {
            if (QuadrantOrder.quadrantOrder.Length != 0)
            {
                Service.SaveQuadrantSliding(QuadrantOrder);
            }
        }

        public DailyReportModel BaseDailyReportDetails(DailyReportModel model)
        {
            List<ReportBasicParty> DNCPrimaryOrdered = new List<ReportBasicParty>();
            List<PatientDetails> pListAll = new List<PatientDetails>();
            List<PatientDetails> pListHide = new List<PatientDetails>();
            model.Preferences = Service.GetProfile(CurrentUser.UserId).preferences;
            foreach (var preferences in model.Preferences)
            {
                if (preferences.preferenceKeyType == "BPD" && preferences.preferenceValue == "0")
                {
                    model.thBP = "hidden";
                }
                if (preferences.preferenceKeyType == "POX" && preferences.preferenceValue == "0")
                {
                    model.thSPO2 = "hidden";
                }
                if (preferences.preferenceKeyType == "TMP" && preferences.preferenceValue == "0")
                {
                    model.thTemp = "hidden";
                }
                if (preferences.preferenceKeyType == "WSM" && preferences.preferenceValue == "0")
                {
                    model.thWeight = "hidden";
                }
                if (preferences.preferenceKeyType == "BGM" && preferences.preferenceValue == "0")
                {
                    model.thGluc = "hidden";
                }
                if (preferences.preferenceKeyType == "SPD" && preferences.preferenceValue == "0")
                {
                    model.thFevPev = "hidden";
                }
                if (preferences.preferenceKeyType == "SHL" && preferences.preferenceValue == "0")
                {
                    model.thFeelings = "hidden";
                }
                if (preferences.preferenceKeyType == "MIL" && preferences.preferenceValue == "0")
                {
                    model.thMILDailyQa = "hidden";
                }
                if (preferences.preferenceKeyType == "DBT" && preferences.preferenceValue == "0")
                {
                    model.thDBTDailyQa = "hidden";
                }
                if (preferences.preferenceKeyType == "MED" && preferences.preferenceValue == "0")
                {
                    model.thMED = "hidden";
                }
                if (preferences.preferenceKeyType == "CVD" && preferences.preferenceValue == "0")
                {
                    model.thCovid = "hidden";
                }
                if (preferences.preferenceKeyType == "EKG" && preferences.preferenceValue == "0")
                {
                    model.thEKG = "hidden";
                }
            }
            int flag = 0;
            if (model.IsListView)
            {
                model.Report = Service.GetDailyReport(CurrentUser.UserId, model.IsFullReport);

                if (model.Report != null && model.Report.patients != null)
                {
                    for (int i = 0; i < model.Report.patients.Length; i++)
                    {
                        if (model.Readings.patients[i].support != null)
                        {
                            var doctors = model.Report.patients[i].support.Where(p => p.partyType == "Doctor").ToList().OrderByDescending(q => q.isPrimary).ThenBy(r => r.name).ToList();
                            var clinicians = model.Report.patients[i].support.Where(p => p.partyType == "Clinician").ToList().OrderByDescending(q => q.isPrimary).ThenBy(r => r.name).ToList();
                            var caregivers = model.Report.patients[i].support.Where(p => p.partyType == "Caregiver").ToList().OrderByDescending(q => q.isPrimary).ThenBy(r => r.name).ToList();

                            DNCPrimaryOrdered = doctors.Concat(clinicians).Concat(caregivers).ToList();
                            model.Report.patients[i].support = DNCPrimaryOrdered.ToArray();
                        }
                    }
                }
            }
            else
            {
                model.Readings = Service.GetDailyReadingsReport(CurrentUser.UserId, CurrentUser.Roles[0], CurrentUser.Group, model.IsFullReport);

               

                if (model.Readings != null && model.Readings.patients != null)
                {
                    for (int i = 0; i < model.Readings.patients.Length; i++)
                    {
                        if (model.PatientToHide != null)
                        {
                            for (int j = 0; j < model.PatientToHide.Count; j++)
                            {
                                if (model.Readings.patients[i].patient.partyId.Contains(model.PatientToHide[j].ToString()))
                                {
                                    flag = 1;
                                    break;
                                }
                            }
                        }
                        if (CurrentUser.IsInRole(Role.Admin) || CurrentUser.IsInRole(Role.GroupAdmin))
                        {
                            string partyId = model.Readings.patients[i].patient.partyId.ToString();
                            List<ReportBasicParty> doctors = null;
                            List<ReportBasicParty> clinicians = null;
                            List<ReportBasicParty> caregivers = null;
                            DNCPrimaryOrdered = null;
                            if (model.Readings.patients[i].support != null)
                            {
                                doctors = model.Readings.patients[i].support.Where(p => p.partyType == "Doctor").ToList().OrderByDescending(q => q.isPrimary).ThenBy(r => r.name).ToList();
                                clinicians = model.Readings.patients[i].support.Where(p => p.partyType == "Clinician").ToList().OrderByDescending(q => q.isPrimary).ThenBy(r => r.name).ToList();
                                caregivers = model.Readings.patients[i].support.Where(p => p.partyType == "Caregiver").ToList().OrderByDescending(q => q.isPrimary).ThenBy(r => r.name).ToList();
                                DNCPrimaryOrdered = doctors.Concat(clinicians).Concat(caregivers).ToList();
                                model.HiddenSupport = DNCPrimaryOrdered.ToArray();
                            }
                            else
                            {
                                model.HiddenSupport = null;
                            }

                            pListAll.Add(model.Readings.patients[i]);
                            model.HiddenReadings = pListAll.ToArray();
                        }
                        else
                        {
                            //To Show all patients during Page Load
                            if (flag == 0 && model.Readings.patients[i].support != null)
                            {
                                string partyId = model.Readings.patients[i].patient.partyId.ToString();
                                var doctors = model.Readings.patients[i].support.Where(p => p.partyType == "Doctor").ToList().OrderByDescending(q => q.isPrimary).ThenBy(r => r.name).ToList();
                                var clinicians = model.Readings.patients[i].support.Where(p => p.partyType == "Clinician").ToList().OrderByDescending(q => q.isPrimary).ThenBy(r => r.name).ToList();
                                var caregivers = model.Readings.patients[i].support.Where(p => p.partyType == "Caregiver").ToList().OrderByDescending(q => q.isPrimary).ThenBy(r => r.name).ToList();

                                DNCPrimaryOrdered = doctors.Concat(clinicians).Concat(caregivers).ToList();
                                //model.Readings.patients[i].support = DNCPrimaryOrdered.ToArray();

                                pListAll.Add(model.Readings.patients[i]);

                                model.HiddenReadings = pListAll.ToArray();
                                model.HiddenSupport = DNCPrimaryOrdered.ToArray();
                            }

                            if (flag == 1 && model.Readings.patients[i].support != null)
                            {
                                var doctors = model.Readings.patients[i].support.Where(p => p.partyType == "Doctor").ToList().OrderByDescending(q => q.isPrimary).ThenBy(r => r.name).ToList();
                                var clinicians = model.Readings.patients[i].support.Where(p => p.partyType == "Clinician").ToList().OrderByDescending(q => q.isPrimary).ThenBy(r => r.name).ToList();
                                var caregivers = model.Readings.patients[i].support.Where(p => p.partyType == "Caregiver").ToList().OrderByDescending(q => q.isPrimary).ThenBy(r => r.name).ToList();

                                DNCPrimaryOrdered = doctors.Concat(clinicians).Concat(caregivers).ToList();
                                //model.Readings.patients[i].support = DNCPrimaryOrdered.ToArray();

                                pListHide.Add(model.Readings.patients[i]);

                                model.HiddenReadings = pListHide.ToArray();
                                model.HiddenSupport = DNCPrimaryOrdered.ToArray();

                                //To show unhidden patients 
                                var results = model.Readings.patients.Except(model.HiddenReadings);
                                model.HiddenReadings = results.ToArray();
                            }
                            flag = 0;
                        }

                                                
                            if (model.thCovid != "hidden")
                            {
                                //get covid 19 readings for the patient
                                string covidobj = GetCovidReadingForToday(model.Readings.patients[i].patient.partyId);
                                if (covidobj != null && !covidobj.IsEmpty())
                                {
                                    string covidStr = covidobj.Substring(7, (covidobj.Length - 8));
                                    IEnumerable<CovidModel> covidrslt = JsonConvert.DeserializeObject<IEnumerable<CovidModel>>(covidStr);
                                    model.covid.Add(model.Readings.patients[i].patient.partyId, covidrslt);
                                }
                            }
                            if (model.thMED != "hidden")
                            {
                                // get the Pillsy readings for the patient

                                string plsyRsps = GetPillsyReadingForToday(model.Readings.patients[i].patient.partyId);
                                if (plsyRsps != null && !plsyRsps.IsEmpty())
                                {
                                    string plRspStr = plsyRsps.Substring(7, (plsyRsps.Length - 8));

                                    IEnumerable<PillsyReadingModel> rslt1 = JsonConvert.DeserializeObject<IEnumerable<PillsyReadingModel>>(plRspStr);
                                    model.pillsys.Add(model.Readings.patients[i].patient.partyId, rslt1);

                                }
                            }


                        // get the Pause Monitor readings for the patient

                        string rsps = GetPauseMonitorReadingForToday(model.Readings.patients[i].patient.partyId);
                        if (rsps != null && !rsps.IsEmpty())
                        {
                            string rspStr = rsps.Substring(7, (rsps.Length - 8));
                            IEnumerable<PauseMonitorModel> rslt = JsonConvert.DeserializeObject<IEnumerable<PauseMonitorModel>>(rspStr);
                            model.PMNReadings.Add(model.Readings.patients[i].patient.partyId, rslt);
                        }

                        // get the Pillsy readings for the patient

                        //string plsyRsps = GetPillsyReadingForToday(model.Readings.patients[i].patient.partyId);
                        //if (plsyRsps != null && !plsyRsps.IsEmpty())
                        //{
                        //    string plRspStr = plsyRsps.Substring(7, (plsyRsps.Length - 8));

                        //    IEnumerable<PillsyReadingModel> rslt1 = JsonConvert.DeserializeObject<IEnumerable<PillsyReadingModel>>(plRspStr);
                        //    model.pillsys.Add(model.Readings.patients[i].patient.partyId, rslt1);

                        //}
                        //get covid 19 readings for the patient

                        //string covidobj = GetCovidReadingForToday(model.Readings.patients[i].patient.partyId);
                        // if (covidobj != null && !covidobj.IsEmpty())
                        // {
                        //     string covidStr = covidobj.Substring(7, (covidobj.Length - 8));
                        //     IEnumerable<CovidModel> covidrslt = JsonConvert.DeserializeObject<IEnumerable<CovidModel>>(covidStr);
                        //     model.covid.Add(model.Readings.patients[i].patient.partyId, covidrslt);
                        // }

                    }
                }
            }

            //model.Preferences = Service.GetProfile(CurrentUser.UserId).preferences.Where(p => p.preferenceType == "DVC").ToArray();
           // model.Preferences = Service.GetProfile(CurrentUser.UserId).preferences;
          
            return model;
        }


        protected ActionResult BaseDailyReport(DailyReportModel model)
        {
            ViewBag.ControllerName = "BaseParty";
            model = BaseDailyReportDetails(model);
            return View("DailyReport", model);
        }

        protected ActionResult BaseEmailDailyReport(string emails)
        {
            if (string.IsNullOrWhiteSpace(emails) == true)
            {
                throw new ArgumentException("Email address cannot be empty.");
            }

            foreach (var email in emails.Split(','))
            {
                ValidateEmail(email, true);
            }

            Service.EmailDailyReport(CurrentUser.UserId, emails);
            return new EmptyResult();
        }

        public string TransformToString(XmlDocument xmlDocument, string xslFilePath)
        {
            //Load the XSL
            var xslTransform = new XslCompiledTransform();
            xslTransform.Load(xslFilePath);
            string transformedString;

            // Apply XSL & transform to HTML
            using (var stringWriter = new StringWriter())
            {
                var xsltArgs = new XsltArgumentList();
                xslTransform.Transform(xmlDocument.CreateNavigator(), xsltArgs, stringWriter);
                transformedString = stringWriter.ToString();
            }

            return transformedString;
        }

        protected ActionResult BaseEmailDailyReportDialog()
        {
            var model = new EmailTemplateModel()
            {
                From = string.Format("{0}<{1}>", CurrentUser.FullName, CurrentUser.Email)
            };
            return PartialView("_EmailDailyReportDialog", model);
        }

        protected ActionResult BaseGetBgDeviceQuadrant(string patientId)
        {
            var bgQuadrantModel = new BgQuadrantModel();
            bgQuadrantModel.QuadrantDetails = Service.GetBgDeviceQuadrant(patientId);
            if (bgQuadrantModel.QuadrantDetails != null
                    && bgQuadrantModel.QuadrantDetails.ekg != null)
            {
                var endDate = DateTime.Parse(bgQuadrantModel.QuadrantDetails.ekg.measurementTime);
                var startDate = endDate.AddDays(-2.0);
                bgQuadrantModel.EkgReadings = Service.GetBgEkgReadings(patientId, startDate, endDate);
            }
            else
            {
                bgQuadrantModel.EkgReadings = new GetEkgHistoryResponse();
            }
            return PartialView("_BGDeviceQuadrant", bgQuadrantModel);
        }

        protected ActionResult BaseGetBgEkgChart(string patientId, DateTime? fromDate, DateTime? toDate)
        {
            DateTime startDate;
            DateTime endDate;
            ValidateDates(fromDate, toDate, out startDate, out endDate);
            var model = Service.GetBgEkgReadings(patientId, startDate, endDate);
            return PartialView("_BGEKGChart", model);
        }

        protected ActionResult BaseShowDevice(string deviceType, string patientId, DateTime? fromDate, DateTime? toDate)
        {
            return View("ShowDevice", GetDeviceModel(patientId, deviceType, fromDate, toDate));
        }

        protected ActionResult BaseShowEHR(string patientId)
        {
            var model = Service.GetEhrDetail(patientId);
            //var response = Helper.Service.GetProfile(patientId);
            //model.preferences = response.preferences;

            //model.preferences.preferenceValue = Helper.Service.GetProfile(patientId).preferences.ToString();
            var ehrDetailsModel = new EHRDetailsModel(Helper.Service.GetEhrDetail(patientId));

            // set the disease picked data based on the disease associations
            setDiseasePicked(ehrDetailsModel);
            setPatientFeatures(ehrDetailsModel);
            setGroupTypes(ehrDetailsModel);

            ViewData["EhrDetailsModel"] = (EHRDetailsModel)ehrDetailsModel;

            ViewData["ParameterModel"] = new ParameterModel(patientId)
            {
                Parameters = Service.GetParameters(patientId),
                Patient = Service.GetPartyById(patientId)
            };

            ViewData["InsuranceModel"] = MyVitalzRestApi.GetPatientInsurance(patientId);

            return View("ShowEhr", model);
        }

        protected ActionResult BaseEditEhr(string patientId)
        {
            var model = new EHRDetailsModel(Helper.Service.GetEhrDetail(patientId));
            model.Associations = Service.GetAssociationsByParty(patientId);
            PopulateAssociationTypes(model);
            //var response = Helper.Service.GetProfile(patientId);
            //model.Preferences = response.preferences;
            List<Preferences> prefList = new List<Preferences>();
            prefList = model.Details.preferences.Where(p => p.preferenceKeyType == "MRE").ToList();

            if (prefList != null)
            {
                foreach (var pref in prefList)
                {
                    model.PreferenceValue = pref.preferenceValue;
                    model.PreferenceKeyType = pref.preferenceKeyType;
                    model.PreferenceType = pref.preferenceType;
                }
            }

            // set the disease picked data based on the disease associations
            setDiseasePicked(model);
            setPatientFeatures(model);
            setGroupTypes(model);
            model.ShowSubGroups = "true";

            // setting cookies for maintaining Search session
            var ehrPage = Request.Cookies["EHRPage"];
            if (ehrPage == null)
            {
                ehrPage = new HttpCookie("EHRPage");
            }
            SearchModel searchModel = new SearchModel();
            searchModel.EHRPage = "EHRPage";
            ehrPage.Values["EHRPage"] = searchModel.EHRPage;
            Response.Cookies.Remove("EHRPage");
            Response.Cookies.Add(ehrPage);


            model.ParameterModel = new ParameterModel(patientId)
            {
                Parameters = Service.GetParameters(patientId),
                Patient = Service.GetPartyById(patientId)
            };

            model.Insurance = MyVitalzRestApi.GetPatientInsurance(patientId);
            return View("EditEhr", model);
        }

        private void setPatientFeatures(EHRDetailsModel model)
        {
            if (model.Details.patientFeatures != null && model.Details.patientFeatures.Count() > 0)
            {
                // set the patient features flags as per the values from the service
                model.DailyQuestionsValue = model.Details.patientFeatures.ElementAt(0).dailyQuestionsValue;
                model.FeelingsValues = model.Details.patientFeatures.ElementAt(0).feelingsValue;
                model.NotificationsValue = model.Details.patientFeatures.ElementAt(0).notificationsValue;
            }
        }

        private void setDiseasePicked(EHRDetailsModel model)
        {
            // set the diseasepicked values correctly to true or false based on the diseaseAssociations
            model.DiseasesPicked = new string[model.Details.diseaseType.Length];

            for (var idx = 0; idx < model.Details.diseaseType.Length; idx++)
            {
                // initialize to false (Not picked)
                model.DiseasesPicked[idx] = "0";
                if (model.Details.diseaseAssociations != null)
                {
                    for (var indx = 0; indx < model.Details.diseaseAssociations.Length; indx++)
                    {
                        if (model.Details.diseaseAssociations[indx].diseaseTypeId.Equals(model.Details.diseaseType[idx].id.ToString()))
                        {
                            // set to true (Picked)
                            model.DiseasesPicked[idx] = "1";
                        }
                    }
                }
            }
            return;
        }

        private void setGroupTypes(EHRDetailsModel model)
        {
            SubGroupWrapper sgw = new SubGroupWrapper();
            List<SubGroupWrapper> sgwLst = new List<SubGroupWrapper>();

            model.SubGroupTypes = model.Details.subGroupType;
            model.SelectedGT = model.Details.ehr.group;

            if (model.Details.allSubGroups != null && model.Details.allSubGroups.Count() > 0)
            {
                for (var x = 0; x < model.Details.allSubGroups.Length; x++)
                {
                    if (model.Details.ehr.group != null && model.Details.allSubGroups[x].groupType != null)
                    {
                        if (model.Details.allSubGroups[x].groupType.Equals(model.Details.ehr.group.ToString()))
                        {
                            sgw = model.Details.allSubGroups[x];
                            sgwLst.Add(sgw);
                        }
                    }
                }

                if (sgwLst.Count != 0)
                {
                    model.AllSubGroups = sgwLst.ToArray();
                    model.PrimaryPicked = new string[model.AllSubGroups.Length];
                    model.SecondaryPicked = new string[model.AllSubGroups.Length];
                    for (var x = 0; x < model.AllSubGroups.Length; x++)
                    {
                        model.PrimaryPicked[x] = "0";
                        model.SecondaryPicked[x] = "0";
                        if (model.Details.subGroupType != null)
                        {
                            for (var y = 0; y < model.Details.subGroupType.Length; y++)
                            {
                                if (model.AllSubGroups[x].primarySubGroupId != null)
                                {
                                    if (model.AllSubGroups[x].primarySubGroupId.Equals(model.Details.subGroupType[y].primarySubGroupId.ToString()))
                                    {
                                        model.PrimaryPicked[x] = "1";
                                    }
                                }
                                else
                                {
                                    model.PrimaryPicked[x] = "0";
                                }

                                if (model.AllSubGroups[x].secondarySubGroupId != null)
                                {
                                    if (!String.IsNullOrEmpty(model.Details.subGroupType[y].secondarySubGroupId))
                                    {
                                        if (model.AllSubGroups[x].secondarySubGroupId.Equals(model.Details.subGroupType[y].secondarySubGroupId.ToString()))
                                        {
                                            model.SecondaryPicked[x] = "1";
                                        }
                                    }
                                    else
                                    {
                                        model.SecondaryPicked[x] = "0";
                                    }
                                }
                                else
                                {
                                    model.SecondaryPicked[x] = "0";
                                }
                            }
                        }
                    }
                }
                else
                {
                    model.AllSubGroups = null;
                }
            }
            return;
        }

        //public JsonResult BaseGetNewSubGroups(string[] selectedGroup)
        public ActionResult BaseGetNewSubGroup(string[] selectedGroup)
        {
            var al = new ArrayList();
            BasePartyModel bpModel = new BasePartyModel();
            SubGroupWrapper[] selectedSG = null;

            var allSGT = Service.GetAllSubGroupTypes();

            if (allSGT != null && selectedGroup != null)
            {
                selectedSG = allSGT.Where(c => c.groupType == selectedGroup[0]).ToArray();
            }

            if (selectedSG != null)
            {
                if (selectedSG.Length != 0)
                {
                    bpModel.AllSubGroups = selectedSG;
                }

                al.Add(selectedSG);

                bpModel.PrimaryPicked = new string[selectedSG.Length];
                bpModel.SecondaryPicked = new string[selectedSG.Length];
                for (var x = 0; x < selectedSG.Length; x++)
                {
                    bpModel.PrimaryPicked[x] = "false";
                    bpModel.SecondaryPicked[x] = "false";
                }
                al.Add(bpModel.PrimaryPicked);
                al.Add(bpModel.SecondaryPicked);
            }
            else
            {
                bpModel.AllSubGroups = null;
            }

            bpModel.ShowSubGroups = "true";
            if (selectedGroup != null)
            {
                bpModel.SelectedGT = selectedGroup[0];
            }
            //return Json(selectedSG, JsonRequestBehavior.AllowGet);
            return PartialView("_SubGroupTypesTree", bpModel);
        }

        protected void PopulateAssociationTypes(EHRDetailsModel model)
        {
            model.AssociationTypes = GetAssociationTypes();
            model.GroupTypes = GetGroupTypes();
        }

        protected ActionResult BaseEditParty(string partyId)
        {
            //var partyById = Helper.Service.GetPartyById(partyId);
            var model = new EHRDetailsModel(Helper.Service.GetEhrDetail(partyId));
            string homeGroup = null;
            if (model.Details.subGroupType != null)
            {
                var lst = model.Details.subGroupType.Where(p => p.patientId == model.Details.ehr.id).ToList();
                foreach (var hg in lst)
                {
                    homeGroup = hg.groupType;
                }
            }
            //model.Details.allSubGroups.Where(t => t.groupType == "MVZ").GroupBy(p => p.primarySubGroupId).Select(g => g.First()).ToList();
            //var ehrDetailsModel = new EHRDetailsModel()
            //{
            //    Details = new EHRDetails()
            //    {
            //        ehr = new Patient()
            //        {
            //            cellPhone = partyById.cellPhone,
            //            dob = partyById.dob,
            //            email = partyById.email,
            //            firstName = partyById.firstName,
            //            gender = partyById.gender,
            //            homePhone = partyById.homePhone,
            //            group = partyById.group,
            //            id = partyById.id,
            //            lastName = partyById.lastName,
            //            middleName = partyById.middleName,
            //            partyType = partyById.partyType,
            //            ssn = partyById.ssn,
            //            taxId = partyById.taxId,
            //            workphone = partyById.workphone,
            //            address = partyById.address,
            //            isPartyActive = partyById.isPartyActive,
            //            RACE = partyById.RACE,
            //            homeGroup = homeGroup
            //        },
            //        subGroupType = GetAllSubGroupTypes()
            //    },
            //    AssociationTypes = GetAssociationTypes(),
            //    GroupTypes = GetGroupTypes()
            //};
            //ehrDetailsModel.Associations = Service.GetAssociationsByParty(partyId);
            //ehrDetailsModel.SubGroupTypes = model.Details.subGroupType;
            //ehrDetailsModel.SubGroups = model.Details.subGroups;
            //ehrDetailsModel.ShowSubGroups = "true";
            //ehrDetailsModel.Details.subGroupType = model.Details.subGroupType;
            //ehrDetailsModel.Details.subGroups = model.Details.subGroups;
            //setGroupTypes(ehrDetailsModel);
            setGroupTypes(model);
            model.GroupTypes = GetGroupTypes();
            model.ShowSubGroups = "true";

            // setting cookies for maintaining Search session
            var ehrPage = Request.Cookies["EHRPage"];
            if (ehrPage == null)
            {
                ehrPage = new HttpCookie("EHRPage");
            }
            SearchModel searchModel = new SearchModel();
            searchModel.EHRPage = "EHRPage";
            ehrPage.Values["EHRPage"] = searchModel.EHRPage;
            Response.Cookies.Remove("EHRPage");
            Response.Cookies.Add(ehrPage);

            model.ParameterModel = new ParameterModel(partyId)
            {
                Parameters = Service.GetParameters(partyId),
                Patient = Service.GetPartyById(partyId)
            };

            return View("EditEhr", model);
        }

        protected ActionResult BaseShowParameters(string patientId)
        {
            return View("ShowParameters", new ParameterModel(patientId)
            {
                Parameters = Service.GetParameters(patientId),
                Patient = Service.GetPartyById(patientId)
            });
        }

        protected ActionResult BaseSaveParameters(ParameterModel model)
        {
            Service.SaveParameters(model.Parameters, CurrentUser.UserId);
            return EmptyResult;
        }

        #endregion

        #region Common Actions

        public ActionResult GetBgDeviceQuadrant(string patientId)
        {
            return BaseGetBgDeviceQuadrant(patientId);
        }

        public ActionResult GetBgEkgChart(string patientId, DateTime? fromDate, DateTime? toDate)
        {
            return BaseGetBgEkgChart(patientId, fromDate, toDate);
        }

        public ActionResult ShowCharts(string patientId, DateTime? fromDate, DateTime? toDate)
        {
            var model = GetDeviceModel(patientId, fromDate, toDate);

            return PartialView("_ShowCharts", model);
        }

        public ActionResult ShowSoapNotes(string patientId, string patientFullName, string soapNoteId, DateTime? fromDate, DateTime? toDate)
        {
            var model = GetSoapNoteModel(patientId, patientFullName, soapNoteId, fromDate, toDate);
            return PartialView("_ShowSoapNotes", model);
        }

        public ActionResult ShowSoapNotesForDR(string patientId, string patientFullName, string soapNoteId, DateTime? fromDate, DateTime? toDate)
        {
            var model = GetSoapNoteModel(patientId, patientFullName, soapNoteId, fromDate, toDate);
            return PartialView("_ShowSoapNotesForDR", model);
        }

        public void AddSoapNote(SoapNote soapNote)
        {
            if (string.IsNullOrWhiteSpace(soapNote.desc) == true &&
                string.IsNullOrWhiteSpace(soapNote.subjective) == true &&
                string.IsNullOrWhiteSpace(soapNote.objective) == true &&
                string.IsNullOrWhiteSpace(soapNote.assessment) == true &&
                string.IsNullOrWhiteSpace(soapNote.plan) == true)
            {
                throw new ArgumentException("Trying to add an Empty SOAP Note.");
            }
            else
            {
                Service.AddSoapNote(soapNote);
            }
        }

        public ActionResult DailyQuestionsDialog(PatientDevicesModel patientDevicesModel)
        {
            //string diseaseType, string patientId, string patientFirstName, string patientMiddleName, string patientLastName
            patientDevicesModel.PatientDailyQAResponse = new GetPatientDailyQAResponse();

            if (patientDevicesModel.diseaseType == null)
            {
                GetQAResponse entireQa = Service.GetQuestionAnswer(patientDevicesModel.diseaseType, patientDevicesModel.patientId);
                if (entireQa != null && entireQa.qas != null && entireQa.qas.Count() > 0)
                {
                    // separate the questions into the disease types
                    patientDevicesModel.patientsQADiseaseTypes = entireQa.qas.Select(x => x.question.diseaseId).Distinct().ToList();
                    patientDevicesModel.diseaseType = patientDevicesModel.patientsQADiseaseTypes.FirstOrDefault();
                    patientDevicesModel.QA = entireQa.qas.Select(x => x).Where(y => y.question.diseaseId == patientDevicesModel.diseaseType).ToArray();
                    patientDevicesModel.totalQACount = patientDevicesModel.QA.Count();
                }
                else
                {
                    throw new Exception("Party does not have any Daily Questions to Answer");
                }
            }
            else
            {
                patientDevicesModel.currentQADiseaseTypeIndex++;
                if (patientDevicesModel.currentQADiseaseTypeIndex != patientDevicesModel.patientsQADiseaseTypes.Count)
                {
                    patientDevicesModel.diseaseType = patientDevicesModel.patientsQADiseaseTypes[patientDevicesModel.currentQADiseaseTypeIndex];
                    patientDevicesModel.QA = Service.GetQuestionAnswer(patientDevicesModel.diseaseType, patientDevicesModel.patientId).qas;
                    patientDevicesModel.totalQACount = patientDevicesModel.QA.Count();
                }
                else
                {
                    return EmptyResult;
                }
            }
            //To refresh the hidden field value (QuestionID)
            ModelState.Clear();
            return PartialView("_DailyQuestions", patientDevicesModel);
        }

        //To edit the daily questions window with the edit data
        public ActionResult EditDailyQuestionsDialog(string diseaseAssociations, string patientId, string patientFirstName, string patientMiddleName, string patientLastName, string patientDailyQaStatsId)
        {
            var Qa = Service.GetQuestionAnswer(diseaseAssociations, patientId);
            //Qa.qas.isNewQA = false;
            var patientDevicesModel = new PatientDevicesModel()
            {
                QA = (Qa == null ? null : Qa.qas),
                PatientDailyQAResponse = Service.GetFilledDailyQa(patientId, patientDailyQaStatsId, diseaseAssociations),
                patientFirstName = patientFirstName,
                patientMiddleName = patientMiddleName,
                patientLastName = patientLastName,
                patientId = patientId,
                InEdit = true
            };

            return PartialView("_DailyQuestions", patientDevicesModel);
        }

        public ActionResult AddDailyQuestions(PatientDevicesModel model)
        {
            // find if questions 1 thru 9 has been answered.
            int count = model.QA.Count(x => x.question.questionDesc != null);
            model.totalQACount = model.diseaseType == "7" ? model.totalQACount - 1 : model.totalQACount;   /// for mental illness (diseaseType = 7) last question is not required.
            if (count >= model.totalQACount)
            {
                QAAssoc[] qa = new QAAssoc[model.QA.Length];
                for (var i = 0; i < model.QA.Length; i++)
                {
                    string questionId = model.QA[i].question.id;
                    string answerId = model.QA[i].question.questionDesc;
                    QAAssoc qaas = new QAAssoc();
                    qaas.answers = new Answer[1];
                    Answer ans = new Answer();
                    Question qs = new Question();
                    qs.id = questionId;
                    qs.diseaseId = model.diseaseType;
                    ans.id = answerId;
                    qaas.question = qs;
                    qaas.answers[0] = ans;
                    qa[i] = qaas;
                }
                Service.AddDailyQuestions(model.patientId.ToString(), qa);

                // call the next screen                
                return DailyQuestionsDialog(model);
            }
            else
            {
                throw new Exception("Please answer all the questions.");
            }
        }

        [HttpPost]
        public object CalendarPatientDetails(bool isListView = false)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            object patients = null;
            try
            {

                List<string> pList = new List<string>();
                pList = (List<string>)Session["PatientToHide"];
                var model = new DailyReportModel()
                {
                    IsFullReport = isListView ? false : true,
                    IsListView = isListView,
                    PatientToHide = pList
                };
                var Patientdetails = Service.GetDailyReadingsReport(CurrentUser.UserId, CurrentUser.Roles[0], CurrentUser.Group, model.IsFullReport);
                if (Patientdetails != null)
                {
                    patients = serializer.Serialize(Patientdetails.patients);
                }
                else
                {
                    patients = Json(new object[] { new object() }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (WebException e)
            {
                Console.Out.WriteLine("Error : " + e.Message);
            }
            return patients;

        }

        [HttpPost]
        public HttpWebResponse AddManualReading(ManualReadingModel manualReading,string manualdatetime)
        {
            HttpResponse httpResponse = null;
            HttpWebResponse response = null;
            //StreamWriter writer = null;
            string responseText; int flag = 0;
            //string str_BPReading_Systolic = null, str_BPReading_Diastolic = null;
            //string str_SMReading_Fev = null, str_SMReading_Pef = null;
            //string str_A1CReading = null;
            bool a1cPresent = false;

            try
            {
                Hashtable manualReadingList = new Hashtable();

                if (!String.IsNullOrEmpty(manualReading.BPReading_Systolic))
                {
                    if (manualReading.BPReading_Systolic != "0")
                    {
                        //if (manualReading.BPReading.Contains("/"))
                        //{
                        //str_BPReading_Systolic = manualReading.BPReading.Substring(0, (manualReading.BPReading.IndexOf("/")));
                        //str_BPReading_Diastolic = manualReading.BPReading.Substring(((manualReading.BPReading.IndexOf("/")) + 1), (manualReading.BPReading.Length - ((manualReading.BPReading.IndexOf("/")) + 1)));
                        manualReadingList.Add("systolic", manualReading.BPReading_Systolic);
                        //manualReadingList.Add("diastolic", str_BPReading_Diastolic);
                        //}
                        //else
                        //{
                        //    throw new ArgumentException("Please enter Diastolic reading after '/'.");
                        //}
                    }
                    else
                    {
                        throw new ArgumentException("Please enter reading value more than '0'.");
                    }
                }
                else
                {
                    //manualReadingList.Add("systolic", "0");
                    //manualReadingList.Add("diastolic", null);
                    flag++;
                }
                if (!String.IsNullOrEmpty(manualReading.BPReading_Diastolic))
                {
                    if (manualReading.BPReading_Diastolic != "0")
                    {
                        manualReadingList.Add("diastolic", manualReading.BPReading_Diastolic);
                    }
                    else
                    {
                        throw new ArgumentException("Please enter the reading value more than '0'.");
                    }
                }
                else
                {
                    flag++;
                }

                if (!String.IsNullOrEmpty(manualReading.BPReading_PR))
                {
                    if (!String.IsNullOrEmpty(manualReading.BPReading_Systolic))
                    {
                        if (!String.IsNullOrEmpty(manualReading.BPReading_Diastolic))
                        {
                            if (manualReading.BPReading_PR != "0")
                            {
                                manualReadingList.Add("pulse", manualReading.BPReading_PR);
                            }
                            else
                            {
                                throw new ArgumentException("Please enter the reading value more than '0'.");
                            }
                        }
                        else
                        {
                            throw new ArgumentException("Please enter Diastolic reading.");
                        }
                    }
                    else
                    {
                        throw new ArgumentException("Please enter Systolic reading.");
                    }
                }
                else
                {
                    flag++;
                }
                if (!String.IsNullOrEmpty(manualReading.POReading))
                {
                    if (manualReading.POReading != "0")
                    {
                        manualReadingList.Add("oxygen", manualReading.POReading);
                    }
                    else
                    {
                        throw new ArgumentException("Please enter the reading value more than '0'.");
                    }
                }
                else
                {
                    flag++;
                }
                if (!String.IsNullOrEmpty(manualReading.BGReading))
                {
                    if (Convert.ToInt32(manualReading.BGReading) > 29)
                    {
                        manualReadingList.Add("glucose", manualReading.BGReading);
                    }
                    else
                    {
                        throw new ArgumentException("Please enter the reading value more than '29'.");
                    }
                }
                else
                {
                    flag++;
                }
                if (!String.IsNullOrEmpty(manualReading.TSReading))
                {
                    if (manualReading.TSReading != "0")
                    {
                        manualReadingList.Add("temperature", manualReading.TSReading);
                    }
                    else
                    {
                        throw new ArgumentException("Please enter the reading value more than '0'.");
                    }
                }
                else
                {
                    flag++;
                }
                if (!String.IsNullOrEmpty(manualReading.WSReading))
                {
                    if (manualReading.WSReading != "0")
                    {
                        manualReadingList.Add("weight", manualReading.WSReading);
                    }
                    else
                    {
                        throw new ArgumentException("Please enter the reading value more than '0'.");
                    }
                }
                else
                {
                    flag++;
                }

                if (!String.IsNullOrEmpty(manualReading.SMReading_Fev))
                {
                    if (manualReading.SMReading_Fev != "0")
                    {
                        manualReadingList.Add("fev1Best", manualReading.SMReading_Fev);
                    }
                    else
                    {
                        throw new ArgumentException("Please enter the reading value more than '0'.");
                    }
                }
                else
                {
                    flag++;
                }
                if (!String.IsNullOrEmpty(manualReading.SMReading_Pef))
                {
                    if (manualReading.SMReading_Pef != "0")
                    {
                        manualReadingList.Add("pefBest", manualReading.SMReading_Pef);
                    }
                    else
                    {
                        throw new ArgumentException("Please enter reading value more than '0'.");
                    }
                }
                else
                {
                    flag++;
                }
                // checking the A1C reading ...
                if (String.IsNullOrEmpty(manualReading.A1CReading))
                {
                    a1cPresent = false;
                }
                else
                {
                    a1cPresent = true;
                }

                manualReadingList.Add("activity", "1");
                // manualReadingList.Add("measurementTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                manualReadingList.Add("measurementTime", Convert.ToDateTime(manualReading.Measurement_Time).ToString("yyyy-MM-dd HH:mm:ss"));
                manualReadingList.Add("manualReading", "true");

                if (flag == 9 && a1cPresent == false )
                {
                    throw new ArgumentException("Please enter atleast one reading.");
                }

                String patientid = System.Convert.ToBase64String((System.Text.Encoding.UTF8.GetBytes(manualReading.PartyId)));
                if (flag < 9)
                {
                    List<KeyValuePair<string, string>> keyValuePairs = new List<KeyValuePair<string, string>>();

                    //var request = WebRequest.Create(Properties.Settings.ManualReading);
                    //Uri address = new Uri("http://localhost:8080/MyvitalzDataAggregatorWeb/DataAggregatorServlet");
                    Uri address = new Uri(System.Configuration.ConfigurationManager.AppSettings["ManualReadingUrl"]);

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(address);
                    request.Method = "POST";
                    request.ContentType = "application/x-www-form-urlencoded";

                    StringBuilder data = new StringBuilder();
                    data.Append("MobileNurse=true");
                    data.Append("&PatientID=" + patientid);
                    //data.Append("&DeviceType=" + manualReading.getDeviceType());
                    var jsonDeviceReading = JsonConvert.SerializeObject(manualReadingList);
                    data.Append("&DeviceReading=" + jsonDeviceReading);

                    string kvp_string = data.ToString();

                    byte[] bytedata = Encoding.UTF8.GetBytes(kvp_string);
                    request.ContentLength = bytedata.Length;

                    Stream requestStream = request.GetRequestStream();
                    requestStream.Write(bytedata, 0, bytedata.Length);
                    requestStream.Close();

                    //-----------------------------

                    using (response = (HttpWebResponse)request.GetResponse())
                    {
                        StreamReader streamReader = new StreamReader(response.GetResponseStream());
                        responseText = streamReader.ReadToEnd();
                        //httpResponse.Write(responseText);
                    }
                }
                // if a1c data is present, call the new service to post this reading
                if (a1cPresent)
                {
                    responseText = (string)saveBloodHbA1cReading(manualReading.PartyId, manualReading.A1CReading);
                    System.Console.Write(responseText);
                }
                // log a care-time entry for manual data ...
                AddCareTimeForManualEntry(manualReading.PartyId);
            }
            catch (WebException e)
            {
                System.Console.Write(e);
            }
            finally { if (httpResponse != null) httpResponse.Close(); }

            return response;
        }

        [HttpPost]
        public object PatientReadingDetails(string patientId, string doctorId, string fromDate, string toDate, int page, int pageSize)
        {

            object result = "";
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["PatientReadingUrl"] + "readings" + "");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"patientId\":\"" + patientId + "\"," +
                                    "\"doctorId\":\"" + doctorId + "\"," +
                                    "\"fromDate\":\"" + fromDate + "\"," +
                                    "\"toDate\":\"" + toDate + "\"," +
                                    "\"page\":\"" + page + "\"," +
                                    "\"pageSize\":\"" + pageSize + "\"}";
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
            }
            catch
            {
            }
            return result;
        }

        [HttpPost]
        public object ReadmissionSaved(string ReadmissionDate, string partyid)
        {
            //partyid = "1102";
            object result = "";
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["ReadmissionSavedUrl"]);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"date\":\"" + DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss tt") + "\"," +
                                    "\"patient\":\"" + partyid.ToString() + "\"," +
                                    "\"type\":\"readmission\"," +
                                    "\"rea\": {" +
                                    "\"readmissionDate\":\"" + ReadmissionDate + "\"," +
                                    "\"endDate\":\"" + ReadmissionDate + "\" }}";
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
            }
            catch (WebException e)
            {
                return "{\"status\":\"" + e.Message + "\"}";
            }
            return result;
        }

        private void AddCareTimeWhenAssigningKitToPatient( string patientId )
        {
            // add an entry into the caretime table
            DateTime now = DateTime.Now;
            string callDate = DateTime.Now.ToString("yyyy-MM-dd");
            string callTime = now.Hour + ":" + now.Minute + ":" + now.Second;
            string duration = "0";
            string intervention = "CPT 99453";
            string notes = "Initial patient Set up/education";
            LogCareSaved(patientId, callDate, callTime, duration, notes, intervention);
        }

        private void AddCareTimeForViewingReadings(string patientId, string notes )
        {
            // add an entry into the caretime table
            DateTime now = DateTime.Now;
            string callDate = DateTime.Now.ToString("yyyy-MM-dd");
            string callTime = now.Hour + ":" + now.Minute + ":" + now.Second;
            string duration = "2";
            string intervention = "DATA";
            // don't log this if the user is the patient
            if (!CurrentUser.IsInRole(Role.Admin) && !CurrentUser.IsInRole(Role.Patient))
            {
                LogCareSaved(patientId, callDate, callTime, duration, notes, intervention);
            }
        }

        private void AddCareTimeForManualEntry(string patientId)
        {
            // add an entry into the caretime table
            DateTime now = DateTime.Now;
            string callDate = DateTime.Now.ToString("yyyy-MM-dd");
            string callTime = now.Hour + ":" + now.Minute + ":" + now.Second;
            string duration = "5";
            string intervention = "Technical";
            string notes = "Recorded Manual Reading for Patient";

            if (!CurrentUser.IsInRole(Role.Admin) && !CurrentUser.IsInRole(Role.Patient))
            {
                LogCareSaved(patientId, callDate, callTime, duration, notes, intervention);
            }
        }

        [HttpPost]
        public object LogCareSaved(string partyid, string logcareDate, string Calltime, string Duration, string Notes, string Intervention)
        {
            // partyid = "1102";
            string caredBy = CurrentUser.UserId.ToString();
            int hrs = Duration.AsInt() / 60;
            Duration = String.Format("{0:D2}:{1:D2}:{2,2}", Convert.ToString(Duration.AsInt() / 60).PadLeft(2,'0'), Convert.ToString(Duration.AsInt() - (hrs * 60)).PadLeft(2, '0'), "00");
            object result = "";
            string nts = Notes.Replace("\"", "\\\"");
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["LogCareSavedUrl"]);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"date\":\"" + DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss tt") + "\"," +
                                    "\"patient\":\"" + partyid.ToString() + "\"," +
                                    "\"type\":\"careTime\"," +
                                    "\"crt\": {" +
                                    "\"careTimeDate\":\"" + logcareDate + "\"," +
                                    "\"beginTime\":\"" + Calltime + "\"," +
                                    "\"duration\":\"" + Duration + "\"," +
                                    "\"interventionType\":\"" + Intervention + "\"," +
                                    "\"caredBy\":\"" + caredBy + "\"," +
                                    "\"notes\":\"" + nts + "\" }}";
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
            }
            catch (WebException e)
            {
                return "{\"status\":\"" + e.Message + "\"}";
            }
            return result;
        }

        [HttpPost]
        public string PauseMonitorSaved(string partyid, string startDate, string endDate, string reason, string description)
        {
            //partyid = "1102";

            string result = "";
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["PauseMonitorSavedUrl"]);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                     string json = "{\"date\":\"" + DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss zz") + "\"," +
                                    "\"patient\":\"" + partyid.ToString() + "\"," +
                                    "\"type\":\"pauseMonitoring\"," +
                                    "\"pm\": {" +
                                    "\"startDate\":\"" + startDate + "\"," +
                                    "\"endDate\":\"" + endDate + "\"," +
                                    "\"reason\":\"" + reason + "\"," +
                                    "\"description\":\"" + description + "\" }}";
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
            }
            catch (WebException e)
            {
                return "{\"status\":\"" + e.Message + "\"}";
            }
            return result;
        }

        [HttpPost]
        public object saveBloodHbA1cReading(string partyid, string reading)
        {
            //partyid = "1102";
            //call the restapi to delete the reading
            //using (var httpClient = new System.Net.Http.HttpClient())
            //{
            //    ////var url1 = "http://localhost:8080/MyvitalzDataWeb/rest/data";
            //    var url = System.Configuration.ConfigurationManager.AppSettings["ManageDataUrl"];
            //    var rsrc = url + "/" + deviceType + "/" + patientId + "/" + reading1 + "/" + reading2 + "/" + mtime;
            //    Console.Out.WriteLine("Url: " + rsrc);
            //    var content = new System.Net.Http.StringContent(string.Empty, Encoding.UTF8, "application/json");
            //    response = await httpClient.DeleteAsync(rsrc);
            //    rr = await response.Content.ReadAsStringAsync();
            //    Console.Out.WriteLine("Response : " + response.StatusCode);
            //    Console.Out.WriteLine("RR : " + rr);
            //    return Content(rr);
            //}

            object result = "";
            try
            {
                var url = System.Configuration.ConfigurationManager.AppSettings["ManageDataUrl"];
                var rsrc = url + "/A1C/reading";
                Console.Out.WriteLine("Url: " + rsrc);
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(rsrc);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"dateTime\":\"" + DateTime.Now.ToString("yyyy-MM-dd") + "\"," +
                                   "\"patientId\":\"" + partyid.ToString() + "\"," +
                                   "\"reading\":\"" + reading + "\" }}";
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
            }
            catch (WebException e)
            {
                return "{\"status\":\"" + e.Message + "\"}";
            }
            return result;
        }

        [HttpPost]
        public string ReadingGridDetails(string patientId,string devicetype,string fromdate, string todate)
        {
            string result = "";
            try
            {               

                string rt;
                WebRequest request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetReadingDetails"] + devicetype+"/"+patientId+"/"+fromdate+"/"+todate+"");
                WebResponse response = request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                rt = reader.ReadToEnd();
                Console.WriteLine(rt);
                reader.Close();
                response.Close();
                return rt;

            }
            catch (WebException e)
            {
                Console.Out.WriteLine("Error : " + e.Message);
            }
            return result;
        }

        [HttpGet]
        public string getHbA1cHistoricalReadings(string patientId)
        {
            string result = "";
            try
            {

                string rt;
                WebRequest request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetReadingDetails"] + "A1C" + "/" + patientId + "");
                WebResponse response = request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                rt = reader.ReadToEnd();
                Console.WriteLine(rt);
                reader.Close();
                response.Close();
                return rt;

            }
            catch (WebException e)
            {
                Console.Out.WriteLine("Error : " + e.Message);
            }
            return result;
        }

        [HttpGet]
        public string getHbA1cHistoricalReadings(string patientId, string fromdate, string todate)
        {
            string result = "";
            try
            {

                string rt;
                WebRequest request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetReadingDetails"] + "A1C" + "/" + patientId + "/" + fromdate + "/" + todate  + "");
                WebResponse response = request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                rt = reader.ReadToEnd();
                Console.WriteLine(rt);
                reader.Close();
                response.Close();
                return rt;

            }
            catch (WebException e)
            {
                Console.Out.WriteLine("Error : " + e.Message);
            }
            return result;
        }

        [HttpGet]
        public string getPillsyHistoricalReadings(string patientId, string deviceId, string devicetype, string fromdate, string todate)
        {
            string result = "";
            try
            {

                string rt;
                WebRequest request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetReadingDetails"] + devicetype + "/" + patientId + "/" + fromdate + "/" + todate + "/" + deviceId + "");
                WebResponse response = request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                rt = reader.ReadToEnd();
                Console.WriteLine(rt);
                reader.Close();
                response.Close();
                return rt;

            }
            catch (WebException e)
            {
                Console.Out.WriteLine("Error : " + e.Message);
            }
            return result;
        }

        [HttpGet]
        public string GetPauseMonitorReadingForToday(string patientId)
        {
            string result = "";
            string devicetype = "PMN";
            string todayDate = DateTime.Now.ToString("yyyy-MM-dd");
            try
            {

                string rt;
                WebRequest request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetReadingDetails"] + devicetype + "/" + patientId + "/" + todayDate + "");
                WebResponse response = request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                rt = reader.ReadToEnd();
                Console.WriteLine(rt);
                reader.Close();
                response.Close();
                return rt;

            }
            catch (WebException e)
            {
                Console.Out.WriteLine("Error : " + e.Message);
            }
            return result;
        }

        [HttpGet]
        public string GetPillsyReadingForToday(string patientId)
        {
            string result = "";
            string devicetype = "MED";
            string todayDate = DateTime.Now.ToString("yyyy-MM-dd");
            string nextDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
            try
            {

                string rt;
                WebRequest request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetReadingDetails"] + devicetype + "/" + patientId + "/" + todayDate +"/"+ nextDate);
                WebResponse response = request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                rt = reader.ReadToEnd();
                Console.WriteLine(rt);
                reader.Close();
                response.Close();
                return rt;
            }
            catch (WebException e)
            {
                Console.Out.WriteLine("Error : " + e.Message);
            }
            return result;
        }

        [HttpGet]
        public string GetCovidReadingForToday(string patientId)
        {
            string result = "";
            string devicetype = "CVD";
            string todayDate = DateTime.Now.ToString("yyyy-MM-dd");
            string nextDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
            try
            {

                string rt;
                WebRequest request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetReadingDetails"] + devicetype + "/" + patientId + "/" + todayDate + "/" + nextDate);
                WebResponse response = request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                rt = reader.ReadToEnd();
                Console.WriteLine(rt);
                reader.Close();
                response.Close();
                return rt;
            }
            catch (WebException e)
            {
                Console.Out.WriteLine("Error : " + e.Message);
            }
            return result;
        }

        [HttpPost]
        public object DeleteGridDetails(string deleterecord)
        {
            object result = "";
            try
            {

                string rt;
                WebRequest request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetReadingDetails"] + deleterecord);
                request.Method = "DELETE";
                WebResponse response = request.GetResponse();
                
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                rt = reader.ReadToEnd();
                Console.WriteLine(rt);
                reader.Close();
                response.Close();
                return rt;

            }
            catch (WebException e)
            {
                Console.Out.WriteLine("Error : " + e.Message);
            }
            return result;


        }


        
        [HttpGet]
        public string ViewCovidReading_QA(string patientId, string id)
        {
            string result = "";
            try
            {
                string rt;
                WebRequest request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetReadingDetails"]+ "covid/"+ patientId + "/" + id);
                WebResponse response = request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                rt = reader.ReadToEnd();
                Console.WriteLine(rt);
                reader.Close();
                response.Close();
                return rt;

            }
            catch (WebException e)
            {
                Console.Out.WriteLine("Error : " + e.Message);
            }
            return result;
        }
        
        public ActionResult RecordFeelings(Feeling feeling)
        {
            feeling.updateUserId = CurrentUser.UserId;
            Service.RecordFeelings(feeling);
            return BaseGetDeviceQuadrant(feeling.partyId);
        }

        public ActionResult OpenFeelingsDialog(string patientId)
        {
            return PartialView("_FeelingsDialog", new Feeling() { partyId = patientId });
        }

        public ActionResult RefreshCharts(string patientId, DateTime? fromDate, DateTime? toDate)
        {
            var model = GetDeviceModel(patientId, fromDate, toDate);
            return PartialView("_Charts", model);
        }

        public ActionResult RefreshReading(string patientId, string deviceType, DateTime? fromDate, DateTime? toDate)
        {
            var model = GetDeviceModel(patientId, deviceType, fromDate, toDate);
            return PartialView(model.DeviceView, model);
        }

        public ActionResult RefreshSoapNote(string patientId, string patientFullName, string soapNoteId, DateTime? fromDate, DateTime? toDate)
        {
            var model = GetSoapNoteModel(patientId, patientFullName, soapNoteId, fromDate, toDate);
            return PartialView("_SoapNotes", model);
        }

        public ActionResult RefreshSoapNoteForDR(string patientId, string patientFullName, string soapNoteId, DateTime? fromDate, DateTime? toDate)
        {
            var model = GetSoapNoteModel(patientId, patientFullName, soapNoteId, fromDate, toDate);
            return PartialView("_SoapNotes", model);
        }

        public ActionResult OpenReading(string patientId, string deviceType, DateTime? fromDate, DateTime? toDate)
        {
            var model = GetDeviceModel(patientId, deviceType, fromDate, toDate);
            return PartialView("_Device", model);
        }

        public ActionResult OpenPillsyReading(string patientId, string deviceId, string deviceType, DateTime? fromDate, DateTime? toDate)
        {
            var model = GetPillsyDeviceModel(patientId, deviceId, deviceType, fromDate, toDate);
            return PartialView("_Device", model);
        }

        public string BaseSaveEhr(EHRDetailsModel model, string userId)
        {
            var stringBuilder = new StringBuilder();

            if (model.Details.ehr.partyType == "P")
            {
                ValidatePatient(model.Details.ehr, stringBuilder);
            }
            else
            {
                ValidateOtherUsers(model.Details.ehr, stringBuilder);
            }

            if (model.Details.doctors != null)
            {
                for (var index = 0; index < model.Details.doctors.Length; ++index)
                {
                    if (string.IsNullOrWhiteSpace(model.Details.doctors[index].partyID)
                            || model.Details.doctors[index].partyID == "0")
                    {
                        stringBuilder.AppendLine(string.Format("Doctor {0} was not entered properly.", (index + 1)));
                    }
                }
                var list = model.Details.doctors.Where(p => p.status != Operation.Delete)
                            .GroupBy(p => new { p.partyID, p.firstName })
                            .Where(grp => grp.Count() > 1).Select(bp => bp.Key.firstName).ToList();
                if (list.Any() == true)
                {
                    throw new ArgumentException(string.Format("You cannot add the same Doctor  ({0}) more than once", string.Join(",", list)));
                }

                for (var index = 0; index < model.Details.doctors.Length; ++index)
                {
                    if (string.IsNullOrEmpty(model.Details.doctors[index].status))
                    {
                        model.Details.doctors[index].status = Operation.Update;
                    }
                }
            }
            if (model.Details.careGivers != null)
            {
                for (var index = 0; index < model.Details.careGivers.Length; ++index)
                {
                    if (string.IsNullOrWhiteSpace(model.Details.careGivers[index].partyID)
                            || model.Details.careGivers[index].partyID == "0")
                    {
                        stringBuilder.AppendLine(string.Format("Caregiver{0} was not entered properly.", (index + 1)));
                    }
                }
                var list = model.Details.careGivers.Where(p => p.status != Operation.Delete)
                            .GroupBy(p => new { p.partyID, p.firstName })
                            .Where(grp => grp.Count() > 1).Select(bp => bp.Key.firstName).ToList();
                if (list.Count > 0)
                {
                    throw new ArgumentException(string.Format("You cannot add the same Caregiver({0}) more than once", string.Join(",", list)));
                }

                for (var index = 0; index < model.Details.careGivers.Length; ++index)
                {
                    if (string.IsNullOrEmpty(model.Details.careGivers[index].status))
                    {
                        model.Details.careGivers[index].status = Operation.Update;
                    }
                }
            }
            if (model.Details.familyMembers != null)
            {
                for (int index = 0; index < model.Details.familyMembers.Length; ++index)
                {
                    if (string.IsNullOrWhiteSpace(model.Details.familyMembers[index].partyID)
                            || model.Details.familyMembers[index].partyID == "0")
                    {
                        stringBuilder.AppendLine(string.Format("Clinician {0} was not entered properly.", (index + 1)));
                    }
                }
                var list = model.Details.familyMembers.Where(p => p.status != Operation.Delete)
                            .GroupBy(p => new { p.partyID, p.firstName })
                            .Where(grp => grp.Count() > 1).Select(bp => bp.Key.firstName).ToList();
                if (list.Count > 0)
                {
                    throw new ArgumentException(string.Format("You cannot add the same Clinician ({0}) more than once", string.Join(",", (IEnumerable<string>)list)));
                }

                for (var index = 0; index < model.Details.familyMembers.Length; ++index)
                {
                    if (string.IsNullOrEmpty(model.Details.familyMembers[index].status))
                    {
                        model.Details.familyMembers[index].status = Operation.Update;
                    }
                }
            }
            if (stringBuilder.Length != 0)
            {
                throw new ArgumentException(stringBuilder.ToString());
            }

            //if (model.Details.ehr.group != null)
            //{
            //    model.Details.ehr.group = "-";                
            //}

            if (model.Details.ehr.group == null)
            {
                if (model.SelectedGT != null)
                {
                    model.Details.ehr.group = model.SelectedGT;
                }
            }

            if (model.Details.ehr.ssn != null)
            {
                model.Details.ehr.ssn = model.Details.ehr.ssn.Replace("-", string.Empty);
            }
            if (model.Details.ehr.taxId != null)
            {
                model.Details.ehr.taxId = model.Details.ehr.taxId.Replace("-", string.Empty);
            }

            if (model.Details.preferences != null)
            {
                for (int index = 0; index < model.Details.preferences.Length; ++index)
                {
                    model.Details.preferences[index].preferenceKeyType = "MRE";
                }
            }

            if (model.DiseasesPicked != null)
            {
                List<DiseaseAssociations> diseaseAssociationList = new List<DiseaseAssociations>();
                for (int index = 0; index < model.DiseasesPicked.Length; ++index)
                {
                    if (model.DiseasesPicked[index].Equals("true"))
                    {
                        DiseaseAssociations item = new DiseaseAssociations();
                        item.diseaseTypeId = (index + 1).ToString();
                        item.partyId = model.Details.ehr.id;
                        item.updateUser = userId;
                        diseaseAssociationList.Add(item);
                    }
                }
                model.Details.diseaseAssociations = diseaseAssociationList.ToArray();
            }

            if ((model.Details.ehr.partyType == "D" || model.Details.ehr.partyType == "N" || model.Details.ehr.partyType == "C") && CurrentUser.IsInRole(Role.GroupAdmin) == true)
            {
                List<SubGroupWrapper> newSubGroupList = new List<SubGroupWrapper>();

                SubGroupWrapper item = new SubGroupWrapper();
                item.primarySubGroupId = "0";
                item.secondarySubGroupId = "0";
                item.patientId = model.Details.ehr.id;
                item.groupType = CurrentUser.Group;
                newSubGroupList.Add(item);
                model.Details.subGroupType = newSubGroupList.ToArray();
            }

            //if (CurrentUser.IsInRole(Role.Admin) == true)
            if ((model.Details.ehr.partyType == "D" || model.Details.ehr.partyType == "N" || model.Details.ehr.partyType == "C") && CurrentUser.IsInRole(Role.Admin) == true)
            {
                List<SubGroupWrapper> newSubGroupList = new List<SubGroupWrapper>();
                SubGroupWrapper item = new SubGroupWrapper();
                item.primarySubGroupId = "0";
                item.secondarySubGroupId = "0";
                item.patientId = model.Details.ehr.id;
                item.groupType = model.Details.ehr.homeGroup;
                newSubGroupList.Add(item);
                model.Details.subGroupType = newSubGroupList.ToArray();
            }

            if (model.PrimaryPicked != null || model.SecondaryPicked != null)
            {
                List<SubGroupWrapper> newSubGroupList = new List<SubGroupWrapper>();
                string primarySubGroupId = null;
                //string secondarySubGroupId = null;

                for (int x = 0; x < model.PrimaryPicked.Length; ++x)
                {
                    if (model.PrimaryPicked[x].Equals("true"))
                    {
                        primarySubGroupId = model.PrimarySubGroupId[x].ToString();

                        if (model.SecondaryPicked[x] != "0")
                        {
                            if (model.SecondaryPicked[x].Equals("true"))
                            {
                                SubGroupWrapper item = new SubGroupWrapper();
                                item.primarySubGroupId = model.PrimarySubGroupId[x].ToString();
                                item.secondarySubGroupId = model.SecondarySubGroupId[x].ToString();
                                item.patientId = model.Details.ehr.id;
                                if (model.SelectedGT != null)
                                { item.groupType = model.SelectedGT; }
                                else { item.groupType = model.Details.ehr.group; }
                                newSubGroupList.Add(item);
                            }
                            else
                            {
                                SubGroupWrapper item = new SubGroupWrapper();
                                item.primarySubGroupId = model.PrimarySubGroupId[x].ToString();
                                item.secondarySubGroupId = "0";
                                item.patientId = model.Details.ehr.id;
                                if (model.SelectedGT != null)
                                { item.groupType = model.SelectedGT; }
                                else { item.groupType = model.Details.ehr.group; }
                                newSubGroupList.Add(item);
                            }
                        }
                        else
                        {
                            SubGroupWrapper item = new SubGroupWrapper();
                            item.primarySubGroupId = model.PrimarySubGroupId[x].ToString();
                            item.secondarySubGroupId = "0";
                            item.patientId = model.Details.ehr.id;
                            if (model.SelectedGT != null)
                            { item.groupType = model.SelectedGT; }
                            else { item.groupType = model.Details.ehr.group; }
                            newSubGroupList.Add(item);
                        }
                    }
                    else if (model.PrimaryPicked[x].Equals("0"))
                    {
                        if (model.SecondaryPicked[x].Equals("true"))
                        {
                            SubGroupWrapper item = new SubGroupWrapper();
                            item.primarySubGroupId = primarySubGroupId;
                            item.secondarySubGroupId = model.SecondarySubGroupId[x].ToString();
                            item.patientId = model.Details.ehr.id;
                            if (model.SelectedGT != null)
                            { item.groupType = model.SelectedGT; }
                            else { item.groupType = model.Details.ehr.group; }
                            newSubGroupList.Add(item);
                        }
                    }
                }

                //int counter = 0;
               // string psgId = null;

                List<string> psgIdLst = new List<string>();
                List<SubGroupWrapper> sgwLst = new List<SubGroupWrapper>();
                List<SubGroupWrapper> primeSGLst = new List<SubGroupWrapper>();
                string pValue = null;
                //psgIdDistinctLst = psgIdLst.Distinct().ToList();

                for (int i = 0; i < newSubGroupList.Count; i++)
                {
                    for (int j = 0; j < newSubGroupList.Count; j++)
                    {
                        if (newSubGroupList[i].primarySubGroupId == newSubGroupList[j].primarySubGroupId)
                        {
                            psgIdLst.Add(newSubGroupList[i].primarySubGroupId);
                            primeSGLst.Add(newSubGroupList[i]);
                        }
                    }

                    if (pValue != newSubGroupList[i].primarySubGroupId)
                    {
                        if (psgIdLst.Count > 1)
                        {
                            List<SubGroupWrapper> pLst = newSubGroupList.Where(p => p.secondarySubGroupId != "0").ToList();
                            foreach (SubGroupWrapper sgw in pLst)
                            {
                                if (sgw.secondarySubGroupId != "0")
                                {
                                    sgwLst.Add(sgw);
                                    pValue = sgw.primarySubGroupId;
                                }
                            }
                        }
                        else
                        {
                            SubGroupWrapper item = new SubGroupWrapper();
                            item.primarySubGroupId = newSubGroupList[i].primarySubGroupId;
                            item.secondarySubGroupId = newSubGroupList[i].secondarySubGroupId;
                            item.patientId = newSubGroupList[i].patientId;
                            item.groupType = newSubGroupList[i].groupType;

                            List<SubGroupWrapper> pLst = newSubGroupList.Where(p => p.primarySubGroupId == newSubGroupList[i].primarySubGroupId)
                                                                                .Where(q => q.secondarySubGroupId == newSubGroupList[i].secondarySubGroupId).ToList();

                            foreach (SubGroupWrapper sgw in pLst)
                            {
                                sgwLst.Add(sgw);
                            }
                        }
                    }
                    psgIdLst.Clear();
                    primeSGLst.Clear();
                }

                model.Details.subGroupType = sgwLst.ToArray();
            }

            if (model.DailyQuestionsValue != null || model.FeelingsValues != null || model.NotificationsValue != null)
            {
                List<PatientFeatures> patientFeaturesList = new List<PatientFeatures>();

                PatientFeatures pf = new PatientFeatures();
                pf.partyId = model.Details.ehr.id;
                pf.dailyQuestionsValue = model.DailyQuestionsValue == null ? "0" : model.DailyQuestionsValue;
                pf.feelingsValue = model.FeelingsValues;
                pf.notificationsValue = model.NotificationsValue == null ? "0" : model.NotificationsValue;
                pf.partyType = model.Details.ehr.partyType;
                pf.updateUser = userId;
                patientFeaturesList.Add(pf);
                model.Details.patientFeatures = patientFeaturesList.ToArray();
            }
            // device and kits are save in this call ...
            model.Details.ehr.id = Service.SaveEhrDetail(model.Details, userId);

            if (model.Associations != null
                && model.Associations.Length > 0)
            {
                for (var index = 0; index < model.Associations.Count(); ++index)
                {
                    if (string.IsNullOrWhiteSpace(model.Associations[index].xrefType))
                    {
                        throw new ArgumentException(string.Format("Association {0} cannot be empty", (index + 1)));
                    }
                    if (string.IsNullOrWhiteSpace(model.Associations[index].partyID) == true)
                    {
                        model.Associations[index].partyID = model.Details.ehr.id;
                    }
                }
                var associationsList = model.Associations.Where(p => p.status != Operation.Delete)
                    .GroupBy(a => new { a.xrefType, a.xrefTypeDesc })
                    .Where(grp => grp.Count() > 1).Select(bp => bp.Key.xrefTypeDesc).ToList();
                if (associationsList.Count > 0)
                {
                    throw new ArgumentException(
                        string.Format("You cannot add the same association type({0}) more than once",
                            string.Join(",", associationsList)));
                }
                Service.SaveAssociations(model.Associations, userId);
            }
            else
            {
                /*
                 * added this logic to register the kit with STEL 
                 *
                 * -sethu - 06/25/2019
                 */
                if (model.Details.kitDetails != null && !(string.IsNullOrWhiteSpace(model.Details.kitDetails.kitId)))
                {
                    // if the kitid is 5 char long, then assume it is STEL's and register the kit with STEl.
                    //
                    // modified the logic with the introduction of FORA telehealth care devices into our system
                    // -sethu - 2020/03/22
                    //
                    // all STEL devices are prefixed with CV###-<6digits of esn>
                    // all FORA telehealth devices are prefixed with F##-<serial# of the device>
                    //
                    if (model.Details.kitDetails.kitId.Length >= 5)
                    {
                        if (!model.Details.kitDetails.kitId.ToUpper().Contains("QUAL"))
                        {
                            try
                            {
                                char[] seperators = new char[] { '-' };
                                string[] kitIdParts = model.Details.kitDetails.kitId.Split(seperators);
                                string stelKitIdstart = "CV001";
                                string stelKitIdEnd = "CV999";
                                string foraKitIdStart = "F01";
                                string foraKitIdEnd = "F999";
                                Console.Out.WriteLine("Incoming KitID = " + model.Details.kitDetails.kitId);
                                if( kitIdParts.Length > 1 )
                                {
                                    string kitIdPrefix = kitIdParts[0];
                                    string kitId = kitIdParts[1];
                                    Console.Out.WriteLine("prefix = " + kitIdPrefix);
                                    Console.Out.WriteLine("kitId = " + kitId);
                                    // the device is either a STEL or a FORA care device
                                    if (kitIdPrefix != null && kitIdPrefix.Length >= 2 && kitIdPrefix.Substring(0, 2).Equals("CV", StringComparison.OrdinalIgnoreCase))
                                    {
                                        if (String.Compare(kitIdParts[0], stelKitIdstart) >= 0 && String.Compare(kitIdParts[0], stelKitIdEnd) <= 0)
                                        {
                                            RegisterWithSTEL(model, userId, kitId);
                                        }
                                    }
                                    else if (kitIdPrefix != null && kitIdPrefix.Substring(0, 1).Equals("F", StringComparison.OrdinalIgnoreCase))
                                    {
                                        if (String.Compare(kitIdParts[0], foraKitIdStart) >= 0 && String.Compare(kitIdParts[0], foraKitIdEnd) <= 0)
                                        {
                                            // add a patient to the FORA TeleHealth care ...
                                            RegisterWithFORA(model, kitId);
                                        }
                                    }
                                    else
                                    {
                                        // the kit prefix of CVxxx or Fxxx- can be changed when assigning the kit to a different group
                                        // so work with the kit ids instead of the prefix ...
                                        if( kitId.Length == 6 )
                                        {
                                            // this is a STEL kit, register with STEL
                                            RegisterWithSTEL(model, userId, kitId);
                                        }
                                        else if(kitId.Length == 16 ) 
                                        {
                                            // this is a FORA kit ...
                                            RegisterWithFORA(model, kitId);
                                        }
                                        else //if (kitId.Length == 15 || kitId.Length == 4)
                                        {
                                            Console.Out.WriteLine("Assigning Cellular kit...");
                                        }
                                        //else
                                        //{
                                        //    // unknown kit ..
                                        //    Console.Out.WriteLine("Kit Id is one that we dont understand ... error out ...");
                                        //    throw new Exception("Invalid Kit ID " + kitIdPrefix + "-" + kitId);
                                        //}
                                    }
                                }
                                else
                                {
                                    string kitId = kitIdParts[0];
                                    Console.Out.WriteLine("No prefix for this kit.  The kit ID : " + kitId);
                                    // one more check to identify the two STEL trial kit that we have that are 6 digits long
                                    // do we need this or convert those two to use CV### format also
                                    // until then I will keep this logic
                                    // existing STEL kit id 1 = 3E8C54
                                    // second kit id = 827D78
                                    //add new kit id =E3B7CA - kitId.StartsWith("E3B7CA") 10/07/2020
                                    if (kitId.StartsWith("3E8C54") || kitId.StartsWith("827D78") || kitId.StartsWith("E3B7CA"))
                                    {
                                        RegisterWithSTEL(model, userId, kitId);
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                throw new ArgumentException(ex.Message);
                            }
                        }
                    }
                    else
                    {
                        if (model.Details.kitDetails.kitId.Length < 5)
                        {
                            throw new ArgumentException("Kit It must be atleast 5 character long");
                        }
                    }
                }
            }
            //var id = Service.SaveParty(model.Party, model.Preferences, userId);
            if (model.ParameterModel != null)
            {
                Service.SaveParameters(model.ParameterModel.Parameters, CurrentUser.UserId);
            }
            //
            // save the insurance information 
            if( model.Insurance != null)
            {
                MyVitalzRestApi.SaveInsurance(model.Details.ehr.id, model.Insurance, CurrentUser.UserId);
            }
            return model.Details.ehr.id;
        }

        private void RegisterWithSTEL(EHRDetailsModel model, string userId, string kitId)
        {
            StelProvider stel = new StelProvider();
            model.Details.kitDetails.kitId = kitId;  // reset the kid id in the model without the prefix
            stel.RegisterKit(model);
            Service.SaveAssociations(model.Associations, userId);
            Console.Out.WriteLine("Registered Kit with STEL ...");
        }

        private void RegisterWithFORA(EHRDetailsModel model, string kitId)
        {
            // add a patient to the FORA TeleHealth care ...
            ForaTeleHealthProvider fora = new ForaTeleHealthProvider();
            model.Details.kitDetails.kitId = kitId;  // reset the kid id in the model without the prefix
            string rsp = fora.AddPatientToForaTeleHealth(model);
            if (!rsp.Equals("success", StringComparison.OrdinalIgnoreCase))
            {
                throw new Exception(rsp);
            }
        }

        [HttpPost]
        public ActionResult BaseAddDoctor(EHRDetailsModel model)
        {
            if (model.Details.doctors != null
                    && model.Details.doctors.Count(p => p.status != Operation.Delete) == 10)
            {
                throw new ArgumentException("You cannot add more than 10 Doctor ");
            }
            model.Details.doctors = AddPatientParty("Doctor", model.Details.doctors, model.NewlyAddedPartyId);
            return PartialView("_Doctors", model);
        }

        [HttpPost]
        public ActionResult BaseRemoveDoctor(EHRDetailsModel model)
        {
            model.Details.doctors = BasePartyController.RemovePatientParty("Doctor", model.Details.doctors, model.IndexRemoved);
            return PartialView("_Doctors", model);
        }

        [HttpPost]
        public ActionResult BaseAddCaregiver(EHRDetailsModel model)
        {
            if (model.Details.careGivers != null
                && model.Details.careGivers.Count(p => p.status != Operation.Delete) == 5)
            {
                throw new ArgumentException("You cannot add more than 5 Caregivers");
            }
            model.Details.careGivers = AddPatientParty("Caregiver", model.Details.careGivers, model.NewlyAddedPartyId);
            return PartialView("_Caregivers", model);
        }

        [HttpPost]
        public ActionResult BaseRemoveCaregiver(EHRDetailsModel model)
        {
            model.Details.careGivers = BasePartyController.RemovePatientParty("Caregiver", model.Details.careGivers, model.IndexRemoved);
            return PartialView("_Caregivers", model);
        }

        [HttpPost]
        public ActionResult BaseAddClinician(EHRDetailsModel model)
        {
            if (model.Details.familyMembers != null &&
                model.Details.familyMembers.Count(p => p.status != Operation.Delete) == 10)
            {
                throw new ArgumentException("You cannot add more than 10 Clinicians");
            }
            model.Details.familyMembers = AddPatientParty("Clinician", model.Details.familyMembers, model.NewlyAddedPartyId);
            return PartialView("_Nurses", model);
        }

        [HttpPost]
        public ActionResult BaseRemoveClinician(EHRDetailsModel model)
        {
            model.Details.familyMembers = BasePartyController.RemovePatientParty("Clinician", model.Details.familyMembers, model.IndexRemoved);
            return PartialView("_Nurses", model);
        }

        [HttpPost]
        public ActionResult BaseAddAssociation(EHRDetailsModel model)
        {
            var list = new List<Xref>();
            if (model.Associations != null)
            {
                list.AddRange(model.Associations.ToList());
            }
            for (var index = 0; index < list.Count; ++index)
            {
                if (string.IsNullOrWhiteSpace(list[index].xrefType))
                {
                    throw new ArgumentException(string.Format("Association {0} cannot be empty", (index + 1)));
                }
            }
            PopulateAssociationTypes(model);
            list.Add(new Xref()
            {
                status = Operation.Add,
                partyID = model.Details.ehr.id
            });
            model.Associations = list.ToArray();
            return PartialView("_Associations", model);
        }

        [HttpPost]
        public ActionResult BaseRemoveAssociation(EHRDetailsModel model)
        {
            var list = model.Associations.ToList();
            if (model.Associations == null
                    || model.Associations.Any() == false)
            {
                throw new ArgumentException("There is no Association to remove");
            }
            PopulateAssociationTypes(model);
            if (string.IsNullOrWhiteSpace(list[model.IndexRemoved].id)
                || list[model.IndexRemoved].id == "0")
            {
                list.RemoveAt(model.IndexRemoved);
            }
            else
            {
                list[model.IndexRemoved].status = Operation.Delete;
            }
            model.Associations = list.ToArray();
            return PartialView("_Associations", model);
        }

        [HttpPost]
        public ActionResult AddDevice(EHRDetailsModel model)
        {
            if (model.Details.deviceDetails == null)
            {
                model.Details.deviceDetails = new Device[0];
            }
            var list = model.Details.deviceDetails.ToList();
            list.Add(new Device()
            {
                id = Guid.NewGuid().ToString(),
                kitId = model.CurrentKitId,
                status = Operation.Add,
                startDate = DateTime.Now.ToString("MM-dd-yyyy")
            });
            model.Details.deviceDetails = list.ToArray();
            model.GroupTypes = GetGroupTypes();
            return PartialView("_Devices", model);
        }

        [HttpPost]
        public ActionResult RemoveDevice(EHRDetailsModel model)
        {
            if (model.Details.deviceDetails == null)
            {
                throw new ArgumentException("There is no Device found.");
            }
            model.Details.deviceDetails[model.IndexRemoved].status = Operation.Delete;
            model.GroupTypes = GetGroupTypes();
            return PartialView("_Devices", model);
        }

        [HttpPost]
        public ActionResult AddKit(EHRDetailsModel model)
        {
            if (model.Details.kitDetails == null
                || string.IsNullOrWhiteSpace(model.Details.kitDetails.id) == true)
            {
                model.Details.kitDetails = new DeviceKit()
                {
                    id = Guid.NewGuid().ToString(),
                    status = Operation.Add,
                    groupId = model.Details.ehr.group
                };
            }
            model.GroupTypes = GetFilteredGroupTypes(true);
            return PartialView("_Devices", model);
        }

        [HttpPost]
        public ActionResult AssignKit(EHRDetailsModel model)
        {
            if (model.Details.kitDetails == null
                    || string.IsNullOrWhiteSpace(model.Details.kitDetails.kitId) == true)
            {
                throw new ArgumentException("You cannot add None as a Kit");
            }
            var kitAndDeviceDetails = Service.GetKit(model.Details.kitDetails.kitId);
            model.Details.kitDetails = kitAndDeviceDetails.deviceKit;
            model.Details.kitDetails.status = Operation.Update;
            model.Details.deviceDetails = kitAndDeviceDetails.deviceDetails;
            if (model.Details.deviceDetails != null
                    && model.Details.deviceDetails.Length > 0)
            {
                model.Details.kitDetails.id = model.Details.deviceDetails.First().kitId;
                model.Details.kitDetails.groupId = model.Details.ehr.group;
            }
            model.GroupTypes = GetGroupTypes();
            // add an logCareTime entry for reviewing patients feelings
            AddCareTimeWhenAssigningKitToPatient(model.Details.ehr.id);
            return PartialView("_Devices", model);
        }

        [HttpPost]
        public ActionResult ReturnKitToInventory(string partyId, string kitId, string stelPid)
        {
            if( kitId != null && !kitId.ToUpper().Equals("NULL") )
            {
                // unregister esn with STEL
                if (kitId.Length >= 5)
                {
                    if (!kitId.ToUpper().Contains("QUAL"))
                    {
                        try
                        {
                            char[] seperators = new char[] { '-' };
                            string[] kitIdParts = kitId.Split(seperators);
                            string stelKitIdstart = "CV001";
                            string stelKitIdEnd = "CV999";
                            string foraKitIdStart = "F01";
                            string foraKitIdEnd = "F999";
                            Console.Out.WriteLine("Incoming KitID = " + kitId);
                            if (kitIdParts.Length > 1)
                            {
                                string kitIdPrefix = kitIdParts[0];
                                string kit_Id = kitIdParts[1];
                                Console.Out.WriteLine("prefix = " + kitIdPrefix);
                                Console.Out.WriteLine("kitId = " + kit_Id);
                                // the device is either a STEL or a FORA care device
                                if (kitIdPrefix != null && kitIdPrefix.Length >= 2 && kitIdPrefix.Substring(0, 2).Equals("CV", StringComparison.OrdinalIgnoreCase))
                                {
                                    if (String.Compare(kitIdParts[0], stelKitIdstart) >= 0 && String.Compare(kitIdParts[0], stelKitIdEnd) <= 0)
                                    {
                                        deleteSTELPatient(kit_Id, stelPid);
                                    }
                                }
                                else if (kitIdPrefix != null && kitIdPrefix.Substring(0, 1).Equals("F", StringComparison.OrdinalIgnoreCase))
                                {
                                    if (String.Compare(kitIdParts[0], foraKitIdStart) >= 0 && String.Compare(kitIdParts[0], foraKitIdEnd) <= 0)
                                    {
                                        // do not delete the FORA TeleHealth care kit from FORA system...
                                        deleteForaPatient(kit_Id, partyId);
                                    }
                                }
                                else
                                {
                                    // The Prefix of CVxxx will be chanaged to another one when the kit is assigned to another group.
                                    // we have to deal with that also.
                                    // The only thing that is constant is the kit_id which will be 6 long for STEL kits and 16 long for FORA kits
                                    //
                                    // Not going to check the prefix, just work with the kit_id length
                                    if (kit_Id.Length == 6)
                                    {
                                        // this will be STEL kit, do the STEL logic here ...
                                        deleteSTELPatient(kit_Id, stelPid);
                                    }
                                    else if (kit_Id.Length == 16)
                                    {
                                        // this will be FORA Kit ...
                                        deleteForaPatient(kit_Id, partyId);
                                    }
                                    else //if (kit_Id.Length == 15 || kit_Id.Length == 4)
                                    {
                                        Console.Out.WriteLine("Returning Cellular kit...");
                                    }
                                    //else
                                    //{
                                    //    Console.Out.WriteLine("Kit Id is one that we dont understand ... error out ...");
                                    //    throw new Exception("Invalid Kit ID " + kitId);
                                    //}
                                }
                            }
                            else
                            {
                                string kitIdFull = kitIdParts[0];
                                Console.Out.WriteLine("No prefix for this kit.  The kit ID : " + kitIdFull);
                                // one more check to identify the two STEL trial kit that we have that are 6 digits long
                                // do we need this or convert those two to use CV### format also
                                // until then I will keep this logic
                                // existing STEL kit id 1 = 3E8C54
                                // second kit id = 827D78
                                //add new kit id =E3B7CA - kitId.StartsWith("E3B7CA") 10/07/2020
                                if (kitIdFull.StartsWith("3E8C54") || kitIdFull.StartsWith("827D78") || kitId.StartsWith("E3B7CA"))
                                {
                                    deleteSTELPatient(kitIdFull, stelPid);
                                }
                            }
                        }
                        catch (NotImplementedException nie)
                        {
                            Console.Out.WriteLine("FORA TeleHealth Patient is NOT removed from FORA system.");
                            Console.Out.WriteLine("Error : " + nie.Message);
                        }
                        catch (Exception ex)
                        {
                            throw new ArgumentException(ex.Message);
                        }
                    }
                }
                else
                {
                    if (kitId.Length < 5)
                    {
                        throw new ArgumentException("Kit It must be atleast 5 character long");
                    }
                }
            }
            // call myvitalz service to return kit
            Service.ReturnKitToInventory(partyId, CurrentUser.UserId);
            return new EmptyResult();
        }

        private void deleteForaPatient(string kitId, string partyid)
        {
            // decided ....
            // not delete the FORA TeleHealth care kit from FORA system...
            //ForaTeleHealthProvider fora = new ForaTeleHealthProvider();
            //string rsp = fora.RemoveKitFromTeleHealthPatient(kitId, partyId);

            ForaTeleHealthProvider fora = new ForaTeleHealthProvider();
            string result = fora.DeleteForaKite(kitId);

            string rsp = "success";
            Console.Out.WriteLine("Not Removing Kit from FORA TeleHealth Patient ...");
            if (!rsp.Equals("success", StringComparison.OrdinalIgnoreCase))
            {
                throw new Exception(rsp);
            }
        }

        private void deleteSTELPatient(string kitId, string stelPatientId)
        {
            // this will be STEL kit, do the STEL logic here ...
            if (stelPatientId != null && !stelPatientId.ToUpper().Equals("NULL"))
            {
                StelProvider stel = new StelProvider();
                stel.UnRegisterKit(kitId, stelPatientId);
                Console.Out.WriteLine("Removed Kit from STEL Patient...");
            }
            else
            {
                Console.Out.WriteLine("STEL Patient ID is NULL ");
            }
        }

        [HttpPost]
        public ActionResult GetKitSearchDialog()
        {
            var groups = GetFilteredGroupTypes(true);
            return PartialView("_KitSearchDialog", groups);
        }

        [HttpGet]
        public JsonResult GetAvailableKits(string groupId)
        {
            var kitIds = Service.GetKits(groupId);
            return Json(new { Kits = kitIds }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RemoveKit(EHRDetailsModel model)
        {
            if (model.Details.kitDetails == null)
            {
                throw new ArgumentException("There is no Kit found.");
            }
            if (model.Details.kitDetails.status == Operation.Add)
            {
                model.Details.kitDetails = null;
                model.Details.deviceDetails = null;
            }
            else
            {
                model.Details.kitDetails.status = Operation.Delete;

                //Mark the device also to delete
                if (model.Details.deviceDetails != null)
                {
                    foreach (var item in model.Details.deviceDetails)
                    {
                        if (item.status != Operation.Add)
                        {
                            item.status = Operation.Delete;
                        }
                    }
                }
            }
            model.GroupTypes = GetGroupTypes();
            return PartialView("_Devices", model);
        }

        public void ExportToExcel(string title, string deviceType, string patientId, DateTime? fromDate, DateTime? toDate)
        {
            var datatable = GetDatatable(deviceType, patientId, fromDate, toDate);
            ExportDatatableToExcel(title, datatable);
        }
        public void ExportToExcelMED(string title, string deviceType, string patientId, DateTime? fromDate, DateTime? toDate,string deviceId)
        {
            var datatable = GetDatatable(deviceType, patientId, fromDate, toDate);
            DataTable dt = datatable.Select("DeviceId = " + deviceId.ToString()).CopyToDataTable();
            dt.Columns.Remove("DeviceId");            
            ExportDatatableToExcel(title, dt);
        }

        public void ExportToCsv(string title, string deviceType, string patientId, DateTime? fromDate, DateTime? toDate)
        {
            var datatable = GetDatatable(deviceType, patientId, fromDate, toDate);
            ExportDatatableToCsv(title, datatable);
        }
        public void ExportToCsvMED(string title, string deviceType, string patientId, DateTime? fromDate, DateTime? toDate, string deviceId)
        {
            var datatable = GetDatatable(deviceType, patientId, fromDate, toDate);
            DataTable dt = datatable.Select("DeviceId = " + deviceId.ToString()).CopyToDataTable();
            dt.Columns.Remove("DeviceId");
            ExportDatatableToCsv(title, dt);
        }

        public ActionResult PrintReadings(string title, string deviceType, string patientId, DateTime? fromDate, DateTime? toDate)
        {
            var datatable = GetDatatable(deviceType, patientId, fromDate, toDate);
            return View("PrintReadings", new PrintModel()
            {
                DeviceType = deviceType,
                Title = title,
                Data = datatable
            });
        }

        public ActionResult PrintReadingsMED(string title, string deviceType, string patientId, DateTime? fromDate, DateTime? toDate, string deviceId)
        {
            var datatable = GetDatatable(deviceType, patientId, fromDate, toDate);
            DataTable dt = datatable.Select("DeviceId = " + deviceId.ToString()).CopyToDataTable();
            dt.Columns.Remove("DeviceId");
            return View("PrintReadings", new PrintModel()
            {
                DeviceType = deviceType,
                Title = title,
                Data = dt
            });
        }

        public ActionResult GetEmailDialog(string partyId)
        {
            var model = new EmailTemplateModel()
            {
                PartyId = partyId,
                From = string.Format("{0}<{1}>", CurrentUser.FullName, CurrentUser.Email)
            };
            return PartialView("_EmailDialog", model);
        }

        public ActionResult GetManualReadingDialog(string partyId)
        {
            var model = new ManualReadingModel()
            {
                PartyId = partyId
            };
            return PartialView("_ManualReadingDialog", model);
        }

        public ActionResult SoapNotesDialog(string partyId, string patientFullName)
        {
            string
                BPM_Device = null, BPM_Reading = null, BPM_MTime = null,
                BPM_PR_Device = null, BPM_PR_Reading = null, BPM_PR_MTime = null,
                POM_Device = null, POM_Reading = null, POM_MTime = null,
                TMM_Device = null, TMM_Reading = null, TMM_MTime = null,
                WSM_Device = null, WSM_Reading = null, WSM_MTime = null,
                BCM_Device = null, BCM_Reading = null, BCM_MTime = null,
                SPR_Device = null, SPR_Reading = null, SPR_MTime = null,
                MIL_Device = null, MIL_Reading = null, MIL_MTime = null,
                DBT_Device = null, DBT_Reading = null, DBT_MTime = null,

                FTM_Pain_Feeling = null, FTM_Pain_Desc = null, FTM_Pain_MTime = null,
                FTM_Mood_Feeling = null, FTM_Mood_Desc = null, FTM_Mood_MTime = null,
                FTM_Energy_Feeling = null, FTM_Energy_Desc = null, FTM_Energy_MTime = null,
                FTM_Breathing_Feeling = null, FTM_Breathing_Desc = null, FTM_Breathing_MTime = null,

                BPM = null, BPM_PR = null, POM = null, TMM = null, WSM = null, BCM = null, SPR = null,
                MIL = null, DBT = null, FTM_Pain = null, FTM_Mood = null, FTM_Energy = null, FTM_Breathing = null;

            Double WeightDiff = 0.0;

            string readings;
            var Qd = Service.GetQuadrantDetailsByPatient(partyId, CurrentUser.UserId);

            DateTime dt = DateTime.Today;
            //var Qd = Service.GetAggregateHistory(partyId, dt, dt);

            if ((Qd.bloodPressure != null) && !String.IsNullOrEmpty(Qd.bloodPressure.diastolic))
            {
                BPM_Device = "BP";
                BPM_Reading = Qd.bloodPressure.systolic + " / " + Qd.bloodPressure.diastolic + " mmHg";
                BPM_MTime = Qd.bloodPressure.measurementTime;
                BPM = BPM_Device + "," + BPM_Reading + "," + BPM_MTime + ";";
            }
            else
            { BPM = null; }

            if ((Qd.bloodPressure != null) && !String.IsNullOrEmpty(Qd.bloodPressure.pulseRate))
            {
                BPM_PR_Device = "Pulse Rate";
                BPM_PR_Reading = Qd.bloodPressure.pulseRate + " ppm";
                BPM_PR_MTime = Qd.bloodPressure.measurementTime;
                BPM_PR = BPM_PR_Device + "," + BPM_PR_Reading + "," + BPM_PR_MTime + ";";
            }
            else
            { BPM_PR = null; }

            if ((Qd.pulseOxiMeter != null) && !String.IsNullOrEmpty(Qd.pulseOxiMeter.oxygenReading))
            {
                //POM = "Oxygen" + Qd.pulseOxiMeter.oxygenReading + " %, " + Qd.pulseOxiMeter.measurementTime + ";\r\n ";
                POM_Device = "Oxygen";
                POM_Reading = Qd.pulseOxiMeter.oxygenReading + " %";
                POM_MTime = Qd.pulseOxiMeter.measurementTime;
                POM = POM_Device + "," + POM_Reading + "," + POM_MTime + ";";
            }
            else
            { POM = null; }

            if ((Qd.temperature != null) && !String.IsNullOrEmpty(Qd.temperature.temperatureFahrenheit))
            {
                //TMM = "Temperature" + Qd.temperature.temperatureFahrenheit + " F, " + Qd.temperature.measurementTime + ";\r\n ";
                TMM_Device = "Temperature";
                TMM_Reading = Qd.temperature.temperatureFahrenheit;
                TMM_MTime = Qd.temperature.measurementTime;
                TMM = TMM_Device + "," + TMM_Reading + "," + TMM_MTime + ";";
            }
            else
            { TMM = null; }

            if ((Qd.weighingScale != null) && !String.IsNullOrEmpty(Qd.weighingScale.weightReadingPound))
            {
                //WSM = "Weight" + Qd.weighingScale.weightReadingPound + " lb, " + Qd.weighingScale.measurementTime + ";\r\n ";
                WeightDiff = Qd.weighingScale.weightDifference;
                WSM_Device = "Weight";
                if (WeightDiff > 0)
                    WSM_Reading = Qd.weighingScale.weightReadingPound + " lb &" + Qd.weighingScale.weightDifference + " lb";
                else
                    WSM_Reading = Qd.weighingScale.weightReadingPound + " lb";
                WSM_MTime = Qd.weighingScale.measurementTime;
                WSM = WSM_Device + "," + WSM_Reading + "," + WSM_MTime + ";";
            }
            else
            { WSM = null; }

            if (Qd.bloodGlucose != null && Qd.bloodGlucose.glucoseReading != null)
            {
                //BCM = "Glucose" + Qd.bloodGlucose.glucoseReading + " mg/dL, " + Qd.bloodGlucose.measurementTime + ";\r\n "; SPR = null;
                BCM_Device = "Glucose";
                BCM_Reading = Qd.bloodGlucose.glucoseReading + " mg/dL";
                BCM_MTime = Qd.bloodGlucose.measurementTime;
                BCM = BCM_Device + "," + BCM_Reading + "," + BCM_MTime + ";";
            }
            else
            { BCM = null; }

            if ((Qd.asthmaMonitor != null) && (!String.IsNullOrEmpty(Qd.asthmaMonitor.fev1Best)))
            {
                //SPR = "Spirometer" + Qd.asthmaMonitor.fev1Best + "/" + Qd.asthmaMonitor.pefBest + " mlps, " + Qd.asthmaMonitor.measurementTime + ";\r\n ";
                SPR_Device = "Spirometer";
                SPR_Reading = Qd.asthmaMonitor.fev1Best + " mlps";
                SPR_MTime = Qd.asthmaMonitor.measurementTime;
                SPR = SPR_Device + "," + SPR_Reading + "," + SPR_MTime + ";";
            }
            else
            { SPR = null; }

            if (Qd.feeling != null && Qd.feeling.painReading != null)
            {
                FTM_Pain_Feeling = "Pain";
                FTM_Pain_Desc = Qd.feeling.painDesc;
                FTM_Pain_MTime = Qd.feeling.measurementTime;
                FTM_Pain = FTM_Pain_Feeling + "," + FTM_Pain_Desc + "," + FTM_Pain_MTime + ";";
            }
            else
            { FTM_Pain = null; }
            if (Qd.feeling != null && Qd.feeling.moodReading != null)
            {
                //FTM_Mood = "Mood" + Qd.feeling.moodDesc + ", " + Qd.feeling.measurementTime + ";\r\n ";
                FTM_Mood_Feeling = "Mood";
                FTM_Mood_Desc = Qd.feeling.moodDesc;
                FTM_Mood_MTime = Qd.feeling.measurementTime;
                FTM_Mood = FTM_Mood_Feeling + "," + FTM_Mood_Desc + "," + FTM_Mood_MTime + ";";
            }
            else
            { FTM_Mood = null; }
            if (Qd.feeling != null && Qd.feeling.energyReading != null)
            {
                //FTM_Energy = "Energy" + Qd.feeling.energyDesc + ", " + Qd.feeling.measurementTime + ";\r\n ";
                FTM_Energy_Feeling = "Energy";
                FTM_Energy_Desc = Qd.feeling.energyDesc;
                FTM_Energy_MTime = Qd.feeling.measurementTime;
                FTM_Energy = FTM_Energy_Feeling + "," + FTM_Energy_Desc + "," + FTM_Energy_MTime + ";";
            }
            else
            { FTM_Energy = null; }
            if (Qd.feeling != null && Qd.feeling.breathingReading != null)
            {
                //FTM_Breathing = "Breathing" + Qd.feeling.breathingDesc + ", " + Qd.feeling.measurementTime + ";\r\n ";
                FTM_Breathing_Feeling = "Breathing";
                FTM_Breathing_Desc = Qd.feeling.breathingDesc;
                FTM_Breathing_MTime = Qd.feeling.measurementTime;
                FTM_Breathing = FTM_Breathing_Feeling + "," + FTM_Breathing_Desc + "," + FTM_Breathing_MTime + ";";
            }
            else
            { FTM_Breathing = null; }

            if (Qd.mentalIllnessDailyQaScore != null && (Qd.mentalIllnessDailyQaScore[0].dailyQaScore != null) && !String.IsNullOrEmpty(Qd.mentalIllnessDailyQaScore[0].dailyQaScore))
            {
                MIL_Device = "PHQ9";
                MIL_Reading = Qd.mentalIllnessDailyQaScore[0].dailyQaScore + " - 27";
                MIL_MTime = Qd.mentalIllnessDailyQaScore[0].measurementTime;
                MIL = MIL_Device + "," + MIL_Reading + "," + MIL_MTime + ";";
            }
            else
            { MIL = null; }

            if (Qd.diabetesDailyQaScore != null && (Qd.diabetesDailyQaScore[0].dailyQaScore != null) && !String.IsNullOrEmpty(Qd.diabetesDailyQaScore[0].dailyQaScore))
            {
                DBT_Device = "Diabetes";
                DBT_Reading = Qd.diabetesDailyQaScore[0].dailyQaScore + " - 6";
                DBT_MTime = Qd.diabetesDailyQaScore[0].measurementTime;
                DBT = DBT_Device + "," + DBT_Reading + "," + DBT_MTime + ";";
            }
            else
            { DBT = null; }

            readings = BPM + BPM_PR + POM + TMM + WSM + BCM + SPR + FTM_Pain + FTM_Mood + FTM_Energy + FTM_Breathing + MIL + DBT;

            char[] splitter = { ';' };
            string[] deviceReadings = { };
            if (!string.IsNullOrEmpty(readings))
            {
                deviceReadings = readings.Split(splitter);
            }

            var model = new SoapNotesModel()
            {
                patientId = partyId,
                patientFullName = patientFullName,
                updateUser = CurrentUser.UserId,
                userFullName = CurrentUser.FullName,
                updateTS = DateTime.Now.Date.ToString(),
                readings = readings,
                lstReadings = deviceReadings.ToList(),
                WeightDiff = WeightDiff
            };

            return PartialView("_SoapNotesDialog", model);
        }

        public ActionResult SoapNotesDialogForDR(string partyId, string patientFullName)
        {
            return PartialView("_DRSoapNotesDialog", GetOneDayReadingsForSN(partyId, patientFullName));
        }

        public SoapNotesModel GetOneDayReadingsForSN(string partyId, string patientFullName)
        {
            DateTime dt = DateTime.Today;
            var aggHistory = Service.GetAggregateHistory(partyId, dt, dt);

            string
                BPM_Device = null, BPM_Reading = null, BPM_MTime = null,
                BPM_PR_Device = null, BPM_PR_Reading = null, BPM_PR_MTime = null,
                POM_Device = null, POM_Reading = null, POM_MTime = null,
                TMM_Device = null, TMM_Reading = null, TMM_MTime = null,
                WSM_Device = null, WSM_Reading = null, WSM_MTime = null,
                BCM_Device = null, BCM_Reading = null, BCM_MTime = null,
                SPR_Device = null, SPR_Reading = null, SPR_MTime = null,
                MIL_Device = null, MIL_Reading = null, MIL_MTime = null,
                DBT_Device = null, DBT_Reading = null, DBT_MTime = null,

                FTM_Pain_Feeling = null, FTM_Pain_Desc = null, FTM_Pain_MTime = null,
                FTM_Mood_Feeling = null, FTM_Mood_Desc = null, FTM_Mood_MTime = null,
                FTM_Energy_Feeling = null, FTM_Energy_Desc = null, FTM_Energy_MTime = null,
                FTM_Breathing_Feeling = null, FTM_Breathing_Desc = null, FTM_Breathing_MTime = null,

                BPM = null, BPM_PR = null, POM = null, TMM = null, WSM = null, BCM = null, SPR = null,
                MIL = null, DBT = null, FTM_Pain = null, FTM_Mood = null, FTM_Energy = null, FTM_Breathing = null,
                BPM1 = null, BPM_PR1 = null, POM1 = null, TMM1 = null, WSM1 = null, BCM1 = null, SPR1 = null,
                MIL1 = null, DBT1 = null, FTM_Pain1 = null, FTM_Mood1 = null, FTM_Energy1 = null, FTM_Breathing1 = null;

            Double WeightDiff = 0.0;
            string readings;

            for (int i = 0; i < aggHistory.bloodPressureReading.Length; i++)
            {
                var read = aggHistory.bloodPressureReading[i];
                if ((read != null) && !String.IsNullOrEmpty(read.systolic))
                {
                    BPM_Device = "BP";
                    BPM_Reading = read.systolic + " / " + read.diastolic + " mmHg";
                    BPM_MTime = read.measurementTime;
                    BPM = BPM_Device + "," + BPM_Reading + "," + BPM_MTime + ";";
                    BPM1 = BPM1 + BPM;
                }
                else
                { BPM = null; }


            }

            for (int i = 0; i < aggHistory.bloodPressureReading.Length; i++)
            {
                var read = aggHistory.bloodPressureReading[i];
                if ((read != null) && !String.IsNullOrEmpty(read.pulseRate))
                {
                    BPM_PR_Device = "Pulse";
                    BPM_PR_Reading = read.pulseRate + " ppm";
                    BPM_PR_MTime = read.measurementTime;
                    BPM_PR = BPM_PR_Device + "," + BPM_PR_Reading + "," + BPM_PR_MTime + ";";
                    BPM_PR1 = BPM_PR1 + BPM_PR;
                }
                else
                { BPM_PR = null; }
            }

            for (int i = 0; i < aggHistory.pulseOxiMeterReading.Length; i++)
            {
                var read = aggHistory.pulseOxiMeterReading[i];
                if ((read != null) && !String.IsNullOrEmpty(read.oxygenReading))
                {
                    POM_Device = "Oxygen";
                    POM_Reading = read.oxygenReading + " %";
                    POM_MTime = read.measurementTime;
                    POM = POM_Device + "," + POM_Reading + "," + POM_MTime + ";";
                    POM1 = POM1 + POM;
                }
                else
                { POM = null; }
            }

            for (int i = 0; i < aggHistory.temperatureReading.Length; i++)
            {
                var read = aggHistory.temperatureReading[i];
                if ((read != null) && !String.IsNullOrEmpty(read.temperatureFahrenheit))
                {
                    TMM_Device = "Temperature";
                    TMM_Reading = read.temperatureFahrenheit;
                    TMM_MTime = read.measurementTime;
                    TMM = TMM_Device + "," + TMM_Reading + "," + TMM_MTime + ";";
                    TMM1 = TMM1 + TMM;
                }
                else
                { TMM = null; }
            }

            for (int i = 0; i < aggHistory.weighingScaleReading.Length; i++)
            {
                var read = aggHistory.weighingScaleReading[i];
                if ((read != null) && !String.IsNullOrEmpty(read.weightReadingPound))
                {
                    WeightDiff = read.weightDifference;
                    WSM_Device = "Weight";
                    if (WeightDiff > 0)
                        WSM_Reading = read.weightReadingPound + " lb &" + read.weightDifference + " lb";
                    else
                        WSM_Reading = read.weightReadingPound + " lb";
                    WSM_MTime = read.measurementTime;
                    WSM = WSM_Device + "," + WSM_Reading + "," + WSM_MTime + ";";
                    WSM1 = WSM1 + WSM;
                }
                else
                { WSM = null; }
            }

            for (int i = 0; i < aggHistory.bloodGlucoseReading.Length; i++)
            {
                var read = aggHistory.bloodGlucoseReading[i];
                if (read != null && read.glucoseReading != null)
                {
                    BCM_Device = "Glucose";
                    BCM_Reading = read.glucoseReading + " mg/dL";
                    BCM_MTime = read.measurementTime;
                    BCM = BCM_Device + "," + BCM_Reading + "," + BCM_MTime + ";";
                    BCM1 = BCM1 + BCM;
                }
                else
                { BCM = null; }
            }

            for (int i = 0; i < aggHistory.spirometerReading.Length; i++)
            {
                var read = aggHistory.spirometerReading[i];
                if ((read != null) && (!String.IsNullOrEmpty(read.fev1Best)))
                {
                    SPR_Device = "Spirometer";
                    SPR_Reading = read.fev1Best + "/" + read.pefBest + " mlps";
                    SPR_MTime = read.measurementTime;
                    SPR = SPR_Device + "," + SPR_Reading + "," + SPR_MTime + ";";
                    SPR1 = SPR1 + SPR;
                }
                else
                { SPR = null; }
            }


            for (int i = 0; i < aggHistory.feeling.Length; i++)
            {
                var read = aggHistory.feeling[i];
                if (read != null && read.painReading != null)
                {
                    FTM_Pain_Feeling = "Pain";
                    FTM_Pain_Desc = read.painDesc;
                    FTM_Pain_MTime = read.measurementTime;
                    FTM_Pain = FTM_Pain_Feeling + "," + FTM_Pain_Desc + "," + FTM_Pain_MTime + ";";
                    FTM_Pain1 = FTM_Pain1 + FTM_Pain;
                }
                else
                { FTM_Pain = null; }
                if (read != null && read.moodReading != null)
                {
                    FTM_Mood_Feeling = "Mood";
                    FTM_Mood_Desc = read.moodDesc;
                    FTM_Mood_MTime = read.measurementTime;
                    FTM_Mood = FTM_Mood_Feeling + "," + FTM_Mood_Desc + "," + FTM_Mood_MTime + ";";
                    FTM_Mood1 = FTM_Mood1 + FTM_Mood;
                }
                else
                { FTM_Mood = null; }
                if (read != null && read.energyReading != null)
                {
                    FTM_Energy_Feeling = "Energy";
                    FTM_Energy_Desc = read.energyDesc;
                    FTM_Energy_MTime = read.measurementTime;
                    FTM_Energy = FTM_Energy_Feeling + "," + FTM_Energy_Desc + "," + FTM_Energy_MTime + ";";
                    FTM_Energy1 = FTM_Energy1 + FTM_Energy;
                }
                else
                { FTM_Energy = null; }
                if (read != null && read.breathingReading != null)
                {
                    //FTM_Breathing = "Breathing" + Qd.feeling.breathingDesc + ", " + Qd.feeling.measurementTime + ";\r\n ";
                    FTM_Breathing_Feeling = "Breathing";
                    FTM_Breathing_Desc = read.breathingDesc;
                    FTM_Breathing_MTime = read.measurementTime;
                    FTM_Breathing = FTM_Breathing_Feeling + "," + FTM_Breathing_Desc + "," + FTM_Breathing_MTime + ";";
                    FTM_Breathing1 = FTM_Breathing1 + FTM_Breathing;
                }
                else
                { FTM_Breathing = null; }
            }

            for (int i = 0; i < aggHistory.mentalIllnessDailyQaScore.Length; i++)
            {
                var read = aggHistory.mentalIllnessDailyQaScore[i];
                if ((read.dailyQaScore != null) && !String.IsNullOrEmpty(read.dailyQaScore))
                {
                    MIL_Device = "PHQ9";
                    MIL_Reading = read.dailyQaScore + " - 27";
                    MIL_MTime = read.measurementTime;
                    MIL = MIL_Device + "," + MIL_Reading + "," + MIL_MTime + ";";
                    MIL1 = MIL1 + MIL;
                }
                else
                { MIL = null; }
            }

            for (int i = 0; i < aggHistory.diabetesDailyQaScore.Length; i++)
            {
                var read = aggHistory.diabetesDailyQaScore[i];
                if ((read.dailyQaScore != null) && !String.IsNullOrEmpty(read.dailyQaScore))
                {
                    DBT_Device = "Diabetes";
                    DBT_Reading = read.dailyQaScore + " - 6";
                    DBT_MTime = read.measurementTime;
                    DBT = DBT_Device + "," + DBT_Reading + "," + DBT_MTime + ";";
                    DBT1 = DBT1 + DBT;
                }
                else
                { DBT = null; }
            }

            readings = BPM1 + BPM_PR1 + POM1 + TMM1 + WSM1 + BCM1 + SPR1 + FTM_Pain1 + FTM_Mood1 + FTM_Energy1 + FTM_Breathing1 + MIL1 + DBT1;

            char[] splitter = { ';' };
            string[] deviceReadings = { };
            if (!string.IsNullOrEmpty(readings))
            {
                deviceReadings = readings.Split(splitter);
            }

            var model = new SoapNotesModel()
            {
                patientId = partyId,
                patientFullName = patientFullName,
                updateUser = CurrentUser.UserId,
                userFullName = CurrentUser.FullName,
                updateTS = DateTime.Now.Date.ToString(),
                AggHistory = aggHistory,
                readings = readings
            };
            return model;

        }

        public ActionResult ViewSoapNotesDialog(string soapNoteId, string patientId, string patientFullName, DateTime updateTs)
        {
            //var Qd = Service.GetQuadrantDetailsByPatient(patientId, CurrentUser.UserId);
            string sNotes = Service.GetSoapNotes(patientId, soapNoteId, updateTs, updateTs).soapNotes.FirstOrDefault().readings;
            char[] splitter = { ';' };
            string[] deviceReadings = { };
            if (!string.IsNullOrEmpty(sNotes))
            {
                deviceReadings = sNotes.Split(splitter);
            }

            var soapNotesModel = new SoapNotesModel()
            {
                soapNoteId = soapNoteId,
                patientId = patientId,
                patientFullName = patientFullName,
                userFullName = CurrentUser.FullName,
                FromDate = updateTs,
                ToDate = updateTs,
                DisplayDate = String.Format("{0:MM/dd/yyyy HH:mm tt}", updateTs),
                lstReadings = deviceReadings.ToList(),
                SoapNotes = Service.GetSoapNotes(patientId, soapNoteId, updateTs, updateTs)
            };
            // add an logCareTime entry for reviewing patients soap notes
            if (!CurrentUser.IsInRole(Role.Admin) && !CurrentUser.IsInRole(Role.Patient))
            {
                AddCareTimeForViewingReadings(patientId, "Review patient SOAP notes");
            }

            return PartialView("_ViewSoapNotesDialog", soapNotesModel);
        }

        public bool ValidateEmail(string email, bool throwException)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                if (throwException)
                {
                    throw new ArgumentException("Email cannot be empty.");
                }
                else
                {
                    return false;
                }
            }
            else
            {
                email = email.Trim();
                if (EmailRegEx.IsMatch(email))
                {
                    return true;
                }
                if (throwException)
                {
                    throw new ArgumentException(string.Format("Email Address ({0}) is not valid", email));
                }
                else
                {
                    return false;
                }
            }
        }

        public ActionResult SendEmail(EmailTemplateModel model)
        {
            if (string.IsNullOrWhiteSpace(model.From) == true)
            {
                throw new ArgumentException("From can't be empty.");
            }
            if (string.IsNullOrWhiteSpace(model.To) == true)
            {
                throw new ArgumentException("To can't be empty.");
            }
            if (string.IsNullOrWhiteSpace(model.Subject) == true)
            {
                throw new ArgumentException("Subject can't be empty.");
            }
            if (string.IsNullOrWhiteSpace(model.Body) == true)
            {
                throw new ArgumentException("Body can't be empty.");
            }
            var message = new MailMessage();
            model.To = model.To.TrimEnd(',', ' ');
            foreach (var str in model.To.Split(','))
            {
                ValidateEmail(str, true);
                message.To.Add(str);
            }
            if (string.IsNullOrWhiteSpace(model.Cc) == false)
            {
                model.Cc = model.Cc.TrimEnd(',', ' ');
                if (model.Cc.EndsWith(","))
                {
                    model.Cc = model.Cc.Remove(model.Cc.Length - 1);
                }
                foreach (var str in model.Cc.Split(','))
                {
                    ValidateEmail(str, true);
                    message.CC.Add(str);
                }
            }
            message.Subject = model.Subject;
            message.From = new MailAddress(model.From);
            message.Body = Settings.Default.EmailTemplate.Replace("~BODY~", model.Body);
            message.Body = message.Body.Replace("~YEAR~", DateTime.Now.Year.ToString());
            message.IsBodyHtml = true;
            var smtpClient = new SmtpClient
            {
                Host = System.Configuration.ConfigurationManager.AppSettings["smtpProviderHost"],         //"smtp.gmail.com"
                Port = System.Configuration.ConfigurationManager.AppSettings["smtpClientPort"].AsInt(),   //587
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["senderEmailAddr"],
                                    System.Configuration.ConfigurationManager.AppSettings["emailPwd"])
            };
            smtpClient.Send(message);

            return new EmptyResult();
        }

        public string updateHubToken(string patientId, string hubID)
        {
            //String qualcommURI = "https://twonetprod.qualcomm.com/cuc/rest/revU/hub/token/update"; //live !!needs to be put in config
            String qualcommURI = "https://twonet-int-gateway.qualcomm.com/demo3/cuc/rest/revU/hub/token/update"; //dev !!needs to be put in config


            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(qualcommURI);
            webRequest.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;";
            webRequest.Method = "POST";
            webRequest.ContentType = "application/xml";

            //live !!needs to be put in config
            //webRequest.Headers.Add("customerId", "MYVIT001");
            //webRequest.Headers.Add("authKey", "MVitL!6348");

            //dev !!needs to be put in config
            webRequest.Headers.Add("customerId", "MYVIT001");
            webRequest.Headers.Add("authKey", "myVitalz!1234");

            if (ServicePointManager.ServerCertificateValidationCallback == null)
            {
                ServicePointManager.ServerCertificateValidationCallback += new System.Net.Security.RemoteCertificateValidationCallback(ValidateRemoteCertificate);
            }

            //!!needs conversion to serialized class  
            string XMLData = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
            + "<UpdateHubToken xmlns=\"urn:com.twonet.sp.cuc.revisionu\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:com.twonet.sp.cuc.revisionu cucRevU.xsd \">"
              + "<hub>"
                + "<HubId>" + hubID + "</HubId>"
                + "<HubIdType>VHID</HubIdType>"
             + "</hub>"
              + "<hubToken>partyID=" + patientId + "</hubToken>"
            + "</UpdateHubToken>";

            StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream(), System.Text.Encoding.ASCII);
            requestWriter.Write(XMLData);
            requestWriter.Close();

            string output = "";
            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)webRequest.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                output = reader.ReadToEnd();
                response.Close();
            }
            catch (WebException e)
            {
                return e.Message;
            }
            finally { if (response != null) response.Close(); }
            return output;

        }

        private static bool ValidateRemoteCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors policyErrors)
        { return true; }

        #endregion
    }
}
