﻿namespace MyVitalz.Web.Helpers
{
    public class Constant
    {
        public const string DateTimeFormat = "MM/dd/yyyy hh:mm tt";
        public const string DateFormat = "MM/dd/yyyy";
        public const string TimeFormat = "hh:mm:ss tt";
        public const string AllGroup = "ALL";
        public const string Yes = "Y";
        public const int FirstPageIndex = 0;
    }
}
