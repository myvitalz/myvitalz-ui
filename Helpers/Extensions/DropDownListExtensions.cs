﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace MyVitalz.Web.Helpers
{
    public static class DropDownListExtensions
    {
        #region Properties

        public const string DefaultItemText = " -- Select -- ";
        public const string OptionItem = "<option {0}value=\"{1}\">{2}</option>";
        public const string SelectedAttribute = "selected=\"selected\" ";
        
        #endregion

        #region Support Methods

        private static MvcHtmlString GenerateEnumDropDownList<T>(HtmlHelper helper, string name, object selectedValue, object defaultValue, object htmlAttributes, List<T> excludeList)
        {
            var enumType = typeof(T);
            if (enumType.IsEnum == false)
            {
                throw new ArgumentException("Type is not an enum.");
            }
            if (selectedValue != null 
                    && selectedValue.GetType() != enumType)
            {
                throw new ArgumentException("Selected object is not " + enumType.ToString());
            }
            if (excludeList == null)
            {
                excludeList = new List<T>();
            }
            var list = new List<SelectListItem>();
            foreach (int num in Enum.GetValues(enumType))
            {
                if (!excludeList.Contains((T)Enum.ToObject(enumType, num)))
                {
                    var selectListItem = new SelectListItem();
                    selectListItem.Value = num.ToString();
                    selectListItem.Text = Enum.GetName(enumType, (object)num);
                    selectListItem.Text = selectListItem.Text.SplitValueByCase();
                    if (selectedValue != null)
                    {
                        selectListItem.Selected = (int)selectedValue == num;
                    }
                    if (selectListItem.Text == "None")
                    {
                        selectListItem.Text = " -- Select -- ";
                    }
                    list.Add(selectListItem);
                }
            }
            var listItems = list.ToList();
            if (defaultValue != null)
            {
                listItems.Insert(0, GetEmptySelectListItem(defaultValue));
            }
            return CreateDropDownList(helper, name, listItems, htmlAttributes);
        }

        private static SelectListItem GetEmptySelectListItem(object value)
        {
            return new SelectListItem()
            {
                Text = " -- Select -- ",
                Value = value.ToString()
            };
        }

        private static MvcHtmlString GetEnumDropDownList(HtmlHelper helper, string name, Type enumType, object selectedValue, object defaultValue, bool useTextAsValue, object htmlAttributes)
        {
            if (enumType.IsEnum == false)
            {
                throw new ArgumentException("Type is not an enum.");
            }
            var listItems = new List<SelectListItem>();
            foreach (int num in Enum.GetValues(enumType))
            {
                var selectListItem = new SelectListItem();
                selectListItem.Text = Enum.GetName(enumType, (object)num);
                selectListItem.Value = !useTextAsValue ? num.ToString() : selectListItem.Text;
                if (selectedValue != null)
                    selectListItem.Selected = selectListItem.Value == selectedValue.ToString() || selectListItem.Text == selectedValue.ToString();
                selectListItem.Text = StringExtension.SplitValueByCase(selectListItem.Text);
                if (selectListItem.Text == "None")
                {
                    selectListItem.Text = " -- Select -- ";
                    if (useTextAsValue)
                        selectListItem.Value = string.Empty;
                }
                listItems.Add(selectListItem);
            }
            if (defaultValue != null)
                listItems.Insert(0, GetEmptySelectListItem(defaultValue));
            return CreateDropDownList(helper, name, listItems, htmlAttributes);
        }

        private static MvcHtmlString CreateDropDownList(HtmlHelper helper, string name, List<SelectListItem> listItems, object htmlAttributes)
        {
            var tagBuilder = new TagBuilder("select");
            tagBuilder.MergeAttribute("name", name);
            tagBuilder.MergeAttribute("id", name.Replace(".", "_"));
            tagBuilder.MergeAttributes<string, object>((IDictionary<string, object>)new RouteValueDictionary(htmlAttributes));
            var stringBuilder = new StringBuilder();
            foreach (var selectListItem in listItems)
                stringBuilder.Append(string.Format("<option {0}value=\"{1}\">{2}</option>", selectListItem.Selected ? (object)"selected=\"selected\" " : (object)string.Empty, (object)selectListItem.Value, (object)selectListItem.Text));
            tagBuilder.InnerHtml = ((object)stringBuilder).ToString();
            return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.Normal));
        }

        private static MvcHtmlString GetListDropDownList(HtmlHelper helper, string name, IEnumerable items, string dataValueField, string dataTextField, object selectedValue, object defaultValue, object htmlAttributes)
        {
            List<SelectListItem> list = Enumerable.ToList<SelectListItem>((IEnumerable<SelectListItem>)new SelectList(items, dataValueField, dataTextField, selectedValue));
            if (defaultValue != null)
            {
                list.Insert(0, GetEmptySelectListItem(defaultValue));
            }
            return SelectExtensions.DropDownList(helper, name, (IEnumerable<SelectListItem>)list, htmlAttributes);
        }

        private static MvcHtmlString GetEnumDropDownList<T>(HtmlHelper helper, string name, object selectedValue, object defaultValue, object htmlAttributes, List<T> excludeList)
        {
            return GenerateEnumDropDownList<T>(helper, name, selectedValue, defaultValue, htmlAttributes, excludeList);
        } 
        #endregion

        public static MvcHtmlString DropDownListFromEnum(this HtmlHelper helper, string name, Type enumType)
        {
            return GetEnumDropDownList(helper, name, enumType, (object)null, (object)null, false, (object)null);
        }

        public static MvcHtmlString DropDownListFromEnum(this HtmlHelper helper, string name, Type enumType, object selectedValue)
        {
            return GetEnumDropDownList(helper, name, enumType, selectedValue, (object)null, false, (object)null);
        }

        public static MvcHtmlString DropDownListFromEnum<T>(this HtmlHelper helper, string name, List<T> excludeList, object selectedValue, object htmlAttributes)
        {
            return GetEnumDropDownList<T>(helper, name, selectedValue, (object)null, htmlAttributes, excludeList);
        }

        public static MvcHtmlString DropDownListFromEnum(this HtmlHelper helper, string name, Type enumType, object selectedValue, object defaultValue)
        {
            return GetEnumDropDownList(helper, name, enumType, selectedValue, defaultValue, false, (object)null);
        }

        public static MvcHtmlString DropDownListFromEnum(this HtmlHelper helper, string name, Type enumType, object selectedValue, object defaultValue, object htmlAttributes)
        {
            return GetEnumDropDownList(helper, name, enumType, selectedValue, defaultValue, false, htmlAttributes);
        }

        public static MvcHtmlString DropDownListFromEnum(this HtmlHelper helper, string name, Type enumType, object selectedValue, object defaultValue, bool useTextAsValue, object htmlAttributes)
        {
            return GetEnumDropDownList(helper, name, enumType, selectedValue, defaultValue, useTextAsValue, htmlAttributes);
        }

        public static MvcHtmlString DropDownListFromList(this HtmlHelper helper, string name, IEnumerable items, string dataValueField, string dataTextField)
        {
            return GetListDropDownList(helper, name, items, dataValueField, dataTextField, (object)null, (object)0, (object)null);
        }

        public static MvcHtmlString DropDownListFromList(this HtmlHelper helper, string name, IEnumerable items, string dataValueField, string dataTextField, object selectedValue)
        {
            return GetListDropDownList(helper, name, items, dataValueField, dataTextField, selectedValue, (object)0, (object)null);
        }

        public static MvcHtmlString DropDownListFromList(this HtmlHelper helper, string name, IEnumerable items, string dataValueField, string dataTextField, object selectedValue, object htmlAttributes)
        {
            return GetListDropDownList(helper, name, items, dataValueField, dataTextField, selectedValue, (object)0, htmlAttributes);
        }

        public static MvcHtmlString DropDownListFromList(this HtmlHelper helper, string name, IEnumerable items, string dataValueField, string dataTextField, object selectedValue, object defaultValue, object htmlAttributes)
        {
            return GetListDropDownList(helper, name, items, dataValueField, dataTextField, selectedValue, defaultValue, htmlAttributes);
        }

        public static MvcHtmlString EmptyDropDownList(this HtmlHelper helper, string name, object htmlAttributes)
        {
            return CreateDropDownList(helper, name, new List<SelectListItem>(), htmlAttributes);
        }
    }
}
