﻿using MyVitalz.Web.Service;

namespace MyVitalz.Web.Helpers
{
	public static class PartyExtension
	{
		#region Methods

        public static string GridContents(this Party party)
        {
            return string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}{12}{13}{14}{15}{16}{17}{18}{19}",
                    party.lastName,
                    party.firstName,
                    party.partyType.ToRoleName(),
                    Helper.GetAssociationsAsString(party.externalPatientId, ","),
                    party.deviceKit !=null?party.deviceKit.kitId:string.Empty,
                    party.cellPhone,
                    party.homePhone,
                    party.workphone,
                    party.email,
                    Helper.GetDate(party.dob),
                    party.gender,
                    party.GetAddressLine1(),
                    party.GetAddressLine2(),
                    party.GetCity(),
                    party.GetState(),
                    party.GetZip5(),
                    party.isPartyActive.ToStatus(),
                    party.group,
                    party.coverageStartDate,
                    party.coverageEndDate);
        }
		public static string GetFullName(this BasicParty party)
		{
			var str = string.Empty;
			if (string.IsNullOrWhiteSpace(party.middleName)
					|| party.middleName == "NULL")
			{
				str = string.Format("{0} {1}", party.firstName, party.lastName);
			}
			else
			{
				str = string.Format("{0} {1} {2}", party.firstName, party.middleName, party.lastName);
			}
			return str;
		}

		public static string GetName(this Party party)
		{
			var str = string.Empty;
			if (string.IsNullOrWhiteSpace(party.middleName)
					|| party.middleName == "NULL")
			{
				str = string.Format("{0} {1}", party.firstName, party.lastName);
			}
			else
			{
				str = string.Format("{0} {1} {2}", party.firstName, party.middleName, party.lastName);
			}
			return str;
		}

		public static string GetDisplayEmail(this BasicParty party)
		{
			var str = string.Format("{0} {1} {2}<{3}>", party.partyType.ToRoleName(), party.firstName, party.lastName, party.email ?? string.Empty);
			return str;
		}

		public static string GetDob(this Party party)
		{
			return GetDate(party.dob);
		}

		public static string GetDate(string value)
		{
			string str = string.Empty;
			if (value != null
				&& value.Length > 19)
			{
				str = value.Substring(0, 19);
			}
			return str;
		}

		public static string GetAddressLine1(this Party party)
		{
			if (party != null
				&& party.address != null
				&& party.address.addressLine1 != null)
			{
				return party.address.addressLine1;
			}
			else
			{
				return string.Empty;
			}
		}

		public static string GetAddressLine2(this Party party)
		{
			if (party != null
				&& party.address != null
				&& party.address.addressLine2 != null)
			{
				return party.address.addressLine2;
			}
			else
			{
				return string.Empty;
			}
		}

		public static string GetCity(this Party party)
		{
			if (party != null && party.address != null && party.address.city != null)
			{
				return party.address.city;
			}
			else
			{
				return string.Empty;
			}
		}

		public static string GetState(this Party party)
		{
			if (party != null && party.address != null && party.address.state != null)
			{
				return party.address.state;
			}
			else
			{
				return string.Empty;
			}
		}

		public static string GetZip5(this Party party)
		{
			if (party != null && party.address != null && party.address.zip5 != null)
			{
				return party.address.zip5;
			}
			else
			{
				return string.Empty;
			}
		}
        
		#endregion    
	}
}
