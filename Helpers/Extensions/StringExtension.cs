﻿using System;
using System.Text.RegularExpressions;
using System.Web;

namespace MyVitalz.Web.Helpers
{
    public static class StringExtension
    {
        #region Local Variables

        private static readonly long UnixEpochTicks = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).Ticks;
        
        #endregion

        //public static T? ToNullable<T>(this string s) where T : struct
        //{
        //    T? nullable = new T?();
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(s))
        //        {
        //            if (s.Trim().Length > 0)
        //                nullable = new T?((T)TypeDescriptor.GetConverter(typeof(T)).ConvertFrom((object)s));
        //        }
        //    }
        //    catch
        //    {
        //    }
        //    return nullable;
        //}

        public static string FormatAsPhoneNumber(this string value)
        {
            if (string.IsNullOrWhiteSpace(value)
                || value.Length < 10)
            {
                return value;
            }
            else
            {
                return Regex.Replace(value, "(\\d{3})(\\d{3})(\\d{4})(.*)", "($1)$2-$3 $4");
            }
        }

        public static string ToRoleName(this string role)
        {
            switch (role)
            {
                case Role.Doctor:
                    role = "Doctor";
                    break;
                case Role.Patient:
                    role = "Patient";
                    break;
                case Role.Caregiver:
                    role = "Caregiver";
                    break;
                case Role.Admin:
                    role = "Admin";
                    break;
                case Role.GroupAdmin:
                    role = "Group Admin";
                    break;
                case Role.Clinician:
                    role = "Clinician";
                    break;
                default:
                    throw new Exception(string.Format("{0} is not a valid role", (object)role));
            }
            return role;
        }

        public static string ToDeviceType(this string deviceType)
        {
            switch (deviceType)
            {
                case DeviceType.BloodGlucose:
                    deviceType = "Blood Glucose";
                    break;
                case DeviceType.Systolic:
                    deviceType = "Systolic";
                    break;
                case DeviceType.Diastolic:
                    deviceType = "Diastolic";
                    break;
                case DeviceType.BloodPressure:
                    deviceType = "Blood Pressure";
                    break;
                case DeviceType.PulseOximeter:
                    deviceType = "Oxygen";
                    break;
                case DeviceType.Temperature:
                    deviceType = "Temperature";
                    break;
                case DeviceType.WeighingScale:
                    deviceType = "Weight";
                    break;
                case DeviceType.PulseRate:
                    deviceType = "Pulse Rate";
                    break;
                case DeviceType.Spirometer:
                    deviceType = "Spirometer";
                    break;
                case DeviceType.Feelings:
                    deviceType = "Feelings";
                    break;
                case DeviceType.MentalIllness:
                    deviceType = "PHQ9 Score";
                    break;
                case DeviceType.Diabetes:
                    deviceType = "Diabetes Score";
                    break;
                //case DeviceType.ImpakHealthJournal:
                //    deviceType = "Impak Health Journal";
                //    break;
                case DeviceType.PauseMonitor:
                    deviceType = "Pause Monitor";
                    break;
                case DeviceType.CareTime:
                    deviceType= "Log Care Time";
                    break;
                case DeviceType.Readmission:
                    deviceType = "Readmission";
                    break;
                case DeviceType.Medication:
                    deviceType = "Medication";
                    break;
                case DeviceType.Covid:
                    deviceType = "Covid-19";
                    break;
                default:
                    throw new Exception(string.Format("{0} is not a valid deviceType", (object)deviceType));
            }
            return deviceType;
        }

        public static int ToDeviceSortOrder(this string deviceType)
        {
            switch (deviceType)
            {
                case DeviceType.BloodPressure:
                    return 1;
                case DeviceType.Systolic:
                    return 2;
                case DeviceType.Diastolic:
                    return 3;
                case DeviceType.PulseRate:
                    return 4;
                case DeviceType.Temperature:
                    return 5;
                case DeviceType.BloodGlucose:
                    return 6;
                case DeviceType.PulseOximeter:
                    return 7;
                case DeviceType.WeighingScale:
                    return 8;
                case DeviceType.Spirometer:
                    return 9;
                case DeviceType.MentalIllness:
                    return 10;
                case DeviceType.Diabetes:
                    return 11;
                default:
                    throw new Exception(string.Format("SortOrder is not defined for the {0} deviceType", (object)deviceType));
            }
        }

        public static string ToPeriod(this string value)
        {
            string period = string.Empty;
            switch (value)
            {
                case "E":
                    period = "Early Morning";
                    break;
                case "M":
                    period = "Morning";
                    break;
                case "A":
                    period = "Afternoon";
                    break;
                case "N":
                    period = "Night";
                    break;
            }
            return period;
        }

        public static HtmlString DisplayField(this string fieldValue, string fieldName)
        {
            var str = string.Empty;
            if (string.IsNullOrWhiteSpace(fieldName) == false)
            {
                fieldName = string.Format("<strong>{0}</strong> : ", (object)fieldName);
            }
            if (string.IsNullOrWhiteSpace(fieldValue) == false)
            {
                str = string.Format("<label>{0}{1}</label>", (object)fieldName, (object)fieldValue);
            }

            return new HtmlString(str);
        }

        public static string SplitValueByCase(this string value)
        {
            return new Regex("(?<=[a-z])(?<x>[A-Z])|(?<=.)(?<x>[A-Z])(?=[a-z])", RegexOptions.Compiled | RegexOptions.Singleline).Replace(value, " ${x}");
        }

        public static string JsMilliSeconds(this string value)
        {
            return ((DateTime.Parse(value).Ticks - StringExtension.UnixEpochTicks) / 10000L).ToString();
        }

        public static bool ToBoolean(this string value)
        {
            return value=="1" ? true : false;
        }

        public static string ToYesNo(this bool value)
        {
            return value ? "Yes" : "No";
        }

        public static string ToStatus(this bool value)
        {
            return value ? "Active" : "Deleted";
        }

        public static string GetReportAlertClass(this string value)
        {
            if (value != null && value.Contains("*"))
                return "alert";
            return "";
        }
        public static string GetReportTextAlertClass(this string value)
        {
            if (value != null && value.Contains("*"))
                return "textalert";
            return "";
        }

        public static string GetReportSystolicAlertClass(this string value)
        {
            if (value != null && value.Contains("@"))
                return "alert";
            return "";
        }

        public static string GetReportDiastolicAlertClass(this string value)
        {
            if (value != null && value.Contains("#"))
                return "alert";
            return "";
        }

        public static string GetReportWeightDiffAlertClass(this string value)
        {
            if (value != null && value == "true" )
                return "alert";
            return "";
        }

        public static string GetReportAlertSucidalClass(this string value)
        {
            if (value != null && value.Contains("#"))
                return "true";
            return "";
        }
    }
}
