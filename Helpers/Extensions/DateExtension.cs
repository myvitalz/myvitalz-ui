﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;

namespace MyVitalz.Web.Helpers
{
    public static class DateExtension
    {
        public static MvcHtmlString TextBoxDate(this HtmlHelper helper, string name, DateTime dateValue, string dateFormat, object htmlAttributes, bool useDefaultDate)
        {
            if (string.IsNullOrEmpty(name) == true)
            {
                throw new ArgumentException("The argument must have a value", "name");
            }
            if (dateValue == DateTime.MinValue 
                    && useDefaultDate == true)
            {
                dateValue = DateTime.Now;
            }
            var str = string.Empty;
            if (dateValue != DateTime.MinValue)
            {
                str = string.IsNullOrEmpty(dateFormat) == false ? dateValue.ToString(dateFormat) : dateValue.ToShortDateString();
            }
            var stringBuilder = new StringBuilder();
            var tagBuilder = new TagBuilder("input");
            tagBuilder.MergeAttribute("id", name.Replace(".", "_"));
            tagBuilder.MergeAttribute("type", "text");
            tagBuilder.MergeAttribute("name", name);
            tagBuilder.MergeAttribute("value", str);
            tagBuilder.MergeAttributes<string, object>((IDictionary<string, object>)new RouteValueDictionary(htmlAttributes), true);
            stringBuilder.Append(tagBuilder.ToString(TagRenderMode.Normal));
            return MvcHtmlString.Create(stringBuilder.ToString());
        }

        public static string ToDateString(this DateTime dateTime)
        {
            return dateTime.Equals(DateTime.MinValue) ? string.Empty : dateTime.ToString("MM-dd-yyyy");
        }

        public static string ToMonthYearString(this DateTime dateTime)
        {
            return dateTime.Equals(DateTime.MinValue) ? string.Empty : dateTime.ToString("MM-yyyy");
        }

        public static string ToDateTimeString(this DateTime dateTime)
        {
            return dateTime.Equals(DateTime.MinValue) ? string.Empty : dateTime.ToString("MM-dd-yyyy HH:mm:ss");
        }

        public static string ToNumberOfDays(this DateTime dateTime)
        {
            return string.Format("{0} Days <font size=\"0.95em\">({1})</font>", (object)((int)(DateTime.Parse(DateTime.Now.ToShortDateString()) - DateTime.Parse(dateTime.ToShortDateString())).TotalDays).ToString("000"), (object)dateTime.ToString("hh:mm tt"));
        }

        public static int ToNumberofDaysFromToday(this DateTime dateTime)
        {
            return (int)(DateTime.Parse(DateTime.Now.ToShortDateString()) - DateTime.Parse(dateTime.ToShortDateString())).TotalDays;
        }
    }
}
