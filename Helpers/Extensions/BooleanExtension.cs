﻿namespace MyVitalz.Web.Helpers
{
    public static class BooleanExtension
    {
        public static string ToThreshold(this bool value)
        {
            return value ? "Abnormal" : "Normal";
        }
    }
}
