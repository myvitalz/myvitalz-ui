﻿using MyVitalz.Web.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.Security.Cryptography;
using System.Text;
using System.Web.Configuration;
using System.Web.Hosting;
using System.Web.Security;

namespace MyVitalz.Web.Helpers
{
    public class CustomMembershipProvider : MembershipProvider
    {
        #region Local Variables

        private MachineKeySection _machineKey;
        private MembershipPasswordFormat _passwordFormat;
        private string _applicationName;
        private User _user; 
        
        #endregion

        #region Overridden Properties

        public override string ApplicationName
        {
            get
            {
                return _applicationName;
            }
            set
            {
                _applicationName = value;
            }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get
            {
                return _passwordFormat;
            }
        }

        public override bool EnablePasswordReset
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override bool EnablePasswordRetrieval
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override int MaxInvalidPasswordAttempts
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override int MinRequiredPasswordLength
        {
            get
            {
                return 6;
            }
        }

        public override int PasswordAttemptWindow
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override string PasswordStrengthRegularExpression
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override bool RequiresUniqueEmail
        {
            get
            {
                throw new NotImplementedException();
            }
        } 
        
        #endregion

        #region Overridden Methods

        public override void Initialize(string name, NameValueCollection config)
        {
            base.Initialize(name, config);
            _applicationName = GetConfigValue(config["applicationName"], HostingEnvironment.ApplicationVirtualPath);
            switch (config["passwordFormat"] ?? "Hashed")
            {
                case "Hashed":
                    _passwordFormat = MembershipPasswordFormat.Hashed;
                    break;
                case "Encrypted":
                    _passwordFormat = MembershipPasswordFormat.Encrypted;
                    break;
                case "Clear":
                    _passwordFormat = MembershipPasswordFormat.Clear;
                    break;
                default:
                    throw new ProviderException("Password format not supported.");
            }
            _machineKey = WebConfigurationManager.OpenWebConfiguration(HostingEnvironment.ApplicationVirtualPath).GetSection("system.web/machineKey") as MachineKeySection;
        }

        public override bool ValidateUser(string userName, string password)
        {
            bool flag = false;
            var party = Helper.Service.ValidateUserCredential(userName, password);
            if (party != null)
            {
                CustomMembershipProvider membershipProvider = this;
                var user = new User(userName);
                user.UserId = party.party.partyID;
                user.NickName = party.party.firstName;
                user.FullName = party.party.GetFullName();
                user.Email = party.party.email;
                user.Group = party.party.group;
                user.IsAuthenticated = true;
                membershipProvider._user = user;
                var list = new List<string>();
                switch (party.party.partyType)
                {
                    case Role.Doctor:
                        list.Add(Role.Doctor);
                        break;
                    case Role.Patient:
                        list.Add(Role.Patient);
                        break;
                    case Role.Caregiver:
                        list.Add(Role.Caregiver);
                        break;
                    case Role.Clinician:
                        list.Add(Role.Clinician);
                        break;
                    case Role.Admin:
                        list.Add(Role.Admin);
                        break;
                    case Role.GroupAdmin:
                        list.Add(Role.GroupAdmin);
                        break;
                    default:
                        throw new ArgumentException("Sorry! You are not authorized to use this application.");
                }
                _user.Roles = list.ToArray();
                flag = true;
            }
            return flag;
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            return (MembershipUser)_user;
        }

        #region Non Implemented Methods

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        #endregion
        
        #endregion

        #region Support Methods
 
        private string GetConfigValue(string configValue, string defaultValue)
        {
            if (string.IsNullOrEmpty(configValue))
                return defaultValue;
            else
                return configValue;
        }

        private bool CheckPassword(string password, string dbpassword)
        {
            string str1 = password;
            string str2 = dbpassword;
            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Hashed:
                    str1 = EncodePassword(password);
                    break;
                case MembershipPasswordFormat.Encrypted:
                    str2 = UnEncodePassword(dbpassword);
                    break;
            }
            return str1 == str2;
        }

        private string UnEncodePassword(string encodedPassword)
        {
            string s = encodedPassword;
            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Clear:
                    return s;
                case MembershipPasswordFormat.Hashed:
                    HMACSHA1 hmacshA1 = new HMACSHA1();
                    hmacshA1.Key = HexToByte(_machineKey.ValidationKey);
                    Convert.ToBase64String(hmacshA1.ComputeHash(Encoding.Unicode.GetBytes(s)));
                    throw new ProviderException("Not implemented password format (HMACSHA1).");
                case MembershipPasswordFormat.Encrypted:
                    s = Encoding.Unicode.GetString(DecryptPassword(Convert.FromBase64String(s)));
                    goto case 0;
                default:
                    throw new ProviderException("Unsupported password format.");
            }
        }

        private string EncodePassword(string password)
        {
            string str = password;
            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Clear:
                    return str;
                case MembershipPasswordFormat.Hashed:
                    HMACSHA1 hmacshA1 = new HMACSHA1();
                    hmacshA1.Key = HexToByte(_machineKey.ValidationKey);
                    str = Convert.ToBase64String(hmacshA1.ComputeHash(Encoding.Unicode.GetBytes(password)));
                    goto case 0;
                case MembershipPasswordFormat.Encrypted:
                    str = Convert.ToBase64String(EncryptPassword(Encoding.Unicode.GetBytes(password)));
                    goto case 0;
                default:
                    throw new ProviderException("Unsupported password format.");
            }
        }

        private byte[] HexToByte(string hexString)
        {
            byte[] numArray = new byte[hexString.Length / 2];
            for (int index = 0; index < numArray.Length; ++index)
                numArray[index] = Convert.ToByte(hexString.Substring(index * 2, 2), 16);
            return numArray;
        } 
        #endregion
    }
}
