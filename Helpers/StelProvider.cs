﻿using MyVitalz.Web.Helpers.Repository;
using MyVitalz.Web.Models;
using MyVitalz.Web.Service;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyVitalz.Web.Helpers
{
    public class StelProvider
    {
        public RestApiRepository stelClient;

        public StelProvider()
        {
            stelClient = new Helpers.Repository.RestApiRepository();
        }

        public void RegisterKit(EHRDetailsModel model)
        {
            // call stl to create a dummy patient
            string pid = CreatePatient(model);
            if( string.IsNullOrEmpty(pid) )
            {
                // error with ESN, stop
                throw new ArgumentException(string.Format("Error with creating new patient"));
            }

            // get the full esn from the provided one
            var fullEsn = GetFullESN(model, pid);
            if (string.IsNullOrEmpty(fullEsn))
            {
                // error with ESN, stop
                throw new ArgumentException(string.Format("Error with getting the full DeviceKit"));
            }
            // call stel service to register this device kit
            var stelClient = new Helpers.Repository.RestApiRepository();
            object dkRsp = stelClient.RegisterHubWithStel(fullEsn, pid);
          
            StelRegisterEsnResponse dkResult = JsonConvert.DeserializeObject<StelRegisterEsnResponse>(dkRsp.ToString());
            if (dkResult != null && dkResult.result != null && dkResult.result.ToUpper().Equals("OK"))
            {
                 new DataServices().ClientDeviceSave(pid);
                // do nothing, it is all good.
            }
            else
            {
                StelErrorResponse dkError = JsonConvert.DeserializeObject<StelErrorResponse>(dkRsp.ToString());
                throw new ArgumentException(string.Format("Error attaching kit to STEL : {0}", dkError.error));
            }
        }

        public void UnRegisterKit(string partialEsn, string pid)
        {
            // get the full esn from the provided one
            var fullEsn = "";
            try
            {
                fullEsn = GetFullESNInUse(partialEsn, pid);

                // if the kit is associated with STEL, then unregister the kit from STEL
                if (!string.IsNullOrEmpty(fullEsn))
                {
                    // call stel service to unregister this device kit
                    var stelClient = new Helpers.Repository.RestApiRepository();
                    object dkRsp = stelClient.UnRegisterHubWithStel(fullEsn, pid);
                    
                    StelRegisterEsnResponse dkResult = JsonConvert.DeserializeObject<StelRegisterEsnResponse>(dkRsp.ToString());
                    if (dkResult != null && dkResult.result != null && dkResult.result.ToUpper().Equals("OK"))
                    {
                        new DataServices().ClientDeviceDelete(pid);
                        // do nothing, it is all good.
                    }
                    else
                    {
                        StelErrorResponse dkError = JsonConvert.DeserializeObject<StelErrorResponse>(dkRsp.ToString());
                        throw new ArgumentException(string.Format("Error unregistering kit to STEL : {0}", dkError.error));
                    }
                }
            }
            catch ( Exception ex )
            {
                // error unregistering the kit from stel.
                throw new ArgumentException( ex.Message.ToString() );
            }
        }

        public string CreatePatient(EHRDetailsModel model)
        {
            Xref[] associations;
            Xref stelAssociation = new Xref();
            // stel dummy patient created, store the stel pid in the ehr 
            object rsp = stelClient.CreateSTELPatient();
            StelCreatePatientResponse result = JsonConvert.DeserializeObject<StelCreatePatientResponse>(rsp.ToString());
            if (result != null && result.result != null && result.result.ToUpper().Equals("OK"))
            {
                stelAssociation.xrefId = result.pid;
                stelAssociation.xrefType = "STL";
                stelAssociation.status = "A";
                stelAssociation.partyID = model.Details.ehr.id;
            }
            if (model.Associations != null)
            {
                int numOfAssociations = model.Associations.Count();
                associations = new Xref[numOfAssociations+1];
                for( int i = 0; i < numOfAssociations; i++ )
                {
                    associations[i] = model.Associations[i];
                }
                associations[numOfAssociations] = stelAssociation;
            }
            else
            {
                associations = new Xref[1];
                associations[0] = stelAssociation;
            }
            model.Associations = associations;
            return result.pid;
        }


        public string GetFullESN(EHRDetailsModel model, string pid)
        {
            string fullEsn = "";
            try
            {
                object rsp = stelClient.GetFullESN(model.Details.kitDetails.kitId, pid);
                var partialEsnLgth = model.Details.kitDetails.kitId.Count();
                StelGetEsnResponse result = JsonConvert.DeserializeObject<StelGetEsnResponse>(rsp.ToString());
                if (result != null && result.result != null && result.result.ToUpper().Equals("OK"))
                {
                    // find the esn that matches the provided esn
                    if (result.gates != null)
                    {
                        for (int idx = 0; idx < result.gates.Count(); idx++)
                        {
                            // find the full esn that matches the provided partial esn and copy that value into the kitid
                            if (result.gates[idx].esn.Substring(0, partialEsnLgth).Equals(model.Details.kitDetails.kitId))
                            {
                                if (result.gates[idx].used.ToUpper().Equals("FALSE"))
                                {
                                    fullEsn = result.gates[idx].esn;
                                    model.Details.kitDetails.kitId = fullEsn;
                                }
                                else
                                {
                                    // the kid is already in use ..
                                    throw new ArgumentException("DeviceKit starting with {0} is already in use.", model.Details.kitDetails.kitId);
                                }
                            }
                        }
                    }
                    else
                    {
                        // the kid is already in use ..
                        throw new ArgumentException("Invalid response from STEL");
                    }
                }
                else
                {
                    // stel response with error ...
                    throw new ArgumentException("Error response from STEL");
                }
            }
            catch( Exception ex )
            {
                throw new ArgumentException(ex.ToString());
            }
            return fullEsn;
        }

        public string GetFullESNInUse(string partialESN, string pid)
        {
            string fullEsn = "";
            try
            {
                object rsp = stelClient.GetFullESN(partialESN, pid);
                var partialEsnLgth = partialESN.Count();
                StelGetEsnResponse result = JsonConvert.DeserializeObject<StelGetEsnResponse>(rsp.ToString());
                if (result != null && result.result != null && result.result.ToUpper().Equals("OK"))
                {
                    // find the esn that matches the provided esn
                    if (result.gates != null)
                    {
                        for (int idx = 0; idx < result.gates.Count(); idx++)
                        {
                            // find the full esn that matches the provided partial esn and copy that value into the kitid
                            if (result.gates[idx].esn.Substring(0, partialEsnLgth).Equals(partialESN))
                            {
                                if (result.gates[idx].used.ToUpper().Equals("TRUE"))
                                {
                                    fullEsn = result.gates[idx].esn;
                                }
                                else
                                {
                                    // the kid is NOT in use ..
                                    //throw new ArgumentException("DeviceKit starting with {0} is NOT in use.", partialESN);
                                    // instead of throwing an error, continue processing ...
                                }
                            }
                        }
                    }
                    else
                    {
                        // the kit not found with stel ..
                        //throw new ArgumentException("Device Not found in STEL");
                        return fullEsn;
                    }
                }
                else
                {
                    // stel response with error ...
                    throw new ArgumentException("Error response from STEL");
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.ToString());
            }
            return fullEsn;
        }

    }
}