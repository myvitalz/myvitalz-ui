﻿
namespace MyVitalz.Web.Helpers
{
    public struct Operation
    {
        public const string Add = "A";
        public const string Delete = "D";
        public const string Update = "U";
    }
}
