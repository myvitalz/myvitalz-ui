﻿
namespace MyVitalz.Web.Helpers
{
    public struct CookieName
    {
        public const string PartySearchCriteria = "PSCriteria";
        public const string KitSearchCriteria = "KSCriteria";
        public const string IsMobileNurse = "IsMobileNurse";
    }
}