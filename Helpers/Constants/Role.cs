﻿
namespace MyVitalz.Web.Helpers
{
    public struct Role
    {
        public const string Patient = "P";
        public const string Doctor = "D";
        public const string Caregiver = "C";
        public const string Admin = "A";
        public const string GroupAdmin = "E";
        public const string Clinician = "N";
        public const string Separator = ",";
    }
}

