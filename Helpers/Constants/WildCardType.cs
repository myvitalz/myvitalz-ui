﻿
namespace MyVitalz.Web.Helpers
{
    public struct WildCardType
    {
        public const string FirstName = "F";
        public const string LastName = "L";
        public const string Email = "E";
        public const string KitId = "K";
    }
}