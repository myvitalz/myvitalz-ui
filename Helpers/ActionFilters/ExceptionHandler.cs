﻿using MyVitalz.Web.Properties;
using System;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MyVitalz.Web.Helpers.ActionFilters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ExceptionHandler : ActionFilterAttribute, IExceptionFilter
    {
        #region Filter Methods

        public void OnException(ExceptionContext filterContext)
        {
            if (filterContext.HttpContext.IsCustomErrorEnabled == false
                    && filterContext.HttpContext.Request.IsAjaxRequest() == false)
            {
                return;
            }
            filterContext.ExceptionHandled = true;
            LogException(filterContext);
            if (filterContext.HttpContext.Request.IsAjaxRequest() == true)
            {
                filterContext.Result = ExceptionHandler.AjaxError(filterContext);
            }
            else
            {
                filterContext.Result = ExceptionHandler.NonAjaxError(filterContext);
            }
        }
 
        #endregion

        #region Support Methods

        public static ActionResult NonAjaxError(ExceptionContext filterContext)
        {
            var handleErrorInfo = new HandleErrorInfo(filterContext.Exception, filterContext.RouteData.Values["controller"].ToString(), filterContext.RouteData.Values["action"].ToString());
            if (handleErrorInfo.Exception.Message.Contains("The parameters dictionary contains a null entry for parameter"))
            {
                handleErrorInfo = new HandleErrorInfo(new Exception("Requested page is not found or Invalid URL"), handleErrorInfo.ControllerName, handleErrorInfo.ActionName);
            }
            var viewResult = new ViewResult();
            viewResult.ViewName = "Error";
            viewResult.ViewData = new ViewDataDictionary(handleErrorInfo);
            return viewResult;
        }

        public static JsonResult AjaxError(ExceptionContext filterContext)
        {
            filterContext.HttpContext.Response.StatusCode = 500;
            filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
            return new JsonResult()
            {
                Data = new
                {
                    Type = JsonResultType.Error.ToString(),
                    Message = filterContext.Exception.Message,
                    ExceptionType = filterContext.Exception.GetType().Name
                },
                ContentEncoding = Encoding.UTF8,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public static JsonResult SendWarning(string message)
        {
            HttpContext.Current.Response.StatusCode = 500;
            HttpContext.Current.Response.TrySkipIisCustomErrors = true;
            return new JsonResult()
            {
                Data = new
                {
                    Type = JsonResultType.Warning.ToString(),
                    Message = message
                },
                ContentEncoding = Encoding.UTF8,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        #region Logging Methods

        public static string DumpObject(object dumpObject)
        {
            var stringBuilder = new StringBuilder();
            if (dumpObject != null)
            {
                var invokeAttr = BindingFlags.GetProperty;
                var type = dumpObject.GetType();
                stringBuilder.AppendLine(type.Name + " Object:");
                foreach (var propertyInfo in type.GetProperties())
                {
                    var obj = type.InvokeMember(propertyInfo.Name, invokeAttr, (Binder)null, dumpObject, (object[])null);
                    var str = obj != null ? obj.ToString() : string.Empty;
                    if (string.IsNullOrWhiteSpace(str) == false)
                    {
                        stringBuilder.AppendLine(string.Format("{0} \t-\t {1}", propertyInfo.Name, str));
                    }
                }
            }
            return stringBuilder.ToString();
        }

        public static string DumpException(ExceptionContext filterContext)
        {
            Exception exception = filterContext.Exception;
            var stringBuilder = new StringBuilder();
            if (exception != null)
            {
                stringBuilder.AppendLine(exception.GetType().Name + "; ");
                stringBuilder.AppendLine("UserName: " + filterContext.Controller.ValueProvider.GetValue("UserName").AttemptedValue + "; ");
                //stringBuilder.AppendLine("Password: " + filterContext.Controller.ValueProvider.GetValue("Password").AttemptedValue + "; ");
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine("Log: ");
                stringBuilder.AppendLine(exception.StackTrace);
                for (var innerException = exception.InnerException; innerException != null; innerException = innerException.InnerException)
                {
                    stringBuilder.Append("Inner Exception:");
                    stringBuilder.AppendLine(innerException.GetType().Name);
                    stringBuilder.AppendLine(innerException.Message);
                    stringBuilder.AppendLine(innerException.StackTrace);
                }
            }
            return stringBuilder.ToString();
        }

        public static void SendEmail(object userObject, ExceptionContext filterContext)
        {
            try
            {
                var stringBuilder = new StringBuilder();
                stringBuilder.AppendLine(DumpException(filterContext));
                stringBuilder.AppendLine(DumpObject(userObject));
                new SmtpClient(Settings.Default.SmtpServerHost).Send(new MailMessage()
                {
                    To = { Settings.Default.ExceptionEmail },
                    Subject = "Myvitalz Error",
                    From = new MailAddress("Error@myvitalz.com"),
                    Body = stringBuilder.ToString()
                });
            }
            catch
            {
            }
        }

        public static void WriteLogEntry(ExceptionContext filterContext)
        {
            SendEmail((object)Helper.CurrentUser, filterContext);
        }

        public static void LogException(ExceptionContext filterContext)
        {
            switch (filterContext.Exception.GetType().Name)
            {
                case "ArgumentException":
                    break;
                case "ArgumentNullException":
                    break;
                case "EndpointNotFoundException":
                    filterContext.Exception = new Exception("Service is unavailable", filterContext.Exception);
                    ExceptionHandler.WriteLogEntry(filterContext);
                    break;
                default:
                    ExceptionHandler.WriteLogEntry(filterContext);
                    break;
            }
        }

        #endregion        
        
        #endregion
    }
}
