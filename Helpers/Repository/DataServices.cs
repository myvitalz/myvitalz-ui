﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using MySql.Data.MySqlClient;
using MyVitalz.Web.Models;
namespace MyVitalz.Web.Helpers.Repository
{
    public class DataServices
    {

        public string ClientDeviceSave(string device_serial_number)
        {
            string output = "";
            try
            {

                DBConnection sc = new DBConnection();
                sc.connect();
                MySqlCommand cmd = new MySqlCommand("Client_Device_Insert", sc.con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DeviceSerial_number", device_serial_number);
                cmd.Parameters.AddWithValue("@Application", System.Configuration.ConfigurationManager.AppSettings["ClientName"]);
                output = cmd.ExecuteNonQuery().ToString();
                sc.disconnect();
            }
            catch (Exception exe)
            {

            }
            return output;
        }
        public string ClientDeviceDelete(string device_serial_number)
        {
            string output = "";
            try
            {

                DBConnection sc = new DBConnection();
                sc.connect();
                MySqlCommand cmd = new MySqlCommand("Client_Device_Delete", sc.con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DeviceSerial_number", device_serial_number);
                cmd.Parameters.AddWithValue("@Application", System.Configuration.ConfigurationManager.AppSettings["ClientName"]);
                output = cmd.ExecuteNonQuery().ToString();
                sc.disconnect();
            }
            catch (Exception exe)
            {

            }
            return output;
        }
        public string DeactiveParty(string partyid)
        {
            string output = "";
            try
            {

                DBConnectionMain sc = new DBConnectionMain();
                sc.connect();
                MySqlCommand cmd = new MySqlCommand("Party_Deactive", sc.con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PartyId", partyid);                
                output = cmd.ExecuteNonQuery().ToString();
                sc.disconnect();
            }
            catch (Exception exe)
            {

            }
            return output;
        }        

        public List<MainGroupTypes>GetMainGroupTypes(string subgroup)
        {
            List<MainGroupTypes> list = new List<MainGroupTypes>();
            try
            {
               // string mst = System.Configuration.ConfigurationManager.AppSettings["ApplicationClient"];

                DBConnection sc = new DBConnection();
                sc.connect();

                MySqlCommand cmd = new MySqlCommand("GetGroup_viasubgroup", sc.con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("subgroup", subgroup);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new MainGroupTypes()
                        {
                            MainGroupName = reader["group_type"].ToString(),
                            MainGroupDesc = reader["group_desc"].ToString()


                        });
                    }
                }
                sc.disconnect();
            }
            catch (Exception exe)
            {

            }
            return list;

        }

        public string UpdatePartyGroup(string assoicatedPartyId,string PartyId)
        {
            string output = "";
            try
            {

                DBConnectionMain sc = new DBConnectionMain();
                sc.connect();
                MySqlCommand cmd = new MySqlCommand("Party_GroupUpdate", sc.con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@assoicatedPartyId", assoicatedPartyId);
                cmd.Parameters.AddWithValue("@PartyId", PartyId);
                output = cmd.ExecuteNonQuery().ToString();
                sc.disconnect();
            }
            catch (Exception exe)
            {

            }
            return output;
        }


        public string AssociationAddDelete(string id,string Party_Id, string XRef_Id, string XRef_Type,string actiontype)
        {
            string output = "";
            try
            {
               
                DBConnectionMain sc = new DBConnectionMain();
                sc.connect();
                MySqlCommand cmd = new MySqlCommand("Association_InsertDelete", sc.con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@Party_Id", Party_Id);
                cmd.Parameters.AddWithValue("@XRef_Id", XRef_Id);
                cmd.Parameters.AddWithValue("@XRef_Type", XRef_Type);
                cmd.Parameters.AddWithValue("@actiontype", actiontype);           
             

                cmd.Parameters.Add("@Result", MySqlDbType.VarChar, 500);
                cmd.Parameters["@Result"].Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                output = (string)cmd.Parameters["@Result"].Value;

                sc.disconnect();
            }
            catch (Exception exe)
            {

            }
            return output;

        }




    }
}