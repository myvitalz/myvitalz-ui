﻿using MyVitalz.Web.BgService;
using MyVitalz.Web.Models;
using MyVitalz.Web.Service;
using System;
using System.IO;

namespace MyVitalz.Web.Helpers
{
    public interface IServiceRepository
    {
        #region Methods

        SearchByUserCredentialResponse ValidateUserCredential(string userName, string password);

        void Register(Party party, string userName, string password);

        void ForgotPassword(string userName);

        void UpdatePassword(string partyId, string oldPassword, string newPassword);

        Patient[] GetPatients(string group, string partyId);

        GetQuadrantDetailsByPatientResponse GetQuadrantDetailsByPatient(string patientId, string userId);

        GetQuadrantOrderResponse GetQuadrantOrder(string doctorId, string patientId);

        void SaveQuadrantSliding(SaveQuadrantOrderRequest QuadrantOrder);

        GetPatientDailyQAResponse GetFilledDailyQa(string patientId, string patientDailyQaStatsId, string diseaseTypeId);

        GetBloodGlucoseHistoryResponse GetBloodGlucoseReadings(string patientId, DateTime startDate, DateTime endDate);

        GetBloodPressureHistoryResponse GetBloodPressureReadings(string patientId, DateTime startDate, DateTime endDate);

        GetPulseOxiMeterHistoryResponse GetPulseOximeterReadings(string patientId, DateTime startDate, DateTime endDate);

        GetTemperatureHistoryResponse GetTemperatureReadings(string patientId, DateTime startDate, DateTime endDate);

        GetWeighingScaleHistoryResponse GetWeighingScaleReadings(string patientId, DateTime startDate, DateTime endDate);

        GetSpirometerHistoryResponse GetSpirometerReadings(string patientId, DateTime startDate, DateTime endDate);

        GetFeelingHistoryResponse GetFeelingReadings(string patientId, DateTime startDate, DateTime endDate);

        PatientDailyQaScore[] GetMIDailyQaScoreReadings(string patientId, DateTime startDate, DateTime endDate);

        PatientDailyQaScore[] GetDiabetesDailyQaScoreReadings(string patientId, DateTime startDate, DateTime endDate);

        GetSoapResponse GetSoapNotes(string patientId, string soapNoteId, DateTime startDate, DateTime endDate);        

        void AddSoapNote(SoapNote soapNote);

        DiseaseType[] GetDiseases();

        GetQAResponse GetQuestionAnswer(string diseaseId, string patientId);

        void AddDailyQuestions(string patientId, QAAssoc[] QAAsso);

        GetPatientFeaturesResponse GetPatientFeatures(string partyId);

        //public void AddManualReading(ManualReadingModel manualReading);

        GetAggregateHistoryResponse GetAggregateHistory(string patientId, DateTime startDate, DateTime endDate);

        EHRDetails GetEhrDetail(string patientId);

        string SaveEhrDetail(EHRDetails ehrDetails, string userId);

        MyVitalz.Web.Service.Threshold[] GetParameters(string patientId);

        void SaveParameters(MyVitalz.Web.Service.Threshold[] parameters, string savedBy);

        Party GetPartyById(string partyId);
        GetPartyResponse GetProfile(string partyId);

        BasicParty[] GetParties(string groupId, string partyType, string wildCardType, string wildCard);

        BasicParty[] GetAssociatedParties(string patientId);

        SavePartyResponse SaveParty(Party party, Preferences[] preferences, string savedBy);

        void DeleteParty(string partyId, string partyType);

        Device GetDevice(string deviceId);

        Party[] GetParties(string groupId, SearchModel model);
        Stream GetPartiesExcel(string p, SearchModel searchModel);
        Stream GetPatientsExcel(string group);
        Stream GetPartiesDNCExcel(string group);
        Stream GetKitsExcel(SearchModel searchModel);

        void SavePatient(Patient patient, string savedBy);

        XrefType[] GetAssociationTypes();

        XrefType[] GetGroupAssociationTypes();

        Xref[] GetAssociationsByParty(string partyId);

        void SaveAssociations(Xref[] associations, string userId);

        void SaveAssociation(XrefType association, string userId);

        GetQuadrantDetailsResponse GetBgDeviceQuadrant(string patientId);

        GetEkgHistoryResponse GetBgEkgReadings(string patientId, DateTime startDate, DateTime endDate);

        Report GetDailyReport(string partyId, bool canOverride);

        string[] GetKits(string groupId);

        //DeviceKit SearchKits(string wildCard);
        DeviceKit[] SearchKitByCriteria(SearchModel model);
        Party[] SearchPartyInKitByCriteria(SearchModel model);
        Patient[] SearchKitAndPartyDetails(SearchModel model);

        GetDeviceDetailsResponse GetKit(string kitId);

        void ReturnKitToInventory(string partyId, string userId);

        Stream GetQuickBookExportFile(string group);

        void ManageSubscription(string email, bool subscribe);

        void EmailDailyReport(string partyId, string email);

        GroupType[] GetGroups();

        SubGroupWrapper[] GetAllSubGroupTypes();

        void SaveGroup(GroupType model, string savedBy);

        GetPatientByGroupResponse GetPatientsByGroup(string groupName, int pageNo);

        GetPartiesDetailsResponse GetPartiesDetails(string groupName, int pageNo);

        void RecordFeelings(Feeling feeling);

        ReadingsReport GetDailyReadingsReport(string partyId, string role, string group, bool canOverride);

        #endregion

    }
}
