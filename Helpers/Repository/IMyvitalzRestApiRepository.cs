﻿using MyVitalz.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyVitalz.Web.Helpers.Repository
{
    public interface IMyvitalzRestApiRepository
    {
        #region Methods

        InsuranceModel GetPatientInsurance(string patientId);
        string SaveInsurance(string patientId, InsuranceModel insurance, string userId);
        string GetSubGroupTypeForParty(string partyId);
        #endregion
    }
}
