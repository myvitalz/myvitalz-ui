﻿using MyVitalz.Web.ForaTeleHealthDataService;
using MyVitalz.Web.Models;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using System.Xml.Serialization;

namespace MyVitalz.Web.Helpers.Repository
{
    public class ForaTeleHealthRepository : IForaTeleHealthRepository
    {
        private string MYVITALZ_USERNAME = System.Configuration.ConfigurationManager.AppSettings["foraTeleCareUserName"];
        private string MYVITALZ_PASSWORD = System.Configuration.ConfigurationManager.AppSettings["foraTeleCarePassword"];
        private string ACCOUNT_USERNAME = System.Configuration.ConfigurationManager.AppSettings["foraTeleCareAccountUserName"];
        private string ACCOUNT_PASSWORD = System.Configuration.ConfigurationManager.AppSettings["foraTeleCareAccountPassword"];

        private string FORA_URL = System.Configuration.ConfigurationManager.AppSettings["foraServiceUrl"];


        public string AddPatientToForaTeleHealth(EHRDetailsModel model)
        {
            ForaTeleHealthDataService.sValidationSoapHeader sHdr = new ForaTeleHealthDataService.sValidationSoapHeader();
            ForaTeleHealthDataService.DataInterchangeRequest req = new ForaTeleHealthDataService.DataInterchangeRequest();
            ForaTeleHealthDataService.DataInterchangeResponse soapRsp = new ForaTeleHealthDataService.DataInterchangeResponse();
            ForaTeleHealthDataService.WS_DataInterchangeServiceSoapClient client = new ForaTeleHealthDataService.WS_DataInterchangeServiceSoapClient();
            sHdr.Username = MYVITALZ_USERNAME;
            sHdr.Password = MYVITALZ_PASSWORD;

            AddNewData aData = new AddNewData();
            aData.Account = ACCOUNT_USERNAME;
            aData.Password = ACCOUNT_PASSWORD;
            aData.ACase = new AddNewDataACase();
            aData.ACase.Case = new AddNewDataACaseCase();
            aData.ACase.Case.PID = model.Details.kitDetails.kitId;   // device id
            aData.ACase.Case.PAcct = model.Details.kitDetails.kitId;   // device id
            aData.ACase.Case.PPwd = "123456654321"; //Membership.GeneratePassword(12, 0);      // generate password, 12 long, with 3 non alpha numerics
            aData.ACase.Case.PSAID = "E01";      // given by FORA
            aData.ACase.Case.PTZ = "T0003";      // time zone give by FORA TELEHEALTH
            aData.ACase.Case.PName = "MyVitalzPatient";
            aData.ACase.Case.PBirthday = "";
            aData.ACase.Case.PSex = 1;
            aData.ACase.Case.PHeight = "";
            aData.ACase.Case.PWeight = "";
            aData.ACase.Case.PTel1 = "";
            aData.ACase.Case.PTel2 = "";
            aData.ACase.Case.PEMail = "";
            aData.ACase.Case.PTGetUp = "06:00";
            aData.ACase.Case.PTBreakfast = "08:00";
            aData.ACase.Case.PTLunch = "12:00";
            aData.ACase.Case.PTDinner = "18:00";
            aData.ACase.Case.PTSleep = "22:00";
            aData.ACase.Case.PAddr = "";
            aData.ACase.Case.PRaceNo = "";
            aData.ACase.Case.MeterData = new AddNewDataACaseCaseMeterData();
            aData.ACase.Case.MeterData.Meter = new AddNewDataACaseCaseMeterDataMeter();
            aData.ACase.Case.MeterData.Meter.MeterType = "TD4122";     // given by FORA
            aData.ACase.Case.MeterData.Meter.MeterExID = 1;
            aData.ACase.Case.MeterData.Meter.MeterID = model.Details.kitDetails.kitId;    // device id
            aData.ACase.Case.MeterData.Meter.GatewayID = "";

            req.sValidationSoapHeader = sHdr;
            req.iCMDType = "A0001";    // code to Add new Patient
            req.iData = ConvertToXML(aData);

            Console.Out.WriteLine("Request to FORA : " + ConvertToXML(req));

            // bypass the error ...
            // "could not establish trust relationship for ssl/tls secure channel with authority telehealth.foracare.com"
            System.Net.ServicePointManager.ServerCertificateValidationCallback += delegate { return true; };
            string rsp = client.DataInterchange( ref sHdr, req.iCMDType, req.iData);
            AddNewResult rslt = ConvertToObj(rsp);
            if( (int)rslt.ReCode == 0 || (int)rslt.ReCode == 35 )
            {
                // successfully created a patient with FORA Telehealth
                new DataServices().ClientDeviceSave(model.Details.kitDetails.kitId);
                return "success";
            }
            else
            {
                return rslt.ReMsg;
            }
        }

        public string DeletePatientFromForaTeleHealth(string kitId, string partyId)
        {
            /*
             * 
             *   <DelData>
                 <Account>TestDemo</Account>
                 <Password>123456</Password>
                 <DCase>
                     <CaseMeterData PID="P001">
                         <Meter>
                             <MeterType>TD3252</MeterType>
                             <MeterExID>2</MeterExID>
                             <MeterID>3252123456789012</MeterID>
                         </Meter>
                     </CaseMeterData>
                 </DCase>
                </DelData> 
             * 
             */
            ForaTeleHealthDataService.sValidationSoapHeader sHdr = new ForaTeleHealthDataService.sValidationSoapHeader();
            ForaTeleHealthDataService.DataInterchangeRequest req = new ForaTeleHealthDataService.DataInterchangeRequest();
            ForaTeleHealthDataService.DataInterchangeResponse soapRsp = new ForaTeleHealthDataService.DataInterchangeResponse();
            ForaTeleHealthDataService.WS_DataInterchangeServiceSoapClient client = new ForaTeleHealthDataService.WS_DataInterchangeServiceSoapClient();
            sHdr.Username = MYVITALZ_USERNAME;
            sHdr.Password = MYVITALZ_PASSWORD;

            DelData delData = new DelData();
            delData.Account = ACCOUNT_USERNAME;
            delData.Password = ACCOUNT_PASSWORD;

            DelDataCaseMeterDataMeter dm = new DelDataCaseMeterDataMeter();
            dm.MeterType = "TD4122";
            dm.MeterExID = 1;
            dm.MeterID = kitId;

            DelDataCaseMeterData arr = new DelDataCaseMeterData();
            arr.PID = kitId;
            arr.Meter = new DelDataCaseMeterDataMeter[1] { dm };

            delData.DCase = new DelDataCaseMeterData[1] { arr };

            req.sValidationSoapHeader = sHdr;
            req.iCMDType = "D0001";    // code to Add new Patient
            req.iData = ConvertToXML(delData);

            Console.Out.WriteLine("Request to FORA : " + ConvertToXML(req));

            // bypass the error ...
            // "could not establish trust relationship for ssl/tls secure channel with authority telehealth.foracare.com"
            System.Net.ServicePointManager.ServerCertificateValidationCallback += delegate { return true; };
            string rsp = client.DataInterchange(ref sHdr, req.iCMDType, req.iData);

            DelResult rslt = ConvertToDelDataObj(rsp);
            if ((int)rslt.ReCode == 20)
            {
                //Delete ClientDevice maping for different application
                new DataServices().ClientDeviceDelete(kitId);
                // successfully created a patient with FORA Telehealth               

                return "success";
            }
            else
            {
                return rslt.ReMsg;
            }
        }


        public string DeleteForaKite(string kiteId)
        {
          return  new DataServices().ClientDeviceDelete(kiteId);
        }

        public string ConvertToXML<T>(T objToSerialize)
        {
            using (var stringwriter = new System.IO.StringWriter())
            {
                var serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(stringwriter, objToSerialize);
                return stringwriter.ToString();
            }
        }

        public AddNewResult ConvertToObj(string strToConvert)
        {
            using (var stringReader = new System.IO.StringReader(strToConvert))
            {
                var serializer = new XmlSerializer(typeof(AddNewResult));
                AddNewResult obj = (AddNewResult)serializer.Deserialize(stringReader);
                return obj;
            }
        }
        public DelResult ConvertToDelDataObj(string strToConvert)
        {
            using (var stringReader = new System.IO.StringReader(strToConvert))
            {
                var serializer = new XmlSerializer(typeof(DelResult));
                DelResult obj = (DelResult)serializer.Deserialize(stringReader);
                return obj;
            }
        }

    }
}