﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace MyVitalz.Web.Helpers.Repository
{
    public class RestApiRepository
    {
        [HttpPut]
        public object CreateSTELPatient()
        {
            object result = "";
            string createPatientUri = "create_patient/";
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["StelEndPoint"] + createPatientUri);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Headers.Add("Authorization", System.Configuration.ConfigurationManager.AppSettings["StelAccessToken"]);
                httpWebRequest.Method = "PUT";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{}";
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
            }
            catch (WebException e)
            {
                return "{\"status\":\"" + e.Message + "\"}";
            }
            return result;
        }


        [HttpPost]
        public object RegisterHubWithStel(string hubId, string stelPatientId)
        {
            object result = "";
            string registerEsnUri = "esn/";
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["StelEndPoint"] + registerEsnUri);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Headers.Add("Authorization", System.Configuration.ConfigurationManager.AppSettings["StelAccessToken"]);
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"esn\":\"" + hubId + "\"," +
                                   "\"pid\":\"" + stelPatientId + "\"," +
                                   "\"req\":\"register\" }";
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
            }
            catch (WebException e)
            {
                return "{\"error\":\"" + e.Message + "\"}";
            }
            return result;
        }

        [HttpPost]
        public object UnRegisterHubWithStel(string hubId, string stelPatientId)
        {
            object result = "";
            string unRegisterEsnUri = "esn/";
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["StelEndPoint"] + unRegisterEsnUri);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Headers.Add("Authorization", System.Configuration.ConfigurationManager.AppSettings["StelAccessToken"]);
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"esn\":\"" + hubId + "\"," +
                                   "\"pid\":\"" + stelPatientId + "\"," +
                                   "\"req\":\"unregister\" }";
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
            }
            catch (WebException e)
            {
                return "{\"error\":\"" + e.Message + "\"}";
            }
            return result;
        }

        public object GetFullESN(string partialESN, string pid)
        {
            object result = "";
            string registerEsnUri = "esn/";
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["StelEndPoint"] + registerEsnUri);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Headers.Add("Authorization", System.Configuration.ConfigurationManager.AppSettings["StelAccessToken"]);
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"partialEsn\":\"" + partialESN + "\"," +
                                   "\"pid\":\"" + pid + "\"," +
                                   "\"req\":\"check\" }";
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
            }
            catch (WebException e)
            {
                return "{\"status\":\"" + e.Message + "\"}";
            }
            return result;
        }

    }

}