﻿using MyVitalz.Web.BgService;
using MyVitalz.Web.Models;
using MyVitalz.Web.Properties;
using MyVitalz.Web.Service;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using BgResponse = MyVitalz.Web.BgService.Response;
using Response = MyVitalz.Web.Service.Response;
using Threshold = MyVitalz.Web.Service.Threshold;
using MyVitalz.Web.Helpers.Repository;
namespace MyVitalz.Web.Helpers
{
    public class ServiceRepository : IServiceRepository
    {
        #region Local Variables

        private MyVitalzServiceClient _service;
        private MyVitalzBGServiceClient _bgService;

        #endregion

        #region Properties

        public MyVitalzServiceClient Service
        {
            get
            {
                if (_service == null)
                    _service = new MyVitalzServiceClient();
                return _service;
            }
        }

        public MyVitalzBGServiceClient BgService
        {
            get
            {
                if (_bgService == null)
                    _bgService = new MyVitalzBGServiceClient();
                return _bgService;
            }
        } 
        #endregion

        #region Support Methods

        private string GetDateTime(string dateAsString, string dateType)
        {
            var result = DateTime.MinValue;
            if (string.IsNullOrWhiteSpace(dateAsString) == false
                    && DateTime.TryParse(dateAsString, out result) == false)
            {
                throw new ArgumentException(string.Format("{0} is not a valid {1}", dateAsString, dateType));
            }
            if (result == DateTime.MinValue)
            {
                return null;
            }
            else
            {
                return result.ToString("MM/dd/yyyy hh:mm tt");
            }
        }

        private void ValidateResponse(Response response)
        {
            if (response != null
                    && response.responseCode != "0")
            {
                throw new Exception(response.responseDesc);
            }
        }

        private void ValidateResponse(BgResponse response)
        {
            if (response != null
                    && response.responseCode != "0")
            {
                throw new Exception(response.responseDesc);
            }
        } 
        #endregion

        public SearchByUserCredentialResponse ValidateUserCredential(string userName, string password)
        {
            var credentialResponse = Service.searchByUserCredential(new UserCredential()
            {
                userID = userName,
                password = password
            });
            ValidateResponse((Response)credentialResponse);
            if (credentialResponse.authenticated == false)
            {
                throw new Exception(credentialResponse.responseDesc);
            }
            else
            {
                return credentialResponse;
            }
        }

        public void Register(Party party, string userName, string password)
        {
            //party.dob = GetDateTime(party.dob, "Date Of Birth");
            party.dob = Convert.ToDateTime(party.dob).ToString("yyyy-MM-dd");
            ValidateResponse(Service.registration(userName, password, party));
        }

        public void ForgotPassword(string userName)
        {
            ValidateResponse(Service.resetPassword(userName));
        }

        public void UpdatePassword(string partyId, string oldPassword, string newPassword)
        {
            ValidateResponse(Service.changePassword(partyId, oldPassword, newPassword));
        }

        public Patient[] GetPatients(string groupId, string partyId)
        {
            var patientsByDoctor = Service.getPatientsByDoctor(partyId, groupId);
            ValidateResponse(patientsByDoctor);
            if (patientsByDoctor.patients != null)
            {
                var activePatients = patientsByDoctor.patients.Where(c => c.isPartyActive == true).ToArray();
                return activePatients;
            }
            else
            {
                return null;
            }
        }

        public BasicParty[] GetAssociatedParties(string patientId)
        {
            var relationshipById = Service.getRelationshipById(patientId, Role.Patient);
            ValidateResponse(relationshipById);
            if (relationshipById.basicParties != null)
            {
                foreach (var party in relationshipById.basicParties)
                {
                    if (party.email == null)
                    {
                        party.email = string.Empty;
                    }
                }
            }
            return relationshipById.basicParties;
        }

        public BasicParty[] GetParties(string groupId, string partyType, string wildCardType, string wildCard)
        {
            var partiesByType = Service.getPartiesByType(partyType, wildCardType, wildCard, groupId);
            ValidateResponse(partiesByType);
            if (partiesByType.response != null)
            {
                //Reset null with Empty for Email Address
                foreach (var party in partiesByType.response)
                {
                    if (party.email == null)
                    {
                        party.email = string.Empty;
                    }
                }
            }
            return partiesByType.response;
        }

        public GetPartiesDetailsResponse GetPartiesDetails(string groupName, int pageNo)
        {
            List<Patient> patients = new List<Patient>();
            var DoctorClinicianCargiver = Service.getPartiesDetails(groupName, pageNo.ToString());
            ValidateResponse(DoctorClinicianCargiver);
            if (DoctorClinicianCargiver.party != null)
            {
                patients = DoctorClinicianCargiver.party.OrderBy(c => c.partyType).ThenBy(c => c.lastName).ToList();
                //if (groupName != "ALL")
                //{
                //    patients = DoctorClinicianCargiver.party.Where(p => p.homeGroup == groupName).ToList();
                //}
                DoctorClinicianCargiver.party = patients.ToArray();
                DoctorClinicianCargiver.totalCount = patients.Count.ToString();
            }
            return DoctorClinicianCargiver;
        }

        public Party GetPartyById(string partyId)
        {
            var response = Service.getPartyByID(partyId, "ALL");
            ValidateResponse(response);
            return response.party;
        }

        public GetPartyResponse GetProfile(string partyId)
        {
            var response = Service.getPartyByID(partyId, "");
            ValidateResponse(response);
            return response;
        }

        public SavePartyResponse SaveParty(Party party, Preferences[] preferences, string savedBy)
        {

//            party.dob = GetDateTime(party.dob, "Date Of Birth");
            party.dob = Convert.ToDateTime(party.dob).ToString("yyyy-MM-dd");
            
            //Set the Propervalue
            if (preferences != null)
            {
                foreach (var preference in preferences)
                {
                    if (preference.preferenceValue == "false"
                        || preference.preferenceValue == null)
                    {
                        preference.preferenceValue = "0";
                    }
                    else if (preference.preferenceValue == "true")
                    {
                        preference.preferenceValue = "1";
                    }
                }
            }

            var request = new SavePartyTypeRequest()
            {
                party = party,
                prefs = preferences,
                userID = savedBy
            };
            var response = Service.saveParty(request);
            ValidateResponse(response);
            return response;
        }

        public GetQuadrantDetailsByPatientResponse GetQuadrantDetailsByPatient(string patientId, string userId)
        {
            var detailsByPatient = Service.getQuadrantDetailsByPatient(patientId, userId);
            if (detailsByPatient.pulseOxiMeter != null && detailsByPatient.pulseOxiMeter.oxygenReading != null)
            {
                detailsByPatient.pulseOxiMeter.oxygenReading = detailsByPatient.pulseOxiMeter.oxygenReading.Replace("%", "");
            }
            ValidateResponse(detailsByPatient);
            return detailsByPatient;
        }

        public GetQuadrantOrderResponse GetQuadrantOrder(string doctorId, string patientId)
        {
            var quadrantOrder = Service.getQuadrantOrder(doctorId, patientId);
            ValidateResponse(quadrantOrder);
            return quadrantOrder;
        }

        public void SaveQuadrantSliding(SaveQuadrantOrderRequest QuadrantOrder)
        {
            var quadrantOrder = Service.setQuadrantOrder(QuadrantOrder);
            ValidateResponse(quadrantOrder);
        }

        public GetPatientDailyQAResponse GetFilledDailyQa(string patientId, string patientDailyQaStatsId, string diseaseTypeId)
        {
            var filledDailyQaStats = Service.getFilledDailyQa(patientId, patientDailyQaStatsId, diseaseTypeId);
            ValidateResponse(filledDailyQaStats);
            return filledDailyQaStats;
        }

        public GetBloodPressureHistoryResponse GetBloodPressureReadings(string patientId, DateTime startDate, DateTime endDate)
        {
            var bloodPressureHistory = Service.getBloodPressureHistory(patientId, startDate.ToString("MM/dd/yyyy"), endDate.AddDays(1).ToString("MM/dd/yyyy"));
            ValidateResponse(bloodPressureHistory);
            return bloodPressureHistory;
        }

        public GetBloodGlucoseHistoryResponse GetBloodGlucoseReadings(string patientId, DateTime startDate, DateTime endDate)
        {
            var bloodGlucoseHistory = Service.getBloodGlucoseHistory(patientId, startDate.ToString("MM/dd/yyyy"), endDate.AddDays(1).ToString("MM/dd/yyyy"));
            if (bloodGlucoseHistory.readings == null)
            {
                bloodGlucoseHistory.readings = new BloodGlucose[0];
            }
            ValidateResponse(bloodGlucoseHistory);
            return bloodGlucoseHistory;
        }

        public GetPulseOxiMeterHistoryResponse GetPulseOximeterReadings(string patientId, DateTime startDate, DateTime endDate)
        {
            var pulseOximeterHistory = Service.getPulseOximeterHistory(patientId, startDate.ToString("MM/dd/yyyy"), endDate.AddDays(1).ToString("MM/dd/yyyy"));
            ValidateResponse(pulseOximeterHistory);
            if (pulseOximeterHistory.readings == null)
            {
                //throw new ArgumentException("Sorry! There are no readings for Oxygen");
                pulseOximeterHistory.readings = new PulseOxiMeter[0];
            }
            else
            {
                foreach (PulseOxiMeter pulseOxiMeter in pulseOximeterHistory.readings)
                {
                    pulseOxiMeter.oxygenReading = pulseOxiMeter.oxygenReading.Replace("%", "");
                }
                if( pulseOximeterHistory.threshold != null && pulseOximeterHistory.threshold.upperLimit.Equals("999"))
                {
                    pulseOximeterHistory.threshold.upperLimit = "100";
                }
            }
            return pulseOximeterHistory;
        }

        public GetTemperatureHistoryResponse GetTemperatureReadings(string patientId, DateTime startDate, DateTime endDate)
        {
            var temperatureHistory = Service.getTemperatureHistory(patientId, startDate.ToString("MM/dd/yyyy"), endDate.AddDays(1).ToString("MM/dd/yyyy"));
            ValidateResponse(temperatureHistory);
            return temperatureHistory;
        }

        public GetWeighingScaleHistoryResponse GetWeighingScaleReadings(string patientId, DateTime startDate, DateTime endDate)
        {
            var weighingScaleHistory = Service.getWeighingScaleHistory(patientId, startDate.ToString("MM/dd/yyyy"), endDate.AddDays(1).ToString("MM/dd/yyyy"));
            ValidateResponse(weighingScaleHistory);
            return weighingScaleHistory;
        }

        public GetSpirometerHistoryResponse GetSpirometerReadings(string patientId, DateTime startDate, DateTime endDate)
        {
            var spirometerHistory = Service.getSpirometerHistory(patientId, startDate.ToString("MM/dd/yyyy"), endDate.AddDays(1).ToString("MM/dd/yyyy"));
            ValidateResponse(spirometerHistory);
            return spirometerHistory;
        }

        public GetFeelingHistoryResponse GetFeelingReadings(string patientId, DateTime startDate, DateTime endDate)
        {
            var feelingHistory = Service.getFeelingHistory(patientId, startDate.ToString("MM/dd/yyyy"), endDate.AddDays(1).ToString("MM/dd/yyyy"));
            if (feelingHistory.reading == null)
            {
                feelingHistory.reading = new Feeling[0];
            }
            ValidateResponse(feelingHistory);
            return feelingHistory;
        }

        public PatientDailyQaScore[] GetMIDailyQaScoreReadings(string patientId, DateTime startDate, DateTime endDate)
        {
            var aggregateHistory = Service.getAggregateHistory(patientId, startDate.ToString(), endDate.ToString());
            var PatientDailyQaScore = aggregateHistory.mentalIllnessDailyQaScore;
            ValidateResponse(aggregateHistory);
            return PatientDailyQaScore;
        }

        public PatientDailyQaScore[] GetDiabetesDailyQaScoreReadings(string patientId, DateTime startDate, DateTime endDate)
        {
            var aggregateHistory = Service.getAggregateHistory(patientId, startDate.ToString(), endDate.ToString());
            var PatientDailyQaScore = aggregateHistory.diabetesDailyQaScore;
            ValidateResponse(aggregateHistory);
            return PatientDailyQaScore;
        }

        public void AddSoapNote(SoapNote soapNote)
        {
            var addSNRes = Service.addSoapNote(soapNote);
            ValidateResponse(addSNRes);
            //return true;
        }

        public GetSoapResponse GetSoapNotes(string patientId, string soapNoteId, DateTime startDate, DateTime endDate)
        {
            var soapNotes = Service.getSoapNotes(patientId, soapNoteId, startDate.ToString("MM/dd/yyyy"), endDate.AddDays(1).ToString("MM/dd/yyyy"));
            ValidateResponse(soapNotes);
            if (soapNotes.soapNotes == null)
            {
                soapNotes.soapNotes = new SoapNote[0];
            }
            return soapNotes;
        }

        public GetQAResponse GetQuestionAnswer(string diseaseId, string patientId)
        {
            var qA = Service.getQuestionAnswer(diseaseId, patientId);
            ValidateResponse(qA);
            if (qA.qas == null)
            {
                qA.qas = new QAAssoc[0];
            }
            return qA;

            //============

            //List<string> ques = new List<string>();
            //List<string> ans = new List<string>();

            //ques.Add("Little Interest"); ques.Add("Feeling down"); 
            //ques.Add("Moving or speaking slowly that other people could have noticed. Or the opposite - being so figety or restless that you have been moving around a lot more than usual."); 
            //ques.Add("Trouble concentrating on things, such as reading the newspaper or watching television ");
            //ans.Add("Not at all"); ans.Add("Several Days"); ans.Add("More than half the days"); ans.Add("Nearly every day");

            //List<string> row = new List<string>();
            //foreach (var q in ques)
            //{
            //    row.Add(q);
            //    foreach (var a in ans)
            //    {
            //        row.Add(a);
            //    }
            //}

            //string[][] all = new string[][] { ques.ToArray(), ans.ToArray(), row.ToArray() };

            //return all;

        }

        public void AddDailyQuestions(string patientId, QAAssoc[] QAAsso)
        {
            SavePatientQARequest request = new SavePatientQARequest();
            request.patientId = patientId;
            request.qaAssoc = QAAsso;
            MyVitalz.Web.Service.Response rsp = Service.savePatientQA(request);
            ValidateResponse(rsp);
        }

        public GetPatientFeaturesResponse GetPatientFeatures(string partyId)
        {
            var Pf = Service.getPatientFeatures(partyId);
            ValidateResponse(Pf);
            if (Pf.patientFeatures == null)
            {
                Pf.patientFeatures = new PatientFeatures[0];
            }
            return Pf;
        }

        public GetAggregateHistoryResponse GetAggregateHistory(string patientId, DateTime startDate, DateTime endDate)
        {
            var aggregateHistory = Service.getAggregateHistory(patientId, startDate.ToString("MM/dd/yyyy"), endDate.AddDays(1).ToString("MM/dd/yyyy"));
            ValidateResponse(aggregateHistory);
            //var aggregateHistory = new GetAggregateHistoryResponse()
            //{
            //    bloodGlucoseReading = GetBloodGlucoseReadings(patientId, startDate, endDate).readings,
            //    bloodPressureReading = GetBloodPressureReadings(patientId, startDate, endDate).readings,
            //    pulseOxiMeterReading = GetPulseOximeterReadings(patientId, startDate, endDate).readings,
            //    spirometerReading = GetSpirometerReadings(patientId, startDate, endDate).readings,
            //    temperatureReading = GetTemperatureReadings(patientId, startDate, endDate).readings,
            //    weighingScaleReading = GetWeighingScaleReadings(patientId, startDate, endDate).readings
            //};
            if (aggregateHistory.bloodGlucoseReading == null)
            {
                aggregateHistory.bloodGlucoseReading = new BloodGlucose[0];
            }
            if (aggregateHistory.bloodPressureReading == null)
            {
                aggregateHistory.bloodPressureReading = new BloodPressure[0];
            }
            if (aggregateHistory.weighingScaleReading == null)
            {
                aggregateHistory.weighingScaleReading = new WeighingScale[0];
            }
            if (aggregateHistory.temperatureReading == null)
            {
                aggregateHistory.temperatureReading = new Temperature[0];
            }
            if (aggregateHistory.pulseOxiMeterReading != null)
            {
                foreach (PulseOxiMeter pulseOxiMeter in aggregateHistory.pulseOxiMeterReading)
                {
                    pulseOxiMeter.oxygenReading = pulseOxiMeter.oxygenReading.Replace("%", "");
                }
            }
            else
            {
                aggregateHistory.pulseOxiMeterReading = new PulseOxiMeter[0];
            }
            if (aggregateHistory.spirometerReading == null)
            {
                aggregateHistory.spirometerReading = new AsthmaMonitor[0];
            }
            if (aggregateHistory.feeling == null)
            {
                aggregateHistory.feeling = new Feeling[0];
            }
            if( aggregateHistory.mentalIllnessDailyQaScore == null )
            {
                aggregateHistory.mentalIllnessDailyQaScore = new PatientDailyQaScore[0];
            }
            if (aggregateHistory.diabetesDailyQaScore == null)
            {
                aggregateHistory.diabetesDailyQaScore = new PatientDailyQaScore[0];
            }
            
            return aggregateHistory;
        }

        public void DeleteParty(string partyId, string partyType)
        {
            ValidateResponse(Service.deleteParty(partyId, partyType));
        }

        public EHRDetails GetEhrDetail(string patientId)
        {
            var ehrDetails = Service.getEHRDetails(patientId);
            ValidateResponse(ehrDetails);
            if (ehrDetails.deviceDetails != null)
            {
                ehrDetails.deviceDetails = ehrDetails.deviceDetails.OrderBy(d => d.deviceDesc).ToArray();
            }
            if (ehrDetails.familyMembers == null)
            {
                ehrDetails.familyMembers = new BasicParty[0];
            }
            if (ehrDetails.doctors == null)
            {
                ehrDetails.doctors = new BasicParty[0];
            }
            if (ehrDetails.careGivers == null)
            {
                ehrDetails.careGivers = new BasicParty[0];
            }
            //ehrDetails.allSubGroups = ehrDetails.allSubGroups.Where(x => x.groupType != ehrDetails.ehr.group).ToArray();
            return ehrDetails;
        }

        public string SaveEhrDetail(EHRDetails ehrDetails, string userId)
        {
            //Set the Propervalue
            if (ehrDetails.preferences != null)
            {
                foreach (var preference in ehrDetails.preferences)
                {
                    if (preference.preferenceValue == "false"
                        || preference.preferenceValue == null)
                    {
                        preference.preferenceValue = "0";
                    }
                    else if (preference.preferenceValue == "true")
                    {
                        preference.preferenceValue = "1";
                    }
                }
            }

            //Set Primary Doctor
            if (ehrDetails.doctors != null)
            {
                foreach (var doctor in ehrDetails.doctors)
                {
                    if (doctor.isPrimary == "false" || doctor.isPrimary == null)
                    { doctor.isPrimary = "0"; }
                    else if (doctor.isPrimary == "true")
                    { doctor.isPrimary = "1"; }
                }
            }

            //Set Primary Party
            if (ehrDetails.familyMembers != null)
            {
                foreach (var familyMember in ehrDetails.familyMembers)
                {
                    if (familyMember.isPrimary == "false" || familyMember.isPrimary == null)
                    { familyMember.isPrimary = "0"; }
                    else if (familyMember.isPrimary == "true")
                    { familyMember.isPrimary = "1"; }
                }
            }

            //Set Primary Party
            if (ehrDetails.careGivers != null)
            {
                foreach (var careGiver in ehrDetails.careGivers)
                {
                    if (careGiver.isPrimary == "false" || careGiver.isPrimary == null)
                    { careGiver.isPrimary = "0"; }
                    else if (careGiver.isPrimary == "true")
                    { careGiver.isPrimary = "1"; }
                }
            }

            ehrDetails.ehr.dob = Convert.ToDateTime(ehrDetails.ehr.dob).ToString("yyyy-MM-dd");
            ehrDetails.ehr.coverageStartDate = GetDateTime(ehrDetails.ehr.coverageStartDate, "Coverage Effective Date");
            ehrDetails.ehr.coverageEndDate = GetDateTime(ehrDetails.ehr.coverageEndDate, "Coverage End Date");

            bool alreadyexits = true;
            if(ehrDetails.ehr.id == null)
            {
                alreadyexits = false;
            }

            var response = Service.updateEHRDetails(ehrDetails, userId);
            ehrDetails.ehr.id = response.partyId; 
            if(ehrDetails.ehr.id!=null)
            {
                if (alreadyexits == false)
                {
                   // new DataServices().DeactiveParty(ehrDetails.ehr.id);
                    if (ehrDetails.ehr.partyType == "P")
                    {
                        new DataServices().UpdatePartyGroup(userId, ehrDetails.ehr.id);
                    }
                }
            }
            ValidateResponse(response);
            if (ehrDetails.kitDetails != null
                    && string.IsNullOrWhiteSpace(ehrDetails.kitDetails.id) == false)
            {
                SaveDevices(ehrDetails.ehr.id, ehrDetails.kitDetails, ehrDetails.deviceDetails, userId);
            }
            return ehrDetails.ehr.id;
        }

        public DiseaseType[] GetDiseases()
        {            
            GetDiseasesResponse rsp = Service.getDiseases("");

            if(rsp != null)
            {
                DiseaseType[] diseases = new DiseaseType[rsp.diseases.Count()];
                for (int i = 0; i < rsp.diseases.Count(); i++)
                {
                    DiseaseType dt = new DiseaseType();
                    dt.id = rsp.diseases.ElementAt(i).id;
                    dt.name = rsp.diseases.ElementAt(i).name;
                    dt.description = rsp.diseases.ElementAt(i).description;
                    diseases[i] = dt;
                }
                return diseases;
            }
            return null;
        }

        public Threshold[] GetParameters(string patientId)
        {
            var parameters = Service.getParameters(patientId);
            ValidateResponse(parameters);
            if (parameters.param != null)
            {
                parameters.param = parameters.param.Where(p => p.deviceType != DeviceType.BloodPressure).ToArray();
                parameters.param =
                    parameters.param.OrderBy(p => p.deviceType.ToDeviceSortOrder()).ToArray();
                var oxyParam = parameters.param.FirstOrDefault(p => p.deviceType == DeviceType.PulseOximeter);
                if (oxyParam != null)
                {
                    oxyParam.upperLimit = "N/A";
                }
            }
            return parameters.param;
        }

        public void SaveParameters(Threshold[] parameters, string savedBy)
        {
            var oxyParam = parameters.FirstOrDefault(p => p.deviceType == DeviceType.PulseOximeter);
            if (oxyParam != null)
            {
                oxyParam.upperLimit = "999";
            }

            ValidateResponse(Service.updateParameters(new ThresholdDetails()
            {
                param = parameters
            }, savedBy));
        }

        public Party[] GetParties(string groupId, SearchModel searchModel)
        {
            if (Helper.CurrentUser.IsInRole(Role.GroupAdmin) == true)
            {
                groupId = Helper.CurrentUser.Group;
            }
            string firstName = null;
            string lastName = null;
            string ssn = null;
            string dob = null;
            string kitId = null;
            string phoneNumber = null;
            string email = null;
            string externalPatientId = null;
            string subGroupId = null;
            switch (searchModel.Criteria)
            {
                case SearchCriteria.None:
                    throw new ArgumentException("Invalid Criteria");
                case SearchCriteria.FirstName:
                    firstName = searchModel.SearchText;
                    break;
                case SearchCriteria.LastName:
                    lastName = searchModel.SearchText;
                    break;
                case SearchCriteria.DateOfBirth:
                    DateTime result;
                    if (DateTime.TryParse(searchModel.SearchText, out result) == false)
                    {
                        throw new ArgumentException(string.Format("{0} is not Valid Date", searchModel.SearchText));
                    }
                    dob = result.ToString("yyyy-MM-dd");
                    //dob = result.ToString("MM/dd/yyyy hh:mm tt");
                    break;
                case SearchCriteria.KitId:
                    kitId = searchModel.SearchText;
                    break;
                case SearchCriteria.PhoneNumber:
                    phoneNumber = searchModel.SearchText.FormatAsPhoneNumber();
                    break;
                case SearchCriteria.Email:
                    email = searchModel.SearchText;
                    break;
                case SearchCriteria.PatientId:
                    externalPatientId = searchModel.SearchText;
                    break;
                case SearchCriteria.Group:
                    groupId = searchModel.SearchText;
                    break;
                case SearchCriteria.SubGroup:
                    subGroupId = searchModel.SearchText;
                    groupId = Helper.CurrentUser.Group;
                    break;
                default:
                    throw new ArgumentException(string.Format("{0} is not valid Criteria", searchModel.Criteria));
            }
            Party[] parties = null;
            switch (searchModel.SearchFor)
            {
                case SearchFor.AllParties:
                    if (searchModel.Criteria == SearchCriteria.Group)
                    {
                        if (Helper.CurrentUser.IsInRole(Role.Admin) != true)
                        {
                            throw new ArgumentException("You need to be admin to search by Group");
                        }
                        var groupResponse = Service.getPartiesByGroup(groupId, "0");
                        ValidateResponse(groupResponse);
                        parties = groupResponse.party;
                    }
                    else
                    {
                        var criteriaResponse = Service.searchByCriteria(lastName, firstName, ssn, dob, kitId, phoneNumber, email, searchModel.Status, groupId, externalPatientId, subGroupId);
                        ValidateResponse(criteriaResponse);
                        parties = criteriaResponse.party;
                    }
                    // select the parties that are active
                    if (searchModel.Status != null && searchModel.Status.Equals("Active") && parties != null )
                    {
                        parties = (Party[])((from x in parties where x.isPartyActive == true select x).ToArray());
                    }
                    else if (searchModel.Status != null && searchModel.Status.Equals("InActive") && parties != null)
                    {
                        parties = (Party[])((from x in parties where x.isPartyActive == false select x).ToArray());
                    }
                    break;
                case SearchFor.PatientsByDoctor:
                case SearchFor.PatientsByCaregiver:
                case SearchFor.PatientsByClinician:
                    var patientsByDoctorResponse = Service.getPatientsByDoctor(searchModel.PartyId, groupId);
                    ValidateResponse(patientsByDoctorResponse);
                    if (patientsByDoctorResponse.patients != null)
                    {
                        parties = patientsByDoctorResponse.patients.Select(p => (Party)p).ToArray();
                    }
                    break;
                default:
                    throw new ArgumentException("Invalid SearchFor");
            }
            
            return parties;
        }

        public DeviceKit[] SearchKitByCriteria(SearchModel searchModel)
        {
            string kitId = null;
            string group = null;
            //string status = searchModel.Status;
            string status = null;

            if (Helper.CurrentUser.IsInRole(Role.GroupAdmin) == false)
            {
                if (searchModel.Group != null)
                {
                    group = searchModel.Group;
                    kitId = searchModel.KitSearchText;
                }
                else
                {
                    switch (searchModel.KitsCriteria)
                    {
                        case SearchKitsCriteria.None:
                            throw new ArgumentException("Invalid Criteria");
                        case SearchKitsCriteria.KitId:
                            kitId = searchModel.KitSearchText;
                            break;
                        case SearchKitsCriteria.Group:
                            group = searchModel.KitSearchText;
                            break;
                        default:
                            throw new ArgumentException(string.Format("{0} is not valid Criteria", searchModel.Criteria));
                    }
                }
            }
            else
            {
                if (searchModel.Group != null)
                {
                    group = searchModel.Group;
                    kitId = searchModel.KitSearchText;
                }
                else
                {
                    searchModel.KitsCriteria = SearchKitsCriteria.KitId;
                }
            }
            DeviceKit[] device = null;
            if (searchModel.SearchFor != null)
            {
                var  response = Service.searchKitByCriteria(kitId, status, group);
                ValidateResponse(response);
                device = response.deviceKit;
            }
            else
            {
                throw new ArgumentException("Invalid SearchFor");
            }

            return device;
        }

        public Party[] SearchPartyInKitByCriteria(SearchModel searchModel)
        {
            string kitId = null;
            string group = null;
            //string status = searchModel.Status;
            string status = null;

            if (searchModel.Group != null)
            {
                //group = searchModel.Group;
                kitId = searchModel.KitSearchText;
            }
            else
            {
                switch (searchModel.KitsCriteria)
                {
                    case SearchKitsCriteria.None:
                        throw new ArgumentException("Invalid Criteria");
                    case SearchKitsCriteria.KitId:
                        kitId = searchModel.KitSearchText;
                        break;
                    //case SearchKitsCriteria.Group:
                    //    group = searchModel.KitSearchText;
                    //    break;
                    default:
                        throw new ArgumentException(string.Format("{0} is not valid Criteria", searchModel.Criteria));
                }
            }

            Party[] parties = null;

            if (searchModel.SearchFor != null)
            {
                var criteriaResponse = Service.searchByCriteria(null, null, null, null, kitId, null, null, status, "ALL", null, null);
                ValidateResponse(criteriaResponse);
                parties = criteriaResponse.party;
            }
            else
            {
                throw new ArgumentException("Invalid SearchFor");
            }

            return parties;
        }

        public Patient[] SearchKitAndPartyDetails(SearchModel searchModel)
        {
            string kitId = null;
            string group = null;

            if (Helper.CurrentUser.IsInRole(Role.GroupAdmin) == true)
            {
                searchModel.KitsCriteria = SearchKitsCriteria.KitId;
                kitId = searchModel.KitSearchText;
                group = Helper.CurrentUser.Group;
            }
            else
            {
                switch (searchModel.KitsCriteria)
                {
                    case SearchKitsCriteria.None:
                        throw new ArgumentException("Invalid Criteria");
                    case SearchKitsCriteria.KitId:
                        kitId = searchModel.KitSearchText;
                        break;
                    case SearchKitsCriteria.Group:
                        group = searchModel.KitSearchText;
                        break;
                    default:
                        throw new ArgumentException(string.Format("{0} is not valid Criteria", searchModel.Criteria));
                }
            }

            Patient[] patients = null;
            if (searchModel.SearchFor != null)
            {
                var response = Service.searchKitAndPartyDetails(kitId, group);
                ValidateResponse(response);
                patients = response.party;
            }
            else
            {
                throw new ArgumentException("Invalid SearchFor");
            }

            //if (Helper.CurrentUser.IsInRole(Role.GroupAdmin) == true)
            //{
                
            //}

            return patients;
        }

        //public Patient[] GetSearchKitAndPartyDetails(SearchModel searchModel)
        //{
        //    Patient[] patientsDetails;
        //    var patients = SearchKitAndPartyDetails(searchModel);

        //    if (Helper.CurrentUser.IsInRole(Role.GroupAdmin) == true)
        //    {
        //        foreach(var patient in patients)
        //        {
        //            if(patient.deviceKit.groupId == Helper.CurrentUser.Group)
        //            {                        
        //                patientsDetails.SetValue(patient, i);                        
        //            }
        //        }
        //    }

        //    return patients;
        //}
        
        public Stream GetPartiesExcel(string group, SearchModel searchModel)
        {
            var parties = GetParties(group, searchModel);
            //parties = parties.Where(p => p.GridContents().IndexOf(searchModel.FilterText, StringComparison.OrdinalIgnoreCase) != -1).ToArray();
            var package = new ExcelPackage();
            #region Worksheet1
            var worksheet = package.Workbook.Worksheets.Add("Parties");

            //Format the header
            using (var header = worksheet.Cells["A1:T1"])
            {
                header.Style.Font.Bold = true;
                header.Style.WrapText = true;
                header.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                header.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                header.Style.Fill.PatternType = ExcelFillStyle.Solid;
                header.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(237, 237, 237));
                header.Style.Font.Size = 12;
                header.AutoFilter = true;
            }
            worksheet.View.FreezePanes(2, 3);
            worksheet.Cells["A1"].Value = "Last Name";
            worksheet.Cells["B1"].Value = "First Name";
            worksheet.Cells["C1"].Value = "Party Type";
            worksheet.Cells["D1"].Value = "Patient ID";
            worksheet.Cells["E1"].Value = "Kit ID";
            worksheet.Cells["F1"].Value = "Cell Phone";
            worksheet.Cells["G1"].Value = "Home Phone";
            worksheet.Cells["H1"].Value = "Work Phone";
            worksheet.Cells["I1"].Value = "Email";
            worksheet.Cells["J1"].Value = "DOB";
            worksheet.Cells["K1"].Value = "Gender";
            worksheet.Cells["L1"].Value = "Address Line1";
            worksheet.Cells["M1"].Value = "Address Line2";
            worksheet.Cells["N1"].Value = "City";
            worksheet.Cells["O1"].Value = "State";
            worksheet.Cells["P1"].Value = "Zip";
            worksheet.Cells["Q1"].Value = "Active";
            worksheet.Cells["R1"].Value = "Group";
            worksheet.Cells["S1"].Value = "Start Date";
            worksheet.Cells["T1"].Value = "End Date";

            int rowIndexCurrentRecord = 2;
            foreach (var party in parties)
            {
                worksheet.Cells["A" + rowIndexCurrentRecord].Value = party.lastName;
                worksheet.Cells["B" + rowIndexCurrentRecord].Value = party.firstName;
                worksheet.Cells["C" + rowIndexCurrentRecord].Value = party.partyType.ToRoleName();
                worksheet.Cells["D" + rowIndexCurrentRecord].Value = Helper.GetAssociationsAsString(party.externalPatientId, Environment.NewLine);
                worksheet.Cells["E" + rowIndexCurrentRecord].Value = party.deviceKit != null?party.deviceKit.kitId : string.Empty;
                worksheet.Cells["F" + rowIndexCurrentRecord].Value = party.cellPhone;
                worksheet.Cells["G" + rowIndexCurrentRecord].Value = party.homePhone;
                worksheet.Cells["H" + rowIndexCurrentRecord].Value = party.workphone;
                worksheet.Cells["I" + rowIndexCurrentRecord].Value = party.email;
                worksheet.Cells["J" + rowIndexCurrentRecord].Value = Helper.GetDate(party.dob);
                worksheet.Cells["K" + rowIndexCurrentRecord].Value = party.gender;
                worksheet.Cells["L" + rowIndexCurrentRecord].Value = party.GetAddressLine1();
                worksheet.Cells["M" + rowIndexCurrentRecord].Value = party.GetAddressLine2();
                worksheet.Cells["N" + rowIndexCurrentRecord].Value = party.GetCity();
                worksheet.Cells["O" + rowIndexCurrentRecord].Value = party.GetState();
                worksheet.Cells["P" + rowIndexCurrentRecord].Value = party.GetZip5();
                worksheet.Cells["Q" + rowIndexCurrentRecord].Value = party.isPartyActive.ToYesNo();
                worksheet.Cells["R" + rowIndexCurrentRecord].Value = party.group;
                worksheet.Cells["S" + rowIndexCurrentRecord].Value = party.coverageStartDate;
                worksheet.Cells["T" + rowIndexCurrentRecord].Value = party.coverageEndDate;
                rowIndexCurrentRecord++;
            }
            worksheet.Cells["A1:T" + rowIndexCurrentRecord].AutoFitColumns();
            #endregion
            package.Workbook.Properties.Title = "Myvitalz Parties";
            package.Workbook.Properties.Author = "Myvitalz";
            package.Workbook.Properties.Company = "Myvitalz LLC";
            var stream = new MemoryStream();
            package.SaveAs(stream);
            return stream;
        }

        public Stream GetKitsExcel(SearchModel searchModel)
        {
            DeviceKit deviceKit = new DeviceKit();
            string groupId = deviceKit.groupId;
            string kitId = deviceKit.kitId;
            string patientId = deviceKit.patientId;
            string KitsContents = string.Format("{0}{1}{2}", groupId, kitId, patientId);

            //var kits = SearchKitByCriteria(searchModel);
            var patients = SearchKitAndPartyDetails(searchModel);
            //kits = kits.Where(p => KitsContents.IndexOf(searchModel.FilterText, StringComparison.OrdinalIgnoreCase) != -1).ToArray();
            var package = new ExcelPackage();
            #region Worksheet1
            var worksheet = package.Workbook.Worksheets.Add("Kits");

            //Format the header
            using (var header = worksheet.Cells["A1:E1"])
            {
                header.Style.Font.Bold = true;
                header.Style.WrapText = true;
                header.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                header.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                header.Style.Fill.PatternType = ExcelFillStyle.Solid;
                header.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(237, 237, 237));
                header.Style.Font.Size = 12;
                header.AutoFilter = true;
            }
            //worksheet.View.FreezePanes(2, 3);
            worksheet.Cells["A1"].Value = "Kit Group";
            worksheet.Cells["B1"].Value = "Kit ID";
            worksheet.Cells["C1"].Value = "First Name";
            worksheet.Cells["D1"].Value = "Last Name";
            worksheet.Cells["E1"].Value = "Patient ID";

            int rowIndexCurrentRecord = 2;
            foreach (var patient in patients)
            {
                //if (Helper.CurrentUser.IsInRole(Role.Admin) == false)
                //{
                //    if (patient.group == Helper.CurrentUser.Group && patient.deviceKit.kitId.Contains(searchModel.KitSearchText.ToUpper()))
                //    {
                //        worksheet.Cells["A" + rowIndexCurrentRecord].Value = patient.deviceKit.groupId;
                //        worksheet.Cells["B" + rowIndexCurrentRecord].Value = patient.deviceKit.kitId;
                //        worksheet.Cells["C" + rowIndexCurrentRecord].Value = patient.firstName;
                //        worksheet.Cells["D" + rowIndexCurrentRecord].Value = patient.lastName;
                //        worksheet.Cells["E" + rowIndexCurrentRecord].Value = patient.memberId;
                //        rowIndexCurrentRecord++;
                //    }
                //}
                //else
                //{
                    worksheet.Cells["A" + rowIndexCurrentRecord].Value = patient.deviceKit.groupId;
                    worksheet.Cells["B" + rowIndexCurrentRecord].Value = patient.deviceKit.kitId;
                    worksheet.Cells["C" + rowIndexCurrentRecord].Value = patient.firstName;
                    worksheet.Cells["D" + rowIndexCurrentRecord].Value = patient.lastName;
                    worksheet.Cells["E" + rowIndexCurrentRecord].Value = patient.memberId;
                    rowIndexCurrentRecord++;
                //}
            }
            worksheet.Cells["A1:E" + rowIndexCurrentRecord].AutoFitColumns();
            #endregion
            package.Workbook.Properties.Title = "Myvitalz Kits";
            package.Workbook.Properties.Author = "Myvitalz";
            package.Workbook.Properties.Company = "Myvitalz LLC";
            var stream = new MemoryStream();
            package.SaveAs(stream);
            return stream;
        }

        //public string KitsContents(this DeviceKit deviceKit)
        //{
        //    return string.Format("{0}{1}{2}", deviceKit.groupId, deviceKit.kitId, deviceKit.patientId);
        //}

        public Stream GetPatientsExcel(string group)
        {
            var patients = GetPatientsByGroup(@group, Constant.FirstPageIndex).patients;
            var package = new ExcelPackage();

            #region Worksheet1
            var worksheet = package.Workbook.Worksheets.Add("Patients");

            //Format the header
            using (var header = worksheet.Cells["A1:T1"])
            {
                header.Style.Font.Bold = true;
                header.Style.WrapText = true;
                header.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                header.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                header.Style.Fill.PatternType = ExcelFillStyle.Solid;
                header.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(237, 237, 237));
                header.Style.Font.Size = 12;
                header.AutoFilter = true;
            }
            worksheet.View.FreezePanes(2, 3);
            worksheet.Cells["A1"].Value = "Last Name";
            worksheet.Cells["B1"].Value = "First Name";
            worksheet.Cells["C1"].Value = "Party Type";
            worksheet.Cells["D1"].Value = "Patient ID";
            worksheet.Cells["E1"].Value = "Kit ID";
            worksheet.Cells["F1"].Value = "Cell Phone";
            worksheet.Cells["G1"].Value = "Home Phone";
            worksheet.Cells["H1"].Value = "Work Phone";
            worksheet.Cells["I1"].Value = "Email";
            worksheet.Cells["J1"].Value = "DOB";
            worksheet.Cells["K1"].Value = "Gender";
            worksheet.Cells["L1"].Value = "Address Line1";
            worksheet.Cells["M1"].Value = "Address Line2";
            worksheet.Cells["N1"].Value = "City";
            worksheet.Cells["O1"].Value = "State";
            worksheet.Cells["P1"].Value = "Zip";
            worksheet.Cells["Q1"].Value = "Active";
            worksheet.Cells["R1"].Value = "Group";
            worksheet.Cells["S1"].Value = "Start Date";
            worksheet.Cells["T1"].Value = "End Date";

            int rowIndexCurrentRecord = 2;
            foreach (var patient in patients)
            {
                worksheet.Cells["A" + rowIndexCurrentRecord].Value = patient.lastName;
                worksheet.Cells["B" + rowIndexCurrentRecord].Value = patient.firstName;
                worksheet.Cells["C" + rowIndexCurrentRecord].Value = patient.partyType.ToRoleName();
                worksheet.Cells["D" + rowIndexCurrentRecord].Value = Helper.GetAssociationsAsString(patient.externalPatientId, Environment.NewLine);
                worksheet.Cells["E" + rowIndexCurrentRecord].Value = patient.deviceKit != null ? patient.deviceKit.kitId : string.Empty;
                worksheet.Cells["F" + rowIndexCurrentRecord].Value = patient.cellPhone;
                worksheet.Cells["G" + rowIndexCurrentRecord].Value = patient.homePhone;
                worksheet.Cells["H" + rowIndexCurrentRecord].Value = patient.workphone;
                worksheet.Cells["I" + rowIndexCurrentRecord].Value = patient.email;
                worksheet.Cells["J" + rowIndexCurrentRecord].Value = Helper.GetDate(patient.dob);
                worksheet.Cells["K" + rowIndexCurrentRecord].Value = patient.gender;
                worksheet.Cells["L" + rowIndexCurrentRecord].Value = patient.GetAddressLine1();
                worksheet.Cells["M" + rowIndexCurrentRecord].Value = patient.GetAddressLine2();
                worksheet.Cells["N" + rowIndexCurrentRecord].Value = patient.GetCity();
                worksheet.Cells["O" + rowIndexCurrentRecord].Value = patient.GetState();
                worksheet.Cells["P" + rowIndexCurrentRecord].Value = patient.GetZip5();
                worksheet.Cells["Q" + rowIndexCurrentRecord].Value = patient.isPartyActive.ToYesNo();
                worksheet.Cells["R" + rowIndexCurrentRecord].Value = patient.group;
                worksheet.Cells["S" + rowIndexCurrentRecord].Value = patient.coverageStartDate;
                worksheet.Cells["T" + rowIndexCurrentRecord].Value = patient.coverageEndDate;
                rowIndexCurrentRecord++;
            }
            worksheet.Cells["A1:T" + rowIndexCurrentRecord].AutoFitColumns();
            #endregion
            package.Workbook.Properties.Title = "Myvitalz Patients";
            package.Workbook.Properties.Author = "Myvitalz";
            package.Workbook.Properties.Company = "Myvitalz LLC";
            var stream = new MemoryStream();
            package.SaveAs(stream);
            return stream;
        }

        public Stream GetPartiesDNCExcel(string group)
        {
            var patients = GetPartiesDetails(@group, Constant.FirstPageIndex).party;

            var package = new ExcelPackage();

            #region Worksheet1
            var worksheet = package.Workbook.Worksheets.Add("Parties");

            //Format the header
            using (var header = worksheet.Cells["A1:S1"])
            {
                header.Style.Font.Bold = true;
                header.Style.WrapText = true;
                header.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                header.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                header.Style.Fill.PatternType = ExcelFillStyle.Solid;
                header.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(237, 237, 237));
                header.Style.Font.Size = 12;
                header.AutoFilter = true;
            }
            worksheet.View.FreezePanes(2, 3);
            worksheet.Cells["A1"].Value = "Last Name";
            worksheet.Cells["B1"].Value = "First Name";
            worksheet.Cells["C1"].Value = "Party Type";
            worksheet.Cells["D1"].Value = "Cell Phone";
            worksheet.Cells["E1"].Value = "Home Phone";
            worksheet.Cells["F1"].Value = "Work Phone";
            worksheet.Cells["G1"].Value = "Email";
            worksheet.Cells["H1"].Value = "DOB";
            worksheet.Cells["I1"].Value = "Gender";
            worksheet.Cells["J1"].Value = "Address Line1";
            worksheet.Cells["K1"].Value = "Address Line2";
            worksheet.Cells["L1"].Value = "City";
            worksheet.Cells["M1"].Value = "State";
            worksheet.Cells["N1"].Value = "Zip";
            worksheet.Cells["O1"].Value = "Active";
            worksheet.Cells["P1"].Value = "Group";
            worksheet.Cells["Q1"].Value = "Home Group";
            worksheet.Cells["R1"].Value = "Start Date";
            worksheet.Cells["S1"].Value = "End Date";

            int rowIndexCurrentRecord = 2;
            foreach (var patient in patients)
            {
                worksheet.Cells["A" + rowIndexCurrentRecord].Value = patient.lastName;
                worksheet.Cells["B" + rowIndexCurrentRecord].Value = patient.firstName;
                worksheet.Cells["C" + rowIndexCurrentRecord].Value = patient.partyType.ToRoleName();
                worksheet.Cells["D" + rowIndexCurrentRecord].Value = patient.cellPhone;
                worksheet.Cells["E" + rowIndexCurrentRecord].Value = patient.homePhone;
                worksheet.Cells["F" + rowIndexCurrentRecord].Value = patient.workphone;
                worksheet.Cells["G" + rowIndexCurrentRecord].Value = patient.email;
                worksheet.Cells["H" + rowIndexCurrentRecord].Value = Helper.GetDate(patient.dob);
                worksheet.Cells["I" + rowIndexCurrentRecord].Value = patient.gender;
                worksheet.Cells["J" + rowIndexCurrentRecord].Value = patient.GetAddressLine1();
                worksheet.Cells["K" + rowIndexCurrentRecord].Value = patient.GetAddressLine2();
                worksheet.Cells["L" + rowIndexCurrentRecord].Value = patient.GetCity();
                worksheet.Cells["M" + rowIndexCurrentRecord].Value = patient.GetState();
                worksheet.Cells["N" + rowIndexCurrentRecord].Value = patient.GetZip5();
                worksheet.Cells["O" + rowIndexCurrentRecord].Value = patient.isPartyActive.ToYesNo();
                worksheet.Cells["P" + rowIndexCurrentRecord].Value = patient.group;
                worksheet.Cells["Q" + rowIndexCurrentRecord].Value = patient.homeGroup;
                worksheet.Cells["R" + rowIndexCurrentRecord].Value = patient.coverageStartDate;
                worksheet.Cells["S" + rowIndexCurrentRecord].Value = patient.coverageEndDate;
                rowIndexCurrentRecord++;
            }
            worksheet.Cells["A1:S" + rowIndexCurrentRecord].AutoFitColumns();
            #endregion
            package.Workbook.Properties.Title = "Myvitalz Parties";
            package.Workbook.Properties.Author = "Myvitalz";
            package.Workbook.Properties.Company = "Myvitalz LLC";
            var stream = new MemoryStream();
            package.SaveAs(stream);
            return stream;
        }

        public Stream GetQuickBookExportFile(string group)
        {
            //TODO:Requirement change to export it as Excel. Have to get the new Service Method
            var csvFile = string.Empty;
            var webRequest = WebRequest.Create(Properties.Settings.Default.QuickBookExportUrl);
            webRequest.Method = "POST";
            webRequest.ContentType = "application/x-www-form-urlencoded";
            var httpWebResponse = webRequest.GetResponse() as HttpWebResponse;
            if (httpWebResponse != null)
            {
                if (httpWebResponse.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception("An error occured while generating the CSV file"
                                              + httpWebResponse.StatusCode
                                              + httpWebResponse.StatusDescription);
                }
                using (Stream responseStream = httpWebResponse.GetResponseStream())
                {
                    csvFile = new StreamReader(responseStream, Encoding.UTF8).ReadToEnd();
                }
            }

            //To get In-Active party list
            var csvPartyFile = string.Empty;
            var request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetInActivePartyUrl"]);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            var response = request.GetResponse() as HttpWebResponse;
            if (response != null)
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception("An error occured while generating the CSV file"
                                              + response.StatusCode
                                              + response.StatusDescription);
                }
                using (Stream responseStream = response.GetResponseStream())
                {
                    csvPartyFile = new StreamReader(responseStream, Encoding.UTF8).ReadToEnd();
                }
            }

            var package = new ExcelPackage();
            #region Worksheet
            var worksheet = package.Workbook.Worksheets.Add("Parties");
            //Format the header
            using (var header = worksheet.Cells["A1:S1"])
            {
                header.Style.Font.Bold = true;
                header.Style.WrapText = true;
                header.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                header.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                header.Style.Fill.PatternType = ExcelFillStyle.Solid;
                header.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(237, 237, 237));
                header.Style.Font.Size = 12;
                header.AutoFilter = true;
            }
            worksheet.View.FreezePanes(2, 3);
            worksheet.Cells["A1"].Value = "Last Name";
            worksheet.Cells["B1"].Value = "First Name";
            worksheet.Cells["C1"].Value = "Patient ID";
            worksheet.Cells["D1"].Value = "Kit ID";
            worksheet.Cells["E1"].Value = "Cell Phone";
            worksheet.Cells["F1"].Value = "Home Phone";
            worksheet.Cells["G1"].Value = "Work Phone";
            worksheet.Cells["H1"].Value = "Email";
            worksheet.Cells["I1"].Value = "DOB";
            worksheet.Cells["J1"].Value = "Gender";
            worksheet.Cells["K1"].Value = "Address Line1";
            worksheet.Cells["L1"].Value = "Address Line2";
            worksheet.Cells["M1"].Value = "City";
            worksheet.Cells["N1"].Value = "State";
            worksheet.Cells["O1"].Value = "Zip";
            worksheet.Cells["P1"].Value = "Active";
            worksheet.Cells["Q1"].Value = "Group";
            worksheet.Cells["R1"].Value = "Start Date";
            worksheet.Cells["S1"].Value = "End Date";

            int rowIndexCurrentRecord = 2;
            int csvFineNo = 0;
            foreach (var line in csvFile.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None))
            {
                csvFineNo++;
                if (csvFineNo == 1
                    || string.IsNullOrWhiteSpace(line))
                {
                    continue;
                }
                var cols = line.Split(',');
                if (group == "ALL")
                {
                worksheet.Cells["A" + rowIndexCurrentRecord].Value = cols[0];
                worksheet.Cells["B" + rowIndexCurrentRecord].Value = cols[1];
                worksheet.Cells["C" + rowIndexCurrentRecord].Value = cols[2];
                worksheet.Cells["D" + rowIndexCurrentRecord].Value = cols[3];
                worksheet.Cells["E" + rowIndexCurrentRecord].Value = cols[4];
                worksheet.Cells["F" + rowIndexCurrentRecord].Value = cols[5];
                worksheet.Cells["G" + rowIndexCurrentRecord].Value = cols[6];
                worksheet.Cells["H" + rowIndexCurrentRecord].Value = cols[7];
                worksheet.Cells["I" + rowIndexCurrentRecord].Value = cols[8];
                worksheet.Cells["J" + rowIndexCurrentRecord].Value = cols[9];
                worksheet.Cells["K" + rowIndexCurrentRecord].Value = cols[10];
                worksheet.Cells["L" + rowIndexCurrentRecord].Value = cols[11];
                worksheet.Cells["M" + rowIndexCurrentRecord].Value = cols[12];
                worksheet.Cells["N" + rowIndexCurrentRecord].Value = cols[13];
                worksheet.Cells["O" + rowIndexCurrentRecord].Value = cols[14];
                worksheet.Cells["P" + rowIndexCurrentRecord].Value = cols[15];
                worksheet.Cells["Q" + rowIndexCurrentRecord].Value = cols[16];
                worksheet.Cells["R" + rowIndexCurrentRecord].Value = cols[17];
                worksheet.Cells["S" + rowIndexCurrentRecord].Value = cols[18];
                rowIndexCurrentRecord++;
                }
                else if (group == cols[16].ToString())
                {
                    worksheet.Cells["A" + rowIndexCurrentRecord].Value = cols[0];
                    worksheet.Cells["B" + rowIndexCurrentRecord].Value = cols[1];
                    worksheet.Cells["C" + rowIndexCurrentRecord].Value = cols[2];
                    worksheet.Cells["D" + rowIndexCurrentRecord].Value = cols[3];
                    worksheet.Cells["E" + rowIndexCurrentRecord].Value = cols[4];
                    worksheet.Cells["F" + rowIndexCurrentRecord].Value = cols[5];
                    worksheet.Cells["G" + rowIndexCurrentRecord].Value = cols[6];
                    worksheet.Cells["H" + rowIndexCurrentRecord].Value = cols[7];
                    worksheet.Cells["I" + rowIndexCurrentRecord].Value = cols[8];
                    worksheet.Cells["J" + rowIndexCurrentRecord].Value = cols[9];
                    worksheet.Cells["K" + rowIndexCurrentRecord].Value = cols[10];
                    worksheet.Cells["L" + rowIndexCurrentRecord].Value = cols[11];
                    worksheet.Cells["M" + rowIndexCurrentRecord].Value = cols[12];
                    worksheet.Cells["N" + rowIndexCurrentRecord].Value = cols[13];
                    worksheet.Cells["O" + rowIndexCurrentRecord].Value = cols[14];
                    worksheet.Cells["P" + rowIndexCurrentRecord].Value = cols[15];
                    worksheet.Cells["Q" + rowIndexCurrentRecord].Value = cols[16];
                    worksheet.Cells["R" + rowIndexCurrentRecord].Value = cols[17];
                    worksheet.Cells["S" + rowIndexCurrentRecord].Value = cols[18];
                    rowIndexCurrentRecord++;
                }
            }

            worksheet.Cells["A1:S" + rowIndexCurrentRecord].AutoFitColumns();
            #endregion

            #region Worksheet1            
            var worksheet1 = package.Workbook.Worksheets.Add("In-Active Parties");

            //Format the header
            using (var header = worksheet1.Cells["A1:R1"])
            {
                header.Style.Font.Bold = true;
                header.Style.WrapText = true;
                header.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                header.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                header.Style.Fill.PatternType = ExcelFillStyle.Solid;
                header.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(237, 237, 237));
                header.Style.Font.Size = 12;
                header.AutoFilter = true;
            }
            worksheet1.View.FreezePanes(2, 3);
            worksheet1.Cells["A1"].Value = "Last Name";
            worksheet1.Cells["B1"].Value = "First Name";
            worksheet1.Cells["C1"].Value = "Patient ID";
            //worksheet1.Cells["D1"].Value = "Kit ID";
            worksheet1.Cells["D1"].Value = "Cell Phone";
            worksheet1.Cells["E1"].Value = "Home Phone";
            worksheet1.Cells["F1"].Value = "Work Phone";
            worksheet1.Cells["G1"].Value = "Email";
            worksheet1.Cells["H1"].Value = "DOB";
            worksheet1.Cells["I1"].Value = "Gender";
            worksheet1.Cells["J1"].Value = "Address Line1";
            worksheet1.Cells["K1"].Value = "Address Line2";
            worksheet1.Cells["L1"].Value = "City";
            worksheet1.Cells["M1"].Value = "State";
            worksheet1.Cells["N1"].Value = "Zip";
            worksheet1.Cells["O1"].Value = "Active";
            worksheet1.Cells["P1"].Value = "Group";
            worksheet1.Cells["Q1"].Value = "Start Date";
            worksheet1.Cells["R1"].Value = "End Date";            

            int rowIndexCurrRecord = 2;
            int csvNo = 0;
            foreach (var line in csvPartyFile.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None))
            {
                csvNo++;
                if (csvNo == 1
                    || string.IsNullOrWhiteSpace(line))
                {
                    continue;
                }
                var cols = line.Split(',');
                if (group == "ALL")
                {
                    worksheet1.Cells["A" + rowIndexCurrRecord].Value = cols[0];
                    worksheet1.Cells["B" + rowIndexCurrRecord].Value = cols[1];
                    worksheet1.Cells["C" + rowIndexCurrRecord].Value = cols[2];
                    worksheet1.Cells["D" + rowIndexCurrRecord].Value = cols[3];
                    worksheet1.Cells["E" + rowIndexCurrRecord].Value = cols[4];
                    worksheet1.Cells["F" + rowIndexCurrRecord].Value = cols[5];
                    worksheet1.Cells["G" + rowIndexCurrRecord].Value = cols[6];
                    worksheet1.Cells["H" + rowIndexCurrRecord].Value = cols[7];
                    worksheet1.Cells["I" + rowIndexCurrRecord].Value = cols[8];
                    worksheet1.Cells["J" + rowIndexCurrRecord].Value = cols[9];
                    worksheet1.Cells["K" + rowIndexCurrRecord].Value = cols[10];
                    worksheet1.Cells["L" + rowIndexCurrRecord].Value = cols[11];
                    worksheet1.Cells["M" + rowIndexCurrRecord].Value = cols[12];
                    worksheet1.Cells["N" + rowIndexCurrRecord].Value = cols[13];
                    worksheet1.Cells["O" + rowIndexCurrRecord].Value = cols[14];
                    worksheet1.Cells["P" + rowIndexCurrRecord].Value = cols[15];
                    worksheet1.Cells["Q" + rowIndexCurrRecord].Value = cols[16];
                    worksheet1.Cells["R" + rowIndexCurrRecord].Value = cols[17];
                    //worksheet1.Cells["S" + rowIndexCurrRecord].Value = cols[18];
                    rowIndexCurrRecord++;
                }
                else if (group == cols[15].ToString())
                {
                    worksheet1.Cells["A" + rowIndexCurrRecord].Value = cols[0];
                    worksheet1.Cells["B" + rowIndexCurrRecord].Value = cols[1];
                    worksheet1.Cells["C" + rowIndexCurrRecord].Value = cols[2];
                    worksheet1.Cells["D" + rowIndexCurrRecord].Value = cols[3];
                    worksheet1.Cells["E" + rowIndexCurrRecord].Value = cols[4];
                    worksheet1.Cells["F" + rowIndexCurrRecord].Value = cols[5];
                    worksheet1.Cells["G" + rowIndexCurrRecord].Value = cols[6];
                    worksheet1.Cells["H" + rowIndexCurrRecord].Value = cols[7];
                    worksheet1.Cells["I" + rowIndexCurrRecord].Value = cols[8];
                    worksheet1.Cells["J" + rowIndexCurrRecord].Value = cols[9];
                    worksheet1.Cells["K" + rowIndexCurrRecord].Value = cols[10];
                    worksheet1.Cells["L" + rowIndexCurrRecord].Value = cols[11];
                    worksheet1.Cells["M" + rowIndexCurrRecord].Value = cols[12];
                    worksheet1.Cells["N" + rowIndexCurrRecord].Value = cols[13];
                    worksheet1.Cells["O" + rowIndexCurrRecord].Value = cols[14];
                    worksheet1.Cells["P" + rowIndexCurrRecord].Value = cols[15];
                    worksheet1.Cells["Q" + rowIndexCurrRecord].Value = cols[16];
                    worksheet1.Cells["R" + rowIndexCurrRecord].Value = cols[17];
                    //worksheet.Cells["S" + rowIndexCurrRecord].Value = cols[18];
                    rowIndexCurrRecord++;
                }
            }

            worksheet1.Cells["A1:R" + rowIndexCurrRecord].AutoFitColumns();
            #endregion


            package.Workbook.Properties.Title = "Myvitalz Quickbook";
            package.Workbook.Properties.Author = "MyVitalz";
            package.Workbook.Properties.Company = "MyVitalz LLC";
            var stream = new MemoryStream();
            package.SaveAs(stream);
            return stream;
        }



        public void SavePatient(Patient patient, string savedBy)
        {
            ValidateResponse(Service.savePatient(patient, savedBy));
        }

        public void SaveDevices(string patientId, DeviceKit kit, Device[] devices, string savedBy)
        {
            if (kit != null)
            {
                kit.patientId = patientId;
                kit.userId = savedBy;
                if (kit.status == null)
                {
                    kit.status = Operation.Update;
                }
                if (kit.status == Operation.Add)
                {
                    kit.id = null;
                }
                if (string.IsNullOrWhiteSpace(kit.kitId) == true)
                {
                    throw new ArgumentException("Kit ID cannot be empty.");
                }
                if (devices != null)
                {
                    foreach (Device device in devices)
                    {
                        if (device.status == null)
                        {
                            device.status = Operation.Update;
                        }
                        else if (device.status == Operation.Add)
                        {
                            device.id = string.Empty;
                        }
                        if (string.IsNullOrWhiteSpace(device.deviceID) == true)
                        {
                            throw new ArgumentException("Device ID cannot be empty.");
                        }
                        device.startDate = GetDateTime(device.startDate, "Start Date");
                        device.endDate = GetDateTime(device.endDate, "End Date");
                        device.userId = savedBy;
                    }
                }
            }
            ValidateResponse(Service.saveDevices(kit, devices));
        }

        public XrefType[] GetAssociationTypes()
        {
            var xrefTypes = Service.getXrefTypes("");
            ValidateResponse(xrefTypes);
            return xrefTypes.xrefType;
        }

        public XrefType[] GetGroupAssociationTypes()
        {
            var typeToBeRemoved = new List<int>();
            var xrefTypeResponse = Service.getXrefTypes("");
            ValidateResponse(xrefTypeResponse);
            var xrefTypes = xrefTypeResponse.xrefType.ToList().Where(t => (t.xrefType != "EPC" && t.xrefType != "BGP"));
            return xrefTypes.ToArray();
        }

        public Xref[] GetAssociationsByParty(string partyId)
        {
            var partyXrefById = Service.getPartyXrefById(partyId, Role.Patient);
            ValidateResponse(partyXrefById);
            return partyXrefById.response;
        }

        public void SaveAssociations(Xref[] associations, string userId)
        {
            var msgresult = new StringBuilder();
            foreach (Xref xref in associations)
            {
                xref.updateUser = userId;
                string associationid = "";
                string status = xref.status;
                if (xref.id!=null)
                {
                    associationid = xref.id;
                    if(status==null)
                    {
                        status = "U";
                    }
                }
                
               string result=  new DataServices().AssociationAddDelete(associationid, xref.partyID,xref.xrefId,xref.xrefType, status);
                if(result=="-2")
                {
                    msgresult.AppendLine("Association Type " + xref.xrefType + " Already Exists!");
                }
            }
            if (msgresult.Length != 0) { throw new ArgumentException(msgresult.ToString()); }
            
            //ValidateResponse(Service.savePartyXref(associations));
        }

        public void SaveAssociation(XrefType association, string userId)
        {
            var request = new AddXrefTypeRequest() { xrefs = new XrefType[] { association } };
            ValidateResponse(Service.saveXrefTypes(request));
        }

        public GetQuadrantDetailsResponse GetBgDeviceQuadrant(string patientId)
        {
            return BgService.getQuadrantDetailsByPatient(patientId);
        }

        public GetEkgHistoryResponse GetBgEkgReadings(string patientId, DateTime startDate, DateTime endDate)
        {
            return BgService.getEkgHistory(patientId, startDate.ToString("MM/dd/yyyy"), endDate.ToString("MM/dd/yyyy"));
        }

        public Device GetDevice(string deviceType)
        {
            return new Device()
            {
                deviceType = deviceType
            };
        }

        public string[] GetKits(string groupId)
        {
            if (string.IsNullOrWhiteSpace(groupId) == true
                || groupId == Constant.AllGroup)
            {
                throw new ArgumentException(string.Format("The group ({0}) is invalid", groupId));
            }
            var response = Service.getKitsByGroup(groupId);
            ValidateResponse(response);
            return response.kitIds;
        }

        public GetDeviceDetailsResponse GetKit(string kitId)
        {
            var response = Service.getDeviceDetailsByKit(kitId);
            ValidateResponse(response);
            return response;
        }

        public void ReturnKitToInventory(string partyId, string userId)
        {
            var response = Service.returnKitByPartyId(partyId, userId);
            ValidateResponse(response);
        }

        //public string GetQuickBookExportFile()
        //{
        //    var csvFile = string.Empty;
        //    var webRequest = WebRequest.Create(Properties.Settings.Default.QuickBookExportUrl);
        //    webRequest.Method = "POST";
        //    webRequest.ContentType = "application/x-www-form-urlencoded";
        //    var httpWebResponse = webRequest.GetResponse() as HttpWebResponse;
        //    if (httpWebResponse != null)
        //    {
        //        if (httpWebResponse.StatusCode != HttpStatusCode.OK)
        //        {
        //            throw new Exception("An error occured while generating the CSV file"
        //                                      + httpWebResponse.StatusCode
        //                                      + httpWebResponse.StatusDescription);
        //        }
        //        using (Stream responseStream = httpWebResponse.GetResponseStream())
        //        {
        //            csvFile = new StreamReader(responseStream, Encoding.UTF8).ReadToEnd();
        //        }
        //    }
        //    return csvFile;
        //}

        public Report GetDailyReport(string partyId, bool canOverride)
        {
            var response = Service.getReportDetailsByDoctorId(partyId, canOverride);
            ValidateResponse(response);
            return response.reportResponse;
        }

        public void EmailDailyReport(string partyId, string email)
        {
            var request = WebRequest.Create(Settings.Default.EmailDailyReportUrl);

            request.Method = "POST";
            request.ContentType = "text/plain;charset=utf-8";

            var encoding = new System.Text.UTF8Encoding();
            byte[] bytes = encoding.GetBytes(string.Format(@"{0},{1}", partyId, email));

            request.ContentLength = bytes.Length;

            using (var requestStream = request.GetRequestStream())
            {
                // Send the data.
                requestStream.Write(bytes, 0, bytes.Length);
            }

            var httpWebResponse = request.GetResponse() as HttpWebResponse;
            if (httpWebResponse != null)
            {
                if (httpWebResponse.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception("An error occurred while emailing the Whiteboard"
                                              + httpWebResponse.StatusCode
                                              + httpWebResponse.StatusDescription);
                }
            }
        }

        public void ManageSubscription(string email, bool subscribe)
        {
            var response = Service.emailSubscribeUnsubscribe(email, subscribe ? "Y" : "N");
            ValidateResponse(response);
        }

        public GroupType[] GetGroups()
        {
            var groupTypes = Service.getGroupTypes("");
            ValidateResponse(groupTypes);
            return groupTypes.groupType;
        }

        public SubGroupWrapper[] GetAllSubGroupTypes()
        {
            var groupTypes = Service.getGroupTypes("");
            ValidateResponse(groupTypes);
            return groupTypes.allGroupTypes;
        }

        public void SaveGroup(GroupType group, string userId)
        {
            var request = new AddGroupTypeRequest() { groups = new GroupType[] { group } };
            ValidateResponse(Service.saveGroupTypes(request));
        }

        public GetPatientByGroupResponse GetPatientsByGroup(string groupName, int pageNo)
        {
            var patientsByGroupResponse = Service.getPatientsByGroup(groupName, pageNo.ToString());
            var sortedResponse = patientsByGroupResponse.patients != null ? patientsByGroupResponse.patients.OrderBy(c => c.group).ThenBy(c => c.lastName) : null ;
            patientsByGroupResponse.patients = sortedResponse != null ? sortedResponse.ToArray() : null;
            ValidateResponse(patientsByGroupResponse);
            return patientsByGroupResponse;
        }

        public void RecordFeelings(Feeling feeling)
        {
            var missedFeelings = new List<string>();
            if (string.IsNullOrWhiteSpace(feeling.painReading) == true)
            {
                missedFeelings.Add("Pain");
            }
            if (string.IsNullOrWhiteSpace(feeling.moodReading) == true)
            {
                missedFeelings.Add("Mood");
            }
            if (string.IsNullOrWhiteSpace(feeling.energyReading) == true)
            {
                missedFeelings.Add("Energy");
            }
            if (string.IsNullOrWhiteSpace(feeling.breathingReading) == true)
            {
                missedFeelings.Add("Breathing");
            }
            if (missedFeelings.Any() == true)
            {
                throw new ArgumentException("Rate your " + string.Join(",", missedFeelings.ToArray()));
            }
            var response = Service.recordPatientFeelings(feeling);
            ValidateResponse(response);
        }

        public ReadingsReport GetDailyReadingsReport(string partyId, string role, string group, bool canOverride)
        {
            if( role.Equals(Role.Admin) || role.Equals(Role.GroupAdmin) )
            {
                var response = Service.getDailyReadingsByGroupId(partyId, group, canOverride);
                ValidateResponse(response);
                return response.readingsResponse;
            }
            else if (role.Equals(Role.Doctor) || role.Equals(Role.Clinician) || role.Equals(Role.Caregiver))
            {
                var response = Service.getDailyReadingsByDoctorId(partyId, canOverride);
                ValidateResponse(response);
                return response.readingsResponse;
            }
            else
            {
                // user is a patient, don't show the daily report.
            }
            return null;
        }



    }
}
