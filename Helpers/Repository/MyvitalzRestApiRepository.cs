﻿using MyVitalz.Web.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Policy;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace MyVitalz.Web.Helpers.Repository
{
    public class MyvitalzRestApiRepository : IMyvitalzRestApiRepository
    {

        #region Local Variables

        private MyvitalzRestApiRepository _service;

        #endregion

        #region Properties

        public MyvitalzRestApiRepository MyvitalzRestApiService
        {
            get
            {
                if (_service == null)
                    _service = new MyvitalzRestApiRepository();
                return _service;
            }
        }

        #endregion


        [HttpGet]
        public InsuranceModel GetPatientInsurance(string patientId)
        {
            InsuranceModel ins;
            try
            {
                WebRequest request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetReadingDetails"] + "insurance" + "/" + patientId + "");
                WebResponse response = request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string rspBody = reader.ReadToEnd();
                InsuranceCollection jobject = JsonConvert.DeserializeObject<InsuranceCollection>(rspBody);
                List<InsuranceModel> imList = jobject.Insurances;

                reader.Close();
                response.Close();
                ins = imList != null && imList.Count() > 0 ? imList.ElementAt(0) : new InsuranceModel();
            }
            catch (WebException e)
            {
                ins = null;
                Console.Out.WriteLine("Error : " + e.Message);
            }
            return ins;
        }


        [HttpPost]
        public string SaveInsurance(string patientId, InsuranceModel insurance, string userId)
        {
            string result = "";
            try
            {
                var url = System.Configuration.ConfigurationManager.AppSettings["GetReadingDetails"];
                var rsrc = url + "insurance";
                Console.Out.WriteLine("Url: " + rsrc);
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(rsrc);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    insurance.partyId = patientId;
                    insurance.updateTs = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    insurance.updateUser = userId;
                    insurance.createTs = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    insurance.createUser = userId;
                    var json = new JavaScriptSerializer().Serialize(insurance);
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
            }
            catch (WebException e)
            {
                return "{\"status\":\"" + e.Message + "\"}";
            }
            return result;
        }


        [HttpGet]
        public string GetSubGroupTypeForParty(string partyId)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            try
            {
                WebRequest request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["PatientReadingUrl"] + "party" + "/" + partyId + "/subGroup" + "");
                WebResponse response = request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string rspBody = reader.ReadToEnd();
                if (rspBody != null)
                {
                    result = new JavaScriptSerializer().Deserialize<Dictionary<string, string>>(rspBody);
                } 
                reader.Close();
                response.Close();
            }
            catch (WebException e)
            {
                Console.Out.WriteLine("Error : " + e.Message);
            }
            return result.Count > 0 ? result.ElementAt(0).Value : "";
        }

        /*
                [HttpPost]
                public ActionResult BaseAddDoctor(EHRDetailsModel model)
                {
                    if (model.Details.doctors != null
                            && model.Details.doctors.Count(p => p.status != Operation.Delete) == 10)
                    {
                        throw new ArgumentException("You cannot add more than 10 Doctor ");
                    }
                    model.Details.doctors = AddPatientParty("Doctor", model.Details.doctors, model.NewlyAddedPartyId);
                    return PartialView("_Doctors", model);
                }

                [HttpPost]
                public ActionResult BaseRemoveDoctor(EHRDetailsModel model)
                {
                    model.Details.doctors = BasePartyController.RemovePatientParty("Doctor", model.Details.doctors, model.IndexRemoved);
                    return PartialView("_Doctors", model);
                }

                [HttpPost]
                public ActionResult BaseAddCaregiver(EHRDetailsModel model)
                {
                    if (model.Details.careGivers != null
                        && model.Details.careGivers.Count(p => p.status != Operation.Delete) == 5)
                    {
                        throw new ArgumentException("You cannot add more than 5 Caregivers");
                    }
                    model.Details.careGivers = AddPatientParty("Caregiver", model.Details.careGivers, model.NewlyAddedPartyId);
                    return PartialView("_Caregivers", model);
                }

                [HttpPost]
                public ActionResult BaseRemoveCaregiver(EHRDetailsModel model)
                {
                    model.Details.careGivers = BasePartyController.RemovePatientParty("Caregiver", model.Details.careGivers, model.IndexRemoved);
                    return PartialView("_Caregivers", model);
                }

                [HttpPost]
                public ActionResult BaseAddClinician(EHRDetailsModel model)
                {
                    if (model.Details.familyMembers != null &&
                        model.Details.familyMembers.Count(p => p.status != Operation.Delete) == 10)
                    {
                        throw new ArgumentException("You cannot add more than 10 Clinicians");
                    }
                    model.Details.familyMembers = AddPatientParty("Clinician", model.Details.familyMembers, model.NewlyAddedPartyId);
                    return PartialView("_Nurses", model);
                }

                [HttpPost]
                public ActionResult BaseRemoveClinician(EHRDetailsModel model)
                {
                    model.Details.familyMembers = BasePartyController.RemovePatientParty("Clinician", model.Details.familyMembers, model.IndexRemoved);
                    return PartialView("_Nurses", model);
                }

                [HttpPost]
                public ActionResult BaseAddAssociation(EHRDetailsModel model)
                {
                    var list = new List<Xref>();
                    if (model.Associations != null)
                    {
                        list.AddRange(model.Associations.ToList());
                    }
                    for (var index = 0; index < list.Count; ++index)
                    {
                        if (string.IsNullOrWhiteSpace(list[index].xrefType))
                        {
                            throw new ArgumentException(string.Format("Association {0} cannot be empty", (index + 1)));
                        }
                    }
                    PopulateAssociationTypes(model);
                    list.Add(new Xref()
                    {
                        status = Operation.Add,
                        partyID = model.Details.ehr.id
                    });
                    model.Associations = list.ToArray();
                    return PartialView("_Associations", model);
                }

                [HttpPost]
                public ActionResult BaseRemoveAssociation(EHRDetailsModel model)
                {
                    var list = model.Associations.ToList();
                    if (model.Associations == null
                            || model.Associations.Any() == false)
                    {
                        throw new ArgumentException("There is no Association to remove");
                    }
                    PopulateAssociationTypes(model);
                    if (string.IsNullOrWhiteSpace(list[model.IndexRemoved].id)
                        || list[model.IndexRemoved].id == "0")
                    {
                        list.RemoveAt(model.IndexRemoved);
                    }
                    else
                    {
                        list[model.IndexRemoved].status = Operation.Delete;
                    }
                    model.Associations = list.ToArray();
                    return PartialView("_Associations", model);
                }

                [HttpPost]
                public ActionResult AddDevice(EHRDetailsModel model)
                {
                    if (model.Details.deviceDetails == null)
                    {
                        model.Details.deviceDetails = new Device[0];
                    }
                    var list = model.Details.deviceDetails.ToList();
                    list.Add(new Device()
                    {
                        id = Guid.NewGuid().ToString(),
                        kitId = model.CurrentKitId,
                        status = Operation.Add,
                        startDate = DateTime.Now.ToString("MM-dd-yyyy")
                    });
                    model.Details.deviceDetails = list.ToArray();
                    model.GroupTypes = GetGroupTypes();
                    return PartialView("_Devices", model);
                }

                [HttpPost]
                public ActionResult RemoveDevice(EHRDetailsModel model)
                {
                    if (model.Details.deviceDetails == null)
                    {
                        throw new ArgumentException("There is no Device found.");
                    }
                    model.Details.deviceDetails[model.IndexRemoved].status = Operation.Delete;
                    model.GroupTypes = GetGroupTypes();
                    return PartialView("_Devices", model);
                }

                [HttpPost]
                public ActionResult AddKit(EHRDetailsModel model)
                {
                    if (model.Details.kitDetails == null
                        || string.IsNullOrWhiteSpace(model.Details.kitDetails.id) == true)
                    {
                        model.Details.kitDetails = new DeviceKit()
                        {
                            id = Guid.NewGuid().ToString(),
                            status = Operation.Add,
                            groupId = model.Details.ehr.group
                        };
                    }
                    model.GroupTypes = GetFilteredGroupTypes(true);
                    return PartialView("_Devices", model);
                }

                [HttpPost]
                public ActionResult AssignKit(EHRDetailsModel model)
                {
                    if (model.Details.kitDetails == null
                            || string.IsNullOrWhiteSpace(model.Details.kitDetails.kitId) == true)
                    {
                        throw new ArgumentException("You cannot add None as a Kit");
                    }
                    var kitAndDeviceDetails = Service.GetKit(model.Details.kitDetails.kitId);
                    model.Details.kitDetails = kitAndDeviceDetails.deviceKit;
                    model.Details.kitDetails.status = Operation.Update;
                    model.Details.deviceDetails = kitAndDeviceDetails.deviceDetails;
                    if (model.Details.deviceDetails != null
                            && model.Details.deviceDetails.Length > 0)
                    {
                        model.Details.kitDetails.id = model.Details.deviceDetails.First().kitId;
                        model.Details.kitDetails.groupId = model.Details.ehr.group;
                    }
                    model.GroupTypes = GetGroupTypes();
                    // add an logCareTime entry for reviewing patients feelings
                    AddCareTimeWhenAssigningKitToPatient(model.Details.ehr.id);
                    return PartialView("_Devices", model);
                }

                [HttpPost]
                public ActionResult ReturnKitToInventory(string partyId, string kitId, string stelPid)
                {
                    if (kitId != null && !kitId.ToUpper().Equals("NULL"))
                    {
                        // unregister esn with STEL
                        if (kitId.Length >= 5)
                        {
                            if (!kitId.ToUpper().Contains("QUAL"))
                            {
                                try
                                {
                                    char[] seperators = new char[] { '-' };
                                    string[] kitIdParts = kitId.Split(seperators);
                                    string stelKitIdstart = "CV001";
                                    string stelKitIdEnd = "CV999";
                                    string foraKitIdStart = "F01";
                                    string foraKitIdEnd = "F999";
                                    Console.Out.WriteLine("Incoming KitID = " + kitId);
                                    if (kitIdParts.Length > 1)
                                    {
                                        string kitIdPrefix = kitIdParts[0];
                                        string kit_Id = kitIdParts[1];
                                        Console.Out.WriteLine("prefix = " + kitIdPrefix);
                                        Console.Out.WriteLine("kitId = " + kit_Id);
                                        // the device is either a STEL or a FORA care device
                                        if (kitIdPrefix != null && kitIdPrefix.Substring(0, 2).Equals("CV", StringComparison.OrdinalIgnoreCase))
                                        {
                                            if (String.Compare(kitIdParts[0], stelKitIdstart) >= 0 && String.Compare(kitIdParts[0], stelKitIdEnd) <= 0)
                                            {
                                                if (stelPid != null && !stelPid.ToUpper().Equals("NULL"))
                                                {
                                                    StelProvider stel = new StelProvider();
                                                    stel.UnRegisterKit(kit_Id, stelPid);
                                                    Console.Out.WriteLine("Remove Kit from STEL Patient...");
                                                }
                                                else
                                                {
                                                    Console.Out.WriteLine("STEL Patient ID is NULL ");
                                                }
                                            }
                                        }
                                        else if (kitIdPrefix != null && kitIdPrefix.Substring(0, 1).Equals("F", StringComparison.OrdinalIgnoreCase))
                                        {
                                            if (String.Compare(kitIdParts[0], foraKitIdStart) >= 0 && String.Compare(kitIdParts[0], foraKitIdEnd) <= 0)
                                            {
                                                // add a patient to the FORA TeleHealth care ...
                                                ForaTeleHealthProvider fora = new ForaTeleHealthProvider();
                                                //string rsp = fora.RemoveKitFromTeleHealthPatient(kit_Id, partyId);
                                                string rsp = "success";
                                                Console.Out.WriteLine("Removing Kit from FORA TeleHealth Patient ...");
                                                if (!rsp.Equals("success", StringComparison.OrdinalIgnoreCase))
                                                {
                                                    throw new Exception(rsp);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Console.Out.WriteLine("Kit Id is one that we dont understand ... error out ...");
                                            throw new Exception("Invalid Kit ID " + kitId);
                                        }
                                    }
                                    else
                                    {
                                        string kitIdFull = kitIdParts[0];
                                        Console.Out.WriteLine("No prefix for this kit.  The kit ID : " + kitIdFull);
                                        // one more check to identify the two STEL trial kit that we have that are 6 digits long
                                        // do we need this or convert those two to use CV### format also
                                        // until then I will keep this logic
                                        // existing STEL kit id 1 = 3E8C54
                                        // second kit id = 827D78
                                        if (kitIdFull.StartsWith("3E8C54") || kitIdFull.StartsWith("827D78"))
                                        {
                                            if (stelPid != null && !stelPid.ToUpper().Equals("NULL"))
                                            {
                                                StelProvider stel = new StelProvider();
                                                stel.UnRegisterKit(kitIdFull, stelPid);
                                                Console.Out.WriteLine("Remove Kit from STEL Patient...");
                                            }
                                            else
                                            {
                                                Console.Out.WriteLine("STEL Patient ID is NULL ");
                                            }
                                        }
                                    }

                                }
                                catch (NotImplementedException nie)
                                {
                                    Console.Out.WriteLine("FORA TeleHealth Patient is NOT removed from FORA system.");
                                    Console.Out.WriteLine("Error : " + nie.Message);
                                }
                                catch (Exception ex)
                                {
                                    throw new ArgumentException(ex.Message);
                                }
                            }
                        }
                        else
                        {
                            if (kitId.Length < 5)
                            {
                                throw new ArgumentException("Kit It must be atleast 5 character long");
                            }
                        }
                    }
                    // call myvitalz service to return kit
                    Service.ReturnKitToInventory(partyId, CurrentUser.UserId);
                    return new EmptyResult();
                }

                [HttpPost]
                public ActionResult GetKitSearchDialog()
                {
                    var groups = GetFilteredGroupTypes(true);
                    return PartialView("_KitSearchDialog", groups);
                }

                [HttpGet]
                public JsonResult GetAvailableKits(string groupId)
                {
                    var kitIds = Service.GetKits(groupId);
                    return Json(new { Kits = kitIds }, JsonRequestBehavior.AllowGet);
                }

                [HttpPost]
                public ActionResult RemoveKit(EHRDetailsModel model)
                {
                    if (model.Details.kitDetails == null)
                    {
                        throw new ArgumentException("There is no Kit found.");
                    }
                    if (model.Details.kitDetails.status == Operation.Add)
                    {
                        model.Details.kitDetails = null;
                        model.Details.deviceDetails = null;
                    }
                    else
                    {
                        model.Details.kitDetails.status = Operation.Delete;

                        //Mark the device also to delete
                        if (model.Details.deviceDetails != null)
                        {
                            foreach (var item in model.Details.deviceDetails)
                            {
                                if (item.status != Operation.Add)
                                {
                                    item.status = Operation.Delete;
                                }
                            }
                        }
                    }
                    model.GroupTypes = GetGroupTypes();
                    return PartialView("_Devices", model);
                }



                [HttpPost]
                public object LogCareSaved(string partyid, string logcareDate, string Calltime, string Duration, string Notes, string Intervention)
                {
                    // partyid = "1102";
                    string caredBy = CurrentUser.UserId.ToString();
                    int hrs = Duration.AsInt() / 60;
                    Duration = String.Format("{0:D2}:{1:D2}:{2,2}", Convert.ToString(Duration.AsInt() / 60).PadLeft(2,'0'), Convert.ToString(Duration.AsInt() - (hrs * 60)).PadLeft(2, '0'), "00");
                    object result = "";
                    string nts = Notes.Replace("\"", "\\\"");
                    try
                    {
                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["LogCareSavedUrl"]);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Method = "POST";
                        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                        {
                            string json = "{\"date\":\"" + DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss tt") + "\"," +
                                            "\"patient\":\"" + partyid.ToString() + "\"," +
                                            "\"type\":\"careTime\"," +
                                            "\"crt\": {" +
                                            "\"careTimeDate\":\"" + logcareDate + "\"," +
                                            "\"beginTime\":\"" + Calltime + "\"," +
                                            "\"duration\":\"" + Duration + "\"," +
                                            "\"interventionType\":\"" + Intervention + "\"," +
                                            "\"caredBy\":\"" + caredBy + "\"," +
                                            "\"notes\":\"" + nts + "\" }}";
                            streamWriter.Write(json);
                            streamWriter.Flush();
                            streamWriter.Close();
                        }
                        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                        {
                            result = streamReader.ReadToEnd();
                        }
                    }
                    catch (WebException e)
                    {
                        return "{\"status\":\"" + e.Message + "\"}";
                    }
                    return result;
                }

                [HttpPost]
                public string PauseMonitorSaved(string partyid, string startDate, string endDate, string reason, string description)
                {
                    //partyid = "1102";

                    string result = "";
                    try
                    {
                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["PauseMonitorSavedUrl"]);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Method = "POST";
                        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                        {
                             string json = "{\"date\":\"" + DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss zz") + "\"," +
                                            "\"patient\":\"" + partyid.ToString() + "\"," +
                                            "\"type\":\"pauseMonitoring\"," +
                                            "\"pm\": {" +
                                            "\"startDate\":\"" + startDate + "\"," +
                                            "\"endDate\":\"" + endDate + "\"," +
                                            "\"reason\":\"" + reason + "\"," +
                                            "\"description\":\"" + description + "\" }}";
                            streamWriter.Write(json);
                            streamWriter.Flush();
                            streamWriter.Close();
                        }
                        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                        {
                            result = streamReader.ReadToEnd();
                        }
                    }
                    catch (WebException e)
                    {
                        return "{\"status\":\"" + e.Message + "\"}";
                    }
                    return result;
                }

                [HttpPost]
                public object saveBloodHbA1cReading(string partyid, string reading)
                {
                    //partyid = "1102";
                    //call the restapi to delete the reading
                    //using (var httpClient = new System.Net.Http.HttpClient())
                    //{
                    //    ////var url1 = "http://localhost:8080/MyvitalzDataWeb/rest/data";
                    //    var url = System.Configuration.ConfigurationManager.AppSettings["ManageDataUrl"];
                    //    var rsrc = url + "/" + deviceType + "/" + patientId + "/" + reading1 + "/" + reading2 + "/" + mtime;
                    //    Console.Out.WriteLine("Url: " + rsrc);
                    //    var content = new System.Net.Http.StringContent(string.Empty, Encoding.UTF8, "application/json");
                    //    response = await httpClient.DeleteAsync(rsrc);
                    //    rr = await response.Content.ReadAsStringAsync();
                    //    Console.Out.WriteLine("Response : " + response.StatusCode);
                    //    Console.Out.WriteLine("RR : " + rr);
                    //    return Content(rr);
                    //}

                    object result = "";
                    try
                    {
                        var url = System.Configuration.ConfigurationManager.AppSettings["ManageDataUrl"];
                        var rsrc = url + "/A1C/reading";
                        Console.Out.WriteLine("Url: " + rsrc);
                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(rsrc);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Method = "POST";
                        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                        {
                            string json = "{\"dateTime\":\"" + DateTime.Now.ToString("yyyy-MM-dd") + "\"," +
                                           "\"patientId\":\"" + partyid.ToString() + "\"," +
                                           "\"reading\":\"" + reading + "\" }}";
                            streamWriter.Write(json);
                            streamWriter.Flush();
                            streamWriter.Close();
                        }
                        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                        {
                            result = streamReader.ReadToEnd();
                        }
                    }
                    catch (WebException e)
                    {
                        return "{\"status\":\"" + e.Message + "\"}";
                    }
                    return result;
                }

                [HttpPost]
                public string ReadingGridDetails(string patientId,string devicetype,string fromdate, string todate)
                {
                    string result = "";
                    try
                    {               

                        string rt;
                        WebRequest request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetReadingDetails"] + devicetype+"/"+patientId+"/"+fromdate+"/"+todate+"");
                        WebResponse response = request.GetResponse();
                        Stream dataStream = response.GetResponseStream();
                        StreamReader reader = new StreamReader(dataStream);
                        rt = reader.ReadToEnd();
                        Console.WriteLine(rt);
                        reader.Close();
                        response.Close();
                        return rt;

                    }
                    catch (WebException e)
                    {
                    }
                    return result;
                }

                [HttpGet]
                public string getHbA1cHistoricalReadings(string patientId)
                {
                    string result = "";
                    try
                    {

                        string rt;
                        WebRequest request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetReadingDetails"] + "A1C" + "/" + patientId + "");
                        WebResponse response = request.GetResponse();
                        Stream dataStream = response.GetResponseStream();
                        StreamReader reader = new StreamReader(dataStream);
                        rt = reader.ReadToEnd();
                        Console.WriteLine(rt);
                        reader.Close();
                        response.Close();
                        return rt;

                    }
                    catch (WebException e)
                    {
                    }
                    return result;
                }

                [HttpGet]
                public string getHbA1cHistoricalReadings(string patientId, string fromdate, string todate)
                {
                    string result = "";
                    try
                    {

                        string rt;
                        WebRequest request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetReadingDetails"] + "A1C" + "/" + patientId + "/" + fromdate + "/" + todate  + "");
                        WebResponse response = request.GetResponse();
                        Stream dataStream = response.GetResponseStream();
                        StreamReader reader = new StreamReader(dataStream);
                        rt = reader.ReadToEnd();
                        Console.WriteLine(rt);
                        reader.Close();
                        response.Close();
                        return rt;

                    }
                    catch (WebException e)
                    {
                    }
                    return result;
                }

                [HttpGet]
                public string getPillsyHistoricalReadings(string patientId, string deviceId, string devicetype, string fromdate, string todate)
                {
                    string result = "";
                    try
                    {

                        string rt;
                        WebRequest request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetReadingDetails"] + devicetype + "/" + patientId + "/" + fromdate + "/" + todate + "/" + deviceId + "");
                        WebResponse response = request.GetResponse();
                        Stream dataStream = response.GetResponseStream();
                        StreamReader reader = new StreamReader(dataStream);
                        rt = reader.ReadToEnd();
                        Console.WriteLine(rt);
                        reader.Close();
                        response.Close();
                        return rt;

                    }
                    catch (WebException e)
                    {
                    }
                    return result;
                }

                [HttpGet]
                public string GetPauseMonitorReadingForToday(string patientId)
                {
                    string result = "";
                    string devicetype = "PMN";
                    string todayDate = DateTime.Now.ToString("yyyy-MM-dd");
                    try
                    {

                        string rt;
                        WebRequest request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetReadingDetails"] + devicetype + "/" + patientId + "/" + todayDate + "");
                        WebResponse response = request.GetResponse();
                        Stream dataStream = response.GetResponseStream();
                        StreamReader reader = new StreamReader(dataStream);
                        rt = reader.ReadToEnd();
                        Console.WriteLine(rt);
                        reader.Close();
                        response.Close();
                        return rt;

                    }
                    catch (WebException e)
                    {
                    }
                    return result;
                }

                [HttpGet]
                public string GetPillsyReadingForToday(string patientId)
                {
                    string result = "";
                    string devicetype = "MED";
                    string todayDate = DateTime.Now.ToString("yyyy-MM-dd");
                    string nextDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                    try
                    {

                        string rt;
                        WebRequest request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetReadingDetails"] + devicetype + "/" + patientId + "/" + todayDate +"/"+ nextDate);
                        WebResponse response = request.GetResponse();
                        Stream dataStream = response.GetResponseStream();
                        StreamReader reader = new StreamReader(dataStream);
                        rt = reader.ReadToEnd();
                        Console.WriteLine(rt);
                        reader.Close();
                        response.Close();
                        return rt;
                    }
                    catch (WebException e)
                    {
                    }
                    return result;
                }

                [HttpPost]
                public object DeleteGridDetails(string deleterecord)
                {
                    object result = "";
                    try
                    {

                        string rt;
                        WebRequest request = WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetReadingDetails"] + deleterecord);
                        request.Method = "DELETE";
                        WebResponse response = request.GetResponse();

                        Stream dataStream = response.GetResponseStream();
                        StreamReader reader = new StreamReader(dataStream);
                        rt = reader.ReadToEnd();
                        Console.WriteLine(rt);
                        reader.Close();
                        response.Close();
                        return rt;

                    }
                    catch (WebException e)
                    {
                    }
                    return result;


                }


                [HttpPost]
                public HttpWebResponse AddManualReading(ManualReadingModel manualReading)
                {
                    HttpResponse httpResponse = null;
                    HttpWebResponse response = null;
                    //StreamWriter writer = null;
                    string responseText; int flag = 0;
                    string str_BPReading_Systolic = null, str_BPReading_Diastolic = null;
                    string str_SMReading_Fev = null, str_SMReading_Pef = null;
                    string str_A1CReading = null;
                    bool a1cPresent = false;

                    try
                    {
                        Hashtable manualReadingList = new Hashtable();

                        if (!String.IsNullOrEmpty(manualReading.BPReading_Systolic))
                        {
                            if (manualReading.BPReading_Systolic != "0")
                            {
                                //if (manualReading.BPReading.Contains("/"))
                                //{
                                //str_BPReading_Systolic = manualReading.BPReading.Substring(0, (manualReading.BPReading.IndexOf("/")));
                                //str_BPReading_Diastolic = manualReading.BPReading.Substring(((manualReading.BPReading.IndexOf("/")) + 1), (manualReading.BPReading.Length - ((manualReading.BPReading.IndexOf("/")) + 1)));
                                manualReadingList.Add("systolic", manualReading.BPReading_Systolic);
                                //manualReadingList.Add("diastolic", str_BPReading_Diastolic);
                                //}
                                //else
                                //{
                                //    throw new ArgumentException("Please enter Diastolic reading after '/'.");
                                //}
                            }
                            else
                            {
                                throw new ArgumentException("Please enter reading value more than '0'.");
                            }
                        }
                        else
                        {
                            //manualReadingList.Add("systolic", "0");
                            //manualReadingList.Add("diastolic", null);
                            flag++;
                        }
                        if (!String.IsNullOrEmpty(manualReading.BPReading_Diastolic))
                        {
                            if (manualReading.BPReading_Diastolic != "0")
                            {
                                manualReadingList.Add("diastolic", manualReading.BPReading_Diastolic);
                            }
                            else
                            {
                                throw new ArgumentException("Please enter the reading value more than '0'.");
                            }
                        }
                        else
                        {
                            flag++;
                        }

                        if (!String.IsNullOrEmpty(manualReading.BPReading_PR))
                        {
                            if (!String.IsNullOrEmpty(manualReading.BPReading_Systolic))
                            {
                                if (!String.IsNullOrEmpty(manualReading.BPReading_Diastolic))
                                {
                                    if (manualReading.BPReading_PR != "0")
                                    {
                                        manualReadingList.Add("pulse", manualReading.BPReading_PR);
                                    }
                                    else
                                    {
                                        throw new ArgumentException("Please enter the reading value more than '0'.");
                                    }
                                }
                                else
                                {
                                    throw new ArgumentException("Please enter Diastolic reading.");
                                }
                            }
                            else
                            {
                                throw new ArgumentException("Please enter Systolic reading.");
                            }
                        }
                        else
                        {
                            flag++;
                        }
                        if (!String.IsNullOrEmpty(manualReading.POReading))
                        {
                            if (manualReading.POReading != "0")
                            {
                                manualReadingList.Add("oxygen", manualReading.POReading);
                            }
                            else
                            {
                                throw new ArgumentException("Please enter the reading value more than '0'.");
                            }
                        }
                        else
                        {
                            flag++;
                        }
                        if (!String.IsNullOrEmpty(manualReading.BGReading))
                        {
                            if (Convert.ToInt32(manualReading.BGReading) > 29)
                            {
                                manualReadingList.Add("glucose", manualReading.BGReading);
                            }
                            else
                            {
                                throw new ArgumentException("Please enter the reading value more than '29'.");
                            }
                        }
                        else
                        {
                            flag++;
                        }
                        if (!String.IsNullOrEmpty(manualReading.TSReading))
                        {
                            if (manualReading.TSReading != "0")
                            {
                                manualReadingList.Add("temperature", manualReading.TSReading);
                            }
                            else
                            {
                                throw new ArgumentException("Please enter the reading value more than '0'.");
                            }
                        }
                        else
                        {
                            flag++;
                        }
                        if (!String.IsNullOrEmpty(manualReading.WSReading))
                        {
                            if (manualReading.WSReading != "0")
                            {
                                manualReadingList.Add("weight", manualReading.WSReading);
                            }
                            else
                            {
                                throw new ArgumentException("Please enter the reading value more than '0'.");
                            }
                        }
                        else
                        {
                            flag++;
                        }

                        if (!String.IsNullOrEmpty(manualReading.SMReading_Fev))
                        {
                            if (manualReading.SMReading_Fev != "0")
                            {
                                manualReadingList.Add("fev1Best", manualReading.SMReading_Fev);
                            }
                            else
                            {
                                throw new ArgumentException("Please enter the reading value more than '0'.");
                            }
                        }
                        else
                        {
                            flag++;
                        }
                        if (!String.IsNullOrEmpty(manualReading.SMReading_Pef))
                        {
                            if (manualReading.SMReading_Pef != "0")
                            {
                                manualReadingList.Add("pefBest", manualReading.SMReading_Pef);
                            }
                            else
                            {
                                throw new ArgumentException("Please enter reading value more than '0'.");
                            }
                        }
                        else
                        {
                            flag++;
                        }
                        // checking the A1C reading ...
                        if (String.IsNullOrEmpty(manualReading.A1CReading))
                        {
                            a1cPresent = false;
                        }
                        else
                        {
                            a1cPresent = true;
                        }

                        manualReadingList.Add("activity", "1");
                        manualReadingList.Add("measurementTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        manualReadingList.Add("manualReading", "true");

                        if (flag == 9 && a1cPresent == false )
                        {
                            throw new ArgumentException("Please enter atleast one reading.");
                        }

                        String patientid = System.Convert.ToBase64String((System.Text.Encoding.UTF8.GetBytes(manualReading.PartyId)));
                        if (flag < 9)
                        {
                            List<KeyValuePair<string, string>> keyValuePairs = new List<KeyValuePair<string, string>>();

                            //var request = WebRequest.Create(Properties.Settings.ManualReading);
                            //Uri address = new Uri("http://localhost:8080/MyvitalzDataAggregatorWeb/DataAggregatorServlet");
                            Uri address = new Uri(System.Configuration.ConfigurationManager.AppSettings["ManualReadingUrl"]);

                            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(address);
                            request.Method = "POST";
                            request.ContentType = "application/x-www-form-urlencoded";

                            StringBuilder data = new StringBuilder();
                            data.Append("MobileNurse=true");
                            data.Append("&PatientID=" + patientid);
                            //data.Append("&DeviceType=" + manualReading.getDeviceType());
                            var jsonDeviceReading = JsonConvert.SerializeObject(manualReadingList);
                            data.Append("&DeviceReading=" + jsonDeviceReading);

                            string kvp_string = data.ToString();

                            byte[] bytedata = Encoding.UTF8.GetBytes(kvp_string);
                            request.ContentLength = bytedata.Length;

                            Stream requestStream = request.GetRequestStream();
                            requestStream.Write(bytedata, 0, bytedata.Length);
                            requestStream.Close();

                            //-----------------------------

                            using (response = (HttpWebResponse)request.GetResponse())
                            {
                                StreamReader streamReader = new StreamReader(response.GetResponseStream());
                                responseText = streamReader.ReadToEnd();
                                //httpResponse.Write(responseText);
                            }
                        }
                        // if a1c data is present, call the new service to post this reading
                        if (a1cPresent)
                        {
                            responseText = (string)saveBloodHbA1cReading(manualReading.PartyId, manualReading.A1CReading);
                            System.Console.Write(responseText);
                        }
                        // log a care-time entry for manual data ...
                        AddCareTimeForManualEntry(manualReading.PartyId);
                    }
                    catch (WebException e)
                    {
                        System.Console.Write(e);
                    }
                    finally { if (httpResponse != null) httpResponse.Close(); }

                    return response;
                }

                [HttpPost]
                public object PatientReadingDetails(string patientId, string doctorId, string fromDate, string toDate, int page, int pageSize)
                {

                    object result = "";
                    try
                    {
                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["PatientReadingUrl"]);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Method = "POST";
                        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                        {
                            string json = "{\"patientId\":\"" + patientId + "\"," +
                                            "\"doctorId\":\"" + doctorId + "\"," +
                                            "\"fromDate\":\"" + fromDate + "\"," +
                                            "\"toDate\":\"" + toDate + "\"," +
                                            "\"page\":\"" + page + "\"," +
                                            "\"pageSize\":\"" + pageSize + "\"}";
                            streamWriter.Write(json);
                            streamWriter.Flush();
                            streamWriter.Close();
                        }
                        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                        {
                            result = streamReader.ReadToEnd();
                        }
                    }
                    catch
                    {
                    }
                    return result;
                }

                [HttpPost]
                public object ReadmissionSaved(string ReadmissionDate, string partyid)
                {
                    //partyid = "1102";
                    object result = "";
                    try
                    {
                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["ReadmissionSavedUrl"]);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Method = "POST";
                        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                        {
                            string json = "{\"date\":\"" + DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss tt") + "\"," +
                                            "\"patient\":\"" + partyid.ToString() + "\"," +
                                            "\"type\":\"readmission\"," +
                                            "\"rea\": {" +
                                            "\"readmissionDate\":\"" + ReadmissionDate + "\"," +
                                            "\"endDate\":\"" + ReadmissionDate + "\" }}";
                            streamWriter.Write(json);
                            streamWriter.Flush();
                            streamWriter.Close();
                        }
                        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                        {
                            result = streamReader.ReadToEnd();
                        }
                    }
                    catch (WebException e)
                    {
                        return "{\"status\":\"" + e.Message + "\"}";
                    }
                    return result;
                }


        */

    }
}