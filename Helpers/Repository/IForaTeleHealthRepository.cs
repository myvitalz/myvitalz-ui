﻿using MyVitalz.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyVitalz.Web.Helpers.Repository
{
    public interface IForaTeleHealthRepository
    {
        #region Methods

        string AddPatientToForaTeleHealth(EHRDetailsModel model);
        string DeletePatientFromForaTeleHealth(string kitId, string partyId);
        #endregion
    }
}
