﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using MySql.Data.MySqlClient;
namespace MyVitalz.Web.Helpers.Repository
{
    public class DBConnectionMain
    {
        public MySqlConnection con;

        public DBConnectionMain()
        {
            con = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyVitalz"].ConnectionString);

        }
        public void connect()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }
        public void disconnect()
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }
}