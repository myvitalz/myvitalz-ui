﻿using MyVitalz.Web.Models;
using MyVitalz.Web.Service;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;

namespace MyVitalz.Web.Helpers
{
    public class Helper
    {
        public const string NoReading = "No Reading";
        public static IServiceRepository Service
        {
            get
            {
                return (IServiceRepository)new ServiceRepository();
            }
        }

        public static Patient GetPatient(Party party)
        {
            Patient patient = null;
            if (party != null)
            {
                patient = new Patient()
                {

                    address = party.address,
                    cellPhone = party.cellPhone,
                    dob = party.dob,
                    email = party.email,
                    firstName = party.firstName,
                    gender = party.gender,
                    group = party.group,
                    homePhone = party.homePhone,
                    id = party.id,
                    lastName = party.lastName,
                    middleName = party.middleName,
                    partyType = party.partyType,
                    ssn = party.ssn,
                    taxId = party.taxId,
                    workphone = party.workphone,
                    isPartyActive = party.isPartyActive,
                    RACE = party.RACE,
                };
            }

            return patient;
        }

        //public static Patient GetPartyType()
        //{
        //    PartyType patient = null;
        //    if (party != null)
        //    {
        //        patient = new Patient()
        //        {

        //            address = party.address,
        //            cellPhone = party.cellPhone,
        //            dob = party.dob,
        //            email = party.email,
        //            firstName = party.firstName,
        //            gender = party.gender,
        //            group = party.group,
        //            homePhone = party.homePhone,
        //            id = party.id,
        //            lastName = party.lastName,
        //            middleName = party.middleName,
        //            partyType = party.partyType,
        //            ssn = party.ssn,
        //            taxId = party.taxId,
        //            workphone = party.workphone,
        //            isPartyActive = party.isPartyActive,
        //            RACE = party.RACE,
        //        };
        //    }

        //    return patient;
        //}

        public static List<Item> GetPartyType()
        {
            return new List<Item>()
            {
                new Item("P", "Patient"),
                new Item("C", "Care Giver"),
                new Item("D", "Doctor"),
                new Item("E", "Group Admin"),
                new Item("N", "Clinician")
            };
        }

        public static User CurrentUser
        {
            get
            {
                var user = (User)null;
                if (HttpContext.Current.User != null)
                    user = HttpContext.Current.User as User;
                return user;
            }
        }

        public static List<Item> GetGenderList()
        {
            return new List<Item>()
                    {
                        new Item("M", "Male"),
                        new Item("F", "Female")
                    };
        }
        /*
         * American Indian/Alaskan Native 
Asian - 
Black, Non-Hispanic 
Hispanic/Latino -
White, Non-Hispanic 
Native Hawaiian/Pacific Islander 
Two or More Races 
Other - A person unable to identify with the other ethnicity categories.
         * */
        public static List<Item> GetRaceList()
        {
            return new List<Item>()
            {
                new Item("AI", "AmericanIndian/Alaskan Native"),
                new Item("AS", "Asian"),
                new Item("BN", "Black, Non Hispanic"),
                new Item("HL", "Hispanic/Latino"),
                new Item("WN", "White, Non Hispanic"),
                new Item("HP", "Native Hawaiian/Pacific Islander"),
                new Item("TM", "Two or More Races"),
                new Item("OT", "Others - A person unable to identify with the other ethnicity categories")
            };
        }

        public static string GetRace(string value)
        {
            string rslt = "";

            if( string.Equals(value, "AI") )
            {
                rslt = "AmericanIndian/Alaskan Native";
            }
            else if( string.Equals(value, "AS"))
            {
                rslt = "Asian";
            }
            else if( string.Equals(value, "BN"))
            {
                rslt = "Black, Non Hispanic";
            }
            else if( string.Equals(value, "HL"))
            {
                rslt = "Hispanic/Latino";
            }
            else if (string.Equals(value, "WN"))
            {
                rslt = "White, Non Hispanic";
            }
            else if (string.Equals(value, "HP"))
            {
                rslt = "Native Hawaiian/Pacific Islander";
            }
            else if (string.Equals(value, "TM"))
            {
                rslt = "Two or More Races";
            }
            else if (string.Equals(value, "OT"))
            {
                rslt = "Others - A person unable to identify with the other ethnicity categories";
            }
            else
            {
                rslt = "Not Available";
            }

            return rslt;
        }

        public static List<Item> GetStatusList()
        {
            return new List<Item>()
                      {
                        //new Item("", "All"),
                        new Item("Active", "Active"),
                        new Item("InActive", "InActive")
                      };
        }

        public static string GetAssociationsAsString(Xref[] associations, string separator)
        {
            var associationsAsString = string.Empty;
            if (associations != null)
            {
                foreach (var association in associations)
                {
                    associationsAsString += string.Format("{0}:{1} {2}", association.xrefType, association.xrefId, separator);
                }
            }
            return associationsAsString;
        }

        public static List<Item> GetDeviceTypes()
        {
            return new List<Item>()
                      {
                        new Item(DeviceType.BloodGlucose, DeviceType.BloodGlucose.ToDeviceType()),
                        new Item(DeviceType.BloodPressure, DeviceType.BloodPressure.ToDeviceType() ),
                        new Item(DeviceType.PulseOximeter, DeviceType.PulseOximeter.ToDeviceType() ),
                        new Item(DeviceType.Temperature, DeviceType.Temperature.ToDeviceType() ),
                        new Item(DeviceType.WeighingScale, DeviceType.WeighingScale.ToDeviceType() ),
                        new Item(DeviceType.Spirometer, DeviceType.Spirometer.ToDeviceType() ),
                        new Item(DeviceType.MentalIllness, DeviceType.MentalIllness.ToDeviceType()),
                        new Item(DeviceType.Diabetes, DeviceType.Diabetes.ToDeviceType()),
                        new Item(DeviceType.Medication, DeviceType.Medication.ToDeviceType())
                      };
        }

        public static List<Item> GetFrequencyList()
        {
            return new List<Item>()
                      {
                        new Item("1", "Once"),
                        new Item("2", "Twice")
                      };
        }

        public static string GetDateTime(string value)
        {
            string str = string.Empty;
            if (value != null 
                    && value.Length > 16)
            {
                str = value.Substring(0, 16);
            }
            return str;
        }

        public static string GetDate(string value)
        {
            string str = value;
            if (value != null 
                    && value.Length > 10)
            {
                str = value.Substring(0, 10);
            }
            return str;
        }

        public static string GetNewDate(string value)
        {
            string str = value;
            if (value != null
                    && value.Length > 10)
            {
                str = value.Substring(0, 10);
            }
            if (str != null)
            {
                // flip the format from YYYY-MM-DD to MM/DD/YYYY
                string nStr = str.Substring(5, 2) + "/" + str.Substring(8, 2) + "/" + str.Substring(0, 4);
                str = nStr;
            }
            return str;
        }

        public Boolean WriteJsonDataToFile()
        {
            JObject readingObj = new JObject(
                new JProperty("patientId", 9),
                new JProperty("glucose", 9),
                new JProperty("measurementTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));

            File.WriteAllText(@"c:\bgdata"+ DateTime.Now.ToString("yyyy-MM-dd")+".json", readingObj.ToString());

            // write JSON directly to a file
            using (StreamWriter file = File.CreateText(@"c:\videogames.json"))
            using (JsonTextWriter writer = new JsonTextWriter(file))
            {
                readingObj.WriteTo(writer);
            }

            return true;
        }

    }
}
