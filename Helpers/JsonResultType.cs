﻿namespace MyVitalz.Web.Helpers
{
    public enum JsonResultType
    {
        None,
        Success,
        Warning,
        Error,
    }
}
