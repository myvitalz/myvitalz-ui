﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace MyVitalz.Web.Models
{
    [DataContract]
    public class CovidModel
    {
        [DataMember(Name = "id")]
        public String Id { get; set; }

        [DataMember(Name = "diseaseScore")]
        public String DiseaseScore { get; set; }

        [DataMember(Name = "diseaseTypeId")]
        public String DiseaseTypeId { get; set; }
      
        [DataMember(Name = "dailyQaTakenTime")]
        public String DailyQaTakenTime { get; set; }

        [DataMember(Name = "partyId")]
        public String PartyId { get; set; }
 
       
      
    }
    [DataContract]
    public class CovidModelCollection
    {
        [DataMember(Name = "CVD")]
        public IEnumerable<CovidModel> CovidReading { get; set; }
    }
}