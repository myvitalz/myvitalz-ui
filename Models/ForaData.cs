﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyVitalz.Web.Models
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class AddNewData
    {

        private string accountField;

        private string passwordField;

        private AddNewDataACase aCaseField;

        /// <remarks/>
        public string Account
        {
            get
            {
                return this.accountField;
            }
            set
            {
                this.accountField = value;
            }
        }

        /// <remarks/>
        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        public AddNewDataACase ACase
        {
            get
            {
                return this.aCaseField;
            }
            set
            {
                this.aCaseField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AddNewDataACase
    {

        private AddNewDataACaseCase caseField;

        /// <remarks/>
        public AddNewDataACaseCase Case
        {
            get
            {
                return this.caseField;
            }
            set
            {
                this.caseField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AddNewDataACaseCase
    {

        private string pIDField;

        private string pAcctField;

        private string pPwdField;

        private string pSAIDField;

        private string pTZField;

        private string pNameField;

        private object pBirthdayField;

        private byte pSexField;

        private object pHeightField;

        private object pWeightField;

        private object pTel1Field;

        private object pTel2Field;

        private object pEMailField;

        private string pTGetUpField;

        private string pTBreakfastField;

        private string pTLunchField;

        private string pTDinnerField;

        private string pTSleepField;

        private object pAddrField;

        private object pRaceNoField;

        private AddNewDataACaseCaseMeterData meterDataField;

        /// <remarks/>
        public string PID
        {
            get
            {
                return this.pIDField;
            }
            set
            {
                this.pIDField = value;
            }
        }

        /// <remarks/>
        public string PAcct
        {
            get
            {
                return this.pAcctField;
            }
            set
            {
                this.pAcctField = value;
            }
        }

        /// <remarks/>
        public string PPwd
        {
            get
            {
                return this.pPwdField;
            }
            set
            {
                this.pPwdField = value;
            }
        }

        /// <remarks/>
        public string PSAID
        {
            get
            {
                return this.pSAIDField;
            }
            set
            {
                this.pSAIDField = value;
            }
        }

        /// <remarks/>
        public string PTZ
        {
            get
            {
                return this.pTZField;
            }
            set
            {
                this.pTZField = value;
            }
        }

        /// <remarks/>
        public string PName
        {
            get
            {
                return this.pNameField;
            }
            set
            {
                this.pNameField = value;
            }
        }

        /// <remarks/>
        public object PBirthday
        {
            get
            {
                return this.pBirthdayField;
            }
            set
            {
                this.pBirthdayField = value;
            }
        }

        /// <remarks/>
        public byte PSex
        {
            get
            {
                return this.pSexField;
            }
            set
            {
                this.pSexField = value;
            }
        }

        /// <remarks/>
        public object PHeight
        {
            get
            {
                return this.pHeightField;
            }
            set
            {
                this.pHeightField = value;
            }
        }

        /// <remarks/>
        public object PWeight
        {
            get
            {
                return this.pWeightField;
            }
            set
            {
                this.pWeightField = value;
            }
        }

        /// <remarks/>
        public object PTel1
        {
            get
            {
                return this.pTel1Field;
            }
            set
            {
                this.pTel1Field = value;
            }
        }

        /// <remarks/>
        public object PTel2
        {
            get
            {
                return this.pTel2Field;
            }
            set
            {
                this.pTel2Field = value;
            }
        }

        /// <remarks/>
        public object PEMail
        {
            get
            {
                return this.pEMailField;
            }
            set
            {
                this.pEMailField = value;
            }
        }

        /// <remarks/>
        public string PTGetUp
        {
            get
            {
                return this.pTGetUpField;
            }
            set
            {
                this.pTGetUpField = value;
            }
        }

        /// <remarks/>
        public string PTBreakfast
        {
            get
            {
                return this.pTBreakfastField;
            }
            set
            {
                this.pTBreakfastField = value;
            }
        }

        /// <remarks/>
        public string PTLunch
        {
            get
            {
                return this.pTLunchField;
            }
            set
            {
                this.pTLunchField = value;
            }
        }

        /// <remarks/>
        public string PTDinner
        {
            get
            {
                return this.pTDinnerField;
            }
            set
            {
                this.pTDinnerField = value;
            }
        }

        /// <remarks/>
        public string PTSleep
        {
            get
            {
                return this.pTSleepField;
            }
            set
            {
                this.pTSleepField = value;
            }
        }

        /// <remarks/>
        public object PAddr
        {
            get
            {
                return this.pAddrField;
            }
            set
            {
                this.pAddrField = value;
            }
        }

        /// <remarks/>
        public object PRaceNo
        {
            get
            {
                return this.pRaceNoField;
            }
            set
            {
                this.pRaceNoField = value;
            }
        }

        /// <remarks/>
        public AddNewDataACaseCaseMeterData MeterData
        {
            get
            {
                return this.meterDataField;
            }
            set
            {
                this.meterDataField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AddNewDataACaseCaseMeterData
    {

        private AddNewDataACaseCaseMeterDataMeter meterField;

        /// <remarks/>
        public AddNewDataACaseCaseMeterDataMeter Meter
        {
            get
            {
                return this.meterField;
            }
            set
            {
                this.meterField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AddNewDataACaseCaseMeterDataMeter
    {

        private string meterTypeField;

        private byte meterExIDField;

        private string meterIDField;

        private object gatewayIDField;

        /// <remarks/>
        public string MeterType
        {
            get
            {
                return this.meterTypeField;
            }
            set
            {
                this.meterTypeField = value;
            }
        }

        /// <remarks/>
        public byte MeterExID
        {
            get
            {
                return this.meterExIDField;
            }
            set
            {
                this.meterExIDField = value;
            }
        }

        /// <remarks/>
        public string MeterID
        {
            get
            {
                return this.meterIDField;
            }
            set
            {
                this.meterIDField = value;
            }
        }

        /// <remarks/>
        public object GatewayID
        {
            get
            {
                return this.gatewayIDField;
            }
            set
            {
                this.gatewayIDField = value;
            }
        }
    }


    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class AddNewResult
    {

        private byte reCodeField;

        private string reMsgField;

        /// <remarks/>
        public byte ReCode
        {
            get
            {
                return this.reCodeField;
            }
            set
            {
                this.reCodeField = value;
            }
        }

        /// <remarks/>
        public string ReMsg
        {
            get
            {
                return this.reMsgField;
            }
            set
            {
                this.reMsgField = value;
            }
        }
    }


}