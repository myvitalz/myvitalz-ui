﻿using System;
using System.Collections.Generic;
using MyVitalz.Web.Service;

namespace MyVitalz.Web.Models
{
    public class PatientDevicesModel
    {
        #region Properties

        public Party Patient { get; set; }

        public string patientFirstName { get; set; }
        public string patientMiddleName { get; set; }
        public string patientLastName { get; set; }
        public string patientId { get; set; }
        public string diseaseType { get; set; }
        public string diseaseId { get; set; }

        public GetQuadrantDetailsByPatientResponse QuadrantDetail { get; set; }

        public Preferences[] Preferences { get; set; }
        public DiseaseAssociations[] diseaseAssociations { get; set; }
        public List<String> DiseaseNameByPatientLst { get; set; }

        public string CurrentUserId { get; set; }

        public DateTime date = DateTime.Now;

        public QAAssoc[] QA { get; set; }        
        public QAAssoc[] EntireQA { get; set; }
        public int currentQADiseaseTypeIndex { get; set; }
        public List<string> patientsQADiseaseTypes { get; set; }
        public int totalQACount { get; set; }

        public Boolean sliding { get; set; }

        public PatientFeatures[] PF { get; set; }

        #region Quadrant Sliding

        public GetQuadrantOrderResponse QuadrantOrder { get; set; }
        public QuadrantOrder[] QuadrantOrderArr { get; set; }
        public String[] QOStringArr { get; set; }

        #endregion

        public bool CanShowThirdQuadrant
        {
            get { if (QuadrantDetail == null) { return false;} else {
                return (( QuadrantDetail.asthmaMonitor != null || 
                          QuadrantDetail.bloodGlucose != null) || 
                          QuadrantDetail.mentalIllnessDailyQaScore != null ||
                          QuadrantDetail.diabetesDailyQaScore != null ); }}
        }

        public string GetHiddenClass(bool isHidden)
        {
            if (isHidden == true)
            {
                return "otherMembers";
            }
            else
            {
                return string.Empty;
            }
        }

        public GetPatientDailyQAResponse PatientDailyQAResponse { get; set; }

        public bool InEdit { get; set; }
        #endregion
    }

    //[Serializable]
    //public class SlidingModel 
    //{
    //    public OrderModel[] SlidingOrder {get; set;}
    //}
    //[Serializable]
    //public class OrderModel
    //{
    //    public string PartyId { get; set; }
    //    public string PatientId { get; set; }
    //    public string DeviceType { get; set; }
    //    public string Order { get; set; }
    //}

    [Serializable]
    public class SlidingModel
    {
        public string[] SlidingOrder { get; set; }
    }
    [Serializable]
    public class OrderModel
    {
        public string[] Order { get; set; }
    }
}
