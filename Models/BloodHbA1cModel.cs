﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace MyVitalz.Web.Models
{
    [DataContract]
    public class BloodHbA1cModel
    {
        [DataMember(Name = "id")]
        public string Id { get; set; }
        [DataMember(Name = "partyId")]
        public string PartyId { get; set; }
        [DataMember(Name = "hbA1c_READING")]
        public string HbA1c_READING { get; set; }
        [DataMember(Name = "measurementTime")]
        public string MeasurementTime { get; set; }
        [DataMember(Name = "device")]
        public string Device { get; set; }
        [DataMember(Name = "transmissionTime")]
        public string TransmissionTime { get; set; }
        [DataMember(Name = "updateTs")]
        public string UpdateTs { get; set; }
        [DataMember(Name = "updateUser")]
        public string UpdateUser { get; set; }
    }

    [DataContract]
    public class BloodHbA1cCollection
    {
        [DataMember(Name = "A1C")]
        public IEnumerable<BloodHbA1cModel> BloodHbA1cCol { get; set; }
    }
}