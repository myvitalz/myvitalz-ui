﻿
namespace MyVitalz.Web.Models
{
    public enum SearchPatientCriteria
    {
        None,
        FirstName,
        LastName,
        Email
    }
}