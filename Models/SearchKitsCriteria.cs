﻿namespace MyVitalz.Web.Models
{
    public enum SearchKitsCriteria
    {
        None = 0,
        KitId = 1,
        Group = 2
    }
}
