﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyVitalz.Web.Models
{
    public class VseeModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string WaitingRoomUrl { get; set; }
    }
}