﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace MyVitalz.Web.Models
{
    [DataContract]
    public class PauseMonitorModel
    {    

        [DataMember(Name = "id")]
        public string Id { get; set; }
        [DataMember(Name = "partyId")]
        public string PartyId { get; set; }
        [DataMember(Name = "startDate")]
        public string StartDate { get; set; }
        [DataMember(Name = "endDate")]
        public string EndDate { get; set; }
        [DataMember(Name = "reason")]
        public string Reason { get; set; }
        [DataMember(Name = "description")]
        public string Description { get; set; }
        [DataMember(Name = "updateTs")]
        public string UpdateTs { get; set; }
        [DataMember(Name = "updateUser")]
        public string UpdateUser { get; set; }
        
    }
    [DataContract]
    public class PauseMonitorCollection
    {
        [DataMember(Name = "PMN")]
        public IEnumerable<PauseMonitorModel> PauseMonitor { get; set; }
    }
}