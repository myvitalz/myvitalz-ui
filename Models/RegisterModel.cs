﻿using MyVitalz.Web.Service;
using System.ComponentModel.DataAnnotations;

namespace MyVitalz.Web.Models
{
    public class RegisterModel
    {
        #region Properties

        [Required]
        [Display(Name = "Party Type")]
        public string PartyType { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Birth Date")]
        public string BirthDate { get; set; }

        [Required]
        [Display(Name = "Zip")]
        public string Zip { get; set; }

        [Display(Name = "User name")]
        [Required]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Password")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Confirm password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        public Party Party
        {
            get
            {
                return new Party()
                {
                    partyType = PartyType,
                    firstName = FirstName,
                    lastName = LastName,
                    address = new Address()
                    {
                        zip5 = Zip
                    },
                    dob = BirthDate,
                };
            }
        } 
        
        #endregion
    }
}
