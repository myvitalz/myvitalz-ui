﻿using System.ComponentModel.DataAnnotations;
using MyVitalz.Web.Service;

namespace MyVitalz.Web.Models
{
  public class LogOnModel
  {
      #region Properties

      [Display(Name = "User name")]
      [Required]
      public string UserName { get; set; }

      [DataType(DataType.Password)]
      [Required]
      [Display(Name = "Password")]
      public string Password { get; set; }

      [Display(Name = "Remember me?")]
      public bool RememberMe { get; set; } 

      public SearchByUserCredentialResponse userCredentialResponse { get; set;}
      #endregion
  }
}
