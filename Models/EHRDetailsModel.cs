﻿using MyVitalz.Web.Helpers;
using MyVitalz.Web.Service;
using System;
using System.Linq;
using System.Collections.Generic;

namespace MyVitalz.Web.Models
{
    public class EHRDetailsModel
    {
        #region Properties

        public string ViewName { get; set; }

        public EHRDetails Details { get; set; }

        public int IndexRemoved { get; set; }

        public string CurrentKitId { get; set; }

        public string NewlyAddedPartyId { get; set; }

        public ParameterModel ParameterModel { get; set; }

        public BasicParty[] Patients { get; set; }

        public Xref[] Associations { get; set; }

        public XrefType[] AssociationTypes { get; set; }

        public GroupType[] GroupTypes { get; set; }

        public SubGroupWrapper[] SubGroupTypes { get; set; }

        public SubGroupWrapper[] SubGroups { get; set; }

        public SubGroupWrapper[] AllSubGroups { get; set; }
        public SubGroupWrapper[] Changed_SubGroups { get; set; }

        public GroupType CurrentGroup
        {

            get
            {
                if (string.IsNullOrWhiteSpace(Details.kitDetails.groupId) == true)
                {
                    throw new Exception("The kit was not associated with a group.");
                }
                var group = GroupTypes.FirstOrDefault(g => g.groupType == Details.kitDetails.groupId);
                if (group == null)
                {
                    throw new Exception(string.Format("Kit Group ({0}) did not exist in the system", Details.kitDetails.groupId));
                }
                return group;
            }
        }

        public string[] AvailableKitIds { get; set; }

        public string SelectedGroupId { get; set; }

        public int PreferenceIndex { get; set; }

        public string PreferenceValue = null;
        public string PreferenceKeyType = null;
        public string PreferenceType = null;

        public string[] DiseasesPicked { get; set; }
        public int DiseaseTypeIndex { get; set; }
        public string DailyQuestionsValue { get; set; }
        public string FeelingsValues { get; set; }
        public string NotificationsValue { get; set; }

        public int DailyQuestionsIndex { get; set; }
        public int FeelingsIndex { get; set; }
        public int NotificationsIndex { get; set; }

        public string FeelingsValue = null;

        //public string[] SubGroupTypesPicked { get; set; }
        public int SubGroupTypesIndex { get; set; }

        public string[] PrimaryPicked { get; set; }
        public string[] PrimarySubGroupId { get; set; }
        public int PrimaryIndex { get; set; }

        public string[] SecondaryPicked { get; set; }
        public string[] SecondarySubGroupId { get; set; }
        public int SecondaryIndex { get; set; }

        public string SelectedGT { get; set; }
        public string ShowSubGroups { get; set; }
        public string HomeGroup { get; set; }
        public InsuranceModel Insurance { get; set; }
        #endregion

        #region Constructor

        public EHRDetailsModel()
        {
            Details = new EHRDetails()
            {
                ehr = new Patient()
                {
                    address = new Address()
                }
            };
        }

        public EHRDetailsModel(string viewName)
            : this()
        {
            ViewName = viewName;
        }

        public EHRDetailsModel(EHRDetails details)
        {
            Details = details;
        } 
        #endregion

        #region Methods

        public string GetViewName(string action)
        {
            string str;
            switch (action.ToUpper())
            {
                case "EDITEHR":
                case "SHOWEHR":
                    str = "ShowEHR";
                    break;
                default:
                    str = "ShowParty";
                    break;
            }
            return str;
        }

        public string GetTitle(string action)
        {
            if (action == "EditParty"
                || action == "EditPatient")
            {
                return string.Format("Edit {0} ({1})", Details.ehr.GetName(), Details.ehr.partyType.ToRoleName());
            }
            else if (action == "EditEHR")
            {
                return string.Format("Edit {0} EHR", Details.ehr.GetName());
            }
            else
            {
                return action.Replace("Party", string.Empty).SplitValueByCase();
            }
        }

        public string GetXrefTypeDesc(string xrefType)
        {
            var str = "Unknown";
            var assoXrefType = AssociationTypes.FirstOrDefault(a => a.xrefType == xrefType);
            if (assoXrefType != null)
                str = assoXrefType.xrefTypeDesc;
            return str;
        }

        public BasePartyModel GetBasePartyModel()
        {
            //return new BasePartyModel("Details.ehr.", Details.ehr, AssociationTypes, GroupTypes, Associations);
            return new BasePartyModel("Details.ehr.", Details.ehr, AssociationTypes, GroupTypes, Associations, Details.subGroupType, Details.subGroups, AllSubGroups, PrimaryPicked, SecondaryPicked, PrimarySubGroupId, SecondarySubGroupId, ShowSubGroups, SelectedGT, HomeGroup, Insurance);
        }

        //public string GetPreferences()
        //{
        //    List<Preferences> prefList = new List<Preferences>();
        //    prefList = Details.preferences.Where(p => p.preferenceKeyType == "MRE").ToList();

        //    if (prefList != null)
        //    {
        //        foreach (var pref in prefList)
        //        {
        //            PreferenceValue = pref.preferenceValue;
        //            PreferenceKeyType = pref.preferenceKeyType;
        //            PreferenceType = pref.preferenceType;
        //        }
        //    }
        //    return PreferenceValue;
        //}
        #endregion
    }
}
