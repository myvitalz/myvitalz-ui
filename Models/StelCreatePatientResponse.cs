﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyVitalz.Web.Models
{
    public class StelCreatePatientResponse
    {
        public string pid { set; get; }
        public string result { set; get; }
    }
}