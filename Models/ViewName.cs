﻿
namespace MyVitalz.Web.Models
{
    public struct ViewName
    {
        public const string Index = "Index";
        public const string PatientHome = "PatientHome";
        public const string ShowPatients = "ShowPatients";
        public const string ShowPatientDevices = "ShowPatientDevices";
        public const string ShowDevice = "ShowDevice";
        public const string ShowEHR = "ShowEHR";
        public const string ShowParameters = "ShowParameters";
        public const string ShowParty = "ShowParty";
        public const string Admin = "Admin";
        public const string DailyReport = "DailyReport";
        public const string ListParties = "_ListParties";
    }
}
