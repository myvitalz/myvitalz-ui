﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MyVitalz.Web.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyVitalz.Web.Models
{
    public class SoapNotesModel
    {
        #region Properties

        [Required]
        public string soapNoteId { get; set; }

        [Required]
        public string desc { get; set; }

        [AllowHtml]
        [Required]
        public string patientId { get; set; }

        [Required]
        [AllowHtml]
        public string updateUser { get; set; }

        [Required]
        [AllowHtml]
        public string updateTS { get; set; }

        [Required]
        public string subjective { get; set; }

        [Required]
        public string objective { get; set; }

        [Required]
        public string assessment { get; set; }

        [Required]
        public string plan { get; set; }

        public string readings { get; set; }

        public List<string> lstReadings { get; set; }

        public GetSoapResponse SoapNotes { get; set; }

        public string userFullName { get; set; }

        public string patientFullName { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public DateTime FromDateForDR { get; set; }

        public DateTime ToDateForDR { get; set; }

        public string DisplayDate { get; set; }

        public GetQuadrantDetailsByPatientResponse QuadrantDetail { get; set; }

        public Double WeightDiff;

        public GetAggregateHistoryResponse AggHistory { get; set; }

        #endregion
    }
}
