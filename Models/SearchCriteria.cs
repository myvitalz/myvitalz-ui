﻿namespace MyVitalz.Web.Models
{
    public enum SearchCriteria
    {
        None = 0,
        FirstName = 1,
        LastName = 2,
        Email = 3,
        DateOfBirth = 4,
        PhoneNumber = 5,
        KitId = 6,
        PatientId = 7,
        Group = 8,
        SubGroup = 9
    }
}
