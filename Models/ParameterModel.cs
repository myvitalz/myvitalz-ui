﻿using MyVitalz.Web.Helpers;
using MyVitalz.Web.Service;

namespace MyVitalz.Web.Models
{
    public class ParameterModel
    {
        #region Properties

        public Threshold[] Parameters { get; set; }

        public string PatientId { get; set; }

        public Party Patient { get; set; }

        #endregion

        #region Constructor

        public ParameterModel()
        {
        }

        public ParameterModel(string patientId)
        {
            PatientId = patientId;
        }
        #endregion

        #region Methods

        public int GetDeviceMinimum(string deviceType)
        {
            int num = 0;
            switch (deviceType)
            {
                case DeviceType.BloodGlucose:
                    num = 60;
                    break;
                case DeviceType.BloodPressure:
                    num = 60;
                    break;
                case DeviceType.PulseOximeter:
                    num = 70;
                    break;
                case DeviceType.PulseRate:
                    num = 60;
                    break;
                case DeviceType.Temperature:
                    num = 50;
                    break;
                case DeviceType.WeighingScale:
                    num = 5;
                    break;
                case DeviceType.Spirometer:
                    num = 5;
                    break;
            }
            return num;
        }

        public int GetDeviceMaximum(string deviceType)
        {
            int num = 100;
            switch (deviceType)
            {
                case DeviceType.BloodGlucose:
                    num = 450;
                    break;
                case DeviceType.BloodPressure:
                    num = 160;
                    break;
                case DeviceType.PulseOximeter:
                    num = 160;
                    break;
                case DeviceType.PulseRate:
                    num = 120;
                    break;
                case DeviceType.Temperature:
                    num = 110;
                    break;
                case DeviceType.WeighingScale:
                    num = 450;
                    break;
                case DeviceType.Spirometer:
                    num = 100;
                    break;
            }
            return num;
        }
        #endregion
    }
}
