﻿using MyVitalz.Web.Helpers;
using MyVitalz.Web.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyVitalz.Web.Models
{
    public class DeviceModel
    {
        #region Properties

        public Party Patient { get; set; }

        public Device Device { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public DateTime LastMTime { get; set; }

        public string DeviceView
        {
            get
            {
                return string.Format("_Device{0}", (object)Device.deviceType);
            }
        }

        public IEnumerable<BloodHbA1cModel> A1CReadings { get; set; }
        public IEnumerable<PillsyReadingModel> MEDReadings { get; set; }
        public IEnumerable <CareTimeModel> CRTReadings { get; set; }
        public IEnumerable<PauseMonitorModel> PMNReadings { get; set; }
        public IEnumerable<ReadmissionModel> REAReadings { get; set; }
        public GetWeighingScaleHistoryResponse WSReadings { get; set; }

        public IEnumerable<CovidModel> CVDReadings { get; set; }

        public GetTemperatureHistoryResponse TMReadings { get; set; }

        public GetBloodPressureHistoryResponse BPReadings { get; set; }

        public GetPulseOxiMeterHistoryResponse POReadings { get; set; }

        public GetBloodGlucoseHistoryResponse BGReadings { get; set; }

        public GetFeelingHistoryResponse FeelingReadings { get; set; }

        public AsthmaMonitor[] SPReadings { get; set; }

        //public PatientDailyQaScore[] DailyQaScores { get; set; }

        public PatientDailyQaScore[] MIDailyQaScores { get; set; }

        public PatientDailyQaScore[] DiabetesDailyQaScores { get; set; }

        public GetPatientDailyQAResponse FilledDailyQaStats { get; set; }

        public int TotalNormalReadings { get; set; }

        public int TotalAbNormalReadings { get; set; }

        public int TotalReadings { get; set; }

        public double MissedDays { get; set; }

        public double TakenDays { get; set; }

        public double CRTReadingsTotalCallTime { get; set; }
        public double CRTReadingsCallTimeAvg { get; set; }        

        public double WSReadingsAvg { get; set; }

        public double BPSystolicReadingsAvg { get; set; }

        public double BPDiastolicReadingsAvg { get; set; }

        public double BPPRReadingsAvg { get; set; }

        public double TMReadingsAvg { get; set; }

        public double BGReadingsAvg { get; set; }

        public double POReadingsAvg { get; set; }

        public double SPFevReadingsAvg { get; set; }

        public double SPPefReadingsAvg { get; set; }

        //public double DailyQaScoresAvg { get; set; }

        public double MIDailyQaScoresAvg { get; set; }

        public double DiabetesDailyQaScoresAvg { get; set; }

        public double TotalDays
        {
            get
            {
                //If the days are the same return 1
                return ToDate.Subtract(FromDate).TotalDays + 1;
            }
        }

        public double NormalReadingPercentage
        {
            get
            {
                //Avoid Divide by Zero
                if (TotalReadings == 0)
                {
                    return 0;
                }
                return TotalNormalReadings * 100 / TotalReadings;
            }
        }

        public double AbnormalReadingPercentage
        {
            get
            {
                //Avoid Divide by Zero
                if (TotalReadings == 0)
                {
                    return 0;
                }

                return TotalAbNormalReadings * 100 / TotalReadings;
            }
        }

        public double MissedDaysPercentage
        {
            get
            {
                return MissedDays * 100 / TotalDays;
            }
        }

        public double TakenDaysPercentage
        {
            get
            {
                return TakenDays * 100 / TotalDays;
            }
        }

        #endregion

        #region Methods

        public DeviceModel GetDeviceModel(string deviceType)
        {
            Device = new Device()
            {
                deviceType = deviceType
            };
            return this;
        }

        public string GetStatusClass(string reading)
        {
            if (reading == "4"
                  || reading == "5"
                  || reading == "True")
            {
                return "alert";
            }
            return string.Empty;
        }

        public string GetStatusClass(bool isAbnormal)
        {
            if (isAbnormal == true)
            {
                return "alert";
            }
            return string.Empty;
        }

        public IEnumerable<BloodGlucose> MorningBGReadings
        {
            get
            {
                if (BGReadings != null)
                {
                    return
                        BGReadings.readings.Where(r => (r.period == "M"));
                }
                else
                {
                    return null;
                }
            }
        }
        public IEnumerable<BloodGlucose> NoonBGReadings
        {
            get
            {
                if (BGReadings != null)
                {
                    return BGReadings.readings.Where(r => (r.period == "A"));
                }
                else
                {
                    return null;
                }
            }
        }
        public IEnumerable<BloodGlucose> NightBGReadings
        {
            get
            {
                if (BGReadings != null)
                {
                    return BGReadings.readings.Where(r => (r.period == "N"));
                }
                else
                {
                    return null;
                }
            }
        }
        public IEnumerable<BloodGlucose> EarlyMorningBGReadings
        {
            get
            {
                if (BGReadings != null)
                {
                    return BGReadings.readings.Where(r => (r.period == "E"));
                }
                else
                {
                    return null;
                }
            }
        }
        public Threshold GetSystolicThreshold()
        {
            return BPReadings.threshold.First(t => t.deviceType == DeviceType.Systolic);
        }

        public Threshold GetDiastolicThreshold()
        {
            return BPReadings.threshold.First(t => t.deviceType == DeviceType.Diastolic);
        }

        public Threshold GetPulseRateThreshold()
        {
            return BPReadings.threshold.First(t => t.deviceType == DeviceType.PulseRate);
        }

        public string GetExportTitle()
        {
            return HttpUtility.JavaScriptStringEncode(string.Format("{0}'s {1} Readings", Patient.GetName(), Device.deviceType.ToDeviceType()));
        }
        #endregion
    }
}
