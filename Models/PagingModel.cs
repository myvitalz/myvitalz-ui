﻿
using System;

namespace MyVitalz.Web.Models
{
    public class PagingModel
    {
        #region Properties

        public int FirstPageNumber { get; set; }
        public int CurrentPageNumber { get; set; }
        public int TotalRowsCount { get; set; }
        public int DisplayRowsCount { get; set; }

        public int TotalPageCount
        {
            get
            {
                if (DisplayRowsCount <= 0)
                {
                    throw new ArgumentException("DisplayRowsCount can't be zero.");
                }
                return (int) Math.Ceiling( (double)TotalRowsCount/DisplayRowsCount);
            } 
        }

        public int LastPageNumber
        {
            get { return TotalPageCount; }
        }

        public int NextPageNumber
        {
            get
            {
                var nextPageNumber = CurrentPageNumber + 1;
                if (nextPageNumber > TotalPageCount)
                {
                    nextPageNumber = 0;
                }
                return nextPageNumber;
            }
        }

        public int PreviousPageNumber
        {
            get
            {
                var previousPageNumber = CurrentPageNumber - 1;
                if (previousPageNumber < 0)
                {
                    previousPageNumber = 0;
                }
                return previousPageNumber;
            }
        }

        #endregion
        
        #region Constructor

        public PagingModel(int totalRowsCount, int displayRowsCount, int currentPageNumber)
        {
            TotalRowsCount = totalRowsCount;
            DisplayRowsCount = displayRowsCount;
            CurrentPageNumber = currentPageNumber;
        }

        #endregion

        #region Methods

        public int GetStarRowtIndex()
        {
            var startIndex = 1;
            if (CurrentPageNumber > 1)
            {
                startIndex = (CurrentPageNumber - 1) * DisplayRowsCount;
            }
            return startIndex;
        }

        public int GetEndRowIndex()
        {
            var endIndex = DisplayRowsCount;
            if (CurrentPageNumber == TotalPageCount)
            {
                endIndex = TotalRowsCount;
            }
            else if (CurrentPageNumber > 1)
            {
                endIndex = CurrentPageNumber * DisplayRowsCount;
            }
            return endIndex;
        }

        #endregion
    }
}