﻿using MyVitalz.Web.Service;

namespace MyVitalz.Web.Models
{
    public class PatientListModel : PagingModel
    {
        #region Properties
        
        public Patient[] Patients { get; set; }

        public Patient[] Parties { get; set; }

        string PartyId { get; set; }

        public bool showAll { get; set; }
        
        #endregion

        #region Constructor

        public PatientListModel() 
            : this(0, 0, 0)
        {

        }

        public PatientListModel(int totalRowsCount, int displayRowsCount, int currentPageNumber) 
            : base(totalRowsCount, displayRowsCount, currentPageNumber)
        {

        }

        #endregion
    
        #region Methods

        public string GetDropDownButtonId(Party party)
        {
            return string.Format("DropButton{0}", party.id);
        }

        #endregion
    }
}