﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace MyVitalz.Web.Models
{
    [DataContract]
    public class CareTimeModel
    {
        [DataMember(Name = "id")]
        public string Id { get; set; }
        [DataMember(Name = "beginTime")]
        public string BeginTime { get; set; }
        [DataMember(Name = "careTimeDate")]
        public string CareTimeDate { get; set; }
        [DataMember(Name = "caredBy")]
        public string CaredBy { get; set; }
        [DataMember(Name = "duration")]
        public string Duration { get; set; }
        [DataMember(Name = "interventionType")]
        public string InterventionType { get; set; }
        [DataMember(Name = "notes")]
        public string Notes { get; set; }
        [DataMember(Name = "partyId")]
        public int PartyId { get; set; }
        [DataMember(Name = "updateTs")]
        public string UpdateTs { get; set; }
        [DataMember(Name = "updateUser")]
        public string UpdateUser { get; set; }

        //#region constructors

        //public CareTimeModel()
        //{
        //}

        //public CareTimeModel(string empty)
        //{
        //}
        //public CareTimeModel( string id, string beginTime, string callDate, string caredBy, string duration, string interventionType, string notes, string partyId, string updateTs, string updateUser)
        //{
        //    this.Id = id;
        //    this.BeginTime = beginTime;
        //    this.CareTimeDate = callDate;
        //    this.CaredBy = caredBy;
        //    this.Duration = duration;
        //    this.InterventionType = interventionType;
        //    this.Notes = notes;
        //    this.PartyId = partyId;
        //    this.UpdateTs = updateTs;
        //    this.UpdateUser = updateUser;
        //}
        //#endregion
        //#region properties
        //public string Id { get; set; }
        //public string BeginTime { get; set; }
        //public string CareTimeDate { get; set; }
        //public String CaredBy { get; set; }
        //public string Duration { get; set; }
        //public String InterventionType { get; set; }
        //public String Notes { get; set; }
        //public string PartyId { get; set; }
        //public string UpdateTs { get; set; }
        //public string UpdateUser { get; set; }
        //#endregion
        //#region methods
        //#endregion
    }
  
    [DataContract]
    public class CareTimeCollection
    {
        [DataMember(Name = "CRT")]
        public IEnumerable<CareTimeModel> CareTime { get; set; }      
    }


}