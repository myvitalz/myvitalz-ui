﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace MyVitalz.Web.Models
{
    [DataContract]
    public class MedicationModel
    {
        [DataMember(Name = "id")]
        public string Id { get; set; }

        [DataMember(Name = "deviceLabel")]
        public string MedicationName { get; set; }

        [DataMember(Name = "deviceId")]
        public string DeviceId { get; set; }

        [DataMember(Name = "measurementTime")]
        public string MeasurementTime { get; set; }

    }
    [DataContract]
    public class MedicationModelCollection
    {
        [DataMember(Name = "MED")]
        public IEnumerable<MedicationModel> Medication { get; set; }
    }
}
