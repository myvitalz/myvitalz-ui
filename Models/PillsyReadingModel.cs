﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace MyVitalz.Web.Models
{
    [DataContract]
    public class PillsyReadingModel
    {
        [DataMember(Name = "id")]
        public String Id { get; set; }
        [DataMember(Name = "batteryLevel")]
        public String BatteryLevel { get; set; }
        [DataMember(Name = "deviceId")]
        public String DeviceId { get; set; }
        [DataMember(Name = "measurementTime")]
        public String MeasurementTime { get; set; }
        [DataMember(Name = "partyId")]
        public String PartyId { get; set; }
        [DataMember(Name = "period")]
        public String Period { get; set; }
        [DataMember(Name = "pillsyReading")]
        public String PillsyReading { get; set; }
        [DataMember(Name = "transmissionTime")]
        public String TransmissionTime { get; set; }
        [DataMember(Name = "deviceLabel")]
        public String DeviceLabel { get; set; }
        [DataMember(Name = "updateTs")]
        public String UpdateTs { get; set; }
        [DataMember(Name = "updateUser")]
        public String UpdateUser { get; set; }
    }

    [DataContract]
    public class PillsyReadingModelCollection
    {
        [DataMember(Name = "MED")]
        public IEnumerable<PillsyReadingModel> PillsyReading { get; set; }
    }
}
