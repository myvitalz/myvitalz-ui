﻿using MyVitalz.Web.Service;

namespace MyVitalz.Web.Models
{
    public class BreadCrumbsModel
    {
        #region Properties

        public string Page { get; set; }

        public string PartyId { get; set; }

        public string PatientId { get; set; }

        public Device Device { get; set; } 
        
        #endregion

        #region Constructor

        public BreadCrumbsModel(string page)
        {
            Page = page;
        }

        public BreadCrumbsModel(string page, string partyId)
        {
            Page = page;
            PartyId = partyId;
        }

        public BreadCrumbsModel(string page, string partyId, string patientId)
        {
            Page = page;
            PartyId = partyId;
            PatientId = patientId;
        }

        public BreadCrumbsModel(string page, string partyId, string patientId, Device device)
        {
            Page = page;
            PartyId = partyId;
            PatientId = patientId;
            Device = device;
        } 
        #endregion
    }
}
