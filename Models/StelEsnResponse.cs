﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyVitalz.Web.Models
{
    public class StelEsnResponse
    {
        public string esn { get; set; }
        public string used { get; set; }
        public string version { get; set; }
        public string connection { get; set; }
        public string hw { get; set; }
    }
}