﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyVitalz.Web.Models
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class DelData
    {

        private string accountField;

        private string passwordField;

        private DelDataCaseMeterData[] dCaseField;

        /// <remarks/>
        public string Account
        {
            get
            {
                return this.accountField;
            }
            set
            {
                this.accountField = value;
            }
        }

        /// <remarks/>
        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("CaseMeterData", IsNullable = false)]
        public DelDataCaseMeterData[] DCase
        {
            get
            {
                return this.dCaseField;
            }
            set
            {
                this.dCaseField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class DelDataCaseMeterData
    {

        private DelDataCaseMeterDataMeter[] meterField;

        private string pIDField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Meter")]
        public DelDataCaseMeterDataMeter[] Meter
        {
            get
            {
                return this.meterField;
            }
            set
            {
                this.meterField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PID
        {
            get
            {
                return this.pIDField;
            }
            set
            {
                this.pIDField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class DelDataCaseMeterDataMeter
    {

        private string meterTypeField;

        private byte meterExIDField;

        private string meterIDField;

        /// <remarks/>
        public string MeterType
        {
            get
            {
                return this.meterTypeField;
            }
            set
            {
                this.meterTypeField = value;
            }
        }

        /// <remarks/>
        public byte MeterExID
        {
            get
            {
                return this.meterExIDField;
            }
            set
            {
                this.meterExIDField = value;
            }
        }

        /// <remarks/>
        public string MeterID
        {
            get
            {
                return this.meterIDField;
            }
            set
            {
                this.meterIDField = value;
            }
        }
    }



    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class DelResult
    {

        private byte reCodeField;

        private string reMsgField;

        /// <remarks/>
        public byte ReCode
        {
            get
            {
                return this.reCodeField;
            }
            set
            {
                this.reCodeField = value;
            }
        }

        /// <remarks/>
        public string ReMsg
        {
            get
            {
                return this.reMsgField;
            }
            set
            {
                this.reMsgField = value;
            }
        }
    }



}