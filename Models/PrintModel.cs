﻿using System.Data;

namespace MyVitalz.Web.Models
{
    public class PrintModel
    {
        #region Properties
        public string GetStatusClass(string reading)
        {
            if (reading.Contains("4")
                  || reading.Contains("5")
                  || reading.Contains("True") )
            {
                return "alert";
            }
            return string.Empty;
        }

        public string GetStatusClass(bool isAbnormal)
        {
            if (isAbnormal == true)
            {
                return "alert";
            }
            return string.Empty;
        }

        public string DeviceType { get; set; }

        public string Title { get; set; }

        public DataTable Data { get; set; } 
        #endregion
    }
}
