﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace MyVitalz.Web.Models
{
    [DataContract]
    public class BloodHbA1cReportModel
    {
        [DataMember(Name = "firstName")]
        public String FirstName { get; set; }
        [DataMember(Name = "lastName")]
        public String LastName { get; set; }
        [DataMember(Name = "dob")]
        public String Dob { get; set; }
        [DataMember(Name = "patientId")]
        public String PatientId { get; set; }
        [DataMember(Name = "group")]
        public String Group { get; set; }
        [DataMember(Name = "subGroup")]
        public String SubGroup { get; set; }
        [DataMember(Name = "county")]
        public String County { get; set; }
        [DataMember(Name = "avgGlucose")]
        public String AvgGlucose { get; set; }
        [DataMember(Name = "id")]
        public String Id { get; set; }
        [DataMember(Name = "deviceId")]
        public String DeviceId { get; set; }
        [DataMember(Name = "hbA1c_READING")]
        public String HbA1c_READING { get; set; }
        [DataMember(Name = "measurementTime")]
        public String MeasurementTime { get; set; }
        [DataMember(Name = "transmissionTime")]
        public String TransmissionTime { get; set; }
        [DataMember(Name = "updateTs")]
        public String UpdateTs { get; set; }
        [DataMember(Name = "updateUser")]
        public String UpdateUser { get; set; }
        [DataMember(Name = "partyId")]
        public String PartyId { get; set; }
    }

}