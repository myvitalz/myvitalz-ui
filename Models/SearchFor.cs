﻿namespace MyVitalz.Web.Models
{
    public enum SearchFor
    {
        AllParties,
        PatientsByDoctor,
        PatientsByCaregiver,
        PatientsByClinician,
        AllKits
    }
}