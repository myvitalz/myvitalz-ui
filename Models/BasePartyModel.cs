﻿using MyVitalz.Web.Service;
using System.Collections.Generic;
using System.Linq;

namespace MyVitalz.Web.Models
{
    public class BasePartyModel
    {
        #region Constructor

        public BasePartyModel()
            : this(string.Empty)
        {

        }

        public BasePartyModel(string fieldPrefix)
        {
            FieldPrefix = fieldPrefix;
        }

        public BasePartyModel(string fieldPrefix, Patient party)
            : this(fieldPrefix)
        {
            Party = party;
        }

        public BasePartyModel(string fieldPrefix, Patient party, XrefType[] associationTypes, GroupType[] groupTypes)
            : this(fieldPrefix, party)
        {
            GroupTypes = groupTypes;
            AssociationTypes = associationTypes;
        }

        public BasePartyModel(string fieldPrefix, Patient party, XrefType[] associationTypes, GroupType[] groupTypes, Xref[] associations)
            : this(fieldPrefix, party)
        {
            GroupTypes = groupTypes;
            AssociationTypes = associationTypes;
            Associations = associations;
        }

        public BasePartyModel(string fieldPrefix, Patient party, XrefType[] associationTypes, GroupType[] groupTypes, Xref[] associations, SubGroupWrapper[] subgrouptypes, SubGroupWrapper[] subgroups, SubGroupWrapper[] allsubgroups, string[] primaryPicked, string[] secondaryPicked, string[] primarySubGroupId, string[] secondarySubGroupId, string showSubGroups, string selectedGT, string homeGroup)
            : this(fieldPrefix, party)
        {
            GroupTypes = groupTypes;
            AssociationTypes = associationTypes;
            Associations = associations;
            SubGroupTypes = subgrouptypes;
            SubGroups = subgroups;
            PrimaryPicked = primaryPicked;
            SecondaryPicked = secondaryPicked;
            AllSubGroups = allsubgroups;
            ShowSubGroups = showSubGroups;
            PrimarySubGroupId = primarySubGroupId;
            SecondarySubGroupId = secondarySubGroupId;
            SelectedGT = selectedGT;
            HomeGroup = homeGroup;
        }

        public BasePartyModel(string fieldPrefix, Patient party, XrefType[] associationTypes, GroupType[] groupTypes, Xref[] associations, SubGroupWrapper[] subgrouptypes, SubGroupWrapper[] subgroups, SubGroupWrapper[] allsubgroups, string[] primaryPicked, string[] secondaryPicked, string[] primarySubGroupId, string[] secondarySubGroupId, string showSubGroups, string selectedGT, string homeGroup, InsuranceModel insurance)
            : this(fieldPrefix, party)
        {
            GroupTypes = groupTypes;
            AssociationTypes = associationTypes;
            Associations = associations;
            SubGroupTypes = subgrouptypes;
            SubGroups = subgroups;
            PrimaryPicked = primaryPicked;
            SecondaryPicked = secondaryPicked;
            AllSubGroups = allsubgroups;
            ShowSubGroups = showSubGroups;
            PrimarySubGroupId = primarySubGroupId;
            SecondarySubGroupId = secondarySubGroupId;
            SelectedGT = selectedGT;
            HomeGroup = homeGroup;
            Insurance = insurance;
        }

        #endregion

        #region Properties

        public Xref[] Associations { get; set; }

        public XrefType[] AssociationTypes { get; set; }

        public GroupType[] GroupTypes { get; set; }

        public Patient Party { get; set; }

        public string FieldPrefix { get; set; }

        public bool IsProfile { get; set; }

        public Preferences[] Preferences { get; set; }

        public int PreferenceIndex { get; set; }

        //public List<MainGroupTypes>[] MainGroupTypes { get; set; }

        public SubGroupWrapper[] SubGroupTypes { get; set; }
        //public string[] SubGroupTypesPicked { get; set; }
        //public int SubGroupTypesIndex { get; set; }

        public SubGroupWrapper[] SubGroups { get; set; }
        //public string[] SubGroupsPicked { get; set; }
        //public int SubGroupsIndex { get; set; }

        public SubGroupWrapper[] AllSubGroups { get; set; }
        public SubGroupWrapper[] Changed_SubGroups { get; set; }

        public string[] SubGroupTypesPicked { get; set; }
        public int SubGroupTypesIndex { get; set; }

        public string[] PrimaryPicked { get; set; }
        public string[] PrimarySubGroupId { get; set; }
        public int PrimaryIndex { get; set; }

        public string[] SecondaryPicked { get; set; }
        public string[] SecondarySubGroupId { get; set; }
        public int SecondaryIndex { get; set; }

        public string SelectedGT { get; set; }
        public string ShowSubGroups { get; set; }
        public string HomeGroup { get; set; }
        public InsuranceModel Insurance { get; set; }

        #endregion

        #region Methods

        public string GetXrefTypeDesc(string xrefType)
        {
            var str = "Unknown";
            var assoXrefType = AssociationTypes.FirstOrDefault(a => a.xrefType == xrefType);
            if (assoXrefType != null)
                str = assoXrefType.xrefTypeDesc;
            return str;
        }

        public List<Preferences> GetPreferences(string preferenceType)
        {
            var preferences = new List<Preferences>();
            if (Preferences != null)
            {
                preferences = Preferences.Where(p => p.preferenceType == preferenceType).ToList();
            }
            return preferences;
        }

        public List<GroupType> GetGroupTypes()
        {
            var grouptypes = new List<GroupType>();
            if(GroupTypes != null)
            {
                grouptypes = GroupTypes.ToList();
            }
            return grouptypes;
        }

        public List<SubGroupWrapper> GetAllSubGroupTypes()
        {            
            var subgrouptypes = new List<SubGroupWrapper>();
            if (AllSubGroups != null)
            {
                subgrouptypes = AllSubGroups.ToList();
            }
            return subgrouptypes;
        }
        
        #endregion
    }
}