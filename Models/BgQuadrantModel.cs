﻿using MyVitalz.Web.BgService;

namespace MyVitalz.Web.Models
{
    public class BgQuadrantModel
    {
        #region Properties

        public GetQuadrantDetailsResponse QuadrantDetails { get; set; }

        public GetEkgHistoryResponse EkgReadings { get; set; }
        
        #endregion

        #region Methods

        public string GetBodyPosition(string bodyPosition)
        {
            string str = "Unknown";
            switch (bodyPosition)
            {
                case "1":
                case "129":
                    str = "Standing";
                    break;
                case "2":
                case "130":
                    str = "Leaning";
                    break;
                case "3":
                case "131":
                    str = "Lying down";
                    break;
                case "4":
                case "132":
                    str = "Unknown";
                    break;
                case "5":
                    str = "Not set";
                    break;
            }
            return str;
        } 
        #endregion
    }
}
