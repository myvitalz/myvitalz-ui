﻿namespace MyVitalz.Web.Models
{
    public enum SearchFilter
    {
        None = 0,
        Active = 1,
        InActive = 2,
        All = 3,
    }
}
