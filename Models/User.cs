﻿using MyVitalz.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web.Security;

namespace MyVitalz.Web.Models
{
    public class User : MembershipUser, IIdentity, IPrincipal
    {
        #region Properties

        public string AuthenticationType { get; set; }

        public bool IsAuthenticated { get; set; }

        public string Name { get; set; }

        public IIdentity Identity { get; private set; }

        public string NickName { get; set; }

        public string FullName { get; set; }

        public string[] Roles { get; set; }

        public string Group { get; set; }

        public string UserId { get; set; }

        public string Password { get; set; } 

        public string SubGroup { get; set; }
        
        #endregion

        #region Constructor

        public User(string userName)
        {
            Identity = (IIdentity)new GenericIdentity(userName);
        } 
        #endregion

        #region Methods

        public bool IsInRole(string role)
        {
            return Enumerable.Contains<string>((IEnumerable<string>)Roles, role);
        }

        public string ToRoleName()
        {
            return string.Join(" / ", Enumerable.ToArray<string>(Enumerable.Select<string, string>((IEnumerable<string>)Roles, (Func<string, string>)(r => StringExtension.ToRoleName(r)))));
        }

        #endregion
    }
}
