﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MyVitalz.Web.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyVitalz.Web.Models
{
    public class ManualReadingModel
    {
        [Required]
        public string PartyId { get; set; }

        //[RequiredAttribute(@"^['/']$", ErrorMessage = "'Please enter Diastolic reading after ' / '.")]  
        [Required]
        public string BPReading_Systolic { get; set; }

        [Required]
        public string BPReading_Diastolic { get; set; }

        
        public string BPReading_PR { get; set; }

        
        public string POReading { get; set; }

        
        public string BGReading { get; set; }

        
        public string TSReading { get; set; }

        
        public string WSReading { get; set; }

       
        public string SMReading { get; set; }

        public string SMReading_Fev { get; set; }
        public string SMReading_Pef { get; set; }

        public string A1CReading { get; set; }

        [Required]
        public string Measurement_Time { get; set; }

        //-------------------------------
        //public string getDeviceType()
        //{
        //    return "WSM";
        //}

        //[Required]
        //public string Weight { get; set; }

        //private String weight;
        //public String getWeight()
        //{
        //    return weight;
        //}

        //public void setWeight(String weight)
        //{
        //    this.weight = weight;
        //}
    }
}