﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MyVitalz.Web.Models
{
  public class EmailTemplateModel
  {
      #region Properties

      [Required]
      public string PartyId { get; set; }

      [AllowHtml]
      [Required]
      public string From { get; set; }

      [Required]
      [AllowHtml]
      public string To { get; set; }

      [AllowHtml]
      public string Cc { get; set; }

      [Required]
      public string Subject { get; set; }

      [Required]
      public string Body { get; set; } 
      #endregion
  }
}
