﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyVitalz.Web.Models
{

    public class Rootobject
    {
        public Insurance[] Insurance { get; set; }
    }

    public class Insurance
    {
        public string id { get; set; }
        public string carrierName { get; set; }
        public string createUser { get; set; }
        public string createTs { get; set; }
        public string employer { get; set; }
        public string employerGroupId { get; set; }
        public string partyId { get; set; }
        public string policyId { get; set; }
        public string updateUser { get; set; }
        public string updateTs { get; set; }
    }

}