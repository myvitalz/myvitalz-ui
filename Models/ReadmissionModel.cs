﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace MyVitalz.Web.Models
{
    [DataContract]
    public class ReadmissionModel
    {      

        [DataMember(Name = "id")]
        public string Id { get; set; }
        [DataMember(Name = "endDate")]
        public string EndDate { get; set; }
        [DataMember(Name = "partyId")]
        public string PartyId { get; set; }
        [DataMember(Name = "readmissionDate")]
        public string ReadmissionDate { get; set; }
        [DataMember(Name = "updateTs")]
        public string UpdateTs { get; set; }
        [DataMember(Name = "updateUser")]
       public string UpdateUser { get; set; }   
    }
    [DataContract]
    public class ReadmissionModelCollection
    {
        [DataMember(Name = "PMN")]
        public IEnumerable<ReadmissionModel> Readmission { get; set; }
    }
}