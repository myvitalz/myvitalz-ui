﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyVitalz.Web.Models
{
    public class StelGetEsnResponse
    {
        public StelEsnResponse[] gates { get; set; }
        public string result { get; set; }
    }
}