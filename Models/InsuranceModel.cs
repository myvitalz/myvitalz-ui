﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace MyVitalz.Web.Models
{
    public class InsuranceModel
    {
        [JsonProperty("id")]
        public string id { get; set; }

        [JsonProperty("partyId")]
        public string partyId { get; set; }

        [JsonProperty("policyId")]
        public string policyId { get; set; }

        [JsonProperty("carrierName")]
        public string carrierName { get; set; }

        [JsonProperty("employer")]
        public string employer { get; set; }

        [JsonProperty("employerGroupId")]
        public string employerGroupId { get; set; }

        [JsonProperty("createTs")]
        public string createTs { get; set; }

        [JsonProperty("createUser")]
        public string createUser { get; set; }

        [JsonProperty("updateTs")]
        public string updateTs { get; set; }

        [JsonProperty("updateUser")]
        public string updateUser { get; set; }

    }

    public class InsuranceCollection
    {
        [JsonProperty("Insurance")]
        public List<InsuranceModel> Insurances { get; set; }
    }


}