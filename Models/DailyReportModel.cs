﻿using MyVitalz.Web.Service;
using System.Collections.Generic;
namespace MyVitalz.Web.Models
{
    public class DailyReportModel
    {
        public Report Report { get; set; }
        public ReadingsReport Readings { get; set; }
        public PatientDetails[] HiddenReadings { get; set; }

        public ReportBasicParty[] HiddenSupport { get; set; }     
        
        public bool IsFullReport { get; set; }
        public bool IsListView { get; set; }
        public Preferences[] Preferences { get; set; }
        public List<string> PatientToHide { get; set; }

        public string thBP { get; set; }
        public string thSPO2 { get; set; }
        public string thTemp { get; set; }
        public string thWeight { get; set; }
        public string thGluc { get; set; }
        public string thFevPev { get; set; }
        public string thFeelings { get; set; }
        public string thMILDailyQa { get; set; }
        public string thDBTDailyQa { get; set; }
        public string thMED { get; set; }
        public string thCovid { get; set; }
        public string thEKG { get; set; }
        public Dictionary<string, IEnumerable<PauseMonitorModel>> PMNReadings = new Dictionary<string, IEnumerable<PauseMonitorModel>>();

        public Dictionary<string, IEnumerable<PillsyReadingModel>> pillsys = new Dictionary<string, IEnumerable<PillsyReadingModel>>();
        public Dictionary<string, IEnumerable<CovidModel>> covid = new Dictionary<string, IEnumerable<CovidModel>>();
        public List<string> ReviewCompletedPatients { get; set; }


        #region Methods
        public string GetStatusClass(string isAbnormal)
        {
            if (isAbnormal == "*" || "true".Equals(isAbnormal) )
            {
                return "alert";
            }
            return string.Empty;
        }
        #endregion
    }
}