﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace MyVitalz.Web.Models
{
    public class BloodA1cReportModel
    {
        public IEnumerable<BloodHbA1cReportModel> a1cReports { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

    }

}