﻿using MyVitalz.Web.Service;
using System.Collections.Generic;
using System.Web.Mvc;
namespace MyVitalz.Web.Models
{
  public class SearchModel
  {
      #region Properties

      public SearchFor SearchFor { get; set; }

      public SearchCriteria Criteria { get; set; }

      public List<SearchCriteria> ExcludedCriteria { get; set; }

      public string SearchText { get; set; }

      public SearchPatientCriteria PatientCriteria { get; set; }

      public string PartyName { get; set; }

      public string FilterText { get; set; }

      public string SortColumn { get; set; }

      public string SortOrder { get; set; }

      public string PartyId { get; set; }

      public SearchKitsCriteria KitsCriteria { get; set; }

      public string KitId { get; set; }

      public string KitSearchText { get; set; }

      public List<SelectListItem> SearchByGAList { get; set; }

      public string Group { get; set; }

      public string Status { get; set; }

      public Party[] Parties { get; set; }

      public DeviceKit[] DeviceKit { get; set; }

      public Patient[] Patients { get; set; }

      public string EHRPage { get; set; }
      
      #endregion

      #region Constructor

      public SearchModel()
      {
          ExcludedCriteria = new List<SearchCriteria>();
          SearchByGAList = new List<SelectListItem>();
          SearchByGAList.Add(new SelectListItem { Text = "Kit Id", Value = "1" });
      }

      #endregion

      #region Methods

      public List<SearchCriteria> GetExculdeList()
      {
          return ExcludedCriteria;
      }

      //public List<SearchCriteria> GetKitSearchTypeList()
      //{
      //    List<SearchCriteria> lstSC = GetExculdeList();
      //    foreach(var kitid in lstSC)
      //    {
      //        if (kitid == KitId)
      //        {
      //            ExcludedCriteria.Add(kitid);
      //        }
      //    }
      //    return ExcludedCriteria;
      //}

      public SearchCriteria GetKitSearchTypeList()
      {          
          return SearchCriteria.KitId;
      }
      
      #endregion
  }
}
