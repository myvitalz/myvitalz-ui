﻿//// Type: MyVitalz.Web.MvcApplication
//// Assembly: MyVitalz.Web, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
//// MVID: 5DFB40F2-FD4B-4A92-9CB7-01EA3C877ECE
//// Assembly location: D:\Data\Haraku\MyVitalz\bin\MyVitalz.Web.dll

//using MyVitalz.Web.Helpers;
//using MyVitalz.Web.Models;
//using System;
//using System.Security.Principal;
//using System.Threading;
//using System.Web;
//using System.Web.Mvc;
//using System.Web.Optimization;
//using System.Web.Routing;

//namespace MyVitalz.Web
//{
//  public class MvcApplication : HttpApplication
//  {
//    public static void RegisterGlobalFilters(GlobalFilterCollection filters)
//    {
//      filters.Add((object) new HandleErrorAttribute());
//    }

//    public static void RegisterRoutes(RouteCollection routes)
//    {
//      RouteCollectionExtensions.IgnoreRoute(routes, "{resource}.axd/{*pathInfo}");
//      RouteCollectionExtensions.MapRoute(routes, "Default", "{controller}/{action}/{id}", (object) new
//      {
//        controller = "Home",
//        action = "Index",
//        id = UrlParameter.Optional
//      });
//    }

//    protected void Application_Start()
//    {
//      AreaRegistration.RegisterAllAreas();
//      MvcApplication.RegisterGlobalFilters(GlobalFilters.Filters);
//      MvcApplication.RegisterRoutes(RouteTable.Routes);
//      ConfigBundle.RegisterBundles(BundleTable.Bundles);
//    }

//    protected void Application_EndRequest()
//    {
//      HttpContextWrapper httpContextWrapper = new HttpContextWrapper(Context);
//      if (!AjaxRequestExtensions.IsAjaxRequest(httpContextWrapper.Request) || httpContextWrapper.Response.StatusCode != 302)
//        return;
//      Context.Response.Clear();
//      Context.Response.Write("Your session was timed out. Please Login.");
//      Context.Response.StatusCode = 401;
//    }

//    protected void Application_PostAuthenticateRequest(object sender, EventArgs e)
//    {
//      User userFromAuthCookie = Authentication.GetUserFromAuthCookie();
//      if (userFromAuthCookie == null)
//        return;
//      Thread.CurrentPrincipal = (IPrincipal) userFromAuthCookie;
//      HttpContext.Current.User = (IPrincipal) userFromAuthCookie;
//    }
//  }
//}
