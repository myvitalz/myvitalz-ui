﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using MyVitalz.Web.Helpers;
using MyVitalz.Web.Models;

namespace MyVitalz.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void Application_EndRequest()
        {
            HttpContextWrapper httpContextWrapper = new HttpContextWrapper(Context);
            if (!AjaxRequestExtensions.IsAjaxRequest(httpContextWrapper.Request) || httpContextWrapper.Response.StatusCode != 302)
                return;
            Context.Response.Clear();
            Context.Response.Write("Your session was timed out. Please Login.");
            Context.Response.StatusCode = 401;
        }

        protected void Application_PostAuthenticateRequest(object sender, EventArgs e)
        {
            User userFromAuthCookie = Authentication.GetUserFromAuthCookie();
            if (userFromAuthCookie == null)
                return;
            Thread.CurrentPrincipal = (IPrincipal)userFromAuthCookie;
            HttpContext.Current.User = (IPrincipal)userFromAuthCookie;
        }

    }
}