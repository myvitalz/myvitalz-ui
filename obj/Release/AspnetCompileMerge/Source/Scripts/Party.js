﻿var _canAddParty = false;

function BaseAddParty(actionUrl, partyDiv) {
    var parameters = $("#editEhrForm").serialize();
    parameters += "&NewlyAddedPartyId=" + $("#NewlyAddedPartyId").val();
    $.ajax({
        type: "POST",
        url: actionUrl,
        dataType: "html",
        data: parameters,
        success: function (data) {
            $(partyDiv).html(data);
            $("#NewlyAddedPartyId").val("");
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
}

function BaseRemoveParty(actionUrl, partyDiv, removedIndex) {
    //Set the Index that needs to be removed
    $("#IndexRemoved").val(removedIndex);
    var parameters = $("#editEhrForm").serialize();
    $.ajax({
        type: "POST",
        url: actionUrl,
        dataType: "html",
        data: parameters,
        success: function (data) {
            $(partyDiv).html(data);
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
    //Reset the removal index
    $("#IndexRemoved").val(0);
}

function AddDoctor() {
    BaseAddParty("AddDoctor", "#doctorsDiv");
}

function RemoveDoctor(removedIndex) {
    BaseRemoveParty("RemoveDoctor", "#doctorsDiv", removedIndex);
}

function AddCaregiver() {
    BaseAddParty("AddCaregiver", "#caregiversDiv");
}

function RemoveCaregiver(removedIndex) {
    BaseRemoveParty("RemoveCaregiver", "#caregiversDiv", removedIndex);
}

function AddClinician() {
    BaseAddParty("AddClinician", "#nursesDiv");
}

function RemoveClinician(removedIndex) {
    BaseRemoveParty("RemoveClinician", "#nursesDiv", removedIndex);
}

function AddAssociation() {
    BaseAddParty("AddAssociation", "#associationsDiv");
}

function RemoveAssociation(removedIndex) {
    BaseRemoveParty("RemoveAssociation", "#associationsDiv", removedIndex);
}

function AddDevice(kitId) {
    $("#CurrentKitId").val(kitId);
    var parameters = $("#editEhrForm").serialize();
    $.ajax({
        type: "POST",
        url: "AddDevice",
        dataType: "html",
        data: parameters,
        success: function (data) {
            $("#devicesDiv").html(data);
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
}

function RemoveDevice(removedIndex) {
    //Set the Index that needs to be removed
    $("#IndexRemoved").val(removedIndex);
    var parameters = $("#editEhrForm").serialize();
    $.ajax({
        type: "POST",
        url: "RemoveDevice",
        dataType: "html",
        data: parameters,
        success: function (data) {
            $("#devicesDiv").html(data);
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
    //Reset the removal index
    $("#IndexRemoved").val(0);
}

function AddKit() {
    //Make sure that group was selected
    var group = $("#Details_ehr_group").val();
    if (group === ""
        || group === "ALL") {
        ShowError("Select a valid group in the Detail tab for this patient.");
        return;
    }
    var parameters = $("#editEhrForm").serialize();
    $.ajax({
        type: "POST",
        url: "AddKit",
        dataType: "html",
        data: parameters,
        success: function (data) {
            $("#devicesDiv").html(data);
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
}

function RemoveKit(removedIndex) {
    //Set the Index that needs to be removed
    $("#IndexRemoved").val(removedIndex);
    var parameters = $("#editEhrForm").serialize();
    $.ajax({
        type: "POST",
        url: "RemoveKit",
        dataType: "html",
        data: parameters,
        success: function (data) {
            $("#devicesDiv").html(data);
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
    $("#IndexRemoved").val(0);
}

function AssignKit() {
    $("#Details_kitDetails_kitId").val($("#SelectedKitId").val());
    var parameters = $("#editEhrForm").serialize();
    $.ajax({
        type: "POST",
        url: "AssignKit",
        dataType: "html",
        data: parameters,
        success: function (data) {
            CloseDialogWindow();
            $("#devicesDiv").html(data);
        },
        error: function (request, textStatus, errorThrown) {
            ShowDialogCallbackError(request, textStatus, errorThrown);
        }
    });
}

function AttachAutoCompleteEvent(displayControlName, index, partyType, wildCardType) {
    var displayControl = "." + partyType + index + displayControlName;
    var firstNameControl = "." + partyType + index + "FirstNameInput";
    var lastNameControl = "." + partyType + index + "LastNameInput";
    var emailControl = "." + partyType + index + "EmailInput";
    var partyIdControl = "." + partyType + index + "PartyIDInput";
    var messageDiv = "#" + partyType + index + "MessageDiv";
    $(firstNameControl).attr("placeholder", "Type First Name to search");
    $(lastNameControl).attr("placeholder", "Type Last Name to search");
    $(emailControl).attr("placeholder", "Type Email to search");
    $(displayControl).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "GetPartiesBySearchType",
                dataType: 'json',
                data: { partyType: partyType, wildCardType: wildCardType, wildCard: request.term },
                success: function (data) {
                    var rows = new Array();
                    //If there is no data returned
                    if (data.Parties === null) {
                        rows[0] = { value: "No Matches", id: "0", data: "NOMATCHES" };
                        if (_canAddParty === true) {
                            rows[1] = { value: "Add New " + GetPartyName(partyType), id: "0", data: "ADD" };
                        }
                    }
                        //If there is a data returned
                    else {
                        for (var i = 0; i < data.Parties.length; i++) {
                            rows[i] = { value: data.Parties[i].firstName + " " + data.Parties[i].lastName + " ( " + data.Parties[i].email + " ) ", id: data.Parties[i].partyID, data: data.Parties[i].firstName + "~" + data.Parties[i].lastName + "~" + data.Parties[i].email };
                        }
                    }
                    value:
                    //Hides the error message div
                        HideMessage(messageDiv);

                    response(rows);
                },
                error: function (request, textStatus, errorThrown) {
                    var data = jQuery.parseJSON(request.responseText);
                    var rows = new Array();
                    rows[0] = { value: "Error:" + data.Message, id: "0", data: "False" };
                    response(rows);
                }
            });
        },
        minLength: 2,  //characters the user needs to type in before calling the Action
        select: function (event, ui) {
            //It's called when the user selects a value from the list
            if (ui.item) {
                switch (ui.item.id) {
                    default:
                        if (ui.item.data === "ADD") {
                            $(displayControl).val("");
                            AddParty(partyType);
                        } else if (ui.item.data === "NOMATCHES") {
                            $(displayControl).val("");
                        } else {
                            var partyData = ui.item.data.split("~");
                            $(firstNameControl).val(partyData[0]);
                            $(lastNameControl).val(partyData[1]);
                            $(emailControl).val(partyData[2]);
                            $(partyIdControl).val(ui.item.id);
                        }
                        break;

                }
            }
            event.preventDefault();
        }
    });

    //Attach keyup & blur event to avoid invalid entries
    $(displayControl).keyup(function (e) {
        //Ignore Enter & Tab
        if (e.which !== 13
            && e.which !== 9) {
            //Reset the value
            $(partyIdControl).val(0);
        }
    });

    $(displayControl).blur(function (e) {
        //Note:Timeout is given to avoid the timing issue on mouse selection
        setTimeout(function () {
            //If something entered in the displaycontrol & the valuecontrol is still zero then show the error
            if ($(displayControl).val() !== ""
                && $(partyIdControl).val() === "0") {
                $(displayControl).val("");
                $(partyIdControl).val("0");
                //ShowError("A value was not set due to an incomplete entry.", messageDiv);
            }
        }, 500);
    });
}

function AddParty(partyType) {
    var parameters = { partyType: partyType };
    $.ajax({
        type: "POST",
        url: "AddBaseParty",
        dataType: "html",
        data: parameters,
        success: function (data) {
            OpenDialogWindow("Add " + GetPartyName(partyType), data, "50em");
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
}

function EditParty(partyId, partyType) {
    var parameters = { partyId: partyId};
    $.ajax({
        type: "POST",
        url: "EditBaseParty",
        dataType: "html",
        data: parameters,
        success: function (data) {
            OpenDialogWindow("Edit " + GetPartyName(partyType), data, "50em");
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
}

function GetPartyName(partyType) {
    var party = "";
    switch (partyType) {
        case "N":
            party = "Clinician";
            break;
        case "C":
            party = "Caregiver";
            break;
        case "D":
            party = "Doctor";
            break;
        case "P":
            party = "Patient";
            break;
        default:
    }
    return party;
}
function SaveEhr() {
    //Set the Index that needs to be removed
    var parameters = $("#editEhrForm").serialize();
    $.ajax({
        type: "POST",
        url: "SaveEhr",
        dataType: "html",
        data: parameters,
        success: function (data) {
            ShowMessage("success", "Party was saved successfully. Refreshing the info. Please wait...");
            if ($("#Details_ehr_partyType").val() === "P") {
                if (location.href.indexOf("EditEHR") !== -1) {
                    GoToUrlWithDelay("EditEHR?patientId=" + data, 5000);
                } else {
                    GoToUrlWithDelay("EditPatient?partyId=" + data, 5000);
                }
            } else {
                GoToUrlWithDelay("EditParty?partyId=" + data, 5000);
            }
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
    $("#IndexRemoved").val(0);
}

function RefreshEhrDetails()
{
    //Set the Index that needs to be removed
    var parameters = $("#editEhrForm").serialize();
    $.ajax({
        type: "POST",
        url: "RefreshEhrDetails",
        dataType: "html",
        data: parameters,
        success: function (data) {
            $("#ehrContentDiv").html(data);
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
}