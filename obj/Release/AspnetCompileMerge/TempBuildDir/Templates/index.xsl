<?xml version="1.0"?>
<xsl:stylesheet   version="1.0"   xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="no"/>
  <xsl:template match="/">
    <html>
      <head>
        <meta name="viewport" content="width=device-width" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Myvitalz.com</title>
      </head>
      <body bgcolor="#FFFFFF" style="margin: 0px; padding: 0px;">
        <style type="text/css">
          #patientTable{
		    border: 1px solid #DDDDDD;
			text-align: left;
			padding:0px;
			margin: 0px;
			position: relative;
			vertical-align: top;
			border-color: #cccccc;
			border-collapse: collapse;
			border-spacing: 0;
          }
          #patientTable th{
			text-align: center;
          }
          #patientTable td {
			border-collapse: collapse;
			width: 33%;
			vertical-align: top;
			padding-top: 0.5em;
			padding-bottom: 0.5em;
			padding-left: 0.5em;
			margin: 0px;
          }
		  .NoReading{
			color: orange;
			font-size: 0.9em;
		  }
         .partyName {
			font-size: 1.2em;
			font-weight: bold;
          }

          .contactInfo div {
			padding-left: 0em;
			padding-right: 0.5em;
          }

          .align-center{
			text-align:center;
          }

          .deviceName {
			text-align: right;
			font-weight:bold;
			margin-right: 0.5em;
          }
          .readingAlert {
			background-color: #ffff99;
			color: red;
          }
		  
		  .feelingImageSmall {
				height: 1em;
				width: 1em;     
		  }
        </style>
        <table style="width:100%">
          <tr>
            <td style="display: block !important; max-width: 600px !important; margin: 0 auto !important; clear: both !important;" bgcolor="#FFFFFF">
              <div  style="padding: 15px; max-width: 600px; margin: 0 auto; display: block">
                <p style="height:120px;text-align:center;">
                  <img src="https://portal.myvitalz.com/images/logo/DailyReport.jpg" alt="MyVitalzDailyReportLogo" width="100%" height="100%"/>
                </p>
                <p style="text-align:center;">
                  Trouble viewing this email? <a href="https://portal.myvitalz.com/Doctor/DailyReport" target="DailyReportWindow" class="body">Click here to open in browser.</a>
                </p>
				<p style="text-align:center;">
                <table  style="width:100%;">
                  <tr>
                    <td style="text-align: left;">
						Report Date : <xsl:value-of select="report/reportDate"/><br/>
						Total Patients: <strong>
						  <xsl:value-of select="report/patientCount"/>
						</strong> <br/>
						Patients Off-Target: <strong>
						  <xsl:value-of select="report/patientOffTargetCount"/>
						</strong><br/>
                    </td>
                  </tr>

                  <tr valign="top">
                    <td>
                      <table id="patientTable">
                        <tr bgcolor="#cccccc">
                          <th> Patients </th>
                          <th>Support</th>
                          <th>Readings</th>
                        </tr>
                        <xsl:for-each select="report/patients">
                          <tr valign="top">
                            <td>
                              <div class="partyName">
                                <xsl:value-of select="patient/name"/>
                              </div>
                              <xsl:if test="patient/cellPhone!=''">
                                <xsl:variable name="pCellPhone" select="patient/cellPhone"/>
                                M: <a href="tel:{$pCellPhone}">
                                  <xsl:value-of select="patient/cellPhone"/>
                                </a><br/>
                              </xsl:if>
                              <xsl:if test="patient/homePhone!=''">
                                <xsl:variable name="pHomePhone" select="patient/homePhone"/>
                                H: <a href="tel:{$pHomePhone}">
                                  <xsl:value-of select="patient/homePhone"/>
                                </a><br/>
                              </xsl:if>
                              <xsl:if test="patient/workPhone!=''">
                                <xsl:variable name="pWorkPhone" select="patient/workPhone"/>
                                W: <a href="tel:{$pWorkPhone}">
                                  <xsl:value-of select="patient/workPhone"/>
                                </a><br/>
                              </xsl:if>

                              <xsl:if test="patient/email!=''">
                                <xsl:variable name="pEmail" select="patient/email"/>
                                <a href="mailto:{$pEmail}">
                                  Send Email
                                </a>
                              </xsl:if>
                            </td>
                            <td>
                              <xsl:for-each select="support">
                                <div class="partyName">
                                  <xsl:value-of select="name"/>
                                </div>
                                <xsl:value-of select="partyType"/>
                                <br/>
                                <xsl:if test="cellPhone!=''">
                                  <xsl:variable name="sCellPhone" select="cellPhone"/>
                                  M: <a href="tel:{$sCellPhone}">
                                    <xsl:value-of select="cellPhone"/>
                                  </a><br/>
                                </xsl:if>                                
								<xsl:if test="homePhone!='' and partyType!='Doctor' and partyType!='Nurse' ">
                                  <xsl:variable name="sHomePhone" select="homePhone"/>
                                  H: <a href="tel:{$sHomePhone}">
                                    <xsl:value-of select="homePhone"/>
                                  </a><br/>
                                </xsl:if>
                                <xsl:if test="workPhone!=''">
                                  <xsl:variable name="sWorkPhone" select="workPhone"/>
                                  W: <a href="tel:{$sWorkPhone}">
                                    <xsl:value-of select="workPhone"/>
                                  </a><br/>
                                </xsl:if>
                                <xsl:if test="email!=''">
                                  <xsl:variable name="sEmail" select="email"/>
                                  <a href="mailto:{$sEmail}">
                                    Send Email
                                  </a>
                                </xsl:if>
                                <br/>
                                <br/>
                              </xsl:for-each>
                            </td>
                            <td>
							   <xsl:choose>
								 <xsl:when test="systolicReading='No Reading'"> 
									<div>
										<span class="deviceName">BP:</span>
										<span class="NoReading">No Reading</span> 
									</div>
								 </xsl:when>
								 <xsl:when test="systolicReading!=''"> 
									<xsl:variable name="bpAlertVar">
									  <xsl:if test="bpAlert='*'">
										<xsl:value-of select="'readingAlert'"/>
									  </xsl:if>
									</xsl:variable>
									<div class="{$bpAlertVar}">
									  <span class="deviceName">BP:</span>
									  <xsl:value-of select="systolicReading"/> / <xsl:value-of select="diastolicReading"/> mmHg
									</div>
								 </xsl:when>
							   </xsl:choose>


							   <xsl:choose>
								 <xsl:when test="pulseRateReading='No Reading'"> 
								   <div><span class="deviceName">Pulse:</span>
								   <span class="NoReading">No Reading</span></div>
								 </xsl:when>
								 <xsl:when test="pulseRateReading!=''"> 
									<xsl:variable name="pulseAlertVar">
									  <xsl:if test="pulseAlert='*'">
										<xsl:value-of select="'readingAlert'"/>
									  </xsl:if>
									</xsl:variable>
									<div class="{$pulseAlertVar}">
									  <span class="deviceName">Pulse:</span>
									  <xsl:value-of select="pulseRateReading"/> ppm
									</div>
								</xsl:when>
							   </xsl:choose>
							   
							   <xsl:choose>
								 <xsl:when test="oxygenReading='No Reading'"> 
								   <div><span class="deviceName">Oxygen:</span>
								   <span class="NoReading">No Reading</span></div>
								 </xsl:when>
								 <xsl:when test="oxygenReading!=''"> 
									<xsl:variable name="oxygenAlertVar">
									  <xsl:if test="oxygenAlert='*'">
										<xsl:value-of select="'readingAlert'"/>
									  </xsl:if>
									</xsl:variable>
									<div class="{$oxygenAlertVar}">
									  <span class="deviceName">Oxygen:</span>
									  <xsl:value-of select="oxygenReading"/> %
									</div>
								</xsl:when>
							   </xsl:choose>
							   

							   <xsl:choose>
								 <xsl:when test="temperatureReading='No Reading'"> 
								   <div><span class="deviceName">Temp:</span>
								   <span class="NoReading">No Reading</span></div>
								 </xsl:when>
								 <xsl:when test="temperatureReading!=''"> 
									<xsl:variable name="temperatureAlertVar">
									  <xsl:if test="temperatureAlert='*'">
										<xsl:value-of select="'readingAlert'"/>
									  </xsl:if>
									</xsl:variable>
									<div class="{$temperatureAlertVar}">
									  <span class="deviceName">Temp:</span>
									  <xsl:value-of select="temperatureReading"/> &#176; F
									</div>
								</xsl:when>
							   </xsl:choose>
							   
							   <xsl:choose>
								 <xsl:when test="weightReading='No Reading'"> 
								   <div><span class="deviceName">Weight:</span>
								   <span class="NoReading">No Reading</span></div>
								 </xsl:when>
								 <xsl:when test="weightReading!=''"> 
									<xsl:variable name="weightAlertVar">
									  <xsl:if test="weightAlert='*'">
										<xsl:value-of select="'readingAlert'"/>
									  </xsl:if>
									</xsl:variable>
									<div class="{$weightAlertVar}">
									  <span class="deviceName">Weight:</span>
									  <xsl:value-of select="weightReading"/> lbs
									</div>
								</xsl:when>
							   </xsl:choose>
							   
							   <xsl:choose>
								 <xsl:when test="glucoseReading='No Reading'"> 
								   <div><span class="deviceName">Glucose:</span>
								   <span class="NoReading">No Reading</span></div>
								 </xsl:when>
								 <xsl:when test="glucoseReading!=''"> 
									<xsl:variable name="glucoseReadingAlertVar">
									  <xsl:if test="glucoseReadingAlert='*'">
										<xsl:value-of select="'readingAlert'"/>
									  </xsl:if>
									</xsl:variable>
									<div class="{$glucoseReadingAlertVar}">
									  <span class="deviceName">Glucose:</span>
									  <xsl:value-of select="glucoseReading"/>
									</div>
								</xsl:when>
							   </xsl:choose>
							   
							   <xsl:choose>
								 <xsl:when test="spiroReading='No Reading'"> 
								   <div><span class="deviceName">Spirometer:</span>
								   <span class="NoReading">No Reading</span></div>
								 </xsl:when>
								 <xsl:when test="spiroReading!=''"> 
									<xsl:variable name="spirometerReadingAlertVar">
									  <xsl:if test="spiroAlert='*'">
										<xsl:value-of select="'readingAlert'"/>
									  </xsl:if>
									</xsl:variable>
									<div class="{$spirometerReadingAlertVar}">
									  <span class="deviceName">Spirometer:</span>
									  <xsl:value-of select="spiroReading"/>
									</div>
								</xsl:when>
							   </xsl:choose>

							   <xsl:choose>
								 <xsl:when test="painReading='No Reading'"> 
								   <div><span class="deviceName">Pain:</span>
								   <span class="NoReading">No Reading</span></div>
								 </xsl:when>
								 <xsl:when test="painReading!=''"> 
									<xsl:variable name="painAlertVar">
									  <xsl:if test="painAlert='*'">
										<xsl:value-of select="'readingAlert'"/>
									  </xsl:if>
									</xsl:variable>
									<div class="{$painAlertVar}">
									  <span class="deviceName">Pain:</span>
									  <xsl:value-of select="painDesc"/>
									</div>
								</xsl:when>
							   </xsl:choose>

							   <xsl:choose>
								 <xsl:when test="moodReading='No Reading'"> 
								   <div><span class="deviceName">Mood:</span>
								   <span class="NoReading">No Reading</span></div>
								 </xsl:when>
								 <xsl:when test="moodReading!=''"> 
									<xsl:variable name="moodAlertVar">
									  <xsl:if test="moodAlert='*'">
										<xsl:value-of select="'readingAlert'"/>
									  </xsl:if>
									</xsl:variable>
									<div class="{$moodAlertVar}">
									  <span class="deviceName">Mood:</span>
									  <xsl:value-of select="moodDesc"/>
									</div>
								</xsl:when>
							   </xsl:choose>
							   
							   <xsl:choose>
								 <xsl:when test="energyReading='No Reading'"> 
								   <div><span class="deviceName">Energy:</span>
								   <span class="NoReading">No Reading</span></div>
								 </xsl:when>
								 <xsl:when test="energyReading!=''"> 
									<xsl:variable name="energyAlertVar">
									  <xsl:if test="energyAlert='*'">
										<xsl:value-of select="'readingAlert'"/>
									  </xsl:if>
									</xsl:variable>
									<div class="{$energyAlertVar}">
									  <span class="deviceName">Energy:</span>
									  <xsl:value-of select="energyDesc"/>
									</div>
								</xsl:when>
							   </xsl:choose>
							   
							   <xsl:choose>
								 <xsl:when test="breathingReading='No Reading'"> 
								   <div><span class="deviceName">Breathing:</span>
								   <span class="NoReading">No Reading</span></div>
								 </xsl:when>
								 <xsl:when test="breathingReading!=''"> 
									<xsl:variable name="breathingAlertVar">
									  <xsl:if test="breathingAlert='*'">
										<xsl:value-of select="'readingAlert'"/>
									  </xsl:if>
									</xsl:variable>
									<div class="{$breathingAlertVar}">
									  <span class="deviceName">Breathing:</span>
									  <xsl:value-of select="breathingDesc"/>
									</div>
								</xsl:when>
							   </xsl:choose>
							</td>
                          </tr>
                        </xsl:for-each>
                      </table>
                    </td>
                  </tr>
				  <xsl:if test="report/isdailyreport='Y'">
                  <tr>
                    <td>
                      <xsl:variable name="doctorEmail" select="report/email"/>
                      <a href="https://portal.myvitalz.com/Home/ManageSubscription?subscribe=true&amp;email={$doctorEmail}" target="_blank" class="link">Click here to unsubscribe</a>
                    </td>
                  </tr>
				  </xsl:if>
                </table>

				</p>
              </div>
            </td>
          </tr>
        </table>
        <table style="margin: 0px; padding: 0px; clear: both !important" width="100%">
          <tr>
            <td style="display: block !important; max-width: 600px !important; margin: 0 auto !important; clear: both !important;">
              <div style="padding: 15px; max-width: 600px; margin: 0 auto; display: block; border-top: 1px solid #CCCCCC;">
                <table>
                  <tr>
                    <td align="left">
                      <p style="font-weight: 200; font-size: 12px; color: #000000; font-family: Helvetica, Arial, Lucida Grande, sans-serif; line-height: 1.1; margin-bottom:15px;">
                        Copyright 2014 Myvitalz LLC, All rights reserved. <br/>
                        <a href="http://www.myvitalz.com">http://www.myvitalz.com</a>
                      </p>
                    </td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>