﻿// First initialized load Function
var patientdetails = [];
var weekview = true;
var viewdate = '';
var patientid = '';
var allpatients = [];

$(document).ready(function () {

    DateIntialize();

    $("#btnSaveReadmission").on('click', function () {
        SavedReadmission();
    });
    $("#editpatient").on('click', function () {
        localStorage.setItem("BreadCrumbs", "PatientReading");
    });
    $("#btnSaveLogcareTime").on('click', function () {
        SavedLogcareTime();
    });
    $("#btnPauseMoniSave").on('click', function () {
        if ($('#btnPauseMoniSave').val() === 'Save') {
            SavedPauseMonitor();
        }
        else {
            //alert('Updated');
            //alert($('#hdnpausemonitorid').val());
            UpdatePauseMonitor();
        }
    });
    $("#btnPauseMoniCancel").on('click', function () {
        $('#dialogPausemonitor').dialog('close');
    });
    $("#btnReadmissionCancel").on('click', function () {
        $('#dialogReadmission').dialog('close');
    });
    $("#btnLogcaredateCancel").on('click', function () {
        $('#dialogLogcaretime').dialog('close');
    });
    $(".dropdown dt a").on('click', function () {
        $(".dropdown dd ul").slideToggle('fast');
    });

    $(".dropdown dd ul li a").on('click', function () {
        $(".dropdown dd ul").hide();
    });

    $(".add-on").on('click', function () {
        $(".bootstrap-datetimepicker-widget").show();
    });


    $("#btnlogcaretime").on('click', function () {

        $('#txtlogcareDate').val('');
        $('#txtlogcareCallTime').val('');
        $('#txtlogcareDuration').val('');
        $('#ddlIntervention').val('0');
        $('#txtLogCareNotes').val('');

        $('#Calltimepicker').datetimepicker({
            format: 'hh:mm',
            pickDate: false,
            pickSeconds: false,
            language: 'pt-BR'
        });
        $(".bootstrap-datetimepicker-widget").hide();
        $('#dialogLogcaretime').dialog({
            title: 'Log Care Time',
            width: 750,
            height: 400,
            modal: true
        });

    });

    $("#btnpausemonitor").on('click', function () {
        $('#btnPauseMoniSave').val('Save');
        $('#txtpausemonitorstartdate').val('');
        $('#txtpausemonitorenddate').val('');
        $('#txtPausemonDesc').val('');
        $('#dialogPausemonitor').dialog({
            title: 'Pause Monitor',
            width: 750,
            height: 400,
            modal: true
        });
    });

    $("#btnreadmission").on('click', function () {
        $('#txtadmissiondate').val('');
        $('#dialogReadmission').dialog({
            title: 'Readmission',
            width: 400,
            height: 250,
            modal: true
        });
    });

    $(document).bind('click', function (e) {
        var $clicked = $(e.target);
        if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
    });
    $("#chkall").prop("checked", true);
    var checkbox = document.getElementsByName('devicesfilter');
    for (var i = 0, n = checkbox.length; i < n; i++) {
        checkbox[i].checked = true;
    }
    InitialLoadReading();
});

function InitialLoadReading() {
    var nowdate = new Date();
    var monthStartDay = new Date(nowdate.getFullYear(), nowdate.getMonth(), 1);
    var monthEndDay = new Date(nowdate.getFullYear(), nowdate.getMonth() + 1, 0);
    var fromdate = moment(monthStartDay).format("YYYY-MM-DD");
    var todate = moment(monthEndDay).format("YYYY-MM-DD");
    patientid = $('#lblpatientid').text();
    doctorid = $('#lbldoctorid').text();
    LoadPatienDetails();
    loadReadings(patientid, doctorid, fromdate, todate);
}

//patients Previouse and Next functionality 
/*
const getNextIdx = (idx = 0, length, direction) => {
    switch (direction) {
        case 'next': return (idx + 1) % length;
        case 'prev': return (idx == 0) && length - 1 || idx - 1;
        default: return idx;
    }
}
let idx = 0; // idx is undefined, so getNextIdx will take 0 as default
const getNewIndexAndRender = (direction) => {
    idx = getNextIdx(idx, allpatients.length, direction);
    window.location = "PatientReading?patientId=" + allpatients[idx].PartyId;
}
*/


var idx = 0; // idx is undefined, so getNextIdx will take 0 as default
function getNextIdx(idx, length, direction){
    switch (direction) {
        case 'next': return (idx + 1) % length;
        case 'prev': return (idx == 0) && length - 1 || idx - 1;
        default: return idx;
    }
}

function getNewIndexAndRender(direction) {
    idx = getNextIdx(idx, allpatients.length, direction);
    window.location = "PatientReading?patientId=" + allpatients[idx].PartyId;
}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
function LoadPatienDetails() {

    $.ajax({
        type: "POST",
        url: "CalendarPatientDetails",
        dataType: "json",
        "data": {},
        success: function (data) {

            if (data !== null) {
                if (data.length > 1) {
                    allpatients = [];
                    var selectpartyid = getUrlVars()["patientId"];
                    $.each(data, function (index, result) {
                        allpatients.push({
                            PartyId: result.patient.partyId,
                            PatientName: result.patient.name
                        });
                        if (result.patient.partyId === selectpartyid) {
                            idx = index;
                            patientname.innerHTML = result.patient.name;
                            if (index === 0) {
                                $('#btnprev').prop('disabled', true);
                            }
                            if (index === data.length - 1) {
                                $('#btnnext').prop('disabled', true);
                            }
                        }
                    })
                }
                else {
                    $('#divpatients').hide();
                }
            }
            else {
                $('#divpatients').hide();
            }
        },
        "complete": function (data) {
        },
        error: function (request, textStatus, errorThrown) {
        }
    });
}

//Update PauseMonitoring data

function UpdatePauseMonitor() {
    var pausemonitorStartDate = $('#txtpausemonitorstartdate').val();
    var pausemonitorEndDate = $('#txtpausemonitorenddate').val();
    var pausemonitorDescription = $('#txtPausemonDesc').val();
    var rowId = $('#hdnpausemonitorid').val();

    var start = new Date($('#txtpausemonitorstartdate').val()), end = new Date($('#txtpausemonitorenddate').val());
    if (start > end) {
        ShowMessage("error", "Start date should be lesser than End Date");
        ClosePopup();
        return false;
    }
    else if (pausemonitorStartDate === '') {
        ShowMessage("error", "Please enter Start Date");
        ClosePopup();
        return false;
    }
    else if (pausemonitorEndDate === '') {
        ShowMessage("error", "Please enter End Date");
        ClosePopup();
        return false;
    }
    else {
        patient = $('#lblpatientid').text();
        var parameters = {
            "id": rowId,
            "partyid": patient,
            "startDate": moment(pausemonitorStartDate).format("YYYY-MM-DD"),
            "endDate": moment(pausemonitorEndDate).format("YYYY-MM-DD"),
            "reason": $('#ddlpausemonitorreason').val(),
            "description": pausemonitorDescription
        };
        $.ajax({
            type: "POST",
            url: "UpdatePauseMonitor",
            dataType: "html",
            data: parameters,
            success: function (data) {

                var result = jQuery.parseJSON(data);
                if (result.status === 'Ok') {
                    ShowMessage("success", "Pause Monitor successfully updated.");
                    $('#dialogPausemonitor').dialog('close');
                    ClosePopup();
                    var start_date = $('#calendar').fullCalendar('getView').start;
                    var end_date = $('#calendar').fullCalendar('getView').end;
                    var getdatecalender = $('#calendar').fullCalendar('getDate');
                    viewdate = getdatecalender.format();
                    if (weekview === true) {
                        start_date = $('#calendar').fullCalendar('getView').start;
                        end_date = $('#calendar').fullCalendar('getView').end;
                        loadReadings(patientid, doctorid, moment(start_date.format()).format("YYYY-MM-DD"), moment(end_date.format()).format("YYYY-MM-DD"));
                    }
                    RefreshReading();
                }
                else {
                    ShowMessage("error", result.status);
                    ClosePopup();
                }
            },
            error: function (request, textStatus, errorThrown) {
                ShowCallbackError(request, textStatus, errorThrown);
            }
        });
    }
}


function AfterDeleteloadCalendarReading() {
    alert('fdfhd');
    var start_date = $('#calendar').fullCalendar('getView').start;
    var end_date = $('#calendar').fullCalendar('getView').end;
    var getdatecalender = $('#calendar').fullCalendar('getDate');
    viewdate = getdatecalender.format();
    if (weekview === true) {
        start_date = $('#calendar').fullCalendar('getView').start;
        end_date = $('#calendar').fullCalendar('getView').end;
        loadReadings(patientid, doctorid, moment(start_date.format()).format("YYYY-MM-DD"), moment(end_date.format()).format("YYYY-MM-DD"));
    }
}

//Saved Logcare Time
function SavedLogcareTime() {
    var logcareDate = $('#txtlogcareDate').val();
    if ($('#txtlogcareDate').val() === '') {
        ShowMessage("error", "Please enter Log Care Date");
        ClosePopup();
        return false;
    }
    else if ($('#txtlogcareCallTime').val() === '') {
        ShowMessage("error", "Please enter Call Time");
        ClosePopup();
        return false;
    }
    else if ($('#txtlogcareDuration').val() === '') {
        ShowMessage("error", "Please enter Duration");
        ClosePopup();
        return false;
    }
    else if ($('#ddlIntervention').val() === '0') {
        ShowMessage("error", "select Intervention");
        ClosePopup();
        return false;
    }
    else {
        patient = $('#lblpatientid').text();
        var parameters = {
            "partyid": patient,
            "logcareDate": moment(logcareDate).format("YYYY-MM-DD"),
            "Calltime": $('#txtlogcareCallTime').val(),
            "Duration": $('#txtlogcareDuration').val(),
            "Notes": $('#txtLogCareNotes').val(),
            "Intervention": $('#ddlIntervention').val()
        };
        $.ajax({
            type: "POST",
            url: "LogCareSaved",
            dataType: "html",
            data: parameters,
            success: function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status === 'Ok') {
                    ShowMessage("success", "Log Care successfully saved ");
                    $('#dialogLogcaretime').dialog('close');
                    ClosePopup();
                    var start_date = $('#calendar').fullCalendar('getView').start;
                    var end_date = $('#calendar').fullCalendar('getView').end;
                    var getdatecalender = $('#calendar').fullCalendar('getDate');
                    viewdate = getdatecalender.format();
                    if (weekview === true) {
                        start_date = $('#calendar').fullCalendar('getView').start;
                        end_date = $('#calendar').fullCalendar('getView').end;
                        loadReadings(patientid, doctorid, moment(start_date.format()).format("YYYY-MM-DD"), moment(end_date.format()).format("YYYY-MM-DD"));
                    }
                }
                else {
                    ShowMessage("error", result.status);
                    ClosePopup();
                }
            },
            error: function (request, textStatus, errorThrown) {
                ShowCallbackError(request, textStatus, errorThrown);
            }
        });
    }
}


function SavedPauseMonitor() {

    var pausemonitorStartDate = $('#txtpausemonitorstartdate').val();
    var pausemonitorEndDate = $('#txtpausemonitorenddate').val();
    var pausemonitorDescription = $('#txtPausemonDesc').val();

    var start = new Date($('#txtpausemonitorstartdate').val()), end = new Date($('#txtpausemonitorenddate').val());
    if (start > end) {
        ShowMessage("error", "Start date should be lesser than End Date");
        ClosePopup();
        return false;
    }

    else if (pausemonitorStartDate === '') {
        ShowMessage("error", "Please enter Start Date");
        ClosePopup();
        return false;
    }
    else if (pausemonitorEndDate === '') {
        ShowMessage("error", "Please enter End Date");
        ClosePopup();
        return false;
    }


    else {

        patient = $('#lblpatientid').text();
        var parameters = {
            "partyid": patient,
            "startDate": moment(pausemonitorStartDate).format("YYYY-MM-DD"),
            "endDate": moment(pausemonitorEndDate).format("YYYY-MM-DD"),
            "reason": $('#ddlpausemonitorreason').val(),
            "description": pausemonitorDescription
        };
        $.ajax({
            type: "POST",
            url: "PauseMonitorSaved",
            dataType: "html",
            data: parameters,
            success: function (data) {

                var result = jQuery.parseJSON(data);
                if (result.status === 'Ok') {
                    ShowMessage("success", "Pause Monitor successfully saved");
                    $('#dialogPausemonitor').dialog('close');
                    ClosePopup();
                    var start_date = $('#calendar').fullCalendar('getView').start;
                    var end_date = $('#calendar').fullCalendar('getView').end;
                    var getdatecalender = $('#calendar').fullCalendar('getDate');
                    viewdate = getdatecalender.format();
                    if (weekview === true) {
                        start_date = $('#calendar').fullCalendar('getView').start;
                        end_date = $('#calendar').fullCalendar('getView').end;
                        loadReadings(patientid, doctorid, moment(start_date.format()).format("YYYY-MM-DD"), moment(end_date.format()).format("YYYY-MM-DD"));
                    }
                }
                else {
                    ShowMessage("error", result.status);
                    ClosePopup();
                }
            },
            error: function (request, textStatus, errorThrown) {
                ShowCallbackError(request, textStatus, errorThrown);
            }
        });
    }
}

function SavedReadmission() {
    var readmissiondate = $('#txtadmissiondate').val();
    if ($('#txtadmissiondate').val() === '') {
        ShowMessage("error", "Please enter Readmission Date");
        ClosePopup();
        return false;
    }
    else {

        patient = $('#lblpatientid').text();

        var parameters = { "partyid": patient, "ReadmissionDate": moment(readmissiondate).format("YYYY-MM-DD") };
        $.ajax({
            type: "POST",
            url: "ReadmissionSaved",
            dataType: "html",
            data: parameters,
            success: function (data) {

                var result = jQuery.parseJSON(data);

                if (result.status === 'Ok') {
                    ShowMessage("success", "Readmission successfully saved ");

                    ClosePopup();
                    $('#dialogReadmission').dialog('close');

                    var start_date = $('#calendar').fullCalendar('getView').start;
                    var end_date = $('#calendar').fullCalendar('getView').end;
                    var getdatecalender = $('#calendar').fullCalendar('getDate');
                    viewdate = getdatecalender.format();
                    if (weekview === true) {
                        start_date = $('#calendar').fullCalendar('getView').start;
                        end_date = $('#calendar').fullCalendar('getView').end;
                        loadReadings(patientid, doctorid, moment(start_date.format()).format("YYYY-MM-DD"), moment(end_date.format()).format("YYYY-MM-DD"));
                    }
                }
                else {
                    ShowMessage("error", result.status);
                    ClosePopup();
                }
            },
            error: function (request, textStatus, errorThrown) {
                ShowCallbackError(request, textStatus, errorThrown);
            }
        });
    }
}

function loadReadings(patientId, doctorId, fromdate, todate) {
    var arr = new Array();
    var arrItems = [];
    patientdetails = [];
    var parameters = { "patientId": patientId, "doctorId": doctorId, "fromDate": fromdate, "toDate": todate, "page": 1, "pageSize": 20 };
    $.ajax({
        type: "POST",
        url: "PatientReadingDetails",
        dataType: "html",
        data: parameters,
        success: function (data) {
            var result = jQuery.parseJSON(data);

            //BloodGlucose                                  
            $.each(result.BCM, function (key, obj) {
                var BCMstartdatetime = '';
                var BCMendtime = '';
                var BCMreadingvalue = '';
                var BCMClass = 'BloodGlucose';
                var Thres = 'deviceright';
                var hidden = false;
                $.each(obj, function (k, value) {
                    if (k === 'measurementTime') {
                        BCMstartdatetime = moment(value).format("YYYY-MM-DDTHH:mm:ss");
                        var dt = new Date(BCMstartdatetime);
                        dt.setMinutes(dt.getMinutes() + 30);
                        BCMendtime = moment(dt).format("YYYY-MM-DDTHH:mm:ss");
                    }
                    else if (k === 'glucoseReading') {
                        BCMreadingvalue = value;
                    }
                    else if (k === 'aboveThreshold') {
                        if (value === true) {
                            BCMClass = 'ThresBloodGlucose';
                            Thres = 'devicerightThres';
                        }
                    }
                    else if (k === 'hidden') {
                        if (value === true) {
                            hidden = true;
                        }
                    }
                });
                var data = { title: '<span class=deviceleft>Glucose</span><span class=' + Thres + '>' + BCMreadingvalue + ' mg/dL </span>', start: BCMstartdatetime, end: BCMendtime, className: BCMClass, type: 'Blood Glucose', reading: BCMreadingvalue + ' mg/dL', difference: '', level: '', status: '' };
                if (hidden === false) {
                    patientdetails.push(data);
                }
            });

            // Weighing
            $.each(result.WSM, function (key, obj) {
                var WSMstartdatetime = '';
                var WSMendtime = '';
                var WSMreadingvalue = '';
                var WSMdifference = '';
                var WSMClass = 'Weight';
                var WSMdiffclass = '';
                var WSMLevel;
                var Thres = 'deviceright';
                var hidden = false;
                $.each(obj, function (k, value) {
                    if (k === 'measurementTime') {
                        WSMstartdatetime = moment(value).format("YYYY-MM-DDTHH:mm:ss");
                        var dt = new Date(WSMstartdatetime);
                        dt.setMinutes(dt.getMinutes() + 30);
                        WSMendtime = moment(dt).format("YYYY-MM-DDTHH:mm:ss");
                    }
                    else if (k === 'weightReadingPound') {
                        WSMreadingvalue = value;
                    }
                    else if (k === 'weightDifference') {

                        if (value > 0) {
                            WSMdifference = '+' + value;
                            WSMdiffclass = '<span class=devicerightThres><i class="fa fa-arrow-up" style="font-size:9px;color: red;" ></i> ' + WSMdifference + ' lb</span>';
                            WSMLevel = 'high';
                        }
                        else if (value < 0) {
                            WSMdifference = value;
                            WSMdiffclass = '<span class=devicerightThres><i class="fa fa-arrow-down" style="font-size:9px;color:red;" ></i> ' + WSMdifference + ' lb </span>';
                            WSMLevel = 'low';
                        }
                        else {
                            WSMdiffclass = '';
                            WSMdifference = value;
                            WSMLevel = 'equal';
                        }
                    }
                    else if (k === 'aboveThreshold') {
                        if (value === true) {
                            WSMClass = 'ThresWeight';
                            Thres = 'devicerightThres';
                        }
                    }
                    else if (k === 'hidden') {
                        if (value === true) {
                            hidden = true;
                        }
                    }
                });
                var data = {
                    title: '<span class=deviceleft>Weight</span>' + WSMdiffclass + ' <span style="float: right;padding: 0px 10px;">' + WSMreadingvalue + '</span>', start: WSMstartdatetime, end: WSMendtime, className: WSMClass, type: 'Weight', reading: WSMreadingvalue + ' lbs', difference: WSMdifference + ' lbs', level: WSMLevel, status: ''
                };
                if (hidden === false) {
                    patientdetails.push(data);
                }
            });

            // Spirometer
            $.each(result.SPR, function (key, obj) {
                var SPRstartdatetime = '';
                var SPRendtime = '';
                var SPRpefBest = '';
                var SPRfev1Best = '';
                var SPRClass = 'Spirometer';

                var SPRpefReadingDiff = '';
                var SPRfevReadingDiff = '';
                var Thres = 'deviceright';
                var hidden = false;
                $.each(obj, function (k, value) {
                    if (k === 'measurementTime') {
                        SPRstartdatetime = moment(value).format("YYYY-MM-DDTHH:mm:ss");
                        var dt = new Date(SPRstartdatetime);
                        dt.setMinutes(dt.getMinutes() + 30);
                        SPRendtime = moment(dt).format("YYYY-MM-DDTHH:mm:ss");
                    }
                    else if (k === 'pefBest') {
                        SPRpefBest = value;
                    }
                    else if (k === 'fev1Best') {
                        SPRfev1Best = value;
                    }

                    else if (k === 'pefReadingDiff') {
                        SPRpefReadingDiff = value;
                    }
                    else if (k === 'fevReadingDiff') {
                        SPRfevReadingDiff = value;
                    }

                    else if (k === 'aboveThreshold') {
                        if (value === true) {
                            SPRClass = 'ThresSpirometer';
                            Thres = 'devicerightThres';
                        }
                    }
                    else if (k === 'hidden') {
                        if (value === true) {
                            hidden = true;
                        }
                    }
                });
                var data = {
                    title: '<span class=deviceleft>FEV/PEF</span><span class=' + Thres + '>' + SPRfev1Best + '/' + SPRpefBest + ' mlps</span>', start: SPRstartdatetime, end: SPRendtime, className: SPRClass, type: 'Spirometer', reading: SPRfev1Best + ' / ' + SPRpefBest + ' mlps', difference: SPRfevReadingDiff + ' / ' + SPRpefReadingDiff + ' mlps', level: '', status: ''
                };
                if (hidden === false) {
                    patientdetails.push(data);
                }
            });

            // Temperature
            $.each(result.TMM, function (key, obj) {
                var TMMstartdatetime = '';
                var TMMendtime = '';
                var TMMtemperatureFahrenheit = '';
                var TMMClass = 'Temperature';
                var Thres = 'deviceright';
                var hidden = false;
                $.each(obj, function (k, value) {
                    if (k === 'measurementTime') {
                        TMMstartdatetime = moment(value).format("YYYY-MM-DDTHH:mm:ss");
                        var dt = new Date(TMMstartdatetime);
                        dt.setMinutes(dt.getMinutes() + 30);
                        TMMendtime = moment(dt).format("YYYY-MM-DDTHH:mm:ss");
                    }
                    else if (k === 'temperatureFahrenheit') {
                        TMMtemperatureFahrenheit = value;
                    }
                    else if (k === 'aboveThreshold') {
                        if (value === true) {
                            TMMClass = 'ThresTemperature';
                            Thres = 'devicerightThres';
                        }
                    }
                    else if (k === 'hidden') {
                        if (value === true) {
                            hidden = true;
                        }
                    }
                });
                var data = {
                    title: '<span class=deviceleft>Temp</span><span class=' + Thres + '>' + TMMtemperatureFahrenheit + ' F</span>', start: TMMstartdatetime, end: TMMendtime, className: TMMClass, type: 'Temperature', reading: TMMtemperatureFahrenheit + ' F', difference: '', level: '', status: ''
                };
                if (hidden === false) {
                    patientdetails.push(data);
                }
            });

            // Blood Pressure
            $.each(result.BPM, function (key, obj) {
                var BPMstartdatetime = '';
                var BPMendtime = '';
                var BPMsystolic = '';
                var BPMdiastolic = '';
                var BPMpulseRate = '';
                var BPMClass = 'BloodPressure';
                var Thres = 'deviceright';
                var systolicThreshold = 'systolicThreshold';
                var diastolicThreshold = 'diastolicThreshold';
                var pulseRateThreshold = 'pulseRateThreshold';
                var hidden = false;
                $.each(obj, function (k, value) {
                    if (k === 'measurementTime') {
                        BPMstartdatetime = moment(value).format("YYYY-MM-DDTHH:mm:ss");
                        var dt = new Date(BPMstartdatetime);
                        dt.setMinutes(dt.getMinutes() + 30);
                        BPMendtime = moment(dt).format("YYYY-MM-DDTHH:mm:ss");
                    }
                    else if (k === 'diastolic') {
                        BPMdiastolic = value;
                    }
                    else if (k === 'systolic') {
                        BPMsystolic = value;
                    }
                    else if (k === 'pulseRate') {
                        if (value !== null) {
                            BPMpulseRate = value;
                        }
                    } else if (k === 'systolicAboveThreshold') {
                        if (value === true) {
                            BPMClass = 'ThresBloodPressure';
                            Thres = 'devicerightThresAll';
                            systolicThreshold = "systolicThreshold_after";
                        }
                    } else if (k === 'diastolicAboveThreshold') {
                        if (value === true) {
                            BPMClass = 'ThresBloodPressure';
                            Thres = 'devicerightThresAll';
                            diastolicThreshold = "diastolicThreshold_after";
                        }
                    } else if (k === 'pulseRateAboveThreshold') {
                        if (value === true) {
                            BPMClass = 'ThresBloodPressure';
                            Thres = 'devicerightThresAll';
                            pulseRateThreshold = "pulseRateThreshold_after";
                        }
                    }
                    else if (k === 'hidden') {
                        if (value === true) {
                            hidden = true;
                        }
                    }
                    if (diastolicThreshold === 'diastolicThreshold_after' && pulseRateThreshold === 'pulseRateThreshold_after' && systolicThreshold === 'systolicThreshold_after') {
                        Thres = 'devicerightThres';
                    }
                    else {
                        Thres = 'devicerightThresAll';
                    }




                });
                var data = {
                    title: '<span class=deviceleft>BP</span><span class=' + Thres + '><font class=' + systolicThreshold + '>' + BPMsystolic + '</font> / <font class=' + diastolicThreshold + '> ' + BPMdiastolic + '</font> / <font class=' + pulseRateThreshold + '> ' + BPMpulseRate + '</font></span>', start: BPMstartdatetime, end: BPMendtime, className: BPMClass, type: 'Blood Pressure', reading: BPMsystolic + ' / ' + BPMdiastolic + ' / ' + BPMpulseRate, difference: '', level: '', status: ''
                };

                if (hidden === false) {
                    patientdetails.push(data);
                }

                // PulseRate Include
                // if(BPMpulseRate!='')
                //    {
                //       var data = { title: 'Pulse Rate - '+BPMpulseRate+' ppm', start: BPMstartdatetime,end:BPMendtime,className: 'PulseRate',type:'Pulse Rate',patientId:'cda56c2dc4e17a7d98adb04923cec360',reading:BPMpulseRate+' ppm',difference:'', level:'',status:''  };
                //       patientdetails.push(data);
                //        }



            });

            //POM is PulseOximeter - Oxygen
            $.each(result.POM, function (key, obj) {
                var POMstartdatetime = '';
                var POMendtime = '';
                var POMoxygenReading = '';
                var POMheartRate = '';
                var POMClass = 'Oxygen';
                var Thres = 'deviceright';
                var hidden = false;
                $.each(obj, function (k, value) {
                    if (k === 'measurementTime') {
                        POMstartdatetime = moment(value).format("YYYY-MM-DDTHH:mm:ss");
                        var dt = new Date(POMstartdatetime);
                        dt.setMinutes(dt.getMinutes() + 30);
                        POMendtime = moment(dt).format("YYYY-MM-DDTHH:mm:ss");
                    }
                    else if (k === 'oxygenReading') {
                        POMoxygenReading = value;
                    }
                    else if (k === 'heartRate') {
                        if (value !== null) {
                            POMheartRate = '<span class=HeartRate><i class="fa fa-heart" style="font-size:9px;color: red;" ></i> ' + value + ' ppm </span>';
                        }
                    }
                    else if (k === 'aboveThreshold') {
                        if (value === true) {
                            POMClass = 'ThresOxygen';
                            Thres = 'devicerightThres';
                        }
                    }


                    else if (k === 'hidden') {
                        if (value === true) {
                            hidden = true;
                        }
                    }
                });
                var data = {
                    title: '<span class=deviceleft>SPO2</span><span class=' + Thres + '>' + POMoxygenReading + '% </span>', start: POMstartdatetime, end: POMendtime, className: POMClass, type: 'Pulse/Oxygen Saturation', reading: POMoxygenReading + ' %', difference: '', level: '', status: ''
                };

                if (hidden === false) {
                    patientdetails.push(data);
                }
            });

            // DIA - 	DIA is Diabetes (Different from Blood Glucose)- Diabetes
            $.each(result.DIA, function (key, obj) {
                var DIAstartdatetime = '';
                var DIAendtime = '';
                var DIAdailyQaScore = '';
                var DIAClass = 'Diabetes';
                var DIAdailyQaDesc = '';
                var Thres = 'deviceright';
                var hidden = false;
                $.each(obj, function (k, value) {
                    if (k === 'measurementTime') {
                        DIAstartdatetime = moment(value).format("YYYY-MM-DDTHH:mm:ss");
                        var dt = new Date(DIAstartdatetime);
                        dt.setMinutes(dt.getMinutes() + 30);
                        DIAendtime = moment(dt).format("YYYY-MM-DDTHH:mm:ss");
                    }
                    else if (k === 'dailyQaScore') {
                        DIAdailyQaScore = value + ' - 6';
                    }
                    else if (k === 'dailyQaScoreDesc') {
                        DIAdailyQaDesc = value;
                    }
                    else if (k === 'isAboveThreshold') {
                        if (value === true) {
                            DIAClass = 'ThresDiabetes';
                            Thres = 'devicerightThres';
                        }
                    }
                    else if (k === 'hidden') {
                        if (value === true) {
                            hidden = true;
                        }
                    }


                });
                var data = {
                    title: '<span class=deviceleft>Diabetes</span><span class=' + Thres + '>' + DIAdailyQaScore + '</span>', start: DIAstartdatetime, end: DIAendtime, className: DIAClass, type: 'Diabetes', reading: DIAdailyQaScore, difference: '', level: '', status: DIAdailyQaDesc
                };

                if (hidden === false) {
                    patientdetails.push(data);
                }
            });

            // MIA is Mental Illness
            $.each(result.MIA, function (key, obj) {
                var MIAstartdatetime = '';
                var MIAendtime = '';
                var MIAdailyQaScore = '';
                var MIAClass = 'MentalIllness';
                var MIAdailyQaScoreDesc = '';
                var Thres = 'deviceright';
                var hidden = false;
                $.each(obj, function (k, value) {
                    if (k === 'measurementTime') {
                        MIAstartdatetime = moment(value).format("YYYY-MM-DDTHH:mm:ss");
                        var dt = new Date(MIAstartdatetime);
                        dt.setMinutes(dt.getMinutes() + 30);
                        MIAendtime = moment(dt).format("YYYY-MM-DDTHH:mm:ss");
                    }
                    else if (k === 'dailyQaScore') {
                        MIAdailyQaScore = value + ' - 27';
                    }
                    else if (k === 'dailyQaScoreDesc') {
                        MIAdailyQaScoreDesc = value;
                    }

                    else if (k === 'isAboveThreshold') {
                        if (value === true) {
                            MIAClass = 'ThresMentalIllness';
                            Thres = 'devicerightThres';
                        }
                    }

                    else if (k === 'hidden') {
                        if (value === true) {
                            hidden = true;
                        }
                    }

                });
                var data = {
                    title: '<span class=deviceleft>PHQ9</span><span class=' + Thres + '>' + MIAdailyQaScore + '<span>', start: MIAstartdatetime, end: MIAendtime, className: MIAClass, type: 'Mental Illness', reading: MIAdailyQaScore, difference: '', level: '', status: MIAdailyQaScoreDesc
                };

                if (hidden === false) {
                    patientdetails.push(data);
                }
            });

            //Spirometer
            $.each(result.SPN, function (key, obj) {
                var SPNstartdatetime = '';
                var SPNendtime = '';
                var SPNsnId = '';
                var SPNClass = 'SoapNotes';
                var SPNsnDescription = '';
                var SPNsnSubjective = '';
                var SPNsnObjective = '';
                var SPNsnAssessment = '';
                var SPNsnPlan = '';
                var SPNPatientName = '';
                var hidden = false;
                $.each(obj, function (k, value) {
                    if (k === 'updateTimestamp') {
                        SPNstartdatetime = moment(value).format("YYYY-MM-DDTHH:mm:ss");
                        var dt = new Date(SPNstartdatetime);
                        dt.setMinutes(dt.getMinutes() + 30);
                        SPNendtime = moment(dt).format("YYYY-MM-DDTHH:mm:ss");
                    }
                    else if (k === 'snId') {
                        SPNsnId = value;
                    }

                    else if (k === 'snSubjective') {
                        if (value !== null) {
                            SPNsnSubjective = value;
                        }
                    }
                    else if (k === 'snObjective') {
                        if (value !== null) {
                            SPNsnObjective = value;
                        }
                    }
                    else if (k === 'snAssessment') {
                        if (value !== null) {
                            SPNsnAssessment = value;
                        }
                    }
                    else if (k === 'snPlan') {
                        if (value !== null) {
                            SPNsnPlan = value;
                        }
                    }
                    else if (k === 'patientFullName') {
                        SPNPatientName = value;
                    }

                    else if (k === 'hidden') {
                        if (value === true) {
                            hidden = true;
                        }
                    }

                });
                var data = {
                    title: '<span class=deviceleft>Notes</span><span class=deviceright>' + SPNstartdatetime.substring(11, 16) + '</span>', start: SPNstartdatetime, end: SPNendtime, className: SPNClass, type: 'Soap Notes', reading: '', difference: '', level: '', status: SPNsnSubjective + '@' + SPNsnObjective + '@' + SPNsnAssessment + '@' + SPNsnPlan, ticketid: SPNsnId, patientFullName: SPNPatientName
                };

                if (hidden === false) {
                    patientdetails.push(data);
                }
            });

            //Care Time 
            $.each(result.CRT, function (key, obj) {
                var CRTstartdatetime = '';
                var CRTendtime = '';
                var CRTduration = '';
                var CRTcaredBy = '';
                var CRTbegintime = '';
                var CRTnotes = "";
                var BCMClass = 'LogCareTime';
                var CRTIntrType = '';
                var hidden = false;
                $.each(obj, function (k, value) {
                    if (k === 'careTimeDate') {
                        CRTstartdatetime = moment(value).format("YYYY-MM-DDTHH:mm:ss");

                        var dt = new Date(CRTstartdatetime);
                        dt.setMinutes(dt.getMinutes() + 30);
                        CRTendtime = moment(dt).format("YYYY-MM-DDTHH:mm:ss");
                    }
                    else if (k === 'duration') {
                        CRTduration = value;
                        CRTduration = CRTduration.substr(0, 5);
                    }
                    else if (k === 'beginTime') {
                        CRTbegintime = value;
                    }
                    else if (k === 'caredBy') {
                        CRTcaredBy = value;
                    }
                    else if (k === 'hidden') {
                        if (value === true) {
                            hidden = true;
                        }
                    }
                    else if (k === 'notes') {
                        if (value !== null && value.length > 0) {
                            CRTnotes = value;
                        } else {
                            CRTnotes = 'none';
                        }
                    }
                    else if (k === 'interventionType') {
                        CRTIntrType = value;
                    }
                });
                var data = { title: '<span class=deviceleft>' + CRTIntrType + '</span><span class=deviceright>' + CRTduration + ' min</span>', start: CRTstartdatetime, end: CRTendtime, className: BCMClass, type: 'Care Time', reading: '', difference: '', level: '', status: CRTbegintime + '@' + CRTduration + '@' + CRTcaredBy + '@' + CRTnotes};

                if (hidden === false) {
                    patientdetails.push(data);
                }
            });

            // feeling
            $.each(result.FEL, function (key, obj) {
                var FELstartdatetime = '';
                var FELendtime = '';
                var FELenergyReading = '';
                var FELbreathingReading = '';
                var FELmoodReading = '';
                var FELpainReading = '';

                var FELbreathingDesc = '';
                var FELenergyDesc = '';
                var FELmoodDesc = '';
                var FELpainDesc = '';

                var FELClass = 'Feeling';
                var Thres = '';
                var hidden = false;
                $.each(obj, function (k, value) {
                    if (k === 'measurementTs') {
                        FELstartdatetime = moment(value).format("YYYY-MM-DDTHH:mm:ss");

                        var dt = new Date(FELstartdatetime);
                        dt.setMinutes(dt.getMinutes() + 30);
                        FELendtime = moment(dt).format("YYYY-MM-DDTHH:mm:ss");
                    }
                    else if (k === 'energyReading') {
                        FELenergyReading = value;
                    }
                    else if (k === 'breathingReading') {
                        FELbreathingReading = value;
                    }
                    else if (k === 'moodReading') {
                        FELmoodReading = value;
                    }
                    else if (k === 'painReading') {
                        FELpainReading = value;
                    }


                    else if (k === 'breathingDesc') {
                        if (value !== null) {
                            FELbreathingDesc = value;
                        }
                    }
                    else if (k === 'energyDesc') {
                        if (value !== null) {
                            FELenergyDesc = value;
                        }
                    }
                    else if (k === 'moodDesc') {
                        if (value !== null) {
                            FELmoodDesc = value;
                        }
                    }
                    else if (k === 'painDesc') {
                        if (value !== null) {
                            FELpainDesc = value;
                        }
                    }
                    else if (k === 'hidden') {
                        if (value === true) {
                            hidden = true;
                        }
                    }

                });
                var data = { title: '<span class=deviceleft>Well-Being</span><span class=deviceright>' + FELenergyReading + '-' + FELbreathingReading + '-' + FELmoodReading + '-' + FELpainReading + '</span>', start: FELstartdatetime, end: FELendtime, className: FELClass, type: 'Well-Being', reading: '', difference: '', level: '', status: FELbreathingDesc + '@' + FELenergyDesc + '@' + FELmoodDesc + '@' + FELpainDesc };

                if (hidden === false) {
                    patientdetails.push(data);
                }
            });

            // pause monitoring
            $.each(result.PMN, function (key, obj) {
                var PMNstartdatetime = '';
                var PMNendtime = '';
                var PMNreason = '';
                var PMNenddatetime = '';
                var PMNClass = 'PauseMoniter';
                var PMNEnd = '';
                var hidden = false;
                $.each(obj, function (k, value) {
                    if (k === 'startDate') {
                        PMNstartdatetime = moment(value).format("YYYY-MM-DDTHH:mm:ss");
                    }
                    else if (k === 'endDate') {
                        PMNEnd = moment(value).format("YYYY-MM-DDTHH:mm:ss");
                        var dt = new Date(PMNEnd);
                        dt.setDate(dt.getDate() + 1);
                        PMNenddatetime = moment(dt).format("YYYY-MM-DDTHH:mm:ss");

                    }
                    else if (k === 'reason') {
                        PMNreason = value;
                    }

                    else if (k === 'hidden') {
                        if (value === true) {
                            hidden = true;
                        }
                    }


                });
                var data = { title: '<span class=deviceleft>Monitor (Paused)</span><span class=deviceright>' + PMNreason + '</span>', start: PMNstartdatetime, end: PMNenddatetime, className: PMNClass, type: 'Pause Monitor', reading: '', difference: '', level: '', status: PMNreason };

                if (hidden === false) {
                    patientdetails.push(data);
                }
            });

            //Readmission
            $.each(result.REA, function (key, obj) {
                var REAreadmissionDate = '';
                var REApartyId = '';
                var REAClass = 'Readmission';
                var REAendtime = '';
                var hidden = false;
                $.each(obj, function (k, value) {
                    if (k === 'readmissionDate') {
                        REAreadmissionDate = moment(value).format("YYYY-MM-DDTHH:mm:ss");

                        var dt = new Date(REAreadmissionDate);
                        dt.setMinutes(dt.getMinutes() + 30);
                        REAendtime = moment(dt).format("YYYY-MM-DDTHH:mm:ss");

                    }
                    else if (k === 'partyId') {
                        REApartyId = value;
                    }

                    else if (k === 'hidden') {
                        if (value === true) {
                            hidden = true;
                        }
                    }
                });
                var data = { title: '<span class=deviceleft>Readmission</span>', start: REAreadmissionDate, end: REAendtime, className: REAClass, type: 'Readmission', reading: '', difference: '', level: '', status: '' };

                if (hidden === false) {
                    patientdetails.push(data);
                }
            });

            // medication
            $.each(result.PIL, function (key, obj) {
                var PILstartdatetime = '';
                var PILpartyId = '';
                var PILendtime = '';
                var PILClass = 'Meds';
                var PILTimeOpened = '';
                var PILLabel = 'MED';
                var PILDeviceID = '';
                var hidden = false;
                $.each(obj, function (k, value) {
                    if (k === 'measurementTime') {
                        PILstartdatetime = moment(value).format("YYYY-MM-DDTHH:mm:ss");

                        var dt = new Date(PILstartdatetime);
                        dt.setMinutes(dt.getMinutes() + 30);
                        PILendtime = moment(dt).format("YYYY-MM-DDTHH:mm:ss");

                        var dt2 = new Date(PILstartdatetime);
                        PILTimeOpened = PILstartdatetime.substring(11, 16);

                    }
                    else if (k === 'partyId') {
                        PILpartyId = value;
                    }
                    else if (k === 'deviceLabel') {
                        PILLabel = value;
                    }
                    else if (k === 'deviceId') {
                        PILDeviceID = value;
                    }

                    else if (k === 'hidden') {
                        if (value === true) {
                            hidden = true;
                        }
                    }
                });
                var data = { title: '<span class=deviceleft>' + PILLabel + '</span><span class=deviceright>' + PILTimeOpened + '</span>', start: PILstartdatetime, end: PILendtime, className: PILClass, type: 'Medication', reading: '', difference: '', level: '', status: '', deviceid: PILDeviceID };

                if (hidden === false) {
                    patientdetails.push(data);
                }
            });

//Add Covid 19 Process
$.each(result.CVD, function (key, obj) {
                var CVDmeasurementTime = '';
                var CVDdailyQaScore = '';
                var CVDdailyQaScoreDesc = '';      
                var CVDdiseaseTypeDesc = '';              
                var CVDClass = 'Covid';               
                var hidden = false;
                var CVDendtime='';
                $.each(obj, function (k, value) {

                    if (k === 'measurementTime') {
                        CVDmeasurementTime = moment(value).format("YYYY-MM-DDTHH:mm:ss");
                        
                   var dt = new Date(CVDmeasurementTime);
                        dt.setMinutes(dt.getMinutes() + 30);
                        CVDendtime = moment(dt).format("YYYY-MM-DDTHH:mm:ss");
                        
                    }    

              else if (k === 'isAboveThreshold') {
                        if (value === true) {
                             CVDdiffclass = '<span class=devicerightCovid><i class="fa fa-exclamation-triangle" style="font-size:16px;color: #FFD800;" ></i></span>';                                                                      
                 
                        }
                        else
                        {

                          CVDdiffclass = '<span class=devicerightCovid><i class="fa fa-check-circle" style="font-size:18px;color: #00CC00;" ></i></span>';
                        }
                    }
               
                    else if (k === 'dailyQaScore') {
                        CVDdailyQaScore = value;                     
                     
                    }
                    else if (k === 'dailyQaScoreDesc') {
                        CVDdailyQaScoreDesc = value;
                    }
                    else if (k === 'diseaseTypeDesc') {
                        CVDdiseaseTypeDesc = value;
                    }                   
                    else if (k === 'hidden') {
                        if (value === true) {
                            hidden = true;
                        }
                    }
                });
                var data = { title: '<span class=deviceleft>Covid-19</span><span class=deviceright>'+CVDdiffclass+'</span>', start: CVDmeasurementTime, end: CVDendtime, className: CVDClass, type: 'COVID-19 Screening', reading: CVDdailyQaScore, difference: '', level: CVDdailyQaScoreDesc, status: CVDdailyQaScoreDesc };

                if (hidden === false) {
                    patientdetails.push(data);
                }
            });


  



        },
        complete: function (data) {
            $('#calendar').fullCalendar('destroy');
            //PatientReadingBindCalender(patientdetails,'month')
            DevicesFilter();
        },

        error: function (request, textStatus, errorThrown) {

        }
    });
}


function PatientReadingBindCalender(details, view) {
    if (viewdate === '') {
        viewdate = $('#calendar').fullCalendar('today');
    }

    $('#calendar').fullCalendar({
        header: {
            left: 'prevYear,prev,today,next,nextYear',
            center: 'title',
            right:'month,agendaWeek',
            },
        allDaySlot: false,
        slotDuration: '00:15:00',
        defaultView: view,
        //eventOverlap: false,
        slotEventOverlap: false,
        defaultDate: viewdate,
        navLinks: true,
        eventLimit: false,
        events: details,
        displayEventTime: false,
        duration: 1,
        dayClick: false,
        eventRender: function (event, element) {

            var title = element.find('.fc-title');
            title.html(title.text());

            // Onclick functionality for calendar link
            if (event.type === 'Pulse Rate') {
                element.attr('onclick', 'OpenDevicesDetails("' + patientid + '","PRM","' + event.type + ' Reading")');
            }
            else if (event.type === 'Oxygen') {
                element.attr('onclick', 'OpenDevicesDetails("' + patientid + '","POM","' + event.type + ' Reading")');
            }
            else if (event.type === 'Blood Pressure') {
                element.attr('onclick', 'OpenDevicesDetails("' + patientid + '","BPM","' + event.type + ' Reading")');
            }
            else if (event.type === 'Temperature') {
                element.attr('onclick', 'OpenDevicesDetails("' + patientid + '","TMM","' + event.type + ' Reading")');
            }
            else if (event.type === 'Weight') {
                element.attr('onclick', 'OpenDevicesDetails("' + patientid + '","WSM","' + event.type + ' Reading")');
            }
            else if (event.type === 'Blood Glucose') {
                element.attr('onclick', 'OpenDevicesDetails("' + patientid + '","BCM","' + event.type + ' Reading")');
            }
            else if (event.type === 'Pulse/Oxygen Saturation') {
                element.attr('onclick', 'OpenDevicesDetails("' + patientid + '","POM","' + event.type + ' Reading")');
            }
            else if (event.type === 'Spirometer') {
                element.attr('onclick', 'OpenDevicesDetails("' + patientid + '","SPR","' + event.type + ' Reading")');
            }
            else if (event.type === 'Diabetes') {
                element.attr('onclick', 'OpenDevicesDetails("' + patientid + '","DBT","' + event.type + ' Reading")');
            }
            else if (event.type === 'Mental Illness') {
                element.attr('onclick', 'OpenDevicesDetails("' + patientid + '","MIL","' + event.type + ' Reading")');
            }
            else if (event.type === 'Well-Being') {
                element.attr('onclick', 'OpenDevicesDetails("' + patientid + '","FEL","' + event.type + ' Reading")');
            }
            else if (event.type === 'Soap Notes') {
                element.attr('onclick', 'ViewSoapNotesDialog("' + event.ticketid + '","' + patientid + '","' + event.patientFullName + '","' + moment(event.start).format("MM-DD-YYYY HH:mm:ss a") + '")');
            }
            else if (event.type === 'Care Time') {
                element.attr('onclick', 'OpenDevicesDetails("' + patientid + '","CRT","' + event.type + ' Reading")');
            }
            else if (event.type === 'Pause Monitor') {
                element.attr('onclick', 'OpenDevicesDetails("' + patientid + '","PMN","' + event.type + ' Reading")');
            }
            else if (event.type === 'Readmission') {
                element.attr('onclick', 'OpenDevicesDetails("' + patientid + '","REA","' + event.type + ' Reading")');
            }
            if (event.type === 'Medication') {
                element.attr('onclick', 'OpenPillsyReadings("' + patientid + '","' + event.deviceid + '", "MED", "Medication","Calendar")');
            }
           else if (event.type === 'COVID-19 Screening') {
                element.attr('onclick', 'OpenDevicesDetails("' + patientid + '","CVD","' + event.type + ' Reading")');
            }
            else {
                //ToDo... nothing ...
            }
        },
        eventMouseover: function (calEvent, jsEvent) {
            var levelstatus = '';
            if (calEvent.level === 'high') {
                levelstatus = '<span>' + calEvent.difference + '</span> <i class="fa fa-caret-up mx-1" style="margin-top: -6px;position: absolute;font-size:25px;display: inline-block; color: red; margin-left:5px"></i>';
            }
            else if (calEvent.level === 'low') {
                levelstatus = '<span>' + calEvent.difference + '</span><i class="fa fa-caret-down mx-1" style="margin-top: -6px;position: absolute;font-size:25px;display: inline-block; color: green; margin-left:5px"></i>';
            }
            else if (calEvent.level === 'equal') {
                levelstatus = '<i class="fa fa fa-arrows-h" style="font-size:25px;color: #FF4500;" ></i>';
            }
            else {
                levelstatus = '<span>' + calEvent.difference + '</span>';
            }
            var tooltip = '<div class="tooltipevent" style="background:#FFF;color:black;position:absolute;z-index:10001; padding:2px; border:1px solid black; border-radius:5px;">'
                + '<p style="text-align: center; margin-bottom: 0; font-weight: bold;">' + calEvent.type + '</p>'
                + '<table style="width:100%; border:none !Important; margin-bottom:0px !Important;"><tr>'
                + '<tr><td>Date Time</td><td>:</td><td>' + moment(calEvent.start).format("MM-DD-YYYY HH:mm:ss") + '</td></tr>';


            var result;
            if (calEvent.type === 'Soap Notes') {
                result = calEvent.status.split('@');
                if (result[0] !== '') {
                    tooltip = tooltip + '<tr><td>Subjective</td><td>:</td><td>' + result[0] + '</td></tr>';
                }
                if (result[1] !== '') {
                    tooltip = tooltip + '<tr><td>Objective</td><td>:</td><td>' + result[1] + '</td></tr>';
                }
                if (result[2] !== '') {
                    tooltip = tooltip + '<tr><td>Assessment</td><td>:</td><td>' + result[2] + '</td></tr>';
                }
                if (result[3] !== '') {
                    tooltip = tooltip + '<tr><td>Plan</td><td>:</td><td>' + result[3] + '</td></tr>';
                }

            }

            else if (calEvent.type === 'Care Time') {
                tooltip = '<div class="tooltipevent" style="background:#FFF;color:black;position:absolute;z-index:10001; padding:2px; border:1px solid black; border-radius:5px;">'
                    + '<p style="text-align: center; margin-bottom: 0; font-weight: bold;">' + calEvent.type + '</p>'
                    + '<table style="width:100%; border:none !Important; margin-bottom:0px !Important;"><tr>'
                    + '<tr><td>Date </td><td>:</td><td>' + moment(calEvent.start).format("MM-DD-YYYY") + '</td></tr>';

                result = calEvent.status.split('@');
                if (result[0] !== '') {
                    tooltip = tooltip + '<tr><td>Start Time</td><td>:</td><td>' + result[0] + '</td></tr>';
                }
                if (result[1] !== '') {
                    tooltip = tooltip + '<tr><td>Duration</td><td>:</td><td>' + result[1] + ' Minutes</td></tr>';
                }
                if (result[2] !== '') {
                    tooltip = tooltip + '<tr><td>Care By</td><td>:</td><td>' + result[2] + '</td></tr>';
                }
                if (result[3] !== '') {
                    tooltip = tooltip + '<tr><td>Notes</td><td>:</td><td>' + result[3] + '</td></tr>';
                }


            }
            else if (calEvent.type === 'Well-Being') {

                result = calEvent.status.split('@');
                if (result[3] !== '') {
                    tooltip = tooltip + '<tr><td>Pain</td><td>:</td><td>' + result[3] + '</td></tr>';
                }
                if (result[2] !== '') {
                    tooltip = tooltip + '<tr><td>Mood</td><td>:</td><td>' + result[2] + '</td></tr>';
                }
                if (result[1] !== '') {
                    tooltip = tooltip + '<tr><td>Energy</td><td>:</td><td>' + result[1] + '</td></tr>';
                }
                if (result[0] !== '') {
                    tooltip = tooltip + '<tr><td>Breathing</td><td>:</td><td>' + result[0] + '</td></tr>';
                }

            }

            else if (calEvent.type === 'Pause Monitor') {
                tooltip = '<div class="tooltipevent" style="background:#FFF;color:black;position:absolute;z-index:10001; padding:2px; border:1px solid black; border-radius:5px;">'
                    + '<p style="text-align: center; margin-bottom: 0; font-weight: bold;">' + calEvent.type + '</p>'
                    + '<table style="width:100%; border:none !Important; margin-bottom:0px !Important;"><tr>'
                    + '<tr><td>Date </td><td>:</td><td>' + moment(calEvent.start).format("MM-DD-YYYY") + '</td></tr>';
                if (calEvent.status !== '') {
                    tooltip = tooltip + '<tr><td>Reason</td><td>:</td><td>' + calEvent.status + '</td></tr>';
                }
            }
            else if (calEvent.type === 'Readmission') {
                tooltip = '<div class="tooltipevent" style="background:#FFF;color:black;position:absolute;z-index:10001; padding:2px; border:1px solid black; border-radius:5px;">'
                    + '<p style="text-align: center; margin-bottom: 0; font-weight: bold;">' + calEvent.type + '</p>'
                    + '<table style="width:100%; border:none !Important; margin-bottom:0px !Important;"><tr>'
                    + '<tr><td>Date </td><td>:</td><td>' + moment(calEvent.start).format("MM-DD-YYYY") + '</td></tr>';
            }
     else if (calEvent.type === 'Covid') {
                tooltip = '<div class="tooltipevent" style="background:#FFF;color:black;position:absolute;z-index:10001; padding:2px; border:1px solid black; border-radius:5px;">'
                    + '<p style="text-align: center; margin-bottom: 0; font-weight: bold;">' + calEvent.type + '</p>'
                    + '<table style="width:100%; border:none !Important; margin-bottom:0px !Important;"><tr>'
                    + '<tr><td>Date </td><td>:</td><td>' + moment(calEvent.start).format("MM-DD-YYYY") + '</td></tr>';
       if (calEvent.reading !== '') {
                    tooltip = tooltip + '<tr><td>Readings</td><td>:</td><td>' + calEvent.reading + '</td></tr>';
                }
      
       if (calEvent.status !== '') {
                    tooltip = tooltip + '<tr><td>Reason</td><td>:</td><td>' + calEvent.status + '</td></tr>';
                }
            }
            else {
                if (calEvent.reading !== '') {
                    tooltip = tooltip + '<tr><td>Readings</td><td>:</td><td>' + calEvent.reading + '</td></tr>';
                }
                if (calEvent.status !== '') {
                    tooltip = tooltip + '<tr><td>Status</td><td>:</td><td>' + calEvent.status + '</td></tr>';
                }
                if (calEvent.difference !== '') {
                    tooltip = tooltip + '<tr><td>Difference</td><td>:</td><td>' + levelstatus + '</td></tr>';
                }
            }

            tooltip = tooltip + ' </table>' + '</div>';
            var $tooltip = $(tooltip).appendTo('body');
            $(this).mouseover(function (e) {
                $(this).css('z-index', 10000);
                $tooltip.fadeIn('500');
                $tooltip.fadeTo('10', 1.9);
            }).mousemove(function (e) {
                $tooltip.css('top', e.pageY + 10);
                $tooltip.css('left', e.pageX + 20);
                var left = parseInt($('.tooltipevent').css('left'));
                if (left > 1100)
                    $('.tooltipevent').css('left', e.pageX - 100);
            });
        },

        eventMouseout: function (calEvent, jsEvent) {
            $(this).css('z-index', 8);
            $('.tooltipevent').remove();
        },
        eventAfterAllRender: function (calEvent, jsEvent) {
            makeTodaybtnActive();
        }
    });
    $(".fc-month-button").hide();


}
function makeTodaybtnActive() {
    $('#calendar button.fc-today-button').removeAttr('disabled');
    $('#calendar button.fc-today-button').removeClass('fc-state-disabled');
}
function formatDate(date) {
    var time = date._i.split('T');
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
    //       time =[d.getHours(),
    //       d.getMinutes(),
    //       d.getSeconds()].join(':');

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [month, day, year].join('-') + ' ' + time[1];
}
$(document).on('click', '.fc-button', function () {
    var date = $('#calendar').fullCalendar('getDate');
    var aa = date._d;
    var y = date._d.getFullYear();
    var m = date._d.getMonth() + 1;
    var d = date._d.getDate();
    if (m < 10) {
        m = '0' + m;
    }
    if (d < 10) {
        d = '0' + d;
    }
    date = y + '-' + m + '-' + d;
});
$(document).on('click', '.fc-month-button', function (e) {
    $(".fc-month-button").hide();
    $(".fc-agendaWeek-button").show();
    weekview = true;
    makeTodaybtnActive();
});
$(document).on('click', '.fc-agendaWeek-button', function (e) {
    $(".fc-month-button").show();
    $(".fc-agendaWeek-button").hide();
    weekview = false;
    makeTodaybtnActive();
});

$(document).on('click', '.fc-prevYear-button', function (e) {
    var getdatecalender = $('#calendar').fullCalendar('getDate');
    viewdate = getdatecalender.format();
    if (weekview === true) {
        var start_date = $('#calendar').fullCalendar('getView').start;
        var end_date = $('#calendar').fullCalendar('getView').end;
        loadReadings(patientid, doctorid, moment(start_date.format()).format("YYYY-MM-DD"), moment(end_date.format()).format("YYYY-MM-DD"));

    }

});
$(document).on('click', '.fc-prev-button', function (e) {

    var start_date = $('#calendar').fullCalendar('getView').start;
    var end_date = $('#calendar').fullCalendar('getView').end;
    var getdatecalender = $('#calendar').fullCalendar('getDate');
    viewdate = getdatecalender.format();
    if (weekview === true) {
        start_date = $('#calendar').fullCalendar('getView').start;
        end_date = $('#calendar').fullCalendar('getView').end;
        loadReadings(patientid, doctorid, moment(start_date.format()).format("YYYY-MM-DD"), moment(end_date.format()).format("YYYY-MM-DD"));
    }
    makeTodaybtnActive();
});

$(document).on('click', '.fc-today-button', function (e) {
    var getdatecalender = $('#calendar').fullCalendar('getDate');
    viewdate = getdatecalender.format();

    if (weekview === true) {
        var start_date = $('#calendar').fullCalendar('getView').start;
        var end_date = $('#calendar').fullCalendar('getView').end;
        loadReadings(patientid, doctorid, moment(start_date.format()).format("YYYY-MM-DD"), moment(end_date.format()).format("YYYY-MM-DD"));
    }
});

$(document).on('click', '.fc-next-button', function (e) {

    var getdatecalender = $('#calendar').fullCalendar('getDate');
    viewdate = getdatecalender.format();

    if (weekview === true) {
        var start_date = $('#calendar').fullCalendar('getView').start;
        var end_date = $('#calendar').fullCalendar('getView').end;
        loadReadings(patientid, doctorid, moment(start_date.format()).format("YYYY-MM-DD"), moment(end_date.format()).format("YYYY-MM-DD"));
    }
});

$(document).on('click', '.fc-nextYear-button', function (e) {
    var getdatecalender = $('#calendar').fullCalendar('getDate');
    viewdate = getdatecalender.format();

    if (weekview === true) {
        var start_date = $('#calendar').fullCalendar('getView').start;
        var end_date = $('#calendar').fullCalendar('getView').end;
        loadReadings(patientid, doctorid, moment(start_date.format()).format("YYYY-MM-DD"), moment(end_date.format()).format("YYYY-MM-DD"));
    }
});

function AllDevicesFilter(source) {
    var checkboxes = document.getElementsByName('devicesfilter');
    for (var i = 0, n = checkboxes.length; i < n; i++) {
        checkboxes[i].checked = source.checked;
    }
    DevicesFilter();
}
function DevicesFilter() {
    var temppatientdetails = [];
    var chkall = true;
    var checkboxes = document.getElementsByName('devicesfilter');
    for (var i = 0, n = checkboxes.length; i < n; i++) {
        if (checkboxes[i].checked === true) {
            chkall = true;
        }
        else {
            chkall = false;
        }
    }

    temppatientdetails = patientdetails.slice();
    var items = patientdetails;
    var j = patientdetails.length;
    while (j--) {
        for (var ix = 0, nx = checkboxes.length; ix < nx; ix++) {
            if (patientdetails[j].type === checkboxes[ix].value && checkboxes[ix].checked === false) {
                temppatientdetails.splice(j, 1);
            }
        }
    }

    if (chkall === true) {
        $("#chkall").prop("checked", true);
    }
    else {
        $("#chkall").prop("checked", false);
    }

    $('#calendar').fullCalendar('destroy');
    if (weekview === true) {
        PatientReadingBindCalender(temppatientdetails, 'month');
        $(".fc-month-button").hide();
        $(".fc-agendaWeek-button").show();
    }
    else {
        PatientReadingBindCalender(temppatientdetails, 'agendaWeek');
        $(".fc-month-button").show();
        $(".fc-agendaWeek-button").hide();
    }

}
function OpenDevicesDetails(patientId, deviceType, deviceName) {
    var parameters = { patientId: patientId, deviceType: deviceType };
    $.ajax({
        type: "POST",
        url: "OpenReading",
        dataType: "html",
        data: parameters,
        success: function (data) {
            localStorage.setItem("BreadCrumbs", "PatientReading");
            OpenDialogWindow(deviceName, data, "800", "auto");
            $(document).foundation();
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });

}

function SoapNotesOpen(patientId, patientfullname) {
    $.ajax({
        url: "ShowSoapNotes",
        data: { patientId: patientId, patientFullName: patientfullname },
        dataType: "html",
        success: function (html) {
            $("#dialogPatientReading").html(html);
            $('#dialogPatientReading').dialog({
                title: 'Soap Notes',
                width: 1100,
                height: 500,
                modal: true
            });

        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
}

function DateIntialize() {

    var month1 = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
    var date1 = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"];

    $('#txtlogcareDate').datepicker({
        changeMonth: true, changeYear: true,
        maxDate: new Date()
    }).on("change", function (e) {
        var setDate;
        if ($(this).val() !== '') {
            var validate = $(this).val().split("/");
            var dateCheck = new Date(validate);
            if (validate[0] !== undefined && validate[2] !== undefined && dateCheck !== "Invalid Date") {
                setDate = moment($(this).val(), "MM/DD/YYYY").format('MM/DD/YYYY');
                if (setDate !== "Invalid date" && $.inArray(validate[1], date1) > -1 && $.inArray(validate[0].toLowerCase(), month1) > -1 && validate[2].substr(0, 4) >= 1900 && validate[2].substr(0, 4) <= 2099) {
                    $(this).val(setDate);
                    $('#messageDiv').hide();
                }
                else {

                    $(this).val('');
                    ShowMessage("error", "Please select valid format (MM/DD/YYYY).");
                    ClosePopup();
                    return false;
                }
            }
            else {
                $(this).val('');
                ShowMessage("error", "Please select valid1 format (MM/DD/YYYY).");
                ClosePopup();
                return false;
            }
        }
    });



    $('#txtpausemonitorstartdate').datepicker({
        changeMonth: true, changeYear: true

    }).on("change", function (e) {
        var setDate;
        if ($(this).val() !== '') {
            var validate = $(this).val().split("/");
            var dateCheck = new Date(validate);
            if (validate[0] !== undefined && validate[2] !== undefined && dateCheck !== "Invalid Date") {
                setDate = moment($(this).val(), "MM/DD/YYYY").format('MM/DD/YYYY');
                if (setDate !== "Invalid date" && $.inArray(validate[1], date1) > -1 && $.inArray(validate[0].toLowerCase(), month1) > -1 && validate[2].substr(0, 4) >= 1900 && validate[2].substr(0, 4) <= 2099) {
                    $(this).val(setDate);
                    $('#messageDiv').hide();
                }
                else {

                    $(this).val('');
                    ShowMessage("error", "Please select valid format (MM/DD/YYYY).");
                    ClosePopup();
                    return false;
                }
            }
            else {
                $(this).val('');
                ShowMessage("error", "Please select valid format (MM-DD-YYYY).");
                ClosePopup();
                return false;
            }
        }
    });


    $('#txtadmissiondate').datepicker({
        changeMonth: true, changeYear: true,
        maxDate: new Date()

    }).on("change", function (e) {
        var setDate;
        if ($(this).val() !== '') {
            var validate = $(this).val().split("/");
            var dateCheck = new Date(validate);
            if (validate[0] !== undefined && validate[2] !== undefined && dateCheck !== "Invalid Date") {
                setDate = moment($(this).val(), "MM/DD/YYYY").format('MM/DD/YYYY');
                if (setDate !== "Invalid date" && $.inArray(validate[1], date1) > -1 && $.inArray(validate[0].toLowerCase(), month1) > -1 && validate[2].substr(0, 4) >= 1900 && validate[2].substr(0, 4) <= 2099) {
                    $(this).val(setDate);
                    $('#messageDiv').hide();
                }
                else {

                    $(this).val('');
                    ShowMessage("error", "Please select valid format (MM/DD/YYYY).");
                    ClosePopup();
                    return false;
                }
            }
            else {
                $(this).val('');
                ShowMessage("error", "Please select valid format (MM/DD/YYYY).");
                ClosePopup();
                return false;
            }
        }
    });


    $('#txtpausemonitorenddate').datepicker({
        changeMonth: true, changeYear: true

    }).on("change", function (e) {
        var setDate;
        if ($(this).val() !== '') {
            var validate = $(this).val().split("/");
            var dateCheck = new Date(validate);
            if (validate[0] !== undefined && validate[2] !== undefined && dateCheck !== "Invalid Date") {
                setDate = moment($(this).val(), "MM/DD/YYYY").format('MM/DD/YYYY');
                if (setDate !== "Invalid date" && $.inArray(validate[1], date1) > -1 && $.inArray(validate[0].toLowerCase(), month1) > -1 && validate[2].substr(0, 4) >= 1900 && validate[2].substr(0, 4) <= 2099) {
                    $(this).val(setDate);
                    $('#messageDiv').hide();
                }
                else {

                    $(this).val('');
                    ShowMessage("error", "Please select valid format (MM/DD/YYYY).");
                    ClosePopup();
                    return false;
                }
            }
            else {
                $(this).val('');
                ShowMessage("error", "Please select valid format (MM/DD/YYYY).");
                ClosePopup();
                return false;
            }
        }
    });
}

function diff(start, end) {
    start = start.split(":");
    end = end.split(":");
    var startDate = new Date(0, 0, 0, start[0], start[1], start[2]);
    var endDate = new Date(0, 0, 0, end[0], end[1], end[2]);
    var diff = endDate.getTime() - startDate.getTime();
    var hours = Math.floor(diff / 1000 / 60 / 60);

    diff -= hours * 1000 * 60 * 60;

    var minutes = parseInt(Math.abs(diff) / (1000 * 60) % 60);


    var seconds = parseInt(Math.abs(diff) / 1000 % 60);


    return (hours < 9 ? "0" : "") + hours + ":" + (minutes < 9 ? "0" : "") + minutes + ":" + seconds;
}


//Feelings  Button Code start

function OpenFeelings(_patientid) {
    $.ajax({
        url: "OpenFeelingsDialog",
        data: { patientId: _patientid },
        dataType: "html",
        success: function (html) {
            OpenDialogWindow("Tell us how are you Well-Being today?", html, "50em", "auto");
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
}

function CloseRecordFeelings() {
    CloseDialogWindow();
}

function RecordFeelings() {
    $.ajax({
        url: "RecordFeelings",
        data: $("#feelingsForm").serialize(),
        dataType: "html",
        success: function (html) {
            CloseDialogWindow();
            $("#deviceQuadrant").html(html);
            ShowMessage("success", "Well-Being were recorded properly.");
            ClosePopup();
            InitialLoadReading();
        },
        error: function (request, textStatus, errorThrown) {
            ShowDialogCallbackError(request, textStatus, errorThrown);
        }
    });
}
// Feelings End


//Daily Questions Code Start

function GetDailyQuestionsDialog(diseaseType, patientId, patientFirstName, patientMiddleName, patientLastName) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "DailyQuestionsDialog",
        dataType: "html",
        data: { diseaseType: diseaseType, patientId: patientId, patientFirstName: patientFirstName, patientMiddleName: patientMiddleName, patientLastName: patientLastName },
        success: function (data) {
            //$("#DQView").slideToggle(750);
            //$("#DQView").show();
            OpenDialogWindow("Patient Daily Questions", data, 1000, 1500);
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
}


//Daily Questions Code End

function ClosePopup() {
    setTimeout(function () {
        $('#messageDiv').hide();
    }, 5000);
}

function OpenGridDetails(patientid, devicetype) {
    if (devicetype === "CRT") {
        $('#dialogCareLogTimeDetails').dialog({
            title: 'Log Care Time Details', width: 1100, height: 600, autoOpen: false,
            modal: true
        });
    }
    else if (devicetype === "PMN") {
        $('#dialogPauseMonitoringDetails').dialog({
            title: 'Pause Monitor Details', width: 1100, height: 600, modal: true, autoOpen: false
        });

    }
    else if (devicetype === "REA") {
        $('#dialogReadmissionDetails').dialog({
            title: 'Readmission Details', width: 1100,
            height: 600, modal: true, autoOpen: false
        });
    }


    LoadTableDetails(patientid, devicetype);
}

function LoadTableDetails(patinetid, devicetype) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "ReadingGridDetails",
        dataType: "html",
        data: { patientid: patientid, devicetype: devicetype, fromdate: '2019-04-10', todate: '2019-05-31' },
        success: function (data) {
            var result = jQuery.parseJSON(data);
            if (devicetype === "CRT") {
                DataTableCRTDetails(result.CRT, 'CRT');
            }
            else if (devicetype === "PMN") {
                DataTablePMNDetails(result.PMN, 'PMN');
            }
            else if (devicetype === "REA") {
                DataTableREADetails(result.PMN, 'REA');
            }
        },
        complete: function (data) {
            if (devicetype === "CRT") {
                $('#dialogCareLogTimeDetails').dialog('open');

            }
            else if (devicetype === "PMN") {
                $('#dialogPauseMonitoringDetails').dialog('open');

            }
            else if (devicetype === "REA") {
                $('#dialogReadmissionDetails').dialog('open');
            }
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
}





function DataTableREADetails(objdata, devicetype) {
    if ($.fn.DataTable.isDataTable('#tblREADatatable')) {
        $('#tblREADatatable').dataTable().fnDestroy();
    }
    var oTable = $('#tblREADatatable').dataTable({
        "searching": true,
        "processing": false,
        "paging": true,
        "retrieve": true,
        "destroy": true,
        "aaSorting": [],
        data: objdata,
        columns: [
            { data: "readmissionDate" },
            { data: "readmissionDate" }
        ],

        "fnRowCallback": function (tr, data, index) {
            var tr1 = $(tr);
            var td = $('<td />');
            var tds = $(tr1).find("td");
            var REAdata = devicetype + '/' + $('#lblpatientid').text() + '/' + moment(data.readmissionDate).format("YYYY-MM-DD");
            tds.eq(0).html("<span style='cursor:pointer'><a onclick=DeleteRecord('" + REAdata + "','" + devicetype + "') ><i title='Delete' style='font-size: 20px;color: red;' class='fa fa-trash'></i></a></span>");
            tds.eq(1).html(moment(data.endDate).format("MM-DD-YYYY"));

        },
        "initComplete": function (settings) {
            var api = new $.fn.dataTable.Api(settings);
            this.api().columns().every(function () {
                var column = this;
                var title = column.header();

            });
            api.columns().header().each(function (column) {

            });
        }
    });
}

function DataTablePMNDetails(objdata, devicetype) {
    if ($.fn.DataTable.isDataTable('#tblPMNDatatable')) {
        $('#tblPMNDatatable').dataTable().fnDestroy();
    }
    var oTable = $('#tblPMNDatatable').dataTable({
        "searching": true,
        "processing": false,
        "paging": true,
        "retrieve": true,
        "destroy": true,
        "aaSorting": [],
        data: objdata,
        columns: [
            { data: "startDate" },
            { data: "startDate" },
            { data: "endDate" },
            { data: "reason" }
        ],

        "fnRowCallback": function (tr, data, index) {
            var tr1 = $(tr);
            var td = $('<td />');
            var tds = $(tr1).find("td");
            var PMNdata = devicetype + '/' + $('#lblpatientid').text() + '/' + moment(data.startDate).format("YYYY-MM-DD") + '/' + moment(data.endDate).format("YYYY-MM-DD") + '/' + data.reason;
            tds.eq(0).html("<span style='cursor:pointer'><a onclick=DeleteRecord('" + PMNdata + "','" + devicetype + "') ><i title='Delete' style='font-size: 20px;color: red;' class='fa fa-trash'></i></a></span>");
            tds.eq(1).html(moment(data.startDate).format("MM-DD-YYYY"));
            tds.eq(2).html(moment(data.endDate).format("MM-DD-YYYY"));

        },
        "initComplete": function (settings) {
            var api = new $.fn.dataTable.Api(settings);
            this.api().columns().every(function () {
                var column = this;
                var title = column.header();

            });
            api.columns().header().each(function (column) {

            });
        }
    });
}
function DeleteRecord(deleterecord, devicetype) {
    var result = confirm("Are you sure that you want to delete?");
    if (result) {
        $.ajax({
            type: "POST",
            cache: false,
            url: "DeleteGridDetails",
            dataType: "html",
            data: { deleterecord: deleterecord },
            success: function (data) {
                if (devicetype === 'CRT') {
                    //LoadTableDetails(patientid,devicetype)
                    ShowMessage("success", "Log Care Time details deleted successfully");

                    ClosePopup();
                }

                else if (devicetype === 'REA') {
                    LoadTableDetails(patientid, devicetype);
                    ShowMessage("success", "Readadmission details deleted successfully");

                    ClosePopup();
                }
                else if (devicetype === 'PMN') {
                    LoadTableDetails(patientid, devicetype);
                    ShowMessage("success", "Pause monitor details deleted successfully");
                    ClosePopup();
                }

            },
            complete: function (data) {


                var start_date = $('#calendar').fullCalendar('getView').start;
                var end_date = $('#calendar').fullCalendar('getView').end;
                var getdatecalender = $('#calendar').fullCalendar('getDate');
                viewdate = getdatecalender.format();
                if (weekview === true) {
                    start_date = $('#calendar').fullCalendar('getView').start;
                    end_date = $('#calendar').fullCalendar('getView').end;
                    loadReadings(patientid, doctorid, moment(start_date.format()).format("YYYY-MM-DD"), moment(end_date.format()).format("YYYY-MM-DD"));
                    //
                }
            },
            error: function (request, textStatus, errorThrown) {
                ShowCallbackError(request, textStatus, errorThrown);
            }
        });
    }

}

function DataTableCRTDetails(objdata, devicetype) {
    if ($.fn.DataTable.isDataTable('#tblCRTDatatable')) {
        $('#tblCRTDatatable').dataTable().fnDestroy();
    }
    var oTable = $('#tblCRTDatatable').dataTable({
        "searching": true,
        "processing": false,
        "paging": true,
        "retrieve": true,
        "destroy": true,
        "aaSorting": [],
        data: objdata,
        columns: [
            { data: "careTimeDate" },
            { data: "careTimeDate" },
            { data: "beginTime" },
            { data: "duration" },
            { data: "interventionType" }
        ],

        "fnRowCallback": function (tr, data, index) {
            var tr1 = $(tr);
            var td = $('<td />');
            var tds = $(tr1).find("td");
            var CRTdata = devicetype + '/' + $('#lblpatientid').text() + '/' + moment(data.careTimeDate).format("YYYY-MM-DD") + '/' + data.beginTime + '/' + data.duration + '/' + data.interventionType;
            tds.eq(0).html("<span style='cursor:pointer'><a onclick=DeleteRecord('" + CRTdata + "','" + devicetype + "') ><i title='Delete' style='font-size: 20px;color: red;' class='fa fa-trash'></i></a></span>");
            tds.eq(1).html(moment(data.careTimeDate).format("MM-DD-YYYY"));

        },

        "initComplete": function (settings) {
            var api = new $.fn.dataTable.Api(settings);
            this.api().columns().every(function () {
                var column = this;
                var title = column.header();

            });
            api.columns().header().each(function (column) {

            });



        }


    });
}

function ReadingSearchDates() {
    if (event.target.id === 'CRTDateSearch') {
        alert('CRTDateSearch');
    } else if (event.target.id === 'READateSearch') {
        alert('READateSearch');
    } else if (event.target.id === 'PMNDateSearch') {
        alert('DateSearch');
    }

}
function OpenPillsyReadings(patientId, deviceId, deviceType, deviceTypeDesc, key) {
    localStorage.setItem("deviceId", deviceId);
    var parameters = { patientId: patientId, deviceId: deviceId, deviceType: deviceType };
    $.ajax({
        type: "POST",
        url: "OpenPillsyReading",
        dataType: "html",
        data: parameters,
        success: function (data) {
            localStorage.setItem("BreadCrumbs", "Dashboard");
            OpenDialogWindow(deviceTypeDesc + " Readings", data, "800", "auto");
            $(document).foundation();
            if (key === 'Delete') {
                var start_date = $('#calendar').fullCalendar('getView').start;
                var end_date = $('#calendar').fullCalendar('getView').end;
                var getdatecalender = $('#calendar').fullCalendar('getDate');
                viewdate = getdatecalender.format();
                if (weekview === true) {
                    start_date = $('#calendar').fullCalendar('getView').start;
                    end_date = $('#calendar').fullCalendar('getView').end;
                    loadReadings(patientid, doctorid, moment(start_date.format()).format("YYYY-MM-DD"), moment(end_date.format()).format("YYYY-MM-DD"));
                    //
                }
            }
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
}

function PillsyReadingsDeleteOpen(patientId, deviceId, deviceType, deviceTypeDesc) {
    var parameters = { patientId: patientId, deviceId: deviceId, deviceType: deviceType };
    $.ajax({
        type: "POST",
        url: "OpenPillsyReading",
        dataType: "html",
        data: parameters,
        success: function (data) {
            localStorage.setItem("BreadCrumbs", "Dashboard");
            OpenDialogWindow(deviceTypeDesc + " Readings", data, "800", "auto");
            $(document).foundation();
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
} 
