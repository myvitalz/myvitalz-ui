﻿function ExpandOrCollapse(obj) {
    var moreInfoDiv = $(obj).siblings(".otherMembers");
    moreInfoDiv.slideToggle(400);
    if ($(obj).html() === "Show More") {
        $(obj).html("Show Less");
        $(obj).addClass("toolsarrow-active");
    } else {
        $(obj).html("Show More");
        $(obj).removeClass("toolsarrow-active");
    }
}

function ExpandOrCollapseMoreInfo(obj) {
    var moreInfoRow = $(obj).closest("tr").next(".otherMembers");
    moreInfoRow.slideToggle(400);
}

function ExpandOrCollapseAll(obj) {
    var moreInfoDiv = $(obj).siblings(".otherMembers");

    if ($(obj).html() === "Expand") {
        $(obj).html("Collapse");
        $("table .toolsarrow").html("Show Less");
        $(".otherMembers").slideDown(400);
        $(".toolsarrow").addClass("toolsarrow-active");
    } else {
        $(obj).html("Expand");
        $("table .toolsarrow").html("Show More");
        $(".otherMembers").slideUp(400);
        $(".toolsarrow").removeClass("toolsarrow-active");
    }
}

function ShowDashboard(patientId) {
    location.href = "PatientDevices?patientId=" + patientId;
}

function EmailReport() {
    $.ajax({
        type: "POST",
        url: "GetEmailDailyReportDialog",
        dataType: "html",
        data: {},
        success: function (data) {
            OpenDialogWindow("Email Whiteboard", data, 500, 500);
        },
        error: function (request, textStatus, errorThrown) {
            ShowDialogCallbackError(request, textStatus, errorThrown);
        }
    });
}

function SubscribeOrUnsubscribe(email, flag) {
    var parameters = { email: email, subscribe: flag };
    $.ajax({
        type: "POST",
        url: "/Home/SubscribeOrUnsubscribe",
        dataType: "html",
        data: parameters,
        success: function (data) {
            if (flag === "true") {
                ShowMessage("success", "You have been subscribed successfully.");
            } else {
                ShowMessage("success", "You have been unsubscribed successfully.");
            }
            RefreshPage(2000);
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
}

function SendEmail() {
    $.ajax({
        type: "POST",
        url: "EmailDailyReport",
        dataType: "html",
        data: { emails: $("#To").val() },
        success: function (data) {
            CloseDialogWindow();
            ShowMessage("success", "Whiteboard was sent to " + $("#To").val() + " successfully.");
        },
        error: function (request, textStatus, errorThrown) {
            ShowDialogCallbackError(request, textStatus, errorThrown);
        }
    });
}

function OpenPatientReading(patientId, deviceType, deviceTypeDesc) {
    var parameters = { patientId: patientId, deviceType: deviceType };
    $.ajax({
        type: "POST",
        url: "OpenReading",
        dataType: "html",
        data: parameters,
        success: function (data) {
        localStorage.setItem("BreadCrumbs", "Dashboard");
            OpenDialogWindow(deviceTypeDesc + " Readings", data, "800", "auto");
            $(document).foundation();
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
} 
        
function OpenPillsyReadings(patientId, deviceId, deviceType, deviceTypeDesc,key) {
localStorage.setItem("deviceId", deviceId);
    var parameters = { patientId: patientId, deviceId: deviceId, deviceType: deviceType };
    $.ajax({
        type: "POST",
        url: "OpenPillsyReading",
        dataType: "html",
        data: parameters,
        success: function (data) {
            localStorage.setItem("BreadCrumbs", "Dashboard");
            OpenDialogWindow(deviceTypeDesc + " Readings", data, "800", "auto");
            $(document).foundation();
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
} 

function InitializePatientTable() {
    $('#patientTable').dataTable({
        "bAutoWidth": true,
        "bPaginate": false,
        "bInfo": false,
        "bStateSave": false,
        "sPaginationType": "full_numbers",
        "aaSorting": [],
        "aoColumns": [{ "sWidth": "350px" },
                        { "sWidth": "50px" },
                        { "sWidth": "50px" },
                        { "sWidth": "50px" },
                        { "sWidth": "50px" },
                        { "sWidth": "50px" },
                        { "sWidth": "50px" },
                        { "sWidth": "50px" },
                        { "sWidth": "50px" },
                        { "sWidth": "50px" },
                        { "sWidth": "50px" },
                        { "sWidth": "50px" },
                        { "sWidth": "50px" },
                        { "sWidth": "50px" }
        ]
    });
}