﻿var _isSummaryVisible = true;
var _deviceTable;
var _deviceTableWidth = "";
function IntializeDeviceHistory() {
    InitializeDateControl("#FromDate", null, { maxDate: new Date(), changeMonth: true, changeYear: true });
    InitializeDateControl("#ToDate", null, { maxDate: new Date(), changeMonth: true, changeYear: true });
    var tableWidth = 75 * 1500 / $(window).width();
    tableWidth = parseInt(tableWidth);
    var tableHeight = $("#patientTable").height();
    tableHeight = tableHeight / 2;
    tableHeight = parseInt(tableHeight) + 3;
    if (_deviceTableWidth !== "") {
        _deviceTable = $('#deviceHistory').dataTable({
            "bPaginate": true,
            "iDisplayLength": 30,
            "bLengthChange": false,
            "bJQueryUI": false,
            "bAutoWidth": false,
            "bStateSave": false,
            "sPaginationType": "full_numbers",
            "sScrollX": "100%",
            "sScrollXInner": _deviceTableWidth,
            "bScrollAutoCss": true,
            "bScrollCollapse": false,
            "aaSorting": []
        });
        _deviceTableWidth = "";
    } else {
        _deviceTable = $('#deviceHistory').dataTable({
            "bPaginate": true,
            "iDisplayLength": 30,
            "bLengthChange": false,
            "bJQueryUI": false,
            "bAutoWidth": false,
            "bStateSave": false,
            "sPaginationType": "full_numbers",
            "aaSorting": []
        });
    }
}

var _currentReadingsChart;
var _dialogChartWidth = undefined;
var _chartType = "line";
function DrawChart(chartDiv, chartTitle, chartSubtitle, xAxisText, xAxisData, yAxisText, yAxisSeriesData, chartType) {
    // if device type is Blood Glucose, then do scatter plot
    if (chartDiv === "#BGChart") {
        DrawChartScatterPlot(chartDiv, chartTitle, chartSubtitle, xAxisText, xAxisData, yAxisText, yAxisSeriesData, "scatter");
    } else {

        if (chartType === undefined) {
            chartType = "line";
        }

        _chartType = chartType;
        if (chartType === "line") {
            $(".lineChartLink").hide();
            $(".columnChartLink").show();
        } else {
            $(".lineChartLink").show();
            $(".columnChartLink").hide();
        }
        var NewchartWidth = undefined;
            //setTimeout(function(){
                if ($('.ui-dialog ').is(":visible") === true) {
                    NewchartWidth = $('.section-container').width()-20;
                } else {
                    NewchartWidth = $('.content').width()-30;
                }
              //if($('div').prop('id')=='modalWindowContent'){
                //NewchartWidth = $('.section-container').width()-20;
               // }
                //else{
                //NewchartWidth = $('#PHQ9Chart').width();
              // }
           // }, 500);
        var chartWidth = undefined;
        if ($("#dummyChartDiv").is(":visible") === true) {
            chartWidth = $("#dummyChartDiv").width();
            chartWidth = chartWidth * 0.83;
            //chartWidth = chartWidth - 30;
        } else {
            if ($("#splDummyChartDiv").is(":visible") === true) {
                if (_dialogChartWidth === undefined) {
                    _dialogChartWidth = $("#modalWindowContent").width();
                }
                chartWidth = _dialogChartWidth;
                if ($("#isSmallDeviceDiv").is(":visible") === true) {
                    chartWidth = chartWidth * 0.725;
                } else {
                    chartWidth = chartWidth * 0.590;
                    //chartWidth = chartWidth - 55;
                }
            }
        }
        _currentReadingsChart = $(chartDiv).highcharts({
            global: {
                useUTC: false
            },
            //plotOptions: {
            //    series: {
            //        pointWidth: 20
            //    }
            //},
            chart: {
                type: chartType,
                zoomType: 'x',
              //  width: chartWidth
                  width: NewchartWidth
            },
            title: {
                text: chartTitle
            },
            subtitle: {
                text: chartSubtitle
            },
            xAxis: {
                minRange: 60 * 60 * 1000, //Min level zoom
                type: 'datetime', //y-axis will be in milliseconds
                dateTimeLabelFormats: { //force all formats to be hour:minute:second
                    millisecond: '%I:%M:%S.%L  %p',
                    second: '%I:%M:%S %p',
                    minute: '%I:%M %p',
                    hour: '%I:%M %p',
                    day: '%e. %b',
                    week: '%e. %b',
                    month: '%b \'%y',
                    year: '%Y'
                },
                allowDecimals: false
            },
            yAxis: {
                title: {
                    text: yAxisText
                }
            },
            tooltip: {
                xDateFormat: '%b %e, %Y %I:%M %p',
                shared: true,
                enabled: true
            },
            colors: ['#FF0000'],
            plotOptions: {
                line: {
                    marker: {
                        enabled: false
                    },

                    dataLabels: {
                        enabled: false
                    },
                    enableMouseTracking: true
                },
                series: {
                    lineWidth: 1,
                    pointWidth: 2
                }
            },
            series: yAxisSeriesData
        });
    }
}
function DrawChartNew(chartDiv, chartTitle, chartSubtitle, xAxisText, xAxisData, yAxisText, yAxisSeriesData) {
    // Create the chart
    $(chartDiv).highcharts('StockChart', {


        rangeSelector: {
            selected: 1,
            buttons: [
            {
                type: 'day',
                count: 1,
                text: '1d'
            },
            {
                type: 'week',
                count: 1,
                text: '1w'
            },
            {
                type: 'month',
                count: 1,
                text: '1m'
            }, {
                type: 'month',
                count: 3,
                text: '3m'
            }, {
                type: 'month',
                count: 6,
                text: '6m'
            }, {
                type: 'ytd',
                text: 'YTD'
            }, {
                type: 'year',
                count: 1,
                text: '1y'
            }, {
                type: 'all',
                text: 'All'
            }]
        },

        title: {
            text: chartTitle
        },
        subtitle: {
            text: chartSubtitle
        },
        xAxis: {
            type: 'datetime', //y-axis will be in milliseconds
            dateTimeLabelFormats: { //force all formats to be hour:minute:second
                millisecond: '%H:%M:%S.%L',
                second: '%H:%M:%S',
                minute: '%H:%M',
                hour: '%H:%M',
                day: '%e. %b',
                week: '%e. %b',
                month: '%b \'%y',
                year: '%Y'
            },
            allowDecimals: false
        },
        legend: {
            width: 200,
            itemWidth: 100
        },
        yAxis: {
            title: {
                text: yAxisText
            }
        },

        series: yAxisSeriesData
    });
}

function DrawChartScatterPlot(chartDiv, chartTitle, chartSubtitle, xAxisText, xAxisData, yAxisText, yAxisSeriesData, chartType) {

    $(".lineChartLink").hide();
    $(".columnChartLink").hide();

    var chartWidth = undefined;
    if ($("#dummyChartDiv").is(":visible") === true) {
        chartWidth = $("#dummyChartDiv").width();
        chartWidth = chartWidth * 0.83;
    }
    else {
        if ($("#splDummyChartDiv").is(":visible") === true) {
            if (_dialogChartWidth === undefined) {
                _dialogChartWidth = $("#modalWindowContent").width();
            }
            chartWidth = _dialogChartWidth;

            if ($("#isSmallDeviceDiv").is(":visible") === true) {
                chartWidth = chartWidth * 0.725;
            } else {
                chartWidth = chartWidth * 0.590;
            }
        }
    }

    _currentReadingsChart = $(chartDiv).highcharts({
        global: {
            useUTC: false
        },
        chart: {
            type: chartType,
            zoomType: 'xy'
        },
        title: {
            text: chartTitle
        },
        subtitle: {
            text: chartSubtitle
        },
        xAxis: {
            minRange: 60 * 60 * 1000, //Min level zoom
            type: 'datetime', //y-axis will be in milliseconds
            dateTimeLabelFormats: { //force all formats to be hour:minute:second
                millisecond: '%I:%M:%S.%L  %p',
                second: '%I:%M:%S %p',
                minute: '%I:%M %p',
                hour: '%I:%M %p',
                day: '%e. %b',
                week: '%e. %b',
                month: '%b \'%y',
                year: '%Y'
            },
            allowDecimals: false
        },
        yAxis: {
            title: {
                text: yAxisText
            }
        },
        tooltip: {
            xDateFormat: '%b %e, %Y %I:%M %p',
            shared: true,
            enabled: true
        },
        //legend: {
        //    layout: 'vertical',
        //    align: 'left',
        //    verticalAlign: 'top',
        //    x: 0,
        //    y: 70,
        //    floating: true
        //},
        colors: ['#FF0000'],
        plotOptions: {
            scatter: {
                marker: {
                    radius: 5,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                tooltip: { 
                    // can only override headerFormat, pointFormat, yDecimals, xDateFormat, yPrefix and ySuffix. 
                    // in a scatter plot the series.name by default shows in the headerFormat and point.x and point.y in the pointFormat.
                    headerFormat: '<b>{series.name}</b><br>',
                    pointFormat: 'Date: {point.x:%Y-%m-%d %H:%M:%S} <br>  Glucose: {point.y}'
                }
            }
        },
        //plotOptions: {
        //    line: {
        //        marker: {
        //            enabled: false
        //        },

        //        dataLabels: {
        //            enabled: false
        //        },
        //        enableMouseTracking: true
        //    },
        //    series: {
        //        lineWidth: 1,
        //        pointWidth: 2
        //    }
        //},
        series: yAxisSeriesData
    });
}

function GetEmailDialog(partyId) {
    $.ajax({
        type: "POST",
        url: "GetEmailDialog",
        dataType: "html",
        data: { partyId: partyId },
        success: function (data) {
            OpenDialogWindow("Send Email", data, 500, 500);
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
}

function GetManualReadingDialog(partyId) {
    $.ajax({
        type: "POST",
        url: "GetManualReadingDialog",
        dataType: "html",
        data: { partyId: partyId },        
        success: function (data) {
            OpenDialogWindow("Enter Vitals Manually", data, 700, 700);
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown); 
        }
    });
}

function GetSoapNotesDialog(partyId, patientFullName) {
    $.ajax({
        type: "POST",
        url: "SoapNotesDialog",
        dataType: "html",
        data: { partyId: partyId, patientFullName: patientFullName },
        success: function (data) {
            OpenDialogWindow("SOAP Notes", data, 600, 600);
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
}


function GetSoapNotesDialogForDR(partyId, patientFullName) {
    $.ajax({
        type: "POST",
        url: "SoapNotesDialogForDR",
        dataType: "html",
        data: { partyId: partyId, patientFullName: patientFullName },
        success: function (data) {
            OpenDialogWindow("SOAP Notes", data, 600, 600);
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
}

function ViewSoapNotesDialog(soapNoteId, patientId, patientFullName, updateTS) {
$.ajax({
        type: "POST",
        url: "ViewSoapNotesDialog",
        dataType: "html",
        data: { soapNoteId: soapNoteId, patientId: patientId, patientFullName: patientFullName, updateTS: updateTS },
        success: function (data) {
            OpenDialogWindow("SOAP Notes", data, 600, 600);
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
}

function SendEmail() {
    $.ajax({
        type: "POST",
        url: "SendEmail",
        dataType: "html",
        data: $("#EmailForm").serialize(),
        success: function (data) {
            CloseDialogWindow();
            ShowMessage("success", "Email was sent successfully.");
        },
        error: function (request, textStatus, errorThrown) {
            ShowDialogCallbackError(request, textStatus, errorThrown);
        }
    });
}

function OpenNote() {
    $.ajax({
        type: "POST",
        url: "OpenNote",
        dataType: "html",
        data: $("#SoapNotesForm").serialize(),
        success: function (data) {
            CloseDialogWindow();
        },
        error: function (request, textStatus, errorThrown) {
            ShowDialogCallbackError(request, textStatus, errorThrown);
        }
    });
}

function FormatDate(date) {
    return date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
}
function ChangeDates() {
    var noOfDays = $("#DateRange").val();
    var endDate = new Date();
    var startDate = new Date();
    switch (noOfDays) {
        case "DR":
            $(".dateRangeOptions").slideDown();
            return;
        case "1D":
            startDate.setDate(endDate.getDate());
            break;
        case "1W":
            startDate.setDate(endDate.getDate() - 6);
            break;
        case "1M":
            startDate.setDate(endDate.getDate() - 29);
            break;
        case "3M":
            startDate.setDate(endDate.getDate() - 89);
            break;
        case "6M":
            startDate.setDate(endDate.getDate() - 179);
            break;
        case "1Y":
            startDate.setDate(endDate.getDate() - 364);
            break;
        default:
            break;
    }
    $("#FromDate").val(FormatDate(startDate));
    $("#ToDate").val(FormatDate(endDate));
    RefreshReading();
}

function RefreshReading() {
    var parameters = $("#DeviceForm").serialize();
    if (_isSummaryVisible === true) {
        $.ajax({
            type: "POST",
            url: "RefreshReading",
            dataType: "html",
            data: parameters,
            success: function (data) {
                $("#readingsDiv").html(data);
                $(document).foundation();
            },
            error: function (request, textStatus, errorThrown) {
                ShowCallbackError(request, textStatus, errorThrown);
            }
        });
    } else {
        $.ajax({
            url: "RefreshCharts",
            data: parameters,
            dataType: "html",
            success: function (html) {
                $("#allChartDiv").html(html);
            },
            error: function (request, textStatus, errorThrown) {
                ShowCallbackError(request, textStatus, errorThrown);
            }
        });
    }
}

function ChangeSNDates() {
    var noOfDays = $("#SNDateRange").val();
    var endDate = new Date();
    var startDate = new Date();
    switch (noOfDays) {
        case "DR":
            $(".dateRangeOptions").slideDown();
            return;
        case "1D":
            startDate.setDate(endDate.getDate());
            break;
        case "1W":
            startDate.setDate(endDate.getDate() - 6);
            break;
        case "1M":
            startDate.setDate(endDate.getDate() - 29);
            break;
        case "3M":
            startDate.setDate(endDate.getDate() - 89);
            break;
        case "6M":
            startDate.setDate(endDate.getDate() - 179);
            break;
        case "1Y":
            startDate.setDate(endDate.getDate() - 364);
            break;
        default:
            break;
    }
    $("#FromDate").val(FormatDate(startDate));
    $("#ToDate").val(FormatDate(endDate));
    RefreshSoapNote();
}

function RefreshSoapNote() {
    //_isSummaryVisible == false
    var sn_parameter = $("#SNForm").serialize();
    $.ajax({
        type: "POST",
        url: "RefreshSoapNote",
        dataType: "html",
        data: sn_parameter,
        success: function (data) {
            $("#allSoapNoteDiv").html(data);
            $(document).foundation();
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
}

function RefreshQDforMR() {
    //_isSummaryVisible == false
    var sn_parameter = $("#SNForm").serialize();
    $.ajax({
        type: "POST",
        url: "RefreshSoapNote",
        dataType: "html",
        data: sn_parameter,
        success: function (data) {
            $("#quadrantBPM").html(data);
            $(document).foundation();
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
}

function RefreshDeviceQuadrant() {    
    $.ajax({
        url: "GetDeviceQuadrant",
        data: { patientId: _patientId },
        dataType: "html",
        success: function (html) {
            $("#deviceQuadrant").html(html);
            //RefreshBGDeviceQuadrant();
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
}

function RefreshBGDeviceQuadrant() {
    if (_isBgPatient === true) {
        $("#bgDeviceQuadrant").addClass("quadrant quadCorners");
        $("#bgDeviceQuadrant").html("Loading BodyGuardian Data...");
        $.ajax({
            url: "GetBgDeviceQuadrant",
            data: { patientId: _patientId },
            dataType: "html",
            success: function (html) {
                $("#bgDeviceQuadrant").removeClass("quadrant quadCorners");
                $("#bgDeviceQuadrant").slideToggle();
                $("#bgDeviceQuadrant").html(html);
                $("#bgDeviceQuadrant").slideToggle();
            },
            error: function (request, textStatus, errorThrown) {
                ShowCallbackError(request, textStatus, errorThrown);
            }
        });
    }
}

function OpenReading(deviceType, deviceTypeDesc) {
    var parameters = { patientId: _patientId, deviceType: deviceType };
    $.ajax({
        type: "POST",
        url: "OpenReading",
        dataType: "html",
        data: parameters,
        success: function (data) {
            localStorage.setItem("BreadCrumbs", "Dashboard");
            OpenDialogWindow(deviceTypeDesc + " Readings", data, "800", "auto");
            $(document).foundation();
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
}

function OpenReading1(deviceType, deviceTypeDesc, _patientId) {
    var parameters = { patientId: _patientId, deviceType: deviceType };
    $.ajax({
        type: "POST",
        url: "OpenReading",
        dataType: "html",
        data: parameters,
        success: function (data) {
            localStorage.setItem("BreadCrumbs", "Dashboard");
            OpenDialogWindow(deviceTypeDesc + " Readings", data, "800", "auto");
            $(document).foundation();
        },
        complete: function(data) {
            RefreshReading();
            var weekview = true;
            var viewdate = '';
            var start_date = $('#calendar').fullCalendar('getView').start;
                                var end_date = $('#calendar').fullCalendar('getView').end;
                                var getdatecalender = $('#calendar').fullCalendar('getDate');
                                viewdate = getdatecalender.format();
                                if (weekview === true) {
                                    start_date = $('#calendar').fullCalendar('getView').start;
                                    end_date = $('#calendar').fullCalendar('getView').end;
                                    loadReadings(_patientId, HttpContext.Current.User.UserId, moment(start_date.format()).format("YYYY-MM-DD"), moment(end_date.format()).format("YYYY-MM-DD"));
                                }
        },
        error: function (request, textStatus, errorThrown) {
            ShowCallbackError(request, textStatus, errorThrown);
        }
    });
}

function ChangeChartType(chartType) {
    ReDrawChart(chartType);
}