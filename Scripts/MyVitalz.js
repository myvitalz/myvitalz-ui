﻿//#region Message Functions

//To show any type of message
function ShowMessage(messageType, message, messageDivId, screenSize) {
    if (messageDivId == undefined) {
        messageDivId = "#messageDiv";
    }
    if (screenSize == undefined) {
        screenSize = "fullscreen";
    }

    //Format Message.
    //Replace new lines with 
    message = message.replace(/\r\n$/gi, "");
    message = message.replace(/^\r\n/gi, "");
    message = message.replace(/\r\n/g, "<br/>");

    var iconClass;
    var messageClass;
    switch (messageType) {
        case "info":
            iconClass = "fa fa-info-sign";
            messageClass = "info";
            type = " Info: ";
            break;
        case "success":
            iconClass = "fa fa-ok-sign";
            messageClass = "success";
            type = " Success: ";
            break;
        case "warning":
            iconClass = "fa fa-warning-sign";
            messageClass = "warning";
            type = " Warning: ";
            break;
        case "error":
        default:
            iconClass = "fa fa-exclamation-sign";
            messageClass = "error";
            type = " Error: ";
            break;

    }

    if (screenSize == "dialog") {
        //messageClass = messageClass + "-dialog";
    }

    //Empty the div to clear old messages
    $(messageDivId).empty();
    $(messageDivId).attr("class", messageClass);
    $(messageDivId).append('<i class="' + iconClass + ' icon-white"></i><strong>' + type + '</strong>' + message);
    $(messageDivId).append('<i class="fa fa-remove icon-white hide-message right"></i>');

    //Show the Message    
    $(messageDivId).show('slide', { direction: 'up' }, 500).fadeIn('slow');

    //Unbind the previous one
    $(messageDivId).unbind("click");
    //Add event to close the message box on click
    $(messageDivId).click(function () {
        HideMessage(messageDivId);
    });

    //Hide the informational & Success message after few sec
    if (messageType == "info"
            || messageType == "success"
            || messageType == "warning") {
        //window.setTimeout("HideMessage('" + messageDivId + "')", 7000);
    }

    //Scroll to the Message    
    $('body,html').animate({ scrollTop: $(messageDivId).offset().top }, 800);
}

//To show the Error Message
function ShowError(message, messageDivId) {
    ShowMessage("error", message, messageDivId);
}

//To show the Callback Errors in the Ajax Calls
function ShowCallbackError(request, textStatus, errorThrown, messageDivId) {
    if (request.status == 500) { // Known Ajax errors
        var data = jQuery.parseJSON(request.responseText);
        ShowError(data.Message, messageDivId);
    } else if (request.responseText != "") { //Check for Timeout Issue
        ShowError(request.responseText, messageDivId);
    } else { //Other issues
        //ShowError("Unknown " + textStatus + " occurred", messageDivId);
    }
}


function HideMessage(messageDivId) {
    if (messageDivId == undefined) {
        messageDivId = "#messageDiv";
    }
    if ($(messageDivId).is(":visible")) {
        $(messageDivId).hide('slide', { direction: 'up' }, 500).fadeOut('slow');
    }
}

function ScrollToTop() {
    $('body,html').animate({ scrollTop: 0 }, 800);
}
//#endregion

var DefaultDataTableLengthMenu = [[10, 25, 50, -1], [10, 25, 50, "All"]];
var _selectedRowClass = "selectedRow";

function GoBack() {
    history.go(-1);
}
//Function that enable or disables the pagination depending upon no of records
function EnableOrDisablePagination() {
    //Hide the length menu only when there is less than 10 records
    if (this.fnSettings().fnRecordsTotal() <= 10) {
        $('.dataTables_length').css("display", "none");
    }

    if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) {
        $('.dataTables_paginate').css("display", "block");
    } else {
        $('.dataTables_paginate').css("display", "none");
    }
}

//Define the OnEnterKey Event
$.fn.onEnterKey =
    function (onEnterKeyFunction) {
        $(this).keypress(
            function (event) {
                var code = event.keyCode ? event.keyCode : event.which;
                if (code == 13) {
                    onEnterKeyFunction();
                    return false;
                }
            });
    };


function ExportReadings(url, title, deviceType, patientId, fromDate, toDate) {
    //Submit the Data to server to write the Excel File
    $("body").append('<form id="exportForm" method="post" action="' + url + '" target="exportWindow"><input type="hidden" id="title" name="title" /><input type="hidden" id="patientId" name="patientId" /><input type="hidden" id="deviceType" name="deviceType" /><input type="hidden" id="fromDate" name="fromDate" /><input type="hidden" id="toDate" name="toDate" /></form>');
    $("#title").val(title);
    $("#deviceType").val(deviceType);
    $("#patientId").val(patientId);
    $("#fromDate").val(fromDate);
    $("#toDate").val(toDate);
    $("#exportForm").submit().remove();
}

function ExportReadingsMED(url, title, deviceType, patientId, fromDate, toDate) {
    //Submit the Data to server to write the Excel File
    $("body").append('<form id="exportForm" method="post" action="' + url + '" target="exportWindow"><input type="hidden" id="title" name="title" /><input type="hidden" id="patientId" name="patientId" /><input type="hidden" id="deviceType" name="deviceType" /><input type="hidden" id="fromDate" name="fromDate" /><input type="hidden" id="toDate" name="toDate" /><input type="hidden" id="deviceid" name="deviceId" /></form>');
    $("#title").val(title);
    $("#deviceType").val(deviceType);
    $("#patientId").val(patientId);
    $("#fromDate").val(fromDate);
    $("#toDate").val(toDate);
    $("#deviceid").val(localStorage.getItem("deviceId"));
    $("#exportForm").submit().remove();
}


function InitializeMasking() {
    $.mask.definitions['t'] = '[AP]';
    $.mask.definitions['m'] = '[M]';
    $(".dateField").mask("99/99/9999");
    $(".dateField").attr("placeholder", "mm/dd/yyyy");

    $(".dateTimeField").mask("99/99/9999 ?99:99 tm");
    $(".dateTimeField").attr("placeholder", "mm/dd/yyyy hh:mm tt");

    $(".phoneField").mask("(999) 999-9999");
    $(".phoneField").attr("placeholder", "(999) 999-9999");

    $(".taxIdField").mask("99-9999999");
    $(".taxIdField").attr("placeholder", "99-9999999");

    $(".ssnField").mask("999-99-9999");
    $(".ssnField").attr("placeholder", "999-99-9999");
}

function ApplyDatatableJScrollpane() {
    var table_header;
    _this = this;
    table_header = $('.dataTables_scrollHeadInner').css('position', 'relative');
    $('.dataTables_scrollBody').bind('jsp-scroll-x', function (event, scrollPositionX, isAtLeft, isAtRight) {
        table_header.css('right', scrollPositionX);
    }).jScrollPane({ showArrows: true, horizontalGutter: 5 });
    //$('.dataTables_scrollBody').niceScroll({ autohidemode: false, cursorcolor: "#8B8B9F", cursorwidth: "20px" });
}

//Opens the AppDialog with the given content
function OpenDialogWindow(title, data, width, height, dialogDivId) {
    if (dialogDivId == undefined) {
        dialogDivId = "#modalWindow";
    } else {
        dialogDivId = "#modalWindow1";
    }
    HideDialogError(dialogDivId);
    ClearDialogContent(dialogDivId);
    if (data != null) {
        $(dialogDivId + 'Content').html(data);
    }
    $(dialogDivId + 'Error').click(function () {
        if ($(this).is(":visible")) {
            $(this).hide('slide', { direction: 'up' }, 500).fadeOut('slow');
        }
    });
    $(dialogDivId).attr("title", title);
    $("#ui-dialog-title-modalWindow").html(title); //This is needed when we try to use reuse the dialog without killing it
    $(dialogDivId).dialog({
        resizable: false,
        draggable: true,
        modal: true,
        width:width,
        position: "top",
        show: {
            effect: "clip",
            duration: 500
        },
        hide: {
            effect: "explode",
            duration: 1000
        },
        // Dynamically controls size of dialog 
        create: function () {
            //$('.ui-dialog').addClass('ui-dialog-' + dialogSize);
        },
        // Controls fade of dialog, don't change
        open: function () {
            //$('.ui-widget-overlay').hide().fadeTo(800, 0.80);
            $(document).foundation();
        },
        // Removes styles if there is more than one dialog 
        close: function (event, ui) {
            ClearDialogContent(dialogDivId);
            $(this).dialog('destroy');
        }
    });
    
    $(dialogDivId).bind("focus", function (event, ui) {
        $(dialogDivId + " input").first().focus();
    });
}

//To display message on the Dialog
function ShowDialogMessage(messageType, message, dialogDivId) {
    if (dialogDivId == undefined) {
        dialogDivId = "#modalWindow";
    }
    ShowMessage(messageType, message, dialogDivId + "Error", "dialog");
}

//To set the AppDialog Error
function ShowDialogError(message, dialogDivId) {
    ShowDialogMessage("error", message, dialogDivId);
}

//Hides the AppDialog Error
function HideDialogError(dialogDivId) {
    if (dialogDivId == undefined) {
        dialogDivId = "#modalWindow";
    }
    $(dialogDivId + 'Error').hide();
}

//To set the Dialog Callback Errors
function ShowDialogCallbackError(request, textStatus, errorThrown, dialogDivId) {
    if (request.status == 500) { // Known Ajax errors
        var data = jQuery.parseJSON(request.responseText);
        ShowDialogError(data.Message, dialogDivId);
    } else if (request.responseText != "") { //Check for Timeout Issue
        ShowDialogError(request.responseText, dialogDivId);
    } else { //Other issues
        ShowDialogError("Unknown " + textStatus + " occurred", dialogDivId);
    }
}


//Clears the Content of the AppDialog
function ClearDialogContent(dialogDivId) {
    $(dialogDivId + "Error").empty();
    $(dialogDivId + 'Content').empty();
}
//Sets the Content of the AppDialog
function SetDialogContent(dialogDivId, content) {
    $(dialogDivId + "Error").empty();
    $(dialogDivId + 'Content').html(content);
}
//Closes the AppDialog
function CloseDialogWindow(dialogDivId) {
    if (dialogDivId == undefined) {
        dialogDivId = "#modalWindow";
    }
    $(dialogDivId).dialog('close');
}

//Center the dialog
function CenterDialogWindow(dialogDivId) {
    if (dialogDivId == undefined) {
        dialogDivId = "#modalWindow";
    }
    $(dialogDivId).dialog("option", "position", { my: "center", at: "center", of: window });
}

function InitializeDateControl(control, controlButton, parameters) {
    if (parameters == undefined) {
        parameters = {};
    }
    $(control).datepicker(parameters);
    //$(controlButton).click(function () {
    //    if ($(control).datepicker("widget").is(":visible")) {
    //        $(control).datepicker("hide");
    //    } else {
    //        $(control).datepicker("show");
    //    }
    //});
}

function ShowAllAutoCompleteItems(control) {
    $(control).autocomplete('search', "");
}

function RefreshPage() {
    location.reload();
}

function RefreshPageWithDelay(delayTime) {
    window.setTimeout(RefreshPage, delayTime);
}

function GoToUrl(url) {
    location.href = url;
}

function GoToUrlWithDelay(url, delayTime) {
    window.setTimeout(GoToUrl(url), delayTime);
}

function OpenProviderRoom() {
    window.open("/VideoConsultation/Provider", "VideoConsultationWindow");
}

function OpenProviderRoomForSafari()
{
    window.open("https://myvitalz.vsee.me/init.php", "_target");
    window.setTimeout(OpenProviderRoom, "1000")
}