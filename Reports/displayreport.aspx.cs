﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;

namespace MyVitalz.Web
{
    public partial class displayreport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
             if (!Page.IsPostBack)
            {
                string strAssociatedPartyId = Request.QueryString["prmAssociatedPartyId"];
                //string strAssociatedDoctorId = Request.QueryString["prmAssociatedDoctorId"]; 
                string strPartyType = Request.QueryString["PartyType"];

                if (strPartyType == "A" || strPartyType == "E")
                {
                    doctorDiv.Visible = true;
                    dd1Div.Visible = false;
                }
                if (strPartyType == "D" || strPartyType == "N" || strPartyType == "C")
                {
                    doctorDiv.Visible = false;
                }              
                if(strPartyType == "P")
                {
                    doctorDiv.Visible = false;
                    dd1Div.Visible = false;                    
                }
                try
                {
                    //ddlPatientSelection.Items.Add(new ListItem("Select a Patient", "-1"));
                    ddlPatientSelection.Items.Add(new ListItem(" ", "-2"));
                    ddlPatientSelection.Items.Add(new ListItem("All", "0"));
                    ddlPatientSelection.DataBind();

                    ddlDoctorSelection.Items.Add(new ListItem(" ", "-4"));
                    ddlDoctorSelection.Items.Add(new ListItem("All", "0"));
                    ddlDoctorSelection.DataBind();
                }
                catch (TargetInvocationException ex)
                {
                    throw ex;
                }
                if (strPartyType == "D" || strPartyType == "N" || strPartyType == "C")
                {
                    ddlDoctorSelection.SelectedItem.Value = strAssociatedPartyId;
                } 

               // string prmPatient = "-1";
                string prmPatient = "-2";
                //string prmPatient = ddlDoctorSelection.SelectedValue;
                string prmActiveStatus = "Y";
              //  string prmPatientName = "All";
               // string prmPatientName = "None";
                string startdt = DateTime.Today.AddMonths(-3).ToString("yyyy-MM-dd");
                string enddt = DateTime.Now.ToString("yyyy-MM-dd");

                string prmEndDate = enddt;
                string prmStartDate = startdt;

                if (ddldateRange.Items.FindByValue("1M") != null)
                {
                    ddldateRange.Items.FindByValue("1M").Selected = true;
                }
                string prmPatientName;
                prmPatientName = prmPatient;

                if (strPartyType == "P")
                {
                    prmPatient = strAssociatedPartyId;
                    prmPatientName = "PatientLogin";
                }

                string prmDoctor = "-2";
                string prmDoctorName = prmDoctor;
                //string drActiveStatus = "Y";

                var parameters = new List<ReportParameter>
                {
                   new ReportParameter("rptPrmPatientId",prmPatient),
                   new ReportParameter("rptPrmActiveStatus",prmActiveStatus),
                   new ReportParameter("rptPrmStartDate", prmStartDate),
                   new ReportParameter("rptPrmEndDate", prmEndDate),
                    new ReportParameter("rptPrmPatientName",prmPatientName),
                };
                ReportViewer1.LocalReport.SetParameters(parameters);
                ReportViewer1.LocalReport.Refresh();                
            }
        }

        protected void RefreshReadingActiveStatus_Click(object sender, EventArgs e)
        {
            string strAssociatedPartyId = Request.QueryString["prmAssociatedPartyId"];
            string strPartyType = Request.QueryString["PartyType"];
            if (strPartyType == "P")
            {
                dd1Div.Visible = false;

            }

            string prmEndDate = "0";
            string prmStartDate = "0";
            ddlPatientSelection.Items.Clear();
            ddlPatientSelection.Items.Add(new ListItem(" ", "-2"));
            ddlPatientSelection.Items.Add(new ListItem("All", "0"));
            ddlPatientSelection.DataBind();

            string prmPatient = "-2";            

            string prmActiveStatus;

            prmActiveStatus = ddActiveStatus.SelectedItem.Value;

            if (ddldateRange.SelectedValue == "DR")
            {
                prmStartDate = Request.Form["startDate"];
                prmEndDate = Request.Form["endDate"];
            }
            else
            {
                string startdt = DateTime.Today.AddMonths(-3).ToString("yyyy-MM-dd");
                string enddt = DateTime.Now.ToString("yyyy-MM-dd");
                switch (ddldateRange.SelectedValue)
                {
                    case "1D":
                        startdt = DateTime.Today.ToString("yyyy-MM-dd");
                        enddt = DateTime.Today.ToString("yyyy-MM-dd");
                        break;
                    case "1W":
                        startdt = DateTime.Today.AddDays(-7).ToString("yyyy-MM-dd");
                        enddt = DateTime.Today.ToString("yyyy-MM-dd");
                        break;
                    case "1M":
                        startdt = DateTime.Today.AddMonths(-1).ToString("yyyy-MM-dd");
                        enddt = DateTime.Today.ToString("yyyy-MM-dd");
                        break;
                    case "3M":
                        startdt = DateTime.Today.AddMonths(-3).ToString("yyyy-MM-dd");
                        enddt = DateTime.Today.ToString("yyyy-MM-dd");
                        break;
                    case "6M":
                        startdt = DateTime.Today.AddMonths(-6).ToString("yyyy-MM-dd");
                        enddt = DateTime.Today.ToString("yyyy-MM-dd");
                        break;
                    case "1Y":
                        startdt = DateTime.Today.AddYears(-1).ToString("yyyy-MM-dd");
                        enddt = DateTime.Today.ToString("yyyy-MM-dd");
                        break;
                    default:
                        break;
                }
                prmStartDate = startdt;
                prmEndDate = enddt;

                string prmPatientName;
                prmPatientName = prmPatient;
                if (strPartyType == "P")
                {
                    prmPatient = strAssociatedPartyId;
                    prmPatientName = "PatientLogin";
                }

                var parameters = new List<ReportParameter>
                {
                    new ReportParameter("rptPrmPatientId",prmPatient),
                    new ReportParameter("rptPrmActiveStatus",prmActiveStatus),
                    new ReportParameter("rptPrmStartDate", prmStartDate),
                    new ReportParameter("rptPrmEndDate", prmEndDate),
                    new ReportParameter("rptPrmPatientName",prmPatient),
                };
                ReportViewer1.LocalReport.SetParameters(parameters);

                ReportViewer1.LocalReport.Refresh();
            }
        }

        protected void RefreshReading_Click(object sender, EventArgs e)
        {
            string strAssociatedPartyId = Request.QueryString["prmAssociatedPartyId"];
            //string strAssociatedDoctorId = Request.QueryString["prmAssociatedDoctorId"]; 
            string strPartyType = Request.QueryString["PartyType"];

            if (strPartyType == "P")
            {
                dd1Div.Visible = false;
            }
            if (strPartyType != "A" && strPartyType != "E")
            {
                ddlDoctorSelection.SelectedItem.Value = strAssociatedPartyId;
            }
            string prmPatient;
            string prmPatientName;
            string prmEndDate = "0";
            string prmStartDate = "0";
            string prmActiveStatus ;

            prmPatient = ddlPatientSelection.SelectedItem.Value;
            prmPatientName = ddlPatientSelection.SelectedItem.Text;
            prmActiveStatus = ddActiveStatus.SelectedItem.Value;

            if (ddldateRange.SelectedValue == "DR")
            {
                prmStartDate = Request.Form["startDate"];
                prmEndDate = Request.Form["endDate"];
            }
            else
            {
                string startdt = DateTime.Today.AddMonths(-3).ToString("yyyy-MM-dd");
                string enddt = DateTime.Now.ToString("yyyy-MM-dd");
                switch(ddldateRange.SelectedValue)
                {
                  case "1D":
                    startdt = DateTime.Today.ToString("yyyy-MM-dd");
                    enddt = DateTime.Today.ToString("yyyy-MM-dd");
                     break;
                 case "1W":
                     startdt = DateTime.Today.AddDays(-7).ToString("yyyy-MM-dd");
                    enddt = DateTime.Today.ToString("yyyy-MM-dd");
                     break;
                 case "1M":
                     startdt = DateTime.Today.AddMonths(-1).ToString("yyyy-MM-dd");
                    enddt = DateTime.Today.ToString("yyyy-MM-dd");
                     break;
                 case "3M":
                     startdt = DateTime.Today.AddMonths(-3).ToString("yyyy-MM-dd");
                    enddt = DateTime.Today.ToString("yyyy-MM-dd");
                     break;
                 case "6M":
                     startdt = DateTime.Today.AddMonths(-6).ToString("yyyy-MM-dd");
                    enddt = DateTime.Today.ToString("yyyy-MM-dd");
                     break;
                 case "1Y":
                     startdt = DateTime.Today.AddYears(-1).ToString("yyyy-MM-dd");
                    enddt = DateTime.Today.ToString("yyyy-MM-dd");
                     break;
                 default:
                     break;
             }
                prmStartDate = startdt;
                prmEndDate = enddt;
                
                //prmStartDate = StartDateHidden.Value;
                //prmEndDate = EndDateHidden.Value;

            }
            if (prmPatient == "All")
                prmPatient = "0";
           
            if (strPartyType == "P")
            {
                prmPatient = strAssociatedPartyId;
                prmPatientName = "PatientLogin";
            }

            var parameters = new List<ReportParameter>
                {
                    new ReportParameter("rptPrmPatientId",prmPatient),
                    new ReportParameter("rptPrmActiveStatus",prmActiveStatus),
                    new ReportParameter("rptPrmStartDate", prmStartDate),
                    new ReportParameter("rptPrmEndDate", prmEndDate),
                    new ReportParameter("rptPrmPatientName",prmPatientName),
                };
            ReportViewer1.LocalReport.SetParameters(parameters);

            ReportViewer1.LocalReport.Refresh();

        }

        protected void RefreshDoctorReading_Click(object sender, EventArgs e)
        {
            string strAssociatedPartyId = Request.QueryString["prmAssociatedPartyId"];
            //string strAssociatedDoctorId = Request.QueryString["prmAssociatedDoctorId"]; 
            string strPartyType = Request.QueryString["PartyType"];
            if (strPartyType == "A" || strPartyType == "E")
            {                
                dd1Div.Visible = true;
            }
            if (strPartyType == "P")
            {
                doctorDiv.Visible = false;
                dd1Div.Visible = false;
            }
            ddlPatientSelection.Items.Clear();
            ddlPatientSelection.Items.Add(new ListItem(" ", "-2"));
            ddlPatientSelection.Items.Add(new ListItem("All", "0"));
            ddlPatientSelection.DataBind();

            //string prmPatient;
            //string prmPatientName;
            //string prmEndDate = "0";
            //string prmStartDate = "0";
            //string prmActiveStatus ;
            //string selectedDoctor;

            //prmPatient = "-2";
            //prmActiveStatus = "Y";
            //prmPatientName = prmPatient;

            ////string prmDoctor = "-2";
            ////string prmDoctorName = prmDoctor;

            //string prmDoctor = ddlDoctorSelection.SelectedItem.Value;
            //string prmDoctorName = ddlDoctorSelection.SelectedItem.Text;

            //selectedDoctor = ddlDoctorSelection.SelectedItem.Value;

            ////prmPatientName = ddlPatientSelection.SelectedItem.Text;
            ////ObjectDataSource3.SelectParameters = selectedDoctor;

            //ddlPatientSelection.Items.Add(new ListItem(" ", "-2"));
            //ddlPatientSelection.Items.Add(new ListItem("All", "0"));
            //ddlPatientSelection.DataBind();

            //if (ddldateRange.SelectedValue == "DR")
            //{
            //    prmStartDate = Request.Form["startDate"];
            //    prmEndDate = Request.Form["endDate"];
            //}
            //else
            //{
            //    string startdt = DateTime.Today.AddMonths(-3).ToString("yyyy-MM-dd");
            //    string enddt = DateTime.Now.ToString("yyyy-MM-dd");
            //    switch(ddldateRange.SelectedValue)
            //    {
            //      case "1D":
            //        startdt = DateTime.Today.ToString("yyyy-MM-dd");
            //        enddt = DateTime.Today.ToString("yyyy-MM-dd");
            //         break;
            //     case "1W":
            //         startdt = DateTime.Today.AddDays(-7).ToString("yyyy-MM-dd");
            //        enddt = DateTime.Today.ToString("yyyy-MM-dd");
            //         break;
            //     case "1M":
            //         startdt = DateTime.Today.AddMonths(-1).ToString("yyyy-MM-dd");
            //        enddt = DateTime.Today.ToString("yyyy-MM-dd");
            //         break;
            //     case "3M":
            //         startdt = DateTime.Today.AddMonths(-3).ToString("yyyy-MM-dd");
            //        enddt = DateTime.Today.ToString("yyyy-MM-dd");
            //         break;
            //     case "6M":
            //         startdt = DateTime.Today.AddMonths(-6).ToString("yyyy-MM-dd");
            //        enddt = DateTime.Today.ToString("yyyy-MM-dd");
            //         break;
            //     case "1Y":
            //         startdt = DateTime.Today.AddYears(-1).ToString("yyyy-MM-dd");
            //        enddt = DateTime.Today.ToString("yyyy-MM-dd");
            //         break;
            //     default:
            //         break;
            // }
            //    prmStartDate = startdt;
            //    prmEndDate = enddt;
                
            //    //prmStartDate = StartDateHidden.Value;
            //    //prmEndDate = EndDateHidden.Value;

            //}
            ////if (prmPatient == "All")
            ////    prmPatient = "0";
           
            //if (strPartyType == "P")
            //{
            //    prmPatient = strAssociatedPartyId;
            //    prmPatientName = "PatientLogin";
            //}

            //var parameters = new List<ReportParameter>
            //    {
            //        new ReportParameter("rptPrmPatientId",prmPatient),
            //        new ReportParameter("rptPrmActiveStatus",prmActiveStatus),
            //        new ReportParameter("rptPrmStartDate", prmStartDate),
            //        new ReportParameter("rptPrmEndDate", prmEndDate),
            //        new ReportParameter("rptPrmPatientName",prmPatientName),
            //        //new ReportParameter("rptPrmDoctorId", prmDoctor),
            //        //new ReportParameter("rptPrmDoctorName",prmDoctorName),
            //    };
            //ReportViewer1.LocalReport.SetParameters(parameters);

            //ReportViewer1.LocalReport.Refresh();
        }

        protected void RefreshDoctorReadingActiveStatus_Click(object sender, EventArgs e)
        {
            string strAssociatedPartyId = Request.QueryString["prmAssociatedPartyId"];
            //string strAssociatedDoctorId = Request.QueryString["prmAssociatedDoctorId"]; 
            string strPartyType = Request.QueryString["PartyType"];
            if (strPartyType == "P")
            {
                dd1Div.Visible = false;
                doctorDiv.Visible = false;
            }

            string prmEndDate = "0";
            string prmStartDate = "0";

            ddlDoctorSelection.Items.Clear();
            ddlDoctorSelection.Items.Add(new ListItem(" ", "-2"));
            ddlDoctorSelection.Items.Add(new ListItem("All", "0"));
            ddlDoctorSelection.DataBind();

            string prmActiveStatus;

            prmActiveStatus = ddActiveDoctorStatus.SelectedItem.Value;

            if (ddldateRange.SelectedValue == "DR")
            {
                prmStartDate = Request.Form["startDate"];
                prmEndDate = Request.Form["endDate"];
            }
            else
            {
                string startdt = DateTime.Today.AddMonths(-3).ToString("yyyy-MM-dd");
                string enddt = DateTime.Now.ToString("yyyy-MM-dd");
                switch (ddldateRange.SelectedValue)
                {
                    case "1D":
                        startdt = DateTime.Today.ToString("yyyy-MM-dd");
                        enddt = DateTime.Today.ToString("yyyy-MM-dd");
                        break;
                    case "1W":
                        startdt = DateTime.Today.AddDays(-7).ToString("yyyy-MM-dd");
                        enddt = DateTime.Today.ToString("yyyy-MM-dd");
                        break;
                    case "1M":
                        startdt = DateTime.Today.AddMonths(-1).ToString("yyyy-MM-dd");
                        enddt = DateTime.Today.ToString("yyyy-MM-dd");
                        break;
                    case "3M":
                        startdt = DateTime.Today.AddMonths(-3).ToString("yyyy-MM-dd");
                        enddt = DateTime.Today.ToString("yyyy-MM-dd");
                        break;
                    case "6M":
                        startdt = DateTime.Today.AddMonths(-6).ToString("yyyy-MM-dd");
                        enddt = DateTime.Today.ToString("yyyy-MM-dd");
                        break;
                    case "1Y":
                        startdt = DateTime.Today.AddYears(-1).ToString("yyyy-MM-dd");
                        enddt = DateTime.Today.ToString("yyyy-MM-dd");
                        break;
                    default:
                        break;
                }
                prmStartDate = startdt;
                prmEndDate = enddt;

                string prmDoctor = "-2";
                string prmDoctorName = prmDoctor;

                var parameters = new List<ReportParameter>
                {
                    //new ReportParameter("rptPrmPatientId",prmPatient),
                    new ReportParameter("rptPrmActiveStatus",prmActiveStatus),
                    new ReportParameter("rptPrmStartDate", prmStartDate),
                    new ReportParameter("rptPrmEndDate", prmEndDate),
                    //new ReportParameter("rptPrmPatientName",prmPatient),
                };
                ReportViewer1.LocalReport.SetParameters(parameters);

                ReportViewer1.LocalReport.Refresh();
            }
        }
    }
}