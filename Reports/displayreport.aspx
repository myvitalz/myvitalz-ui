﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="displayreport.aspx.cs" Inherits="MyVitalz.Web.displayreport" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <!--
        <link href="/Packages/Foundation/Css/normalize.css" rel="stylesheet" type="text/css" />
        <link href="/Packages/Foundation/Css/Foundation.css" rel="stylesheet" type="text/css" />
        
    
    <link type="text/css" href="/Packages/JScrollpane/css/jquery.jscrollpane.css" rel="stylesheet" media="all" />
    <link type="text/css" href="/Packages/JScrollpane/css/jquery.jscrollpane-Lozenge.css" rel="stylesheet" media="all" />

    <link href="/Packages/Datatables/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
        <link href="/Packages/FontAwesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600&amp;lang=en-SG" />
    <link href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />
    
    <script src="//code.jquery.com/jquery-1.9.1.js"></script>
    <script src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script src="/Packages/Foundation/Js/vendor/custom.modernizr.js"></script>


        <link href="/css/style.css" rel="stylesheet" type="text/css" />

<%--    <script src="../Scripts/jquery-1.8.2.js"></script>
    <script src="../Scripts/jquery-ui-1.8.24.js"></script>--%>
     <script>

         function ChangeReportDates() {
             //var noOfDays = $("#DateRange").val();
             var noOfDays = $("[id*=ddldateRange]").val();
             var endDate = new Date();
             var startDate = new Date();
             switch (noOfDays) {
                 case "DR":
                     $(".dateRangeOptions").slideDown();
                     return false;
                 case "1D":
                     startDate.setDate(endDate.getDate());;
                     break;
                 case "1W":
                     startDate.setDate(endDate.getDate() - 6);;
                     break;
                 case "1M":
                     startDate.setDate(endDate.getDate() - 29);
                     break;
                 case "3M":
                     startDate.setDate(endDate.getDate() - 89);
                     break;
                 case "6M":
                     startDate.setDate(endDate.getDate() - 179);
                     break;
                 case "1Y":
                     startDate.setDate(endDate.getDate() - 364);
                     break;
                 default:
                     break;
             }
             //$("#FromDate").val(FormatDate(startDate));
             //$("#ToDate").val(FormatDate(endDate));

             //$("#FromDate").datepicker('yyyy-mm-dd', "dateFormat", startDate)
             //$("#ToDate").datepicker('yyyy-mm-dd', "dateFormat", endDate)

             //$.datepicker.formatDate('yyyy-mm-dd', startDate)
             //$.datepicker.formatDate('yyyy-mm-dd', endDate)
             var s = startDate.format("yyyy-MM-dd")
             var e = endDate.format("yyyy-MM-dd")

             document.getElementById('StartDateHidden').value = s
             document.getElementById('EndDateHidden').value = e
             //$("#FromDate").datepicker.formatDate('mm/dd/yyyy', startDate)
             //$("#ToDate").datepicker.formatDate('mm/dd/yyyy', endDate)
             //RefreshReport();
             return true;
         }

         //function PassDateRange() {
         //    $("#datepicker").datepicker("option", "dateFormat", $(this).val());
         //    var s = $("#datepicker").val();
         //    $("#enddatepicker").datepicker("option", "dateFormat", $(this).val());
         //    var e = $("#enddatepicker").val();
         //    document.getElementById('StartDateHidden').value = s
         //    document.getElementById('EndDateHidden').value = e
         //    return true;
         //}


         $(function () {
             var pickerOpts = {
                 dateFormat: "yy-mm-dd"
             };
             $("#startdatepicker").datepicker(pickerOpts);
             //$("#startdatepicker").datepicker();
             $("#format").change(function () {
                 $("#startdatepicker").datepicker("option", "dateFormat", $(this).val());
                 // $("#startdatepicker").datepicker("yyyy-MM-dd", "dateFormat", $(this).val());
             });
         });

         $(function () {
             var pickerOpts = {
                 dateFormat: "yy-mm-dd"
             };
             $("#enddatepicker").datepicker(pickerOpts);
             //$("#enddatepicker").datepicker();
             $("#format").change(function () {
                 //$("#enddatepicker").datepicker("option", "dateFormat", $(this).val());
                 $("#enddatepicker").datepicker("yyyy-MM-dd", "dateFormat", $(this).val());
             });
         });

         function GetPatientByDoctor() {
             $("#ddlPatientSelection :selected").val() = $("#ddlDoctorSelection :selected").val();
         }



    </script>

      <style>
.jspTrack {
    background: #e0e0e0 !important;
}

.jspDrag {
    background: darkgray !important;
}
        fieldset {
            display: table-column;
            width: 100%;
        }
            
                fieldset {
                    display: inline;
                }
                div.dataTables_scrollBody { min-height: 0%; }
            
    </style>
</head>
   
<body style="padding:0px;margin:0px">
    <table  style="width:100%; background-color:#f2f3f4">
        <tr>
            <td style="width:30%"><img src="/images/logo/MyVitalz_finallogo.png" /></td>
            
            <td style="width:70%"> </td>
        </tr>
        <tr class="headerText boxedLayoutPadding">
            <td class="row" style="width:100%; font-family:Helvetica Neue; font-size:3em; color:white; text-align:center">Readings Report</td>
        </tr>
    </table>

    <form id="form1" runat="server">
        <asp:HiddenField ID="StartDateHidden" runat="server" />
 <asp:HiddenField ID="EndDateHidden" runat="server" />
            <div class="row" style="height: 842px">
        <div id="divTop" class="Foundation.css">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <div id="doctorDiv" runat="server" class="row">
            <div style="width: 15%; float:left">
                <label>Doctor / Clinician</label>       
                    <asp:DropDownList ID="ddlDoctorSelection" runat="server" DataSourceID="ObjectDataSource9" DataTextField="doctorName" DataValueField="doctorId" AutoPostBack="True" OnSelectedIndexChanged="RefreshDoctorReading_Click" AppendDataBoundItems="True">
                    </asp:DropDownList>
                <asp:ObjectDataSource ID="ObjectDataSource9" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="MyVitalz.Web.myvitalzDataSetTableAdapters.doctorListTableAdapter">
                    <SelectParameters>
                        <%--<asp:QueryStringParameter Name="associatedPartyId" QueryStringField="prmAssociatedPartyId" Type="Int64" DefaultValue="Y" />--%>
                        <asp:ControlParameter ControlID="ddActiveStatus" DefaultValue="Y" Name="prmActive" PropertyName="SelectedValue" Type="String" />
                        <asp:QueryStringParameter DefaultValue="ALL" Name="prmPartyGroup" QueryStringField="prmAssociatedPartyId" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
            <div style="width: 85%; float:right">
                <label>Active/Inactive</label>
                <asp:DropDownList ID="ddActiveDoctorStatus" runat="server"  AutoPostBack="True" OnSelectedIndexChanged="RefreshDoctorReadingActiveStatus_Click" >
                    <asp:ListItem Value="Y">Active</asp:ListItem>
                    <asp:ListItem Value="N">InActive</asp:ListItem>
                    <asp:ListItem Value="0">All</asp:ListItem>
                </asp:DropDownList>  
            </div>
        </div>

        <div id="dd1Div" runat="server" class="row">
            <div  style="width: 15%; float:left">
                 <label>Patient</label>
       
        <asp:DropDownList ID="ddlPatientSelection" runat="server" DataSourceID="ObjectDataSource3" DataTextField="patientName" DataValueField="patientId" AutoPostBack="True" OnSelectedIndexChanged="RefreshReading_Click" AppendDataBoundItems="True">
        </asp:DropDownList>
        <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="MyVitalz.Web.myvitalzDataSetTableAdapters.patientListTableAdapter">
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlDoctorSelection" DefaultValue="1001" Name="associatedPartyId" PropertyName="SelectedValue" Type="String" />
                <asp:ControlParameter ControlID="ddActiveStatus" DefaultValue="Y" Name="prmActive" PropertyName="SelectedValue" Type="String" />
                <asp:QueryStringParameter Name="prmPartyType" QueryStringField="PartyType" Type="String" />
                <asp:QueryStringParameter Name="prmGroupAdminID" QueryStringField="prmAssociatedPartyId" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
                </div>
             <div style="width: 85%; float:right">
                <label>Active/Inactive</label>
                <asp:DropDownList ID="ddActiveStatus" runat="server"  AutoPostBack="True" OnSelectedIndexChanged="RefreshReadingActiveStatus_Click" >
                    <asp:ListItem Value="Y">Active</asp:ListItem>
                    <asp:ListItem Value="N">InActive</asp:ListItem>
                    <asp:ListItem Value="0">All</asp:ListItem>
                </asp:DropDownList>  
            </div>
            </div>
     
    
        <div class="row">
            <div class="large-8 columns">
                &nbsp;
                
            </div>
        </div>
        <table class="row">
            <tr class="large-4 row">
                <td>
                <label>Show Readings For</label>
                <asp:DropDownList ID="ddldateRange" runat="server"  AutoPostBack="True" OnSelectedIndexChanged="RefreshReading_Click"  onchange="if(!ChangeReportDates()) return false;">
                    <asp:ListItem Value="1D">Today</asp:ListItem>
                    <asp:ListItem Value="1W">Last Week</asp:ListItem>
                    <asp:ListItem Value="1M">Last Month</asp:ListItem>
                    <asp:ListItem Value="3M">Last 3 Months</asp:ListItem>
                    <asp:ListItem Value="6M">Last 6 Months</asp:ListItem>
                    <asp:ListItem Value="1Y">Last Year</asp:ListItem>
                    <asp:ListItem Value="DR">Specify Date Range</asp:ListItem>
                </asp:DropDownList>            
                </td>

                <td class="large-3 row hide dateRangeOptions" style="display:none">
                    <%--<p>From: <input type="text" id="startdatepicker" size="30" /></p>--%>
                    <label>From Date:  </label>
                    <input type="text" id="startdatepicker" name="startDate" size="30" />
                </td>

                <td class="large-3 row hide dateRangeOptions" style="display:none">
                   <%-- <p>To: <input type="text" id="enddatepicker" size="30" /></p>--%>
                    <label>To Date:  </label><input type="text" id="enddatepicker" name="endDate" size="30" />
                </td>

                <td class="large-2 row hide dateRangeOptions" style="display:none">
                   <%-- <a href="javascript:void(0)"  onclick="GoButton_Click()" class="button small" >Show Report</a>--%>
                    <br />
                    <asp:Button class="button small" ID="GoButton" Text="Go" runat="server"  Onclick="RefreshReading_Click"  />
                </td>
            </tr>
        </table>
            <br />  
   <%-- </form>--%>
        <%--<div class="row">
            <div class="large-4 columns">
                <label>Show Readings For</label>

       
        <asp:DropDownList ID="ddDateSelection" runat="server">
            <asp:ListItem Value="1D">Today</asp:ListItem>
            <asp:ListItem Value="1W">Last Week</asp:ListItem>
            <asp:ListItem Value="1M">Last Month</asp:ListItem>
            <asp:ListItem Value="3M">Last 3 Months</asp:ListItem>
            <asp:ListItem Value="6M">Last 6 Months</asp:ListItem>
            <asp:ListItem Value="1Y">Last Year</asp:ListItem>
            <asp:ListItem Value="DR">Specify Date Range</asp:ListItem>
        </asp:DropDownList>
                </div>
        

        
             <div id="divDateRange" class="large-3 columns hide dateRangeOptions">
                <label>From Date</label>
                <input class="dateField" data-val="true" data-val-date="The field FromDate must be a date." data-val-required="The FromDate field is required." id="FromDate" name="FromDate" type="text" value="7/1/2014" />
            </div>
            <div class="large-3 columns hide dateRangeOptions">
                <label>To Date</label>
                <input class="dateField" data-val="true" data-val-date="The field ToDate must be a date." data-val-required="The ToDate field is required." id="ToDate" name="ToDate" type="text" value="9/28/2014" />
             </div>
             <div class="large-2 columns hide dateRangeOptions"> 
                 <a href="javascript:void(0)"  class="button small">Go</a>
            </div>
        </div>  
            </div> --%> 
    <div>    

        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Arial" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" AsyncRendering="False" Height="102%" Width="102%" BackColor="White" BorderColor="Black" ClientIDMode="Static" ToolBarItemBorderColor="Gray" ToolBarItemBorderWidth="2px" BorderWidth="0px" SizeToReportContent="True" PageCountMode="Actual">
            <LocalReport ReportPath="Reports\PatientsReportwithHighlight.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" Name="dsPatientReading" />
                    <%--<rsweb:ReportDataSource DataSourceId="ObjectDataSource9" Name="dsDoctorReading" />--%>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource8" Name="dsReportReading" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
       <asp:ObjectDataSource ID="ObjectDataSource8" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="MyVitalz.Web.myvitalzDataSetTableAdapters.RRTableAdapter">
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlDoctorSelection" DefaultValue="1001" Name="assoicatedPartyId" PropertyName="SelectedValue" Type="String" />
                <asp:QueryStringParameter Name="prmPartyType" QueryStringField="PartyType" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
       <%-- <asp:ObjectDataSource ID="ObjectDataSource7" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="MyVitalz.Web.myvitalzDataSetTableAdapters.pr_withHLTableAdapter">
            <SelectParameters>
                <asp:QueryStringParameter Name="assoicatedPartyId" QueryStringField="prmAssociatedPartyId" Type="Int64" />
            </SelectParameters>
        </asp:ObjectDataSource>--%>
        <%--<asp:ObjectDataSource ID="ObjectDataSource6" runat="server"></asp:ObjectDataSource>--%>
       <asp:ObjectDataSource runat="server" ID="ObjectDataSource5" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="MyVitalz.Web.myvitalzDataSetTableAdapters.PatientReadingsWithHighLightsTableAdapter">
            <SelectParameters>
                <asp:QueryStringParameter Name="assoicatedPartyId" QueryStringField="prmAssociatedPartyId" Type="Int64" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <%--<asp:ObjectDataSource ID="ObjectDataSource4" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="MyVitalz.Web.myvitalzDataSetTableAdapters.PatientReadingWithHLTableAdapter">
            <SelectParameters>
                <asp:QueryStringParameter Name="assoicatedPartyId" QueryStringField="prmAssociatedPartyId" Type="Int64" />
            </SelectParameters>
        </asp:ObjectDataSource>--%>
        <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="MyVitalz.Web.myvitalzDataSetTableAdapters.patientListTableAdapter">
            <SelectParameters>
                <asp:QueryStringParameter Name="associatedPartyId" QueryStringField="prmAssociatedPartyId" Type="Int64" DefaultValue="Y" />
                <asp:ControlParameter ControlID="ddActiveStatus" DefaultValue="Y" Name="prmActive" PropertyName="SelectedValue" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" DeleteMethod="Delete" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="MyVitalz.Web.myvitalzDataSetTableAdapters.partyTableAdapter" UpdateMethod="Update">
            <DeleteParameters>
                <asp:Parameter Name="p1" Type="Int64" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="p1" Type="String" />
                <asp:Parameter Name="p2" Type="String" />
                <asp:Parameter Name="p3" Type="String" />
                <asp:Parameter Name="p4" Type="String" />
                <asp:Parameter Name="p5" Type="String" />
                <asp:Parameter Name="p6" Type="DateTime" />
                <asp:Parameter Name="p7" Type="String" />
                <asp:Parameter Name="p8" Type="String" />
                <asp:Parameter Name="p9" Type="String" />
                <asp:Parameter Name="p10" Type="String" />
                <asp:Parameter Name="p11" Type="String" />
                <asp:Parameter Name="p12" Type="String" />
                <asp:Parameter Name="p13" Type="String" />
                <asp:Parameter Name="p14" Type="String" />
                <asp:Parameter Name="p15" Type="String" />
                <asp:Parameter Name="p16" Type="String" />
                <asp:Parameter Name="p17" Type="String" />
                <asp:Parameter Name="p18" Type="String" />
                <asp:Parameter Name="p19" Type="DateTime" />
                <asp:Parameter Name="p20" Type="String" />
                <asp:Parameter Name="p21" Type="DateTime" />
                <asp:Parameter Name="p22" Type="String" />
                <asp:Parameter Name="p23" Type="String" />
                <asp:Parameter Name="p24" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="p1" Type="String" />
                <asp:Parameter Name="p2" Type="String" />
                <asp:Parameter Name="p3" Type="String" />
                <asp:Parameter Name="p4" Type="String" />
                <asp:Parameter Name="p5" Type="String" />
                <asp:Parameter Name="p6" Type="DateTime" />
                <asp:Parameter Name="p7" Type="String" />
                <asp:Parameter Name="p8" Type="String" />
                <asp:Parameter Name="p9" Type="String" />
                <asp:Parameter Name="p10" Type="String" />
                <asp:Parameter Name="p11" Type="String" />
                <asp:Parameter Name="p12" Type="String" />
                <asp:Parameter Name="p13" Type="String" />
                <asp:Parameter Name="p14" Type="String" />
                <asp:Parameter Name="p15" Type="String" />
                <asp:Parameter Name="p16" Type="String" />
                <asp:Parameter Name="p17" Type="String" />
                <asp:Parameter Name="p18" Type="String" />
                <asp:Parameter Name="p19" Type="DateTime" />
                <asp:Parameter Name="p20" Type="String" />
                <asp:Parameter Name="p21" Type="DateTime" />
                <asp:Parameter Name="p22" Type="String" />
                <asp:Parameter Name="p23" Type="String" />
                <asp:Parameter Name="p24" Type="String" />
                <asp:Parameter Name="p25" Type="Int64" />
            </UpdateParameters>
        </asp:ObjectDataSource>

    </div>
    </div>
    </form>
</body>
</html>

