﻿// Type: MyVitalz.Web.Properties.Settings
// Assembly: MyVitalz.Web, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5DFB40F2-FD4B-4A92-9CB7-01EA3C877ECE
// Assembly location: D:\Data\Haraku\MyVitalz\bin\MyVitalz.Web.dll

using System.CodeDom.Compiler;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace MyVitalz.Web.Properties
{
  [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "11.0.0.0")]
  [CompilerGenerated]
  internal sealed class Settings : ApplicationSettingsBase
  {
    private static Settings defaultInstance = (Settings) SettingsBase.Synchronized((SettingsBase) new Settings());

    public static Settings Default
    {
      get
      {
        return Settings.defaultInstance;
      }
    }

    [DefaultSettingValue("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n<head>\r\n<meta name=\"viewport\" content=\"width=device-width\" />\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\r\n<title>Myvitalz.com</title>\t\r\n<link rel=\"stylesheet\" type=\"text/css\" href=\"stylesheets/email.css\" />\r\n</head>\r\n<body bgcolor=\"#FFFFFF\" style=\"margin: 0px; padding: 0px;\">\r\n<table width=\"100%\" bgcolor=\"gray\">\r\n\t<tr>\r\n\t\t<td></td>\r\n\t\t<td class=\"header\" style=\"display: block !important; max-width: 600px !important; margin: 0 auto !important; clear: both !important;\">\t\t\t\r\n\t\t\t\t<div style=\"padding: 5px; max-width: 600px; margin: 0 auto; display: block;\">\r\n\t\t\t\t<table width=\"100%\">\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td><img style=\"width: 179px; height: 38px;\" src=\"http://portal.myvitalz.com/images/logo/Logo-NeonGreen.png\" /></td>\r\n\t\t\t\t\t\t<td align=\"right\"></td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</table>\r\n\t\t\t\t</div>\t\t\r\n\t\t</td>\r\n\t\t<td></td>\r\n\t</tr>\r\n</table>\r\n<table width=\"100%\">\r\n\t<tr>\r\n\t\t<td></td>\r\n\t\t<td style=\"display: block !important; max-width: 600px !important; margin: 0 auto !important; clear: both !important;\" bgcolor=\"#FFFFFF\">\r\n\t\t\t<div  style=\"padding: 15px; max-width: 600px; margin: 0 auto; display: block;\">\r\n\t\t\t<table>\r\n\t\t\t\t<tr>\r\n\t\t\t\t\t<td>\r\n<pre style=\"font-family:inherit\">~BODY~</pre>\r\n                    </td>\r\n\t\t\t\t</tr>\r\n\t\t\t</table>\r\n\t\t\t</div>\t\t\t\t\t\r\n\t\t</td>\r\n\t\t<td></td>\r\n\t</tr>\r\n</table>\r\n<table style=\"margin: 0px; padding: 0px; clear: both !important\" width=\"100%\">\r\n\t<tr>\r\n\t\t<td></td>\r\n\t\t<td style=\"display: block !important; max-width: 600px !important; margin: 0 auto !important; clear: both !important;\">\r\n\t\t\t\t<div style=\"padding: 15px; max-width: 600px; margin: 0 auto; display: block; border-top: 1px solid #CCCCCC;\">\r\n\t\t\t\t<table>\r\n\t\t\t\t<tr>\r\n\t\t\t\t\t<td align=\"left\">\r\n\t\t\t\t\t\t<p style=\"font-weight: 200; font-size: 12px; color: #000000; font-family: Helvetica, Arial, Lucida Grande, sans-serif; line-height: 1.1; margin-bottom:15px;\">\r\n\t\t\t\t\t\t\tCopyright &copy; 2013 Myvitalz LLC, All rights reserved. \r\n\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t<p style=\"font-weight: 200; font-size: 12px; color: #000000; font-family: Helvetica, Arial, Lucida Grande, sans-serif; line-height: 1.1; margin-bottom:15px;\">\r\n\t\t\t\t\t\t\t<a href=\"http://www.myvitalz.com\">http://www.myvitalz.com</a>\r\n\t\t\t\t\t\t</p>\r\n\t\t\t\t\t</td>\r\n\t\t\t\t</tr>\r\n\t\t\t</table>\r\n\t\t\t\t</div>\r\n\t\t</td>\r\n\t\t<td></td>\r\n\t</tr>\r\n</table>\r\n</body>\r\n</html>")]
    [DebuggerNonUserCode]
    [ApplicationScopedSetting]
    public string EmailTemplate
    {
      get
      {
        return (string) this["EmailTemplate"];
      }
    }

    [DefaultSettingValue("162.216.42.175")]
    [ApplicationScopedSetting]
    [DebuggerNonUserCode]
    public string SmtpServerHost
    {
      get
      {
        return (string) this["SmtpServerHost"];
      }
    }

    [ApplicationScopedSetting]
    [DefaultSettingValue("radhu.karthi@gmail.com")]
    [DebuggerNonUserCode]
    public string ExceptionEmail
    {
      get
      {
        return (string) this["ExceptionEmail"];
      }
    }

    static Settings()
    {
    }
  }
}
