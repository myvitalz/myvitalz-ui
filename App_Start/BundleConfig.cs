﻿using System.Web;
using System.Web.Optimization;

namespace MyVitalz.Web
{
    public class BundleConfig
    {
        public const string CssBundle = "~/Bundles/Css";
        public const string JsHeaderBundle = "~/Bundles/JsHeader";
        public const string JsBundle = "~/Bundles/Js";


        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            var styleBundle = new StyleBundle("~/Bundles/Css");
            styleBundle.Include("~/Packages/Jquery/css/jquery-ui.css", "~/Packages/Jquery/css/jquery-ui.css", "~/Packages/Foundation/Css/normalize.css", "~/Packages/Foundation/Css/Foundation.css", "~/Packages/JScrollpane/css/jquery.jscrollpane.css", "~/Packages/JScrollpane/css/jquery.jscrollpane-Lozenge.css", "~/Packages/Datatables/css/jquery.dataTables.css", "~/Packages/GoogleFont/GoogleFont.css", "~/Packages/FontAwesome.4.7.0/Content/Content/font-awesome.min.css", "~/css/style.css");
            bundles.Add((Bundle)styleBundle);
            var headerBundle = new ScriptBundle("~/Bundles/JsHeader");
            headerBundle.Include("~/Packages/Jquery/js/jquery-1.9.1.js", "~/Packages/Jquery/js/jquery-ui-1.10.3.js", "~/Packages/Foundation/Js/vendor/custom.modernizr.js");
            bundles.Add((Bundle)headerBundle);
            var scriptBundle = new ScriptBundle("~/Bundles/Js");
            scriptBundle.Include("~/Packages/JScrollpane/script/jquery.mousewheel.js", "~/Packages/JScrollpane/script/jquery.jscrollpane.min.js", "~/Packages/NiceScroll/jquery.nicescroll.min.js", "~/Packages/Foundation/Js/Foundation.min.js", "~/Packages/Foundation/Js/Foundation/Foundation.topbar.js", "~/Packages/Foundation/Js/Foundation/Foundation.section.js", "~/Packages/Foundation/Js/Foundation/Foundation.js", "~/Packages/Foundation/Js/Foundation/Foundation.dropdown.js", "~/Packages/Foundation/Js/Foundation/Foundation.placeholder.js", "~/Packages/Foundation/Js/Foundation/Foundation.forms.js", "~/Packages/Foundation/Js/Foundation/Foundation.alerts.js", "~/Packages/Foundation/Js/Foundation/Foundation.magellan.js", "~/Packages/Foundation/Js/Foundation/Foundation.reveal.js", "~/Packages/Foundation/Js/Foundation/Foundation.tooltips.js", "~/Packages/Foundation/Js/Foundation/Foundation.clearing.js", "~/Packages/Foundation/Js/Foundation/Foundation.cookie.js", "~/Packages/Foundation/Js/Foundation/Foundation.joyride.js", "~/Packages/Foundation/Js/Foundation/Foundation.orbit.js", "~/Packages/Foundation/Js/Foundation/Foundation.topbar.js", "~/Packages/Datatables/js/jquery.dataTables.min.js", "~/Packages/HighCharts/js/highcharts.js", "~/Packages/HighCharts/js/themes/gray.js", "~/Packages/Masking/jquery.maskedinput.min.js", "~/Scripts/MyVitalz.js");
            bundles.Add((Bundle)scriptBundle);

        }
    }
}